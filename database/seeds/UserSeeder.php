<?php

use Illuminate\Database\Seeder;
use App\User;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::insert([
        	'name' 			=> 'Motolux Industires',        	
        	'type' 			=> 'Super Admin',
        	'email'			=> 'superadmin@motolux.pk',
        	'password'		=>  Hash::make('11223344'),
        	'status'		=> 'Active',
        	'created_at'	=>  now(),
        	'joining_date'	=>  now(),

        ]);


        $user = User::find(1);

        $user->syncRoles(['Super Admin']);
    }
}
