<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class RolePermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $role_counter = -1;

        $data[++$role_counter] = [  "name" => 'Super Admin','guard_name' => 'web', 'created_at' => now()];

        Role::insert($data);


        $module_list = \FL::getModuleList();

        app()[\Spatie\Permission\PermissionRegistrar::class]->forgetCachedPermissions();

        $count = -1;

        // GENERAL PERMISSIONS
        $data[++$count] = [ "module" => NULL, "name" => "All", "guard_name" => "web"];


        $module = $module_list[0]; // DASHBOARD

        $data[++$count] = [ "module" => $module, "name" => "Dashboard", "guard_name" => "web"];


        $module = $module_list[1]; // WORK ORDERS

        $data[++$count] = [ "module" => $module, "name" => "Add Work Order", "guard_name" => "web"];

        $data[++$count] = [ "module" => $module, "name" => "Edit Work Order", "guard_name" => "web"];

        $data[++$count] = [ "module" => $module, "name" => "Remove Work Order", "guard_name" => "web"];


        $module = $module_list[2]; // PRODUCTION

        $data[++$count] = [ "module" => $module, "name" => "Add Production", "guard_name" => "web"];

        $data[++$count] = [ "module" => $module, "name" => "Edit Production", "guard_name" => "web"];

        $data[++$count] = [ "module" => $module, "name" => "Remove Production", "guard_name" => "web"];

        $data[++$count] = [ "module" => $module, "name" => "Production Detail", "guard_name" => "web"];



        $module = $module_list[3]; // RECEIVE PRODUCTION

        $data[++$count] = [ "module" => $module, "name" => "Receive Production", "guard_name" => "web"];

        $data[++$count] = [ "module" => $module, "name" => "View Receive Production", "guard_name" => "web"];


        $data[++$count] = [ "module" => $module, "name" => "Remove Receive Production", "guard_name" => "web"];



        $module = $module_list[4]; // PAYMENTS

        $data[++$count] = [ "module" => $module, "name" => "Create Invoice", "guard_name" => "web"];

        $data[++$count] = [ "module" => $module, "name" => "View Payment", "guard_name" => "web"];


        $module = $module_list[5]; // PAYROLL

        $data[++$count] = [ "module" => $module, "name" => "Make Payment", "guard_name" => "web"];

        $data[++$count] = [ "module" => $module, "name" => "Add Advance Payment", "guard_name" => "web"];

        $data[++$count] = [ "module" => $module, "name" => "View Advance Payment", "guard_name" => "web"];



        $data[++$count] = [ "module" => $module, "name" => "Remove Payment", "guard_name" => "web"];

        $data[++$count] = [ "module" => $module, "name" => "View Payroll", "guard_name" => "web"];



        $data[++$count] = [ "module" => $module, "name" => "View Ledger", "guard_name" => "web"];


        $module = $module_list[6]; // DEPARTMENT

        $data[++$count] = [ "module" => $module, "name" => "Add Department", "guard_name" => "web"];

        $data[++$count] = [ "module" => $module, "name" => "Edit Department", "guard_name" => "web"];

        $data[++$count] = [ "module" => $module, "name" => "Remove Department", "guard_name" => "web"];

        
        $module = $module_list[7]; // PRODUCTION

        $data[++$count] = [ "module" => $module, "name" => "Add Labour Team", "guard_name" => "web"];

        $data[++$count] = [ "module" => $module, "name" => "Edit Labour Team", "guard_name" => "web"];

        $data[++$count] = [ "module" => $module, "name" => "Remove Labour Team", "guard_name" => "web"];

        $data[++$count] = [ "module" => $module, "name" => "View Profile", "guard_name" => "web"];


        $module = $module_list[8]; // SYSTEM USER

        $data[++$count] = [ "module" => $module, "name" => "Add System User", "guard_name" => "web"];

        $data[++$count] = [ "module" => $module, "name" => "Edit System User", "guard_name" => "web"];

        $data[++$count] = [ "module" => $module, "name" => "Remove System User", "guard_name" => "web"];


        $module = $module_list[9]; // SYSTEM USER

        $data[++$count] = [ "module" => $module, "name" => "Add Product", "guard_name" => "web"];

        $data[++$count] = [ "module" => $module, "name" => "Edit Product", "guard_name" => "web"];

        $data[++$count] = [ "module" => $module, "name" => "Remove Product", "guard_name" => "web"];


        $module = $module_list[10]; // SYSTEM USER

        $data[++$count] = [ "module" => $module, "name" => "Add Role", "guard_name" => "web"];

        $data[++$count] = [ "module" => $module, "name" => "Edit Role", "guard_name" => "web"];

        $data[++$count] = [ "module" => $module, "name" => "Remove Role", "guard_name" => "web"];

        $data[++$count] = [ "module" => $module, "name" => "Assign Permission", "guard_name" => "web"];

        $module = $module_list[11]; // SYSTEM USER

        $data[++$count] = [ "module" => $module, "name" => "Production Issuance Report", "guard_name" => "web"];

        $data[++$count] = [ "module" => $module, "name" => "Production Receiving Report", "guard_name" => "web"];

        $data[++$count] = [ "module" => $module, "name" => "Labour Process Report", "guard_name" => "web"];

        $data[++$count] = [ "module" => $module, "name" => "Labour Payment Report", "guard_name" => "web"];

        $data[++$count] = [ "module" => $module, "name" => "Production Over Due Report", "guard_name" => "web"];


        $data[++$count] = [ "module" => $module, "name" => "Production Status Report", "guard_name" => "web"];


        Permission::insert($data);


        $role = Role::find(1);

        $role->givePermissionTo('All');



    }
}
