<?php

use Illuminate\Database\Seeder;
use App\Department;

class DepartmentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$departments[] = [ 'name' => 'Cutting', 	'is_auto' => 1,  'is_payroll' => 1, 'order' => 1, 'created_at' => now() ];
    	$departments[] = [ 'name' => 'Stitching', 	'is_auto' => 1,  'is_payroll' => 1, 'order' => 2, 'created_at' => now() ];
    	$departments[] = [ 'name' => 'Checking', 	'is_auto' => 1,  'is_payroll' => 1, 'order' => 3, 'created_at' => now() ];
    	$departments[] = [ 'name' => 'Packing',  	'is_auto' => 1,  'is_payroll' => 1, 'order' => 4, 'created_at' => now() ];
    	$departments[] = [ 'name' => 'Office',	 	'is_auto' => 1,  'is_payroll' => 1, 'order' => 5, 'created_at' => now() ];
        $departments[] = [ 'name' => 'Out Source',  'is_auto' => 1,  'is_payroll' => 0, 'order' => 6, 'created_at' => now() ];
        $departments[] = [ 'name' => 'Receive',     'is_auto' => 1,  'is_payroll' => 1, 'order' => 7, 'created_at' => now() ];



        Department::insert($departments);
    }
}
