<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrderReceivesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('order_receives', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('production_id')->nullable();
            $table->bigInteger('order_detail_id')->nullable();
            $table->timestamp('date')->nullable();
            $table->double('quantity')->nullable();
            $table->double('production_cost')->nullable();
            $table->double('bonus_cost')->nullable();
            $table->string('status')->default('Payable');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('order_receives');
    }
}
