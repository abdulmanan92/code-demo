<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('department_id')->nullable();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('email')->unique();
            $table->string('password');
            $table->string('type')->nullable();
            $table->string('name')->nullable();
            $table->string('cnic')->nullable();
            $table->string('contact')->nullable();
            $table->string('address',1000)->nullable();
            $table->timestamp('joining_date')->nullable();

            // CHECKLIST
            $table->integer('cnic_copy')->default(0);
            $table->integer('stamp_paper')->default(0);
            $table->integer('cheque')->default(0);
            $table->integer('photos')->default(0);
            // END CHECKLIST

            $table->string('reference')->nullable();
            $table->double('advance_amount')->nullable();
            $table->double('pocket_money')->nullable();
            $table->string('payroll_group')->nullable();

            $table->timestamp('from')->nullable();
            $table->timestamp('to')->nullable();

            $table->string('status')->default('Deactive');
            $table->string('dp')->nullable();

           
            $table->rememberToken();
            $table->timestamps();


            $table->index('department_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
