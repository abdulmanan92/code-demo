<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDocumentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('documents', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('generic_id')->nullable();
            // GENERIC TYPES: User,
            $table->string('generic_type')->nullable();
            $table->string('title')->nullable();
            $table->string('file')->nullable();
            $table->timestamp('date')->nullable();
            $table->timestamps();


            $table->index('generic_id','generic_type');
            

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('documents');
    }
}
