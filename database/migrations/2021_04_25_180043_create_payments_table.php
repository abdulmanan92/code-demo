<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePaymentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payments', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('user_id')->nullable();
            $table->double('amount')->nullable();

            $table->double('previous_advance')->nullable();
            $table->double('current_advance')->nullable();
            $table->double('balance_advance')->nullable();

            $table->double('previous_pocket')->nullable();
            $table->double('current_pocket')->nullable();
            $table->double('balance_pocket')->nullable();

            


            // UNPAID
            // PENDING
            // PAID
            $table->string('status')->nullable();
            $table->timestamp('date')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payments');
    }
}
