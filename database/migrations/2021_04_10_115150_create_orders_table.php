<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('parent_id')->default(0);
            $table->bigInteger('user_id')->nullable(); // NULLABLE
            $table->bigInteger('product_id')->nullable();
            $table->bigInteger('department_id')->nullable();
            $table->string('order_no')->nullable();
            $table->string('customer_name')->nullable();
            $table->timestamp('date')->nullable();
            $table->double('total_quantity')->nullable(); 
            $table->integer('is_lot')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
