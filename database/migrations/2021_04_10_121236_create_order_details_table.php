<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrderDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('order_details', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('parent_id')->nullable();
            $table->bigInteger('product_variation_id')->nullable();
            $table->bigInteger('production_id')->nullable();
            $table->bigInteger('user_id')->nullable();
            $table->string('color')->nullable();
            $table->string('size')->nullable();
            $table->string('quantity')->nullable();
            $table->timestamp('receive_date')->nullable();
            $table->integer('is_partial')->default(0);
            $table->integer('is_lot')->default(0);
            $table->double('rate')->nullable();
            $table->double('bonus')->nullable();
            $table->double('bonus_limit')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('order_details');
    }
}
