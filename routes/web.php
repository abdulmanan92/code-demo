<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('runstorage', function () {

	\Artisan::call('storage:link');
	
});


Route::get('/', function () {
  
    //\Artisan::call('storage:link');
    
    // $order = new \App\OrderDetail;
    
    // dd($order->userOrderDetailVerification(20,2));
    return view('auth.login');
});

Auth::routes();

Route::get('home', 'HomeController@index')->name('home')->middleware('auth');

// DEPARTMENTS
Route::get('departments', 'DepartmentController@index')->name('departments')->middleware('auth');

Route::post('storedepartment', 'DepartmentController@storeDepartment')->name('store.department')->middleware('auth');

Route::match(['get','post'],'updatedepartment', 'DepartmentController@updateDepartment')->name('update.department')->middleware('auth');

Route::get('removedepartment', 'DepartmentController@removeDepartment')->name('removedepartment')->middleware('auth');
// END DEPARTMENTS

// USER / TEAM MEMBER
Route::get('teammembers', 'UserController@showTeamMember')->name('teammembers')->middleware('auth');

Route::get('labourprofile', 'UserController@teamMemberProfile')->name('labourprofile')->middleware('auth');
Route::get('bonusinfo', 'UserController@getBonusInfo')->name('bonusinfo')->middleware('auth');


Route::post('storeteammember', 'UserController@storeTeamMember')->name('store.teammember')->middleware('auth');

Route::match(['get','post'],'updateteammember', 'UserController@updateTeamMember')->name('update.teammember')->middleware('auth');

Route::get('employees', 'UserController@showEmployee')->name('employees')->middleware('auth');

Route::post('storeemployee', 'UserController@storeEmployee')->name('store.employee')->middleware('auth');

Route::match(['get','post'],'updateemployee', 'UserController@updateEmployee')->name('update.employee')->middleware('auth');

Route::get('removeuser', 'UserController@removeUser')->name('removeuser')->middleware('auth');
// END USER / TEAM MEMBER

// PRODUCT VARIATIONS
Route::get('productvariations', 'ProductVariationController@index')->name('productvariations')->middleware('auth');

Route::post('storeproductvariation', 'ProductVariationController@storeProductVariation')->name('store.productvariation')->middleware('auth');

Route::match(['get','post'],'updateproductvariation', 'ProductVariationController@updateProductVariation')->name('update.productvariation')->middleware('auth');

Route::get('removeproductvariation', 'ProductVariationController@removeProductVariation')->name('removeproductvariation')->middleware('auth');
// END PRODUCT VARIATIONS

// PRODUCTS

Route::get('products', 'ProductController@index')->name('products')->middleware('auth');

Route::post('storeproduct', 'ProductController@storeProduct')->name('store.product')->middleware('auth');

Route::match(['get','post'],'updateproduct', 'ProductController@updateProduct')->name('update.product')->middleware('auth');

Route::get('removeproduct', 'ProductController@removeProduct')->name('removeproduct')->middleware('auth');
// END PRODUCTS

// DOCUMENTS
Route::get('documents/{generic_id}/{generic_type}', 'DocumentController@index')->name('documents')->middleware('auth');

Route::post('storedocument', 'DocumentController@storeDocument')->name('store.document')->middleware('auth');

Route::match(['get','post'],'updatedocument', 'DocumentController@updateDocument')->name('update.document')->middleware('auth');

Route::get('removedocument', 'DocumentController@removeDocument')->name('removedocument')->middleware('auth');
// END DOCUMENTS

// ORDER
Route::get('workorders', 'OrderController@index')->name('workorders')->middleware('auth');

Route::get('detailorder/{id}', 'OrderController@orderDetail')->name('detailorder')->middleware('auth');

Route::get('reorders', 'OrderController@showReOrder')->name('reorders')->middleware('auth');


Route::get('departmentvariants', 'OrderController@loadVariantComponent')->name('departmentvariants')->middleware('auth');


Route::post('storeworkorder', 'OrderController@storeOrder')->name('store.workorder')->middleware('auth');

Route::match(['get','post'],'updateworkorder', 'OrderController@updateOrder')->name('update.workorder')->middleware('auth');

Route::get('removeworkorder', 'OrderController@removeOrder')->name('removeworkorder')->middleware('auth');

Route::get('productvariants', 'OrderController@loadProductVariantComponent')->name('productvariants')->middleware('auth');

// END ORDER


// ORDER RECEIVE
Route::get('workorderreceive/{id}', 'OrderReceiveController@index')->name('workorderreceive')->middleware('auth');

Route::get('orderdetailinfo', 'OrderDetailController@getOrderDetail')->name('orderdetailinfo')->middleware('auth');


Route::match(['post','get'],'storeworkorderreceive', 'OrderReceiveController@storeOrderReceive')->name('store.workorderreceive')->middleware('auth');

Route::match(['get','post'],'updateworkorderreceive', 'OrderReceiveController@updateOrderReceive')->name('update.workorderreceive')->middleware('auth');

Route::get('removeworkorderreceive', 'OrderReceiveController@removeOrderReceive')->name('removeworkorderreceive')->middleware('auth');
// END ORDER RECEIVE


// ORDER DETAIL / WORK ORDER PRODUCTION
Route::get('workorderproduction', 'ProductionController@index')->name('workorderproduction')->middleware('auth');

// Route::get('moveproduction', 'ProductionController@moveNextProduction')->name('moveproduction')->middleware('auth');


Route::get('loadordervariant', 'OrderDetailController@loadOrderVariantComponent')->name('loadordervariant')->middleware('auth');

Route::match(['get','post'],'storeworkorderproduction', 'ProductionController@storeProduction')->name('store.workorderproduction')->middleware('auth');

Route::match(['get','post'],'updateworkorderproduction', 'ProductionController@updateProduction')->name('update.workorderproduction')->middleware('auth');

Route::get('removeworkorderproduction', 'ProductionController@removeProduction')->name('removeworkorderproduction')->middleware('auth');


Route::match(['get','post'],'storenextproduction', 'ProductionController@storeNextProduction')->name('store.nextproduction')->middleware('auth');

Route::match(['get','post'],'updatenextproduction', 'ProductionController@updateNextProduction')->name('update.nextproduction')->middleware('auth');

Route::get('productionorderdetail', 'ProductionController@getOrderDetail')->name('productionorderdetail')->middleware('auth');

Route::get('productionorderdetailinfo', 'OrderDetailController@getOrderDetailById')->name('productionorderdetailinfo')->middleware('auth');








// END ORDER DETAIL / WORK ORDER PRODUCTION

// PAYMENTS
Route::get('allreceiveorders', 'PaymentController@showPendingOrderReceivePayroll')->name('allreceiveorders')->middleware('auth');





Route::get('payments', 'PaymentController@index')->name('payments')->middleware('auth');

Route::get('advancepayments', 'PaymentController@showAdvanceProductionPayment')->name('advancepayments')->middleware('auth');




Route::match(['get','post'],'storepayment', 'PaymentController@storePayment')->name('store.payment')->middleware('auth');

Route::post('showoendingorderpayments', 'PaymentController@showPendingOrdersPayment')->name('showoendingorderpayments')->middleware('auth');


Route::post('storeadvancepayment', 'PaymentController@storeAdvancePayment')->name('store.advancepayment')->middleware('auth');


Route::match(['get','post'],'updatepayment', 'PaymentController@updatePayment')->name('update.payment')->middleware('auth');


Route::get('updatepaymentstatus/{id}', 'PaymentController@updatePaymentStatus')->name('updatepaymentstatus')->middleware('auth');


Route::get('removepayment/{id}', 'PaymentController@removePayment')->name('removepayment')->middleware('auth');




// END PAYMENTS


// REPORT AND DOCS
Route::get('invoicedoc/{id}', 'ReportController@generateInvoice')->name('invoicedoc')->middleware('auth');

Route::get('invoicelabourdoc/{id}', 'ReportController@generatelabourInvoice')->name('invoicelabourdoc')->middleware('auth');


// END REPORT AND DOCS



// DEPARTMENTS
Route::get('roles', 'RolePermissionController@index')->name('roles')->middleware('auth');

Route::get('rolepermission', 'RolePermissionController@showPermissionByRole')->name('rolepermission')->middleware('auth');


Route::post('storerole', 'RolePermissionController@storeRole')->name('store.role')->middleware('auth');

Route::post('assignpermission', 'RolePermissionController@assignPermission')->name('permission.assign')->middleware('auth');


Route::match(['get','post'],'updaterole', 'RolePermissionController@updateRole')->name('update.role')->middleware('auth');

Route::get('removerole/{id}', 'RolePermissionController@removeRole')->name('removerole')->middleware('auth');
// END DEPARTMENTS


// REPORTS
Route::get('reports', 'ReportController@index')->name('reports')->middleware('auth');

Route::post('productionissuance', 'ReportController@getProductionIssuanceReport')->name('report.productionissuance')->middleware('auth');

Route::post('productionreceiving', 'ReportController@getProductionReceivingReport')->name('report.productionreceiving')->middleware('auth');

Route::post('productionoverdue', 'ReportController@getProductionOverDueReport')->name('report.productionoverdue')->middleware('auth');

Route::post('productionstatus', 'ReportController@getProductionStatusReport')->name('report.productionstatus')->middleware('auth');




Route::post('labourprogress', 'ReportController@getLabourProgressReport')->name('report.labourprogress')->middleware('auth');

Route::post('labourpayment', 'ReportController@getLabourPaymentReport')->name('report.labourpayment')->middleware('auth');



// END REPORTS

// DATA LOGS
Route::get('logsbytype', 'DataLogController@getDataLogByType')->name('logsbytype')->middleware('auth');

// END DATA LOGS










