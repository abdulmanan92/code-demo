function onFetchFormModal(event,route,target_model,bind_model)
{
	event.preventDefault();
  $.LoadingOverlay("show");
	$.get(route,function(data)
	{
    $.LoadingOverlay("hide");
		$(bind_model).html(data);
        $(target_model).modal('show');
	});
}

 function createSearchableDropDown(bind_element,table_api,column_index,dropdown_name,column_size,searching_prefix = find('select'))
{
    table_api.columns( [column_index] ).indexes().flatten().each( function ( i ) {
        var column = table_api.column( i );
        var select = $('<div class="'+column_size+'"><select class="fstdropdown-select form-control form-control-sm "><option value="">'+dropdown_name+'</option></select></div>')
            .appendTo(bind_element)
            .on( 'change', function () {

              if($(this).find('select').val().trim().length > 0)
              {
                  column
                    
                    .search("^" + $(this).find('select').val()+ "$", true, false, true)
                    .draw();
              }
              else
              {
                  column.search('').draw();

              }
             
            } );

        column.data().unique().sort().each( function ( d, j ) {
            select.find('select').append( '<option value="'+d+'">'+d+'</option>' )
        } );
    } );
    setFstDropdown();
    
}

function onFetchTableData(event,route,bind_model)
{
	event.preventDefault();
$.LoadingOverlay("show");
	$.get(route,function(data)
	{
    $.LoadingOverlay("hide");
		$(bind_model).append(data);
	});
}

function isDefaulter(value)
{
	value = parseFloat(value);

	if(isNaN(value))
	{
		return 0;
	}
	else
	{
		return value;
	}
}

 function removeBank(event,obj,action = "true")
    {
        event.preventDefault();

        obj = $(obj);

        obj.closest('tr').next().remove();

        obj.closest('tr').remove();

        if(action != "true")

        calulateCost(event,action);
    }

function removePopUp(event,obj)
{
    event.preventDefault();

    Swal.fire({
        title: 'Are you sure?',
        text: "You want to delete record",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, delete it!'
      }).then((result) => {
        if (result.isConfirmed) {

          window.location = $(obj).attr('href');

          Swal.fire(
            'Deleted!',
            'Your record has been deleted.',
            'success'
          )
        }
    });
}

$(document.body).on('submit','form',function(event){

    if($(this).attr('loader') != "false")
    {
        $.LoadingOverlay("show");

    }


  

});


function paymentPopUp(event,obj)
{
    event.preventDefault();

    Swal.fire({
        title: 'Are you sure?',
        text: "You want to make payment",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, make payment!'
      }).then((result) => {
        if (result.isConfirmed) {

          window.location = $(obj).attr('href');

          Swal.fire(
            'Updated!',
            'Your record has been updated.',
            'success'
          )
        }
    });
}