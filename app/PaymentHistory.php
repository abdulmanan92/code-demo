<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use DB;

class PaymentHistory extends Model
{
    public function __construct()
    {
        date_default_timezone_set("Asia/Karachi");
    }
    
    public function storePaymentHistory($data)
    {
    	return DB::transaction(function() use ($data) {

    		$payment_history = new PaymentHistory;

    		$payment_history->payment_id 	    = $data['payment'];

            if(isset($data['order_receive']))
            {
                $payment_history->order_receive_id  = $data['order_receive'];
            }

            if(isset($data['order_detail']))
            {
                $payment_history->order_detail_id  = $data['order_detail'];
            }


    		if($data['debit'] > 0)
    		{
				$payment_history->debit  = $data['debit'];
				$payment_history->credit = 0; 
    		}
    		else
    		{
    			$payment_history->debit  = 0;
				$payment_history->credit = $data['credit']; 
    		}

    		$payment_history->payment_type 	= $data['payment_type'];

    		$payment_history->save();

    		return with($payment_history);
    	});
    }

    public function updatePaymentHistory($data)
    {
    	return DB::transaction(function() use ($data) {

    		$payment_history = PaymentHistory::find($data['payment_history']);

    		if(isset($payment_history->id))
    		{
    			$payment_history->payment_id 	    = $data['payment'];

	    		$payment_history->order_receive_id 	= $data['order_receive'];

	    		if($data['debit'] > 0)
	    		{
					$payment_history->debit  = $data['debit'];
					$payment_history->credit = 0; 
	    		}
	    		else
	    		{
	    			$payment_history->debit  = 0;
					$payment_history->credit = $data['credit']; 
	    		}

	    		$payment_history->payment_type 	= $data['payment_type'];

		    		$payment_history->update();
	    	}

    		return with($payment_history);
    	});
    }


    public function getAdvanceAndPocketAmount($user_id)
    {
        $advance =  PaymentHistory::where('payment_type','Advance Amount')->whereHas('payment',function($query) use ($user_id) {

              
                $query->where('user_id',$user_id);
            
        })->selectRaw('sum(debit-credit) as balance')->value('balance');

        $pocket =  PaymentHistory::where('payment_type','Pocket Money')->whereHas('payment',function($query) use ($user_id) {

                $query->where('user_id',$user_id);
            
        })->selectRaw('sum(debit-credit) as balance')->value('balance');


        return [$advance,$pocket];
    }

    public function removePaymentHistory($id)
    {

    	return DB::transaction(function() use ($id) {

    		$payment_history = PaymentHistory::find($id);

    		if(isset($payment_history->id))
    		{
    			$payment_history->delete();

    			return with(true);
    		}

    		return with(false);
    	});
    }

    public function getTyepWiseUserLedger($user_id,$payment_type = [],$status = ['Paid','Unpaid'])
    {
        return PaymentHistory::whereIn('payment_type',$payment_type)->whereHas('payment',function($query) use ($user_id,$status) {

            $query->where('user_id',$user_id)->whereIn('status',$status);
            
        })->orderBy('id','Asc')->get();
    }


    public function getDeducationAmount($user_id,$payment_type = [],$status = ['Paid','Unpaid'])
    {
        return PaymentHistory::whereIn('payment_type',$payment_type)->whereHas('payment',function($query) use ($user_id,$status) {

            $query->where('user_id',$user_id)->whereIn('status',$status);
            
        })->sum('credit');
    }

    public function getPaymentHistoryById($id)
    {
    	return PaymentHistory::find($id);
    }

    public function getAllPaymentHistories()
    {
    	return PaymentHistory::orderBy('id','Asc')->get();
    }

    public function orderReceive()
    {
    	return $this->belongsTo(OrderReceive::class,'order_receive_id')->withDefault();
    }

    public function orderDetail()
    {
        return $this->belongsTo(OrderDetail::class,'order_detail_id')->withDefault();
    }

    public function payment()
    {
    	return $this->belongsTo(Payment::class,'payment_id')->withDefault();
    }


}
