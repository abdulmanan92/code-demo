<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
use FL;

class Product extends Model
{
    public function __construct()
    {
        date_default_timezone_set("Asia/Karachi");
    }
    
    public function storeProduct($data)
    {
    	return DB::transaction(function() use ($data) {

    		$product = new Product;

    		$product->name = $data['name'];

            $product->article_no = $data['article_no'];

            $product->dp                       = FL::uploadImage($data['dp']);
            


    		$product->save();

            $production_info = [];



            foreach ($data['department'] as $key => $rows) {

                $production_info[] = [

                    'department_id'   => $rows,
                    'product_id'      => $product->id,
                    'production_rate' => (double)$data['production_rate'][$key],
                    'bonus'           => (double)$data['bonus'][$key],
                    'bonus_limit'     => (double)$data['bonus_limit'][$key],
                    'is_bonus'        => (int)$data['bonus_is_active'][$key],
                    'created_at'      => now(),
                ];


                
            }

            if(@count($production_info) > 0)
            {
                ProductionRate::insert($production_info);
            }


            $variation_info = [];

            foreach ($data['product_variation'] as $key => $rows) {

                $variation_info[] = [

                    'product_variation_id'   => $rows,
                    'product_id'             => $product->id,
                    'created_at'             => now(),
                ];
                
            }

            if(@count($variation_info) > 0)
            {
                Variation::insert($variation_info);
            }

            $description = '';

            foreach($product->productionRates as $rows)
            {
                if($rows->production_rate > 0)
                {
                    $description.= 'Dep Name:'.$rows->department->name.', Amount:'.FL::numberFormat($rows->production_rate).', Bonus Amount:'.FL::numberFormat($rows->bonus_amount).', Bonus Limit:'.FL::numberFormat($rows->bonus_limit).', Bouns Active:'.$rows->is_bonus.'<br><hr><br>';
                }
               
            }

            $data_log = new DataLog;

            $data_log->storeDataLog([
                'generic' => $product->id,
                'type'    => 'Product',
                'status'  => 'New Product Created',
                'description' => $description,
            ]);



    		return with($product);
    	});
    }

    public function updateProduct($data)
    {
    	return DB::transaction(function() use ($data) {

    		$product = Product::find($data['product']);

    		if(isset($product->id))
    		{
    			$product->name = $data['name'];

                $product->article_no = $data['article_no'];

            $product->dp                       = FL::uploadImage($data['dp'],$product->dp);
                


                $product->update();

                $production_info = [];

                ProductionRate::where('product_id',$product->id)->delete();


                foreach ($data['department'] as $key => $rows) {

                    $production_info[] = [

                        'department_id'   => $rows,
                        'product_id'      => $product->id,
                        'production_rate' => (double)$data['production_rate'][$key],
                        'bonus'           => (double)$data['bonus'][$key],
                        'bonus_limit'     => (double)$data['bonus_limit'][$key],
                        'is_bonus'        => (int)$data['bonus_is_active'][$key],
                        'created_at'      => now(),
                    ];
                    
                }

                if(@count($production_info) > 0)
                {
                    ProductionRate::insert($production_info);
                }

                Variation::where('product_id',$product->id)->delete();

                $variation_info = [];

                foreach ($data['product_variation'] as $key => $rows) {

                    $variation_info[] = [

                        'product_variation_id'   => $rows,
                        'product_id'             => $product->id,
                        'created_at'             => now(),
                    ];
                    
                }

                if(@count($variation_info) > 0)
                {
                    Variation::insert($variation_info);
                }

    			$product->update();
    		}

            $description = '';

            foreach($product->productionRates as $rows)
            {
                if($rows->production_rate > 0)
                {
                    $description.= 'Dep Name:'.$rows->department->name.', Amount:'.FL::numberFormat($rows->production_rate).', Bonus Amount:'.FL::numberFormat($rows->bonus_amount).', Bonus Limit:'.FL::numberFormat($rows->bonus_limit).', Bouns Active:'.$rows->is_bonus.'<hr>';
                }
               
            }

            $data_log = new DataLog;

            $data_log->storeDataLog([
                'generic' => $product->id,
                'type'    => 'Product',
                'status'  => 'Product Updated',
                'description' => $description,
            ]);

    		return with($product);
    	});
    }

    public function removeProduct($id)
    {

    	return DB::transaction(function() use ($id) {

    		$product = Product::find($id);

    		if(isset($product->id))
    		{
                ProductionRate::where('product_id',$product->id)->delete();

                Variation::where('product_id',$product->id)->delete();


    			$product->delete();

    			return with(true);
    		}

    		return with(false);
    	});
    }

    public function getProductById($id)
    {
    	return Product::find($id);
    }

    public function getAllProducts()
    {
    	return Product::with(['variation'])->orderBy('id','Asc')->get();
    }

    public function variation()
    {
        return $this->hasMany(Variation::class,'product_id')->orderBy('id','ASC')->with(['productVariation']);
    }


    public function getProductVariants()
    {
        return $this->hasMany(ProductVariation::class,'product_id');
    }

    public function productionRates()
    {
        return $this->hasMany(ProductionRate::class,'product_id')->orderBy('id','ASC');
    }
}
