<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class Production extends Model
{
    public function __construct()
    {
        date_default_timezone_set("Asia/Karachi");
    }
    
	public function getProductionById($id)
	{
		return Production::find($id);
	}

    public function getProductionDepartment($order_id)
    {
        return Production::where('order_id',$order_id)->pluck('department_id')->toArray();
    }

	public function getAllProductions()
	{
		return Production::whereHas('order')->orderBy('id','ASC')->get();
	}

     public function storeProduction($data)
    {
    	return DB::transaction(function() use ($data) {

    		$production = new Production;

                $production->order_id           = $data['order'];
                $production->department_id       = $data['department'];

                if(isset($data['parent']))
                {
                    $production->parent_id = $data['parent'];
                }
                else
                {
                    $production->parent_id = 0;
                }



            if(isset($data['is_lot']))
            {
                $production->is_lot       = 1;
            }
            else
            {
                $production->is_lot       = 0;

            }

            if(isset($data['quantity']))
            {
                $production->total_quantity  = array_sum($data['quantity']); 
            }
            else
            {
                $production->total_quantity = 0;
            }

    		$production->save();

    		if(isset($data['product_variant']) && @count($data['product_variant']) > 0)
    		{
    			foreach ($data['product_variant'] as $key => $value) {

	    			$order_detail = new OrderDetail;

                    $user_id = 0;


                    if(isset($data['user'][$key]))
                    {
                        $user_id = $data['user'][$key]; 
                    }

                    $order_detail_id = 0;


                    if(isset($data['parent_order_detail'][$key]))
                    {
                        $order_detail_id = $data['parent_order_detail'][$key]; 
                    }

	    			$order_detail->storeOrderDetail([

	    				'production' 	        => $production->id,
                        'product_variation'     => $value,
	    				'user'	 	  			=> $user_id,
                        'parent'                => $order_detail_id, 
	    				// 'color' 	  			=> $data['color'][$key],
	    				// 'size' 	 	   			=> $data['size'][$key],
	    				'quantity' 	   			=> $data['quantity'][$key],
                        'receive_date' 			=> $data['receive_date'][$key],
                        'is_partial'   			=> $data['partial'][$key],

	    			]);
    			}
    		}

    		return with($production);
    	});
    }

    public function removeProduction($id)
    {

        return DB::transaction(function() use ($id) {

            $production = Production::find($id);

            if(isset($production->id))
            {

                $production->delete();

                return with(true);
            }

            return with(false);
        });
    }


    public function updateProduction($data)
    {
        return DB::transaction(function() use ($data) {

            $production =  Production::find($data['production']);
            if(isset($production->id))
            {
                $production->order_id           = $data['order'];
                $production->department_id       = $data['department'];

                if(isset($data['is_lot']))
                {
                    $production->is_lot       = 1;
                }
                else
                {
                    $production->is_lot       = 0;

                }

                if(isset($data['quantity']))
                {
                    $production->total_quantity  = array_sum($data['quantity']); 
                }
                else
                {
                    $production->total_quantity = 0;
                }

                $production->save();
                if(isset($data['product_variant']) && @count($data['product_variant']) > 0)
                {
                    foreach($production->productionDetail as $rows)
                    {
                        if(isset($data['order_detail']) && !in_array($rows->id,$data['order_detail']))
                        {
                            $rows->delete();                            
                        }
                    }

                    $key = 0;
                    foreach ($production->productionDetail->fresh()->whereNotNull('id') as  $rows) {

                        if(isset($data['order_detail'][$key]) && $rows->id == $data['order_detail'][$key])
                        {
                            $user_id = 0;

                            if(isset($data['user'][$key]))
                            {
                                $user_id = $data['user'][$key]; 
                            }

                            $order_detail_id = 0;
                    
                            if(isset($data['parent_order_detail'][$key]))
                            {
                                $order_detail_id = $data['parent_order_detail'][$key]; 
                            }

                            $rows->production_id         = $production->id;
                            $rows->product_variation_id  = $data['product_variant'][$key];
                            $rows->user_id               = $user_id;
                            $rows->parent_id               = $order_detail_id;

                            $rows->quantity              = $data['quantity'][$key];
                            $rows->receive_date          = $data['receive_date'][$key];
                            $rows->is_partial            = $data['partial'][$key];


                            if($rows->user_id > 0)
                            {
                                $rows->is_lot          = 1;

                                
                            }
                            else
                            {
                                $rows->is_lot          = 0;

                            }

                            $rows->update();

                            unset($data['product_variant'][$key]);

                            if(isset($data['user'][$key]))
                            {
                            unset($data['user'][$key]);
                                
                            }

                            if(isset($data['parent_order_detail'][$key]))
                            {
                            unset($data['parent_order_detail'][$key]);
                                
                            }

                            unset($data['quantity'][$key]);
                            unset($data['receive_date'][$key]);
                            unset($data['partial'][$key]);
                        }
                       
                       $key++;
                    }
                }


                if(isset($data['product_variant']) && @count($data['product_variant']) > 0)
                {
                    foreach ($data['product_variant'] as $key => $value) {

                        $user_id = 0;

                            if(isset($data['user'][$key]))
                            {
                                $user_id = $data['user'][$key]; 
                            }

                             $order_detail_id = 0;
                    

                            if(isset($data['parent_order_detail'][$key]))
                            {
                                $order_detail_id = $data['parent_order_detail'][$key]; 
                            }
                    
                        $order_detail = new OrderDetail;

                        $order_detail->storeOrderDetail([

                            'production'            => $production->id,
                            'product_variation'     => $value,
                            'user'                  => $user_id,
                        'parent'                => $order_detail_id, 
                        
                            'quantity'              => $data['quantity'][$key],
                            'receive_date'          => $data['receive_date'][$key],
                            'is_partial'            => $data['partial'][$key],

                        ]);
                    }
                }
            }

           



            return with($production);
        });
    }

    public function getReceiveQtyByProduction($production,$product_variation)
    {
        $order_receive = new OrderReceive;

        return $order_receive->getReceiveQtyByProduction($production,$product_variation);
    }



    public function productionDetail()
    {
    	return $this->hasMany(OrderDetail::class,'production_id')->orderBy('id','ASC');
    }

    public function orderReceive()
    {
        return $this->hasMany(OrderReceive::class,'production_id')->orderBy('id','ASC');
    }

    public function order()
    {
    	return $this->belongsTo(Order::class,'order_id')->withDefault();
    }

    public function department()
    {
    	return $this->belongsTo(Department::class,'department_id')->withDefault();
    }

    public function parent()
    {
        return $this->belongsTo(Production::class,'parent_id')->withDefault();
    }

    public function directParent()
    {
        return $this->hasOne(Production::class,'parent_id')->withDefault();

    }

    public function verifyParent($id)
    {
        return Production::where('parent_id',$id)->count();
    }
}
