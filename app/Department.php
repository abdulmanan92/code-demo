<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class Department extends Model
{
    public function __construct()
    {
        date_default_timezone_set("Asia/Karachi");
    }

    public function getNextDepartment($order)
    {
        return Department::where('order','>',$order)->limit(1)->orderBy('id','ASC')->get();
    }

    public function getDepartmentByWithoutId($ids = [])
    {
        return Department::whereNotIn('id',$ids)->orderBy('id','ASC')->get();

    }
    
    public function storeDepartment($data)
    {
    	return DB::transaction(function() use ($data) {

    		$department = new Department;

    		$department->name = $data['name'];

            if(Department::where('order',$data['order'])->count() == 0)
            {
                $department->order = $data['order'];
            }


            if(isset($data['is_payroll']))
            {
                $department->is_payroll = 1;
            }
            else
            {
                $department->is_payroll = 0;
            }


    		$department->save();

    		return with($department);
    	});
    }

    public function updateDepartment($data)
    {
    	return DB::transaction(function() use ($data) {

    		$department = Department::find($data['department']);

    		if(isset($department->id))
    		{
    			$department->name = $data['name'];

                if(Department::where('order',$data['order'])->count() == 0)
                {
                    $department->order = $data['order'];
                }

                if(isset($data['is_payroll']))
                {
                    $department->is_payroll = 1;
                }
                else
                {
                    $department->is_payroll = 0;
                }

    			$department->update();
    		}

    		return with($department);
    	});
    }

    public function removeDepartment($id)
    {

    	return DB::transaction(function() use ($id) {

    		$department = Department::find($id);

    		if(isset($department->id) && $department->is_auto == 0)
    		{
    			$department->delete();

    			return with(true);
    		}

    		return with(false);
    	});
    }

    public function getDepartmentById($id)
    {
    	return Department::find($id);
    }

    public function getAllDepartments()
    {
    	return Department::orderBy('order','Asc')->get();
    }

    public function users()
    {
        return $this->hasMany(User::class,'department_id');
    }
}
