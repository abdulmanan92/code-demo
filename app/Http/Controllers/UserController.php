<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Contracts\Encryption\DecryptException;
use App\Department;
use App\User;
use App\PaymentHistory;
use Spatie\Permission\Models\Role;
use Illuminate\Validation\Rule;




class UserController extends Controller
{
    private $user;
	private $message;
	private $status;

	public function __construct()
    {
    	$this->user = new User;
    	$this->department = new Department;


    	$this->message    = 'Something is wrong';
    	$this->status     = 'error';

    }

	public function getValidationList($new_array = [])
	{
		return $new_array += [

			'name'  			=> 'required|min:0|max:255',
			'email' 			=> 'nullable|email|unique:users|min:0|max:255',
			'contact_no'  		=> 'nullable|min:0|max:20',
			'contact_no'  		=> 'nullable|min:0|max:999',
			'cnic'  	    	=> 'nullable|min:13|max:15',
			'reference'  		=> 'nullable|min:0|max:255',
			'payroll_group'  	=> 'nullable|min:0|max:255',
			'advance_amount'  	=> 'nullable|gte:0',
			'pocket_money'  	=> 'nullable|gte:0',
			'password'          => 'nullable|min:6|max:16|same:confirm_password',
            'confirm_password'  => 'nullable|min:6|max:16|same:password',
            'photo'             => 'nullable|mimes:jpeg,png,jpg,svg|max:250',
            'files.*'           => 'nullable|mimes:jpeg,png,jpg,svg,pdf|max:250',
			'doc_title.*'    	=> 'nullable|min:0|max:255',
			'doc_date.*'    	=> 'nullable|date',
		];
	}

	public function showTeamMember()
	{
		return view('users.team_member',[

			'user_list' => $this->user->getAllUsersByType(['Team Member']),
			'department_list'	=> $this->department->getAllDepartments(),

		]);
	}

	public function showEmployee()
	{
		return view('users.employee',[

			'user_list' => $this->user->getAllUsersByType(['Employee']),
			'department_list'	=> $this->department->getAllDepartments(),
			'role_list'			=> Role::whereNotIn('id',[1])->get(),

		]);
	}

	public function getBonusInfo(Request $request)
	{
		try{

			return $this->user->calculateBonus(decrypt($request->id));

		} catch (DecryptException $e) {
		    //
		}
	}

	public function teamMemberProfile(Request $request)
	{
		try{

			$user = new User;

			$payment_history = new PaymentHistory;

			$user = $user->getUserById(decrypt($request->id));

			if(isset($user->id))
			{

				return view('users.team_member_profile',[
					'user' => $user,
					'payment_list' => $payment_history->getTyepWiseUserLedger($user->id,['Production Amount','Advance Amount','Pocket Money','Bonus Amount','Advance Production Amount'],['Paid']),
				]);
			}

		} catch (DecryptException $e) {
		    //
		}
	}

    public function storeTeamMember(Request $request)
    {
		try 
		{

			$request->validate($this->getValidationList([
            	'cnic'  => 'required|max:100|unique:users,cnic',
			]));

			$collect_data = $request->input();

			$collect_data['department']	= decrypt($collect_data['department']);


			$collect_data['email'] 	  = '';
			$collect_data['password'] = '';
			$collect_data['dp'] 	  =  $request->photo;
			$collect_data['status']   = 'Deactive';
			$collect_data['type']     = 'Team Member';

			$collect_data['files']    = $request->files;


			$user = $this->user->storeUser($collect_data);

			if(isset($user->id))
			{
				$this->message = 'Team member has added succssfully.';
				$this->status  = 'success';
			}


		} catch (DecryptException $e) {
		    //
		}


		return redirect()->back()->with([

			'status'  => $this->status,
			'message' => $this->message,
		]);
    }

    public function storeEmployee(Request $request)
    {
		try 
		{

			$request->validate($this->getValidationList());

			$collect_data = $request->input();

			$collect_data['department']	= decrypt($collect_data['department']);




			$collect_data['date_of_joining'] 	 	= NULL;
			$collect_data['reference'] 				= '';
			$collect_data['payroll_group']  		= '';
			$collect_data['advance_amount'] 		= 0;
			$collect_data['pocket_money']   		= 0;
			$collect_data['dp'] 	  =  $request->photo;
			$collect_data['status']   = 'Deactive';
			$collect_data['type']     = 'Employee';

			$user = $this->user->storeUser($collect_data);

			if(isset($user->id))
			{
				$this->message = 'Employee has added succssfully.';
				$this->status  = 'success';
			}


		} catch (DecryptException $e) {
		    //
		}


		return redirect()->back()->with([

			'status'  => $this->status,
			'message' => $this->message,
		]);
    }


    public function updateTeamMember(Request $request)
    {
		try 
		{
			if($request->isMethod('Post'))
			{
				$request->validate($this->getValidationList([

                    'cnic' => ['required',Rule::unique('users')->ignore(decrypt($request->user)),'max:15'],
				]));

				$collect_data = $request->input();

				$collect_data['department']	= decrypt($collect_data['department']);

				$collect_data['user']	= decrypt($collect_data['user']);

				if(isset($collect_data['payment']))
				{
					foreach ($collect_data['payment'] as $key => $rows) {
						
						$collect_data['payment'][$key] = decrypt($rows);
					}
				}
				else
				{
					$collect_data['payment'] = [];
				}


				$collect_data['email'] 	  = '';
				$collect_data['password'] = '';
				$collect_data['dp'] 	  =  $request->photo;
				// $collect_data['status']   = 'Deactive';
				$collect_data['type']     = 'Team Member';

				$user = $this->user->updateUser($collect_data);

				if(isset($user->id))
				{
					$this->message = 'Team member has updated succssfully.';
					$this->status  = 'success';
				}
			}
			else
			{
				$id = $request->id;

				$user = $this->user->getUserById(decrypt($id));

				if(isset($user->id))
				{
					return view('users.update_team_member',[

						'user' => $user,
						'department_list'	=> $this->department->getAllDepartments(),
						'document_list'		=> $user->documents,

					]);
				}
			}
			


		} catch (DecryptException $e) {
		    //
		}


		return redirect()->back()->with([

			'status'  => $this->status,
			'message' => $this->message,
		]);
    }

    public function updateEmployee(Request $request)
    {
		try 
		{
			if($request->isMethod('Post'))
			{
				$request->validate($this->getValidationList([
					'email' => ['required','email',Rule::unique('users')->ignore(decrypt($request->user)),'min:3','max:255'],
				]));

				$collect_data = $request->input();

				$collect_data['department']	= decrypt($collect_data['department']);

				$collect_data['user']	= decrypt($collect_data['user']);


				$collect_data['date_of_joining'] 	 	= NULL;
				$collect_data['reference'] 				= '';
				$collect_data['payroll_group']  		= '';
				$collect_data['advance_amount'] 		= 0;
				$collect_data['pocket_money']   		= 0;
				$collect_data['dp'] 	  =  $request->photo;
				// $collect_data['status']   = 'Deactive';
				$collect_data['type']     = 'Employee';

				$user = $this->user->updateUser($collect_data);

				if(isset($user->id))
				{
					$this->message = 'User has updated succssfully.';
					$this->status  = 'success';
				}
			}
			else
			{
				$id = $request->id;

				$user = $this->user->getUserById(decrypt($id));

				if(isset($user->id))
				{
					return view('users.modals.update_employee',[

						'user' => $user,
						'department_list'	=> $this->department->getAllDepartments(),
			            'role_list'			=> Role::whereNotIn('id',[1])->get(),


					]);
				}
			}
			


		} catch (DecryptException $e) {
		    //
		}


		return redirect()->back()->with([

			'status'  => $this->status,
			'message' => $this->message,
		]);
    }

    public function removeUser(Request $request)
    {
		try 
		{

			$user = $this->user->removeUser(decrypt($request->id));

			if($user)
			{
				$this->message = 'User has removed succssfully.';
				$this->status  = 'success';
			}


		} catch (DecryptException $e) {
		    //
		}


		return redirect()->back()->with([

			'status'  => $this->status,
			'message' => $this->message,
		]);
    }
}
