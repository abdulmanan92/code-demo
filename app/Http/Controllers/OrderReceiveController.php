<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Contracts\Encryption\DecryptException;
use App\OrderReceive;
use App\OrderDetail;

use App\Order;
use App\Production;


use Illuminate\Validation\Rule;


class OrderReceiveController extends Controller
{
    private $order_receive;
	private $message;
	private $status;

	public function __construct()
    {
    	$this->order_receive   = new OrderReceive;
    	$this->order   		   = new Order;
    	$this->production   		   = new Production;


    	$this->message    	   = 'Something is wrong';
    	$this->status     	   = 'error';

    }

	public function getValidationList($new_array = [])
	{
		return $new_array += [

			'quantity'	=> 'required|gt:0',
			'date' 	=> 'date',
		];
	}

	public function index($id)
	{
		try{

			$production = $this->production->getProductionById(decrypt($id));

			if(isset($production->id))
			{
				return view('order_receive.order_receive',[

					'order_receive_list' => $this->order_receive->getAllOrderReceiveByOrder($production->id),
					'production' => $production,
					
				]);
			}

			

		} catch (DecryptException $e) {
		    //
		}
		
	}

    public function storeOrderReceive(Request $request)
    {
		try 
		{
			if($request->isMethod('post'))
			{
				$request->validate($this->getValidationList());

				$collect_data = $request->input();

				$collect_data['production'] = decrypt($collect_data['production']);

				$collect_data['order_detail'] = decrypt($collect_data['order_detail']);

				$order_detail = new OrderDetail;

				$order_detail = $order_detail->getOrderDetailById($collect_data['order_detail']);


				if(isset($order_detail->id))
				{
					$rece_qty = $order_detail->orderReceive->sum('quantity');

					$balanace_qty = $order_detail->quantity - $rece_qty;

					if($balanace_qty > 0 && $collect_data['quantity'] > 0 && $collect_data['quantity'] <= $balanace_qty)
					{

					}
					else
					{
						$this->message = 'Quantity less than equal to '.$balanace_qty;
						$this->status  = 'error';

						return redirect()->back()->with([

							'status'  => $this->status,
							'message' => $this->message,
						]);
									}
				}
				else
				{
					$this->message = 'Production order not found.';
					$this->status  = 'error';

					return redirect()->back()->with([

							'status'  => $this->status,
							'message' => $this->message,
						]);
				}

				$order_receive = $this->order_receive->storeOrderReceive($collect_data);


				if(isset($order_receive->id))
				{
					$this->message = 'Order receive has added succssfully.';
					$this->status  = 'success';
				}
			}
			else
			{
				$id = decrypt($request->production_id);

				$production = $this->production->getProductionById($id);

				if(isset($production->id))
				{
					return view('order_receive.modals.add_order_receive',[
						
						'production' => $production
					]);
				}
			}
			
		} catch (DecryptException $e) {
		    //
		}


		return redirect()->back()->with([

			'status'  => $this->status,
			'message' => $this->message,
		]);
    }


    public function updateOrderReceive(Request $request)
    {
		try 
		{
			if($request->isMethod('Post'))
			{
				$request->validate($this->getValidationList());

			$collect_data = $request->input();

			$collect_data['production'] = decrypt($collect_data['production']);

			$collect_data['order_receive'] = decrypt($collect_data['order_receive']);

			
			$order_receive = $this->order_receive->updateOrderReceive($collect_data);

				if(isset($order_receive->id))
				{
					$this->message = 'Order receive has updated succssfully.';
					$this->status  = 'success';
				}
			}
			else
			{
				$id = $request->id;

				$order_receive = $this->order_receive->getOrderReceiveById(decrypt($id));

				if(isset($order_receive->id))
				{
					return view('order_receive.modals.update_order_receive',[

						'order_receive' => $order_receive,
						
					]);
				}
			}
			


		} catch (DecryptException $e) {
		    //
		}


		return redirect()->back()->with([

			'status'  => $this->status,
			'message' => $this->message,
		]);
    }

    public function removeOrderReceive(Request $request)
    {
		try 
		{

			$order_receive = $this->order_receive->removeOrderReceive(decrypt($request->id));

			if($order_receive)
			{
				$this->message = 'Order receive has removed succssfully.';
				$this->status  = 'success';
			}


		} catch (DecryptException $e) {
		    //
		}


		return redirect()->back()->with([

			'status'  => $this->status,
			'message' => $this->message,
		]);
    }
}
