<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Contracts\Encryption\DecryptException;
use App\Document;
use App\User;
use Illuminate\Validation\Rule;

class DocumentController extends Controller
{
    private $Document;
	private $message;
	private $status;

	public function __construct()
    {
    	$this->document   = new Document;
    	$this->message    = 'Something is wrong';
    	$this->status     = 'error';

    }

	public function getValidationList($new_array = [])
	{
		return $new_array += [

			'title'	=> 'required|min:0|max:255',
			'date' 	=> 'date',
            'file'  => 'required|mimes:jpeg,png,jpg,svg|max:250',
		];
	}

	public function index($generic_id,$generic_type)
	{
		try{

			$generic_id 	= decrypt($generic_id);
			$generic_type 	= decrypt($generic_type);

			return view('documents.document',[

				'document_list' => $this->document->getAllDocumentByGeneric(($generic_id),($generic_type)),
				'generic_id' 	=> $generic_id,
				'generic_type' 	=> $generic_type,

			]);

		} catch (DecryptException $e) {
		    //
		}
		
	}

    public function storeDocument(Request $request)
    {
		try 
		{
			$request->validate($this->getValidationList());

			$collect_data = $request->input();

			$collect_data['file'] = $request->file;

			$collect_data['generic_id']   = decrypt($collect_data['generic_id']);

			$collect_data['generic_type'] = decrypt($collect_data['generic_type']);

			$document = $this->document->storeDocument($collect_data);

			if(isset($document->id))
			{
				$this->message = 'Document has added succssfully.';
				$this->status  = 'success';
			}


		} catch (DecryptException $e) {
		    //
		}


		return redirect()->back()->with([

			'status'  => $this->status,
			'message' => $this->message,
		]);
    }


    public function updateDocument(Request $request)
    {
		try 
		{
			if($request->isMethod('Post'))
			{
				$request->validate($this->getValidationList([

					'file'  => 'nullable|mimes:jpeg,png,jpg,svg|max:250',

				]));

				$collect_data = $request->input();

				$collect_data['file'] 		  = $request->file;

				$collect_data['document']     = decrypt($collect_data['document']);

				$document = $this->document->updateDocument($collect_data);

				if(isset($document->id))
				{
					$this->message = 'Document has updated succssfully.';
					$this->status  = 'success';
				}
			}
			else
			{
				$id = $request->id;

				$document = $this->document->getDocumentById(decrypt($id));

				if(isset($document->id))
				{
					return view('documents.modals.update_document',[

						'document' => $document,
						
					]);
				}
			}
			


		} catch (DecryptException $e) {
		    //
		}


		return redirect()->back()->with([

			'status'  => $this->status,
			'message' => $this->message,
		]);
    }

    public function removeDocument(Request $request)
    {
		try 
		{

			$document = $this->document->removeDocument(decrypt($request->id));

			if($document)
			{
				$this->message = 'Document has removed succssfully.';
				$this->status  = 'success';
			}


		} catch (DecryptException $e) {
		    //
		}


		return redirect()->back()->with([

			'status'  => $this->status,
			'message' => $this->message,
		]);
    }
}
