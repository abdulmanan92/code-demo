<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Contracts\Encryption\DecryptException;
use App\Product;
use App\ProductVariation;
use App\Department;
use Illuminate\Validation\Rule;



class ProductController extends Controller
{
    private $product;
	private $message;
	private $status;

	public function __construct()
    {
    	$this->product 		     = new Product;
    	$this->department 		 = new Department;
    	$this->product_variation = new ProductVariation;



    	$this->message    = 'Something is wrong';
    	$this->status     = 'error';

    }

	public function getValidationList($new_array = [])
	{
		return $new_array += [

			'name' 		 		  => 'required|min:0|max:255',
			'article_no'	 	  => 'required|unique:products|min:0|max:255',
			'product_variation.*' => 'nullable',
			'department.*' 		  => 'nullable',
			'production_rate.*'   => 'nullable|gte:0',
            'photo'               => 'nullable|mimes:jpeg,png,jpg,svg|max:250',
		];
	}

	public function index()
	{
		return view('products.product',[

			'product_list' 		      => $this->product->getAllProducts(),
			'department_list' 		  => $this->department->getAllDepartments(),
			'product_variation_list' => $this->product_variation->getAllProductVariations(),

		]);
	}

    public function storeProduct(Request $request)
    {
		try 
		{
			$request->validate($this->getValidationList());

			$collect_data = $request->input();

			$collect_data['dp'] = $request->photo;

			if(isset($collect_data['product_variation']))
			{
				foreach ($collect_data['product_variation'] as $key => $value) {

					$collect_data['product_variation'][$key] = decrypt($value);
				}
			}
			else
			{
				$collect_data['product_variation'] = [];
			}

			if(isset($collect_data['department']))
			{
				foreach ($collect_data['department'] as $key => $value) {
					
					$collect_data['department'][$key] = decrypt($value);
				}
			}
			else
			{
				$collect_data['department'] = [];
			}

			$product = $this->product->storeProduct($collect_data);

			if(isset($product->id))
			{
				$this->message = 'Product has added succssfully.';
				$this->status  = 'success';
			}


		} catch (DecryptException $e) {
		    //
		}


		return redirect()->back()->with([

			'status'  => $this->status,
			'message' => $this->message,
		]);
    }


    public function updateProduct(Request $request)
    {
		try 
		{
			if($request->isMethod('Post'))
			{
				$request->validate($this->getValidationList([
					'article_no' => ['required',Rule::unique('products')->ignore(decrypt($request->product)),'min:0','max:255'],
				]));

				$collect_data = $request->input();

				$collect_data['dp'] = $request->photo;

				$collect_data['product'] = decrypt($collect_data['product']);

				if(isset($collect_data['product_variation']))
				{
					foreach ($collect_data['product_variation'] as $key => $value) {

						$collect_data['product_variation'][$key] = decrypt($value);
					}
				}
				else
				{
					$collect_data['product_variation'] = [];
				}

				if(isset($collect_data['department']))
				{
					foreach ($collect_data['department'] as $key => $value) {
						
						$collect_data['department'][$key] = decrypt($value);
					}
				}
				else
				{
					$collect_data['department'] = [];
				}

				$product = $this->product->updateProduct($collect_data);

				if(isset($product->id))
				{
					$this->message = 'Product has updated succssfully.';
					$this->status  = 'success';
				}
			}
			else
			{
				$id = $request->id;

				$product = $this->product->getProductById(decrypt($id));

				if(isset($product->id))
				{
					return view('products.modals.update_product',[

						'product' => $product,
						'department_list' 		  => $this->department->getAllDepartments(),
						'product_variation_list' => $this->product_variation->getAllProductVariations(),
					]);
				}
			}
			


		} catch (DecryptException $e) {
		    //
		}


		return redirect()->back()->with([

			'status'  => $this->status,
			'message' => $this->message,
		]);
    }

    public function removeProduct(Request $request)
    {
		try 
		{

			$product = $this->product->removeProduct(decrypt($request->id));

			if($product)
			{
				$this->message = 'Product has removed succssfully.';
				$this->status  = 'success';
			}


		} catch (DecryptException $e) {
		    //
		}


		return redirect()->back()->with([

			'status'  => $this->status,
			'message' => $this->message,
		]);
    }
}
