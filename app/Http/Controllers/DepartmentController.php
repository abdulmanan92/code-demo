<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Contracts\Encryption\DecryptException;
use App\Department;

class DepartmentController extends Controller
{
	private $department;
	private $message;
	private $status;

	public function __construct()
    {
    	$this->department = new Department;

    	$this->message    = 'Something is wrong';
    	$this->status     = 'error';

    }

	public function getValidationList($new_array = [])
	{
		return $new_array += [

			'name' => 'required|min:0|max:255',
		];
	}

	public function index()
	{
		return view('departments.department',[

			'department_list' => $this->department->getAllDepartments(),
		]);
	}

    public function storeDepartment(Request $request)
    {
		try 
		{
			$request->validate($this->getValidationList());

			$collect_data = $request->input();

			$department = $this->department->storeDepartment($collect_data);

			if(isset($department->id))
			{
				$this->message = 'Department has added succssfully.';
				$this->status  = 'success';
			}


		} catch (DecryptException $e) {
		    //
		}


		return redirect()->back()->with([

			'status'  => $this->status,
			'message' => $this->message,
		]);
    }


    public function updateDepartment(Request $request)
    {
		try 
		{
			if($request->isMethod('Post'))
			{
				$request->validate($this->getValidationList());

				$collect_data = $request->input();

				$collect_data['department'] = decrypt($collect_data['department']);

				$department = $this->department->updateDepartment($collect_data);

				if(isset($department->id))
				{
					$this->message = 'Department has updated succssfully.';
					$this->status  = 'success';
				}
			}
			else
			{
				$id = $request->id;

				$department = $this->department->getDepartmentById(decrypt($id));

				if(isset($department->id))
				{
					return view('departments.modals.update_department',[

						'department' => $department,
					]);
				}
			}
			


		} catch (DecryptException $e) {
		    //
		}


		return redirect()->back()->with([

			'status'  => $this->status,
			'message' => $this->message,
		]);
    }

    public function removeDepartment(Request $request)
    {
		try 
		{

			$department = $this->department->removeDepartment(decrypt($request->id));

			if($department)
			{
				$this->message = 'Department has removed succssfully.';
				$this->status  = 'success';
			}


		} catch (DecryptException $e) {
		    //
		}


		return redirect()->back()->with([

			'status'  => $this->status,
			'message' => $this->message,
		]);
    }
}
