<?php

namespace App\Http\Controllers;
use Illuminate\Contracts\Encryption\DecryptException;

use Illuminate\Http\Request;
use App\Payment;
use App\OrderReceive;
use App\User;
use App\Order;
use App\Department;
use App\Product;
use App\OrderDetail;
use App\PaymentHistory;
use PDF;
USE FL;

class ReportController extends Controller
{
	private $user;
	private $department;
	private $product;
	private $order;


	public function __construct()
    {
    	$this->user 		= new User;
    	$this->department   = new Department;
    	$this->order        = new Order;
    }


    public function filterConditionalArray($array)
    {
        $title = "";

        foreach ($array as $key => $value) 
        {

            if($value == "0")
            {
                unset($array[$key]);
            }
            else
            {
                $title.=ucwords($value).' ';
            }
       }

       return [$array,$title];
    }

	public function index()
	{
		return view('reports.report',[

			'user_list' 		=> $this->user->getAllUsersByType(['Team Member']),
			'department_list' 	=> $this->department->getAllDepartments(),
			'product_list' 	    => $this->product->getAllProducts(),
		]);
	}

	public function getProductionIssuanceReport(Request $request)
	{
		try
		{
			$report_title = '<b><u>PRODUCTION ISSUANCE REPORT</b></u>';

			$condition = [];

			$condition['user_id']       = decrypt($request->labour);

			$condition['department_id'] = decrypt($request->department);

			$condition['product_id']    = decrypt($request->product);

			$from 						= $request->from;

			$to 						= $request->to;

			$dep_name = '';

			if($condition['department_id'] > 0)
			{
				$department = $this->department->getDepartmentById($condition['department_id']);

				if(isset($department->id))
				{
					$dep_name = $department->name;
				}
			}

			$order_detail = new OrderDetail;

			$condition = $this->filterConditionalArray($condition);

			$order_detail_list = $order_detail->getProductionIssuanceReport($from,$to,$condition[0]);

			$html = '<!DOCTYPE html>
				    <html>
				        <head>
				            <title>'.$report_title.'</title>

				            <style>
				            *{
				                margin:0px;
				            }
				            .left-td{
				                text-align:left !important;
				            }
				            .h4-margin{
				            	margin-top:5px;
				            	margin-bottom:5px;

				            }
				            </style>
				        </head>

				        <body style="margin:5%;">

				        <div style="float:left">
				        	<h2>'.$report_title.'</h2>
				        	<h4>Department: '.$dep_name.'</h4>

				        </div>

				        <div style="float:right">
				        	<img src="'.asset('/graphics/logo.png').'">
				        	<p><b>Company: </b>MOTOLUX INDUSTRIES</p>
				        	<p><b>Address: </b>  <span style="  text-transform: uppercase;">Muzzafarpur, Ugoki Road, Sialkot, Pakistan</span></p>

				        </div>

				         <table  border="2" style="font-size:12px; width: 100%; border-collapse: collapse; clear: both; text-align:left; margin-top:30px; text-align:center; border: 1px solid #000;" >
				         	<thead style="background:#000; color:#FFF;">
				         	
				         		<tr>
				         			<th>LABOUR</th>
				         			<th>ORDER #</th>
				         			<th>ARTICLE #</th>
				         			<th>PRODUCT</th>
				         			<th>COLOR</th>
				         			<th>SIZE</th>
				         			<th>QTY</th>
				         			<th>ISSUED ON</th>
				         			<th>RECEIVING DUE DATE</th>

				         		</tr>
				         	</thead>';

				         	foreach ($order_detail_list as $rows) {
				         		
				         		$html.='<tr>
				         			<td>'.$rows->user->name.'</td>
				         			<td>'.$rows->production->order->order_no.'</td>
				         			<td>'.$rows->productVariation->product->article_no.'</td>
				         			<td>'.$rows->productVariation->product->name.'</td>
				         			<td>'.$rows->productVariation->color.'</td>
				         			<td>'.$rows->productVariation->size.'</td>
				         			<td>'.$rows->quantity.'</td>
				         			<td>'.FL::dateFormatWithTime($rows->created_at).'</td>
				         			<td>'.FL::dateFormat($rows->receive_date).'</td>
				         		</tr>';
				         	}




				         $html.='</table>';


			// PDF
			if($request->format == 0)
			{
				$pdf = PDF::loadHtml($html);

        		return $pdf->setOptions(['defaultFont' => 'Helvetica','isRemoteEnabled'=>true])->setPaper('a4', 'landscape')->stream($report_title.'.pdf');
			}
			else // EXCEL
			{
				header("Content-Type: application/xls");

                header("Content-Disposition: attachment; filename=".$report_title.".xls");

                echo $html;

                return '';
			}



		}
		catch (DecryptException $e) {
		    //
		}

		return redirect('reports');
	}



	public function getProductionReceivingReport(Request $request)
	{
		try
		{
			$report_title = '<u><b>PRODUCTION RECEIVING REPORT</b></u>';

			$condition = [];

			$condition['user_id']       = decrypt($request->labour);

			$condition['department_id'] = decrypt($request->department);

			$condition['product_id']    = decrypt($request->product);

			$from 						= $request->from;

			$to 						= $request->to;

			$dep_name = '';

			if($condition['department_id'] > 0)
			{
				$department = $this->department->getDepartmentById($condition['department_id']);

				if(isset($department->id))
				{
					$dep_name = $department->name;
				}
			}

			$order_receive = new OrderReceive;

			$condition = $this->filterConditionalArray($condition);

			$order_receive_list = $order_receive->getProductionReceivingReport($from,$to,$condition[0]);

			$html = '<!DOCTYPE html>
				    <html>
				        <head>
				            <title>'.$report_title.'</title>

				            <style>
				            *{
				                margin:0px;
				            }
				            .left-td{
				                text-align:left !important;
				            }
				            .h4-margin{
				            	margin-top:5px;
				            	margin-bottom:5px;

				            }
				            </style>
				        </head>

				        <body style="margin:5%;">

				        <div style="float:left">
				        	<h2>'.$report_title.'</h2>
				        	<h4>Department: '.$dep_name.'</h4>

				        </div>

				        <div style="float:right">
				        	<img src="'.asset('/graphics/logo.png').'">
				        	<p><b>Company: </b>MOTOLUX INDUSTRIES</p>
				        	<p><b>Address: </b>  <span style="  text-transform: uppercase;">Muzzafarpur, Ugoki Road, Sialkot, Pakistan</span></p>

				        </div>

				         <table  border="2" style="font-size:12px; width: 100%; border-collapse: collapse; clear: both; text-align:left; margin-top:30px; text-align:center; border: 1px solid #000;" >
				         	<thead style="background:#000; color:#FFF;">
				         	
				         		<tr>
				         			<th>LABOUR</th>
				         			<th>ORDER #</th>
				         			<th>ARTICLE #</th>
				         			<th>PRODUCT</th>
				         			<th>COLOR</th>
				         			<th>SIZE</th>
				         			<th>QTY</th>
				         			<th>RECEIVING DUE DATE</th>
				         			<th>RECEIVING ON</th>

				         		</tr>
				         	</thead>';

				         	foreach ($order_receive_list as $rows) {
				         		
				         		$html.='<tr>
				         			<td>'.$rows->orderDetail->user->name.'</td>
				         			<td>'.$rows->orderDetail->production->order->order_no.'</td>
				         			<td>'.$rows->orderDetail->productVariation->product->article_no.'</td>
				         			<td>'.$rows->orderDetail->productVariation->product->name.'</td>
				         			<td>'.$rows->orderDetail->productVariation->color.'</td>
				         			<td>'.$rows->orderDetail->productVariation->size.'</td>
				         			<td>'.$rows->quantity.'</td>
				         			<td>'.FL::dateFormat($rows->orderDetail->receive_date).'</td>
				         			<td>'.FL::dateFormatWithTime($rows->date).'</td>
				         		</tr>';
				         	}




				         $html.='</table>';


			// PDF
			if($request->format == 0)
			{
				$pdf = PDF::loadHtml($html);

        		return $pdf->setOptions(['defaultFont' => 'Helvetica','isRemoteEnabled'=>true])->setPaper('a4', 'landscape')->stream($report_title.'.pdf');
			}
			else // EXCEL
			{
				header("Content-Type: application/xls");

                header("Content-Disposition: attachment; filename=".$report_title.".xls");

                echo $html;

                return '';
			}



		}
		catch (DecryptException $e) {
		    //
		}

		return redirect('reports');
	}


	public function getLabourProgressReport(Request $request)
	{
		try
		{
			$report_title = '<b><u>LABOUR PROGRESS REPORT</u></b>';

			$condition = [];

			$condition['user_id']       = decrypt($request->labour);

			// $condition['department_id'] = decrypt($request->department);

			$condition['product_id']    = decrypt($request->product);

			$from 						= $request->from;

			$to 						= $request->to;

			$dep_name = '';
			$user_name = '';
			$advance = 0;
			$pocket = 0;


			if($condition['user_id'] > 0)
			{
				$user = $this->user->getUserById($condition['user_id']);

				if(isset($user->id))
				{
					$user_name = $user->name;
					$dep_name  = $user->department->name;

					$payment_history = new PaymentHistory;

					$payment_list = $payment_history->getTyepWiseUserLedger($user->id,['Advance Amount','Pocket Money'],['Paid']);

					$advance_list = $payment_list->whereIn('payment_type',['Advance Amount']);

                    $advance      = $advance_list->sum('debit') - $advance_list->sum('credit'); 

                    $pocket_list  = $payment_list->whereIn('payment_type',['Pocket Money']);

                    $pocket       = $pocket_list->sum('debit') - $pocket_list->sum('credit');

				}
			}

			$order_receive = new OrderReceive;

			$condition = $this->filterConditionalArray($condition);

			$order_receive_list = $order_receive->getProductionReceivingReport($from,$to,$condition[0]);

			$html = '<!DOCTYPE html>
				    <html>
				        <head>
				            <title>'.$report_title.'</title>

				            <style>
				            *{
				                margin:0px;
				            }
				            .left-td{
				                text-align:left !important;
				            }
				            .h4-margin{
				            	margin-top:5px;
				            	margin-bottom:5px;

				            }
				            </style>
				        </head>

				        <body style="margin:5%;">

				        <div style="float:left">
				        	<h2>'.$report_title.'</h2>
				        	<h4>Labour Name: '.$user_name.'</h4>
				        	<h4>Department: '.$dep_name.'</h4>
				        	<h4>Advance Amount Balance: Rs '.FL::numberFormat($advance).'</h4>
				        	<h4>Pocket Money Balance: &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Rs '.FL::numberFormat($pocket).'</h4>


				        </div>

				        <div style="float:right">
				        	<img src="'.asset('/graphics/logo.png').'">
				        	<p><b>Company: </b>MOTOLUX INDUSTRIES</p>
				        	<p><b>Address: </b>  <span style="  text-transform: uppercase;">Muzzafarpur, Ugoki Road, Sialkot, Pakistan</span></p>

				        </div>

				         <table  border="2" style="font-size:12px; width: 100%; border-collapse: collapse; clear: both; text-align:left; margin-top:30px; text-align:center; border: 1px solid #000;" >
				         	<thead style="background:#000; color:#FFF;">
				         		<tr>
				         			<th>ORDER #</th>
				         			<th>ARTICLE #</th>
				         			<th>PRODUCT</th>
				         			<th>COLOR</th>
				         			<th>SIZE</th>
				         			<th>QTY</th>
				         			<th>ISSUED ON</th>
				         			<th>RECEIVING DUE DATE</th>
				         			<th>PAYMENT REFERENCE</th>



				         		</tr>
				         	</thead>';

				         	foreach ($order_receive_list as $rows) {
				         		
				         		$html.='<tr>
				         			<td>'.$rows->orderDetail->production->order->order_no.'</td>
				         			<td>'.$rows->orderDetail->productVariation->product->article_no.'</td>
				         			<td>'.$rows->orderDetail->productVariation->product->name.'</td>
				         			<td>'.$rows->orderDetail->productVariation->color.'</td>
				         			<td>'.$rows->orderDetail->productVariation->size.'</td>
				         			<td>'.$rows->quantity.'</td>
				         			<td>'.FL::dateFormatWithTime($rows->orderDetail->created_at).'</td>
				         			<td>'.FL::dateFormat($rows->orderDetail->receive_date).'</td><td>';

				         			$payment = $rows->paymentHistory->payment; 

				         			if($payment->status == "Paid")
				         			{
				         				$html.='Paid <br>'.FL::dateFormat($payment->date);
				         			}
				         			else
				         			{
				         				$html.='Unpaid';
				         			}
				         			

				         		$html.='</td></tr>';
				         	}




				         $html.='</table>';


			// PDF
			if($request->format == 0)
			{
				$pdf = PDF::loadHtml($html);

        		return $pdf->setOptions(['defaultFont' => 'Helvetica','isRemoteEnabled'=>true])->setPaper('a4', 'landscape')->stream($report_title.'.pdf');
			}
			else // EXCEL
			{
				header("Content-Type: application/xls");

                header("Content-Disposition: attachment; filename=".$report_title.".xls");

                echo $html;

                return '';
			}



		}
		catch (DecryptException $e) {
		    //
		}

		return redirect('reports');
	}


	public function getLabourPaymentReport(Request $request)
	{
		try
		{
			$report_title = '<b><u>LABOUR PAYMENT REPORT</u></b>';

			$condition = [];

			$condition['user_id']       = decrypt($request->labour);

			$condition['department_id'] = decrypt($request->department);

			$condition['product_id']    = decrypt($request->product);

			$from 						= $request->from;

			$to 						= $request->to;

			$dep_name = '';

			if($condition['department_id'] > 0)
			{
				$department = $this->department->getDepartmentById($condition['department_id']);

				if(isset($department->id))
				{
					$dep_name = $department->name;
				}
			}

			$payment_history = new PaymentHistory;

			$order_receive = new OrderReceive;

			$condition = $this->filterConditionalArray($condition);

			$order_receive_list = $order_receive->getProductionReceivingReport($from,$to,$condition[0]);

			$html = '<!DOCTYPE html>
				    <html>
				        <head>
				            <title>'.$report_title.'</title>

				            <style>
				            *{
				                margin:0px;
				            }
				            .left-td{
				                text-align:left !important;
				            }
				            .h4-margin{
				            	margin-top:5px;
				            	margin-bottom:5px;

				            }
				            </style>
				        </head>

				        <body style="margin:5%;">

				        <div style="float:left">
				        	<h2>'.$report_title.'</h2>

				        </div>

				        <div style="float:right">
				        	<img src="'.asset('/graphics/logo.png').'">
				        	<p><b>Company: </b>MOTOLUX INDUSTRIES</p>
				        	<p><b>Address: </b>  <span style="  text-transform: uppercase;">Muzzafarpur, Ugoki Road, Sialkot, Pakistan</span></p>

				        </div>

				         <table  border="2" style="font-size:12px; width: 100%; border-collapse: collapse; clear: both; text-align:left; margin-top:30px; text-align:center; border: 1px solid #000;" >
				         	<thead style="background:#000; color:#FFF;">
				         		<tr>
				         			<th>NAME</th>
				         			<th>DEPARTMENT</th>
				         			<th>ORDER #</th>
				         			<th>ARTICLE #</th>
				         			<th>PRODUCT</th>
				         			<th>COLOR</th>
				         			<th>SIZE</th>
				         			<th>QTY</th>
				         			<th>RATE<br>(RS)</th>
				         			<th>AMOUNT<br>(RS)</th>
				         			<th>TOTAL PAYABLE<br>(RS)</th>
				         			<th>DEDUCTION OF ADVACNE<br>(RS)</th>
				         			<th>DEDUCTION OF POCKET<br>(RS)</th>
				         			<th>NET PAYABLE<br>(RS)</th>
				         		</tr>
				         	</thead>';

				         	$grouped_order_receive_list = $order_receive_list->groupBy('orderDetail.user_id');

				         	$total_bonus   = 0;
				         	$total_amount  = 0;
				         	$total_advance = 0;
				         	$total_pocket  = 0;



				         	foreach ($grouped_order_receive_list as $group_rows) {

				         		$flag = true;

				         		$user_id = $group_rows->pluck('orderDetail.user_id')->unique()->first();

				         		$advance = 0;
				         		$pocket  = 0;


				         		if($user_id)
				         		{
				         			$advance = $payment_history->getDeducationAmount($user_id,['Advance Amount'],['Paid','Unpaid']);

				         			$pocket = $payment_history->getDeducationAmount($user_id,['Pocket Money'],['Paid','Unpaid']);

				         			$total_advance += $advance;
				         			$total_pocket += $pocket;

				         		}

				         		$count = @count($group_rows);

				         		foreach ($group_rows as $key => $rows) {

				         			

					         		$order_detail 		= $rows->orderDetail;
					         		$production   		= $order_detail->production;
					         		$product_variation  = $order_detail->productVariation;
					         		$product            = $product_variation->product;

					         		$html.='<tr>';

					         		if($flag)
					         		{
					         			
					         			$html.='<td rowspan="'.@count($group_rows).'">'.$order_detail->user->name.'</td>
					         			<td rowspan="'.@count($group_rows).'">'.$production->department->name.'</td>';
					         		}
					         		
					         		
					         			$html.='<td>'.$production->order->order_no.'</td>
					         			<td>'.$product->article_no.'</td>
					         			<td>'.$product->name.'</td>
					         			<td>'.$product_variation->color.'</td>
					         			<td>'.$product_variation->size.'</td>
					         			<td>'.$rows->quantity.'</td>
					         			<td>'.FL::numberFormat($order_detail->rate).'</td>
					         			<td>'.FL::numberFormat($rows->production_cost).'</td>';


					         			if($flag)
						         		{
						         			$net = (($group_rows->sum('production_cost')) - ($advance + $pocket));
						         			$html.='<td rowspan="'.@count($group_rows).'">'.FL::numberFormat(( $group_rows->sum('production_cost'))).'</td>
						         			<td rowspan="'.@count($group_rows).'">'.FL::numberFormat($advance).'</td>
						         			<td rowspan="'.@count($group_rows).'">'.FL::numberFormat($pocket).'</td>
						         			<td rowspan="'.@count($group_rows).'">'.FL::numberFormat($net).'</td>';	

						         			$total_amount += ($group_rows->sum('production_cost') ); 
						         		}

					         							         			

					         			// $payment = $rows->paymentHistory->payment; 

					         			// if($payment->status == "Paid")
					         			// {
					         			// 	$html.='Paid <br>'.FL::dateFormat($payment->date);
					         			// }
					         			// else
					         			// {
					         			// 	$html.='Unpaid';
					         			// }
					         			

					         		$html.='</tr>';

					         		$flag= false;
				         		}

				         		$html.='<tr><td style="border: 1px solid #fff;"  colspan="14">&nbsp;</td></tr>';


				         		
				         	}

				         $html.='<tr style="font-weight:bolder; background:#000; color:#fff;">
				         <td colspan="10">GRAND TOTAL</td>
				         <td>RS<br>'.FL::numberFormat($total_amount).'</td>
				         <td>RS<br>'.FL::numberFormat($total_advance).'</td>
				         <td>RS<br>'.FL::numberFormat($total_pocket).'</td>
				         <td>RS<br>'.FL::numberFormat($total_amount - ($total_advance + $total_pocket)).'</td>



				         </tr>';	

				         	




				         $html.='</table>';


			// PDF
			if($request->format == 0)
			{
				echo $html;
				exit;
				// $pdf = PDF::loadHtml($html);

    //     		return $pdf->setOptions(['defaultFont' => 'Helvetica','isRemoteEnabled'=>true])->setPaper('a4', 'landscape')->stream($report_title.'.pdf');
			}
			else // EXCEL
			{
				header("Content-Type: application/xls");

                header("Content-Disposition: attachment; filename=".$report_title.".xls");

                echo $html;

                return '';
			}



		}
		catch (DecryptException $e) {
		    //
		}

		return redirect('reports');
	}



	public function getProductionOverDueReport(Request $request)
	{
		try
		{
			$report_title = '<b><u>PRODUCTION OVER DUE REPORT</u></b>';

			$condition = [];

			$condition['order_id']       = decrypt($request->order);

			$order_no = '';

			if($condition['order_id'] > 0)
			{
				$order = $this->order->getOrderById($condition['order_id']);

				if(isset($order->id))
				{
					$order_no = $order->order_no;
				}
			}

			$payment_history = new PaymentHistory;

			$order_detail = new OrderDetail;

			$condition = $this->filterConditionalArray($condition);

			$order_detail_list = $order_detail->getOrderDetailByOrder(false,false,$condition[0]);

			$html = '<!DOCTYPE html>
				    <html>
				        <head>
				            <title>'.$report_title.'</title>

				            <style>
				            *{
				                margin:0px;
				            }
				            .left-td{
				                text-align:left !important;
				            }
				            .h4-margin{
				            	margin-top:5px;
				            	margin-bottom:5px;

				            }
				            </style>
				        </head>

				        <body style="margin:5%;">

				        <div style="float:left">
				        	<h2>'.$report_title.'</h2>
				        	<h4>Order No: '.$order_no.'</h4>


				        </div>

				        <div style="float:right">
				        	<img src="'.asset('/graphics/logo.png').'">
				        	<p><b>Company: </b>MOTOLUX INDUSTRIES</p>
				        	<p><b>Address: </b>  <span style="  text-transform: uppercase;">Muzzafarpur, Ugoki Road, Sialkot, Pakistan</span></p>

				        </div>


				         <table  border="2" style="font-size:12px; width: 100%; border-collapse: collapse; clear: both; text-align:left; margin-top:30px; text-align:center; border: 1px solid #000;" >
				         	<thead style="background:#000; color:#FFF;">
				         		<tr>
				         			<th>PRODUCT</th>
				         			<th>ARTICLE #</th>
				         			<th>COLOR</th>
				         			<th>SIZE</th>
				         			<th>QUANTITY</th>
				         			<th>DEPARTMENT</th>
				         			<th>LABOUR NAME</th>
				         			<th>ISSUED QUANTITY</th>
				         			<th>ISSUED DATE</th>
				         			<th>DUE DATE</th>
				         			<th>RECEIVED  QUANTITY</th>
				         			<th>BALANCE  QUANTITY</th>
				         		</tr>
				         	</thead>';

				         	$grouped_order_detail_list = $order_detail_list->groupBy('production.department_id');

				         	$total_issue_quantity     = 0;
				         	$total_received_quantity  = 0;
				         	$total_quantity     	  = 0;



				         	foreach ($grouped_order_detail_list as $group_rows) {

				         		$flag = true;

				         		$issue_quantity     = 0;
					         	$received_quantity  = 0;
					         	$quantity     	    = 0;


				         		$count = @count($group_rows);

				         		foreach ($group_rows as $key => $rows) {

				       
					         		$order_detail 		= $rows;
					         		$production   		= $order_detail->production;
					         		$product_variation  = $order_detail->productVariation;
					         		$product            = $product_variation->product;

					         		$html.='<tr>';

					         		
					         		
					         		
					         			$html.='<td>'.$product->name.'</td>
					         			<td>'.$product->article_no.'</td>
					         			
					         			<td>'.$product_variation->color.'</td>
					         			<td>'.$product_variation->size.'</td>
					         			<td>'.$product_variation->quantity.'</td>';

					         		// 	if($flag)
						         	// 	{
						         			
						         	// 		$html.='<td rowspan="'.@count($group_rows).'">'.$order_detail->production->department->name.'</td>';
						         	// 	}
						         	
						         		$html.='<td>'.$order_detail->production->department->name.'</td>';


						         		$received_qty = $order_detail->orderReceive->sum('quantity');

									    $total_issue_quantity     +=  $order_detail->quantity;
							         	$total_received_quantity  +=  $received_qty;
							         	$total_quantity     	    +=  $product_variation->quantity;

							         	// $issue_quantity     +=  $order_detail->quantity;
							         	// $received_quantity  +=  $received_qty;
							         	// $quantity     	    +=  $product_variation->quantity;



							         	// $total_issue_quantity     +=  $issue_quantity;
							         	// $total_received_quantity  +=  $received_quantity;
							         	// $total_quantity           +=  $quantity;

						         		$html.='<td>'.$order_detail->user->name.'</td>
					         			<td>'.$order_detail->quantity.'</td>
					         			<td>'.FL::dateFormat($production->created_at).'</td>
					         			<td>'.FL::dateFormat($order_detail->receive_date).'</td>

					         			<td>'.($received_qty).'</td>
					         			<td>'.($order_detail->quantity - $received_qty).'</td>';


					         			 	  



					
					         		$html.='</tr>';

					         		$flag= false;
				         		}

				         		$html.='<tr><td style="border: 1px solid #fff;"  colspan="12">&nbsp;</td></tr>';


				         		
				         	}

				         $html.='<tr style="font-weight:bolder; background:#000; color:#fff;">
				         <td colspan="4">GRAND TOTAL</td>
				         <td>'.($total_quantity).'</td>
				         <td colspan="2"></td>
				         <td>'.($total_issue_quantity).'</td>
				         <td colspan="2"></td>
				         <td>'.($total_received_quantity).'</td>
				         <td>'.($total_issue_quantity - $total_received_quantity).'</td>



				         </tr>';	

				         	




				         $html.='</table>';



			// PDF
			if($request->format == 0)
			{
				$pdf = PDF::loadHtml($html);

        		return $pdf->setOptions(['defaultFont' => 'Helvetica','isRemoteEnabled'=>true])->setPaper('a4', 'landscape')->stream($report_title.'.pdf');
			}
			else // EXCEL
			{
				header("Content-Type: application/xls");

                header("Content-Disposition: attachment; filename=".$report_title.".xls");

                echo $html;

                return '';
			}



		}
		catch (DecryptException $e) {
		    //
		}

		return redirect('reports');
	}


	public function getProductionStatusReport(Request $request)
	{
		try
		{
			$report_title = '<b><u>PRODUCTION STATUS REPORT</u></b>';

			$condition = [];

			$condition['order_id'] = decrypt($request->order);

			$order_no = '';

			if($condition['order_id'] > 0)
			{
				$order = $this->order->getOrderById($condition['order_id']);

				if(isset($order->id))
				{
					$order_no = $order->order_no;
				}
			}

			$payment_history = new PaymentHistory;

			$order_detail = new OrderDetail;

			$condition = $this->filterConditionalArray($condition);

			$order_detail_list = $order_detail->getOrderDetailByOrder(false,false,$condition[0]);

			$html = '<!DOCTYPE html>
				    <html>
				        <head>
				            <title>'.$report_title.'</title>

				            <style>
				            *{
				                margin:0px;
				            }
				            .left-td{
				                text-align:left !important;
				            }
				            .h4-margin{
				            	margin-top:5px;
				            	margin-bottom:5px;

				            }
				            </style>
				        </head>

				        <body style="margin:5%;">

				        <div style="float:left">
				        	<h2>'.$report_title.'</h2>
				        	<h4>Order No: '.$order_no.'</h4>


				        </div>

				        <div style="float:right">
				        	<img src="'.asset('/graphics/logo.png').'">
				        	<p><b>Company: </b>MOTOLUX INDUSTRIES</p>
				        	<p><b>Address: </b>  <span style="  text-transform: uppercase;">Muzzafarpur, Ugoki Road, Sialkot, Pakistan</span></p>

				        </div>


				         <table  border="2" style="font-size:12px; width: 100%; border-collapse: collapse; clear: both; text-align:left; margin-top:30px; text-align:center; border: 1px solid #000;" >
				         	<thead style="background:#000; color:#FFF;">
				         		<tr>
				         			<th>PRODUCT</th>
				         			<th>ARTICLE #</th>
				         			<th>COLOR</th>
				         			<th>SIZE</th>
				         			<th>QUANTITY</th>
				         			<th>DEPARTMENT</th>
				         			<th>LABOUR NAME</th>
				         			<th>ISSUED QUANTITY</th>
				         			<th>RECEIVED  QUANTITY</th>
				         			<th>BALANCE  QUANTITY</th>
				         		</tr>
				         	</thead>';

				         	$grouped_order_detail_list = $order_detail_list->groupBy('production.department_id');

				         	$total_issue_quantity     = 0;
				         	$total_received_quantity  = 0;
				         	$total_quantity     	  = 0;



				         	foreach ($grouped_order_detail_list as $group_rows) {

				         		$flag = true;

				         		$issue_quantity     = 0;
					         	$received_quantity  = 0;
					         	$quantity     	    = 0;


				         		$count = @count($group_rows);

				         		foreach ($group_rows as $key => $rows) {

				         			

					         		$order_detail 		= $rows;
					         		$production   		= $order_detail->production;
					         		$product_variation  = $order_detail->productVariation;
					         		$product            = $product_variation->product;

					         		$html.='<tr>';

					         		
					         		
					         		
					         			$html.='<td>'.$product->name.'</td>
					         			<td>'.$product->article_no.'</td>
					         			
					         			<td>'.$product_variation->color.'</td>
					         			<td>'.$product_variation->size.'</td>
					         			<td>'.$product_variation->quantity.'</td>';

					         		// 	if($flag)
						         	// 	{
						         			
						         	// 		$html.='<td rowspan="'.@count($group_rows).'">'.$order_detail->production->department->name.'</td>';
						         	// 	}
						         	
						         				$html.='<td >'.$order_detail->production->department->name.'</td>';


						         		$received_qty = $order_detail->orderReceive->sum('quantity');


						         		$html.='<td>'.$order_detail->user->name.'</td>
					         			<td>'.$order_detail->quantity.'</td>
					         			

					         			<td>'.($received_qty).'</td>
					         			<td>'.($order_detail->quantity - $received_qty).'</td>';


					         			// $issue_quantity     +=  $order_detail->quantity;
							         	// $received_quantity  +=  $received_qty;
							         	// $quantity     	    +=  $product_variation->quantity;


							         	// $total_issue_quantity     +=  $issue_quantity;
							         	// $total_received_quantity  +=  $received_quantity;
							         	// $total_quantity     	  +=  $quantity;

							         		    $total_issue_quantity     +=  $order_detail->quantity;
							         	$total_received_quantity  +=  $received_qty;
							         	$total_quantity     	    +=  $product_variation->quantity;



					
					         		$html.='</tr>';

					         		$flag= false;
				         		}

				         		$html.='<tr><td style="border: 1px solid #fff;"  colspan="10">&nbsp;</td></tr>';


				         		
				         	}

				         $html.='<tr style="font-weight:bolder; background:#000; color:#fff;">
				         <td colspan="4">GRAND TOTAL</td>
				         <td>'.($total_quantity).'</td>
				         <td colspan="2"></td>
				         <td>'.($total_issue_quantity).'</td>
				         <td>'.($total_received_quantity).'</td>
				         <td>'.($total_issue_quantity - $total_received_quantity).'</td>



				         </tr>';	

				         	




				         $html.='</table>';


			// PDF
			if($request->format == 0)
			{
				$pdf = PDF::loadHtml($html);

        		return $pdf->setOptions(['defaultFont' => 'Helvetica','isRemoteEnabled'=>true])->setPaper('a4', 'landscape')->stream($report_title.'.pdf');
			}
			else // EXCEL
			{
				header("Content-Type: application/xls");

                header("Content-Disposition: attachment; filename=".$report_title.".xls");

                echo $html;

                return '';
			}



		}
		catch (DecryptException $e) {
		    //
		}

		return redirect('reports');
	}

    public function generateInvoice($id)
    {
    	try{

    		$payment = new Payment;

    		$payment = $payment->getPaymentById(decrypt($id));

    		if(isset($payment->id))
    		{
    			$order_receive = new OrderReceive;

    			$order_receive_list = $order_receive->getOrderReceiveByIds($payment->paymentHistories->pluck('order_receive_id')->toArray());



    			if(@count($order_receive_list) > 0)
    			{


	    			$html = '<!DOCTYPE html>
				    <html>
				        <head>
				            <title>INV-'.FL::generateNumber($payment->id).'</title>

				            <style>
				            *{
				                margin:0px;
				            }
				            .left-td{
				                text-align:left !important;
				            }
				            .h4-margin{
				            	margin-top:5px;
				            	margin-bottom:5px;

				            }
				            </style>
				        </head>

				        <body style="margin:5%;">

				         <table border="2" style="font-size:12px; width: 40%; border-collapse: collapse; clear: both; text-align:left; margin-top:30px; float:left;" >
					         <thead>
						         
						         <tr>
					         		<td style="background:#000; color:#fff;  font-weight:bolder;">&nbsp;&nbsp;Payment Slip Number:</td>
					         		<td>&nbsp;'.FL::generateNumber($payment->id).'</td>
					         	</tr>
					         	<tr>
					         		<td style="background:#000; color:#fff;  font-weight:bolder;">&nbsp;&nbsp;Name:</td>
					         		<td>&nbsp;'.$order_receive_list->first()->orderDetail->user->name.'</td>
					         	</tr>

					         	<tr>
					         		<td style="background:#000; color:#fff;  font-weight:bolder;">&nbsp;&nbsp;Department:</td>
					         		<td>&nbsp;'.$order_receive_list->first()->production->department->name.'</td>
					         	</tr>

					         	<tr>
					         		<td style="background:#000; color:#fff;  font-weight:bolder;">&nbsp;&nbsp;Payment Reference Date(s):</td>
					         		<td>&nbsp;'.FL::dateFormat($order_receive_list->first()->date).' - '.FL::dateFormat($order_receive_list->last()->date).'</td>
					         	</tr>

					         	<tr>
					         		<td style="background:#000; color:#fff; font-weight:bolder;">&nbsp;&nbsp;Print Date:</td>
					         		<td>&nbsp;'.FL::dateFormatWithTime($payment->date).'</td>
					         	</tr>
					         </thead>
				         </table>

				         <div style="float:right">
				        	<img src="'.asset('/graphics/logo.png').'">
				        	<p><b>Company: </b>MOTOLUX INDUSTRIES</p>
				        	<p><b>Address: </b>  <span style="  text-transform: uppercase;">Muzzafarpur, Ugoki Road, </span></p>
				        	<p><b>Address: </b>  <span style="  text-transform: uppercase;">Sialkot, Pakistan</span></p>


				        </div>



				         <div class="col-sm-12 col-md-12 col-lg-12 mt-3 mb-2">
				         <br><br>
			                <table border="2" style="font-size:12px; width: 100%; border-collapse: collapse; clear: both; text-align:center; margin-top:30px;" >
			                  <thead style="background:#000; color:#fff;">
			                  <tr>
						         <td style="text-align:center; font-weight:bolder; height:25px;" colspan="7">WORK DETAIL</td>
						         </tr>
			                    <tr>
			                      <th>DATE</th>
			                      <th>PRODUCT</th>
			                      <th>COLOR</th>
			                      <th>SIZE</th>
			                      <th>QTY</th>

			                      <th>RATE (RS) </th>
			                      <th>PAYABLE (RS)</th>
			                    </tr>
			                  </thead>
			                  <tbody>';

			                    foreach($order_receive_list as $rows)
			                    {
			                    	$html.= '<tr>
				                      <td>'.FL::dateFormatWithTime($rows->date).'</td>
				                    <td>'.$rows->orderDetail->productVariation->product->name.'</td>
				                    <td>'.$rows->orderDetail->productVariation->size.'</td>
				                    <td>'.$rows->orderDetail->productVariation->color.'</td>
				                    <td>'.$rows->quantity.'</td>

				                    <td>'.$rows->orderDetail->rate.'</td>
				                    <td>'.FL::numberFOrmat($rows->production_cost).'</td>
				                    </tr>';
			                    }

			                    
			                  $html.='</tbody>
			                  <tfoot style="font-weight: bold;">
			                    <tr>
			                    <td colspan="6" style="background:#000; color:#fff;text-align: right;">TOTAL AMOUNT&nbsp;&nbsp;</td>
			                    <td id="total_cost">RS: '.FL::numberFormat($order_receive_list->sum('production_cost') ).'</td>
			                  </tr>';

			                   $advance = $payment->paymentHistories->whereIn('payment_type',['Advance Amount'])->sum('credit');

			                   $pocket = $payment->paymentHistories->whereIn('payment_type',['Pocket Money'])->sum('credit');

			                    $html.='<tr>
			                      <td colspan="6" style="background:#000; color:#fff;text-align: right;">DEDUCTION OF ADVANCE MONEY (-)&nbsp;&nbsp;</td>
			                      <td>RS: '.FL::numberFormat($advance).'</td>
			                    </tr>


			                      <tr>
			                      <td colspan="6" style="background:#000; color:#fff;text-align: right;">DEDUCTION OF POCKET MONEY (-)&nbsp;&nbsp; </td>

			                      <td>RS: '.FL::numberFormat($pocket).'</td>
			                    </tr>';


			                     foreach($payment->paymentHistories->whereIn('payment_type','Advance Production Amount') as $rows)
			                     {
			                     	$new_order_detail = $rows->orderDetail;

                      					 $html.='<tr>
			                      			<td colspan="6" style="background:#000; color:#fff;text-align: right;">DEDUCTION OF ADVANCE ORDER NO. '.$new_order_detail->production->order->order_no.' | '.$new_order_detail->productVariation->product->name.' | '.$new_order_detail->productVariation->size.' | '.$new_order_detail->productVariation->color.' (-)&nbsp;&nbsp; </td>
			                     			 <td>RS: '.FL::numberFormat($rows->credit).'</td>
			                    			</tr>';
			                     }




			                   $html.='<tr>
			                    <td colspan="6" style="background:#000; color:#fff;text-align: right;">NET PAYABLE AMOUNT &nbsp;&nbsp;</td>

			                    <td >
			                      RS: '.FL::numberFormat($payment->amount).'
			                    </td>
			                  </tr>
			                  </tfoot>
			                </table>
		                </div>&nbsp;&nbsp;&nbsp;&nbsp;

			              	<div style="padding-top:10%;">
			               <p style="float:left; ">Payment Received By: ______________________</p>
			               <p style="float:right; ">Signature: ______________________</p>
			               <br><br>
			               </div>





		                </div>



				        </body>
				    </html>';
    			}
    			else
    			{
    				$html = $this->generateAdvanceReceipt($payment);
    			}


			     $pdf = PDF::loadHtml($html);

        		return $pdf->setOptions(['defaultFont' => 'Helvetica','isRemoteEnabled'=>true])->setPaper('a4', 'portrait')->stream('Invoice.pdf');
    		}


    	} 
    	catch (DecryptException $e) {
		    //
		}

		return redirect('payments');
    }


  public function generatelabourInvoice($id)
    {
    	try{

    		$payment = new Payment;

    		$payment = $payment->getPaymentById(decrypt($id));

    		if(isset($payment->id))
    		{
    			$order_receive = new OrderReceive;

    			$order_receive_list = $order_receive->getOrderReceiveByIds($payment->paymentHistories->pluck('order_receive_id')->toArray());



    			if(@count($order_receive_list) > 0)
    			{


	    			$html = '<!DOCTYPE html>
				    <html>
				        <head>
				            <title>INV-'.FL::generateNumber($payment->id).'</title>

				            <style>
				            *{
				                margin:0px;
				            }
				            .left-td{
				                text-align:left !important;
				            }
				            .h4-margin{
				            	margin-top:5px;
				            	margin-bottom:5px;

				            }
				            </style>
				        </head>

				        <body style="margin:5%;">

				         <table border="2" style="font-size:11px; width: 100%; border-collapse: collapse; clear: both; text-align:left; margin-top:30px; " >
					         <thead>
						         
						         <tr>
					         		<td>&nbsp;Payment Slip #</td>
					         		<td>&nbsp;'.FL::generateNumber($payment->id).'</td>
					         	</tr>
					         	<tr>
					         		<td>&nbsp;Name</td>
					         		<td>&nbsp;'.$order_receive_list->first()->orderDetail->user->name.'</td>
					         	</tr>

					         	<tr>
					         		<td>&nbsp;Department</td>
					         		<td>&nbsp;'.$order_receive_list->first()->production->department->name.'</td>
					         	</tr>

					         	<tr>
					         		<td>&nbsp;Reference Date(s)</td>
					         		<td>&nbsp;From '.FL::dateFormat($order_receive_list->first()->date).' <br>&nbsp;To&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'.FL::dateFormat($order_receive_list->last()->date).'</td>
					         	</tr>

					         	<tr>
					         		<td>&nbsp;Print Date</td>
					         		<td>&nbsp;'.FL::dateFormatWithTime($payment->date).'&nbsp;</td>
					         	</tr>
					         </thead>
				         </table>


				         <table border="2" style="font-size:11px; width: 100%; border-collapse: collapse; clear: both; text-align:left; margin-top:30px; " >
					         <thead>

					         	 <tr>
					         		<td colspan="2" style="  font-weight:bolder; text-align:center; background:#000; color:#ffffff;">ADVANCE AMOUNT DETAIL</td>
					         							         	</tr>
						         
						         <tr>
					         		<td>&nbsp;Previous Amount</td>
					         		<td>&nbsp;Rs: '.FL::numberFormat($payment->previous_advance).'</td>
					         	</tr>
					         	<tr>
					         		<td>&nbsp;Deduction Amount (-)</td>
					         		<td>&nbsp;Rs: '.FL::numberFormat($payment->current_advance).'</td>
					         	</tr>
								
					         	<tr>
					         		<td>&nbsp;Balance Amount</td>
					         		<td>&nbsp;Rs: '.FL::numberFormat($payment->balance_advance).'</td>
					         	</tr>

					        
					         </thead>
				         </table>

				         <table border="2" style="font-size:11px; width: 100%; border-collapse: collapse; clear: both; text-align:left; margin-top:30px; " >
					         <thead>

					         	 <tr>
					         		<td colspan="2" style="  font-weight:bolder; text-align:center; background:#000; color:#ffffff;">POCKET MONEY DETAIL</td>
					         							         	</tr>
						         
						         <tr>
					         		<td>&nbsp;Previous Amount</td>
					         		<td>&nbsp;Rs: '.FL::numberFormat($payment->previous_pocket).'</td>
					         	</tr>
					         	<tr>
					         		<td>&nbsp;Deduction Amount (-)</td>
					         		<td>&nbsp;Rs: '.FL::numberFormat($payment->current_pocket).'</td>
					         	</tr>

					         	<tr>
					         		<td>&nbsp;Balance Amount</td>
					         		<td>&nbsp;Rs: '.FL::numberFormat($payment->balance_pocket).'</td>
					         	</tr>

					        
					         </thead>
				         </table>

				         


				         <div class="col-sm-12 col-md-12 col-lg-12 mt-3 mb-2">
				         <br><br>
			                <table border="2" style="font-size:11px; width: 100%; border-collapse: collapse; clear: both; text-align:left; margin-top:30px; " >
			                  <thead style="">
			                  	<tr>
			                  		<td colspan="2" style="  font-weight:bolder; text-align:center; background:#000; color:#ffffff;">PAYMENT SUMMERY</td>
			                  	</tr>
			                  </thead>
			                  <tfoot style="font-weight: bold;">
			                    <tr>
			                    <td colspan="1" style="text-align: left;">&nbsp;Total Payable Amount&nbsp;&nbsp;</td>
			                    <td id="total_cost">&nbsp;Rs: '.FL::numberFormat($order_receive_list->sum('production_cost') ).'</td>
			                  </tr>';

			                   $advance = $payment->paymentHistories->whereIn('payment_type',['Advance Amount'])->sum('credit');

			                   $pocket = $payment->paymentHistories->whereIn('payment_type',['Pocket Money'])->sum('credit');

			                    $html.='<tr>
			                      <td colspan="1" style="text-align: left;">&nbsp;Deduction of Advance<br>&nbsp;Amount (-)</td>
			                      <td>&nbsp;Rs: '.FL::numberFormat($advance).'</td>
			                    </tr>


			                      <tr>
			                      <td colspan="1" style="text-align: left;">&nbsp;Deduction of<br>&nbsp;Pocket MoneyAmount (-)</td>

			                      <td>&nbsp;Rs: '.FL::numberFormat($pocket).'</td>
			                    </tr>';


			                     foreach($payment->paymentHistories->whereIn('payment_type','Advance Production Amount') as $rows)
			                     {
			                     	$new_order_detail = $rows->orderDetail;

                      					 $html.='<tr>
			                      			<td colspan="1" style="text-align: left;">&nbsp;Deduction of Advance<br>Received Against<br>Order #'.$new_order_detail->production->order->order_no.' | '.$new_order_detail->productVariation->product->name.' | '.$new_order_detail->productVariation->size.' | '.$new_order_detail->productVariation->color.' (-)&nbsp;&nbsp; </td>
			                     			 <td>&nbsp;Rs: '.FL::numberFormat($rows->credit).'</td>
			                    			</tr>';
			                     }




			                   $html.='<tr>
			                    <td colspan="1" style="text-align: left;">&nbsp;Net Payable Amount &nbsp;&nbsp;</td>

			                    <td >
			                      &nbsp;Rs: '.FL::numberFormat($payment->amount).'
			                    </td>
			                  </tr>
			                  </tfoot>
			                </table>
		                </div>
			               </div>





		                </div>



				        </body>
				    </html>';
    			}
    			else
    			{
    				$html = $this->generateAdvanceLabourReceipt($payment);
    			}


			     $pdf = PDF::loadHtml($html);

			     $custom_paper = array(0,0,204,650);

        		return $pdf->setOptions(['defaultFont' => 'Helvetica','isRemoteEnabled'=>true])->setPaper($custom_paper)->stream('Invoice.pdf');
    		}


    	} 
    	catch (DecryptException $e) {
		    //
		}

		return redirect('payments');
    }



    public function generateAdvanceLabourReceipt($payment)
    {
    	      $order_detail = new OrderDetail;

    						$order_detail_list = $order_detail->getOrderDetailByIds($payment->paymentHistories->pluck('order_detail_id')->toArray());

		$html = '<!DOCTYPE html>
				    <html>
				        <head>
				            <title>INV-'.FL::generateNumber($payment->id).'</title>

				            <style>
				            *{
				                margin:0px;
				            }
				            .left-td{
				                text-align:left !important;
				            }
				            .h4-margin{
				            	margin-top:5px;
				            	margin-bottom:5px;

				            }
				            </style>
				        </head>

				        <body style="margin:5%;">


				         <table border="1" style="font-size:12px; width: 100%; border-collapse: collapse; clear: both; text-align:left; margin-top:30px; " >
					         <thead>
						         
						         <tr>
					         		<td>&nbsp;&nbsp;Payment Slip #</td>
					         		<td>&nbsp;'.FL::generateNumber($payment->id).'</td>
					         	</tr>
					         	<tr>
					         		<td>&nbsp;&nbsp;Name</td>
					         		<td>&nbsp;'.$order_detail_list->first()->user->name.'</td>
					         	</tr>

					         	<tr>
					         		<td>&nbsp;&nbsp;Department</td>
					         		<td>&nbsp;'.$order_detail_list->first()->production->department->name.'</td>
					         	</tr>

					         	<tr>
					         		<td>&nbsp;&nbsp;Payment Reference<br>&nbsp;&nbsp;Date(s)</td>
					     			<td>&nbsp;'.FL::dateFormatWithTime($payment->date).'</td>

					         	</tr>

					         	<tr>
					         		<td>&nbsp;&nbsp;Print Date</td>
					         		<td>&nbsp;'.FL::dateFormatWithTime($payment->date).'</td>
					         	</tr>
					         </thead>
					         <tfoot style="font-weight: bold;">
			                  

			                   <tr>
			                    <td colspan="1" style="background:#000; color:#fff;text-align: left;">&nbsp;NET PAYABLE<br>&nbsp;AMOUNT &nbsp;&nbsp;</td>

			                    <td colspan="1" style="background:#000; color:#fff;text-align: left;">
			                     &nbsp;&nbsp;Rs: '.FL::numberFormat($payment->amount).'
			                    </td>
			                  </tr>
			                  </tfoot>
				         </table>

			                  
		                </div>';





			               return $html;
    }

    public function generateAdvanceReceipt($payment)
    {


			                  $order_detail = new OrderDetail;

    						$order_detail_list = $order_detail->getOrderDetailByIds($payment->paymentHistories->pluck('order_detail_id')->toArray());
    	$html = '<!DOCTYPE html>
				    <html>
				        <head>
				            <title>INV-'.FL::generateNumber($payment->id).'</title>

				            <style>
				            *{
				                margin:0px;
				            }
				            .left-td{
				                text-align:left !important;
				            }
				            .h4-margin{
				            	margin-top:5px;
				            	margin-bottom:5px;

				            }
				            </style>
				        </head>

				        <body style="margin:5%;">

				         <table border="2" style="font-size:12px; width: 40%; border-collapse: collapse; clear: both; text-align:left; margin-top:30px; float:left;" >
					         <thead>
						         
						         <tr>
					         		<td style="background:#000; color:#fff;  font-weight:bolder;">&nbsp;&nbsp;Payment Slip Number:</td>
					         		<td>&nbsp;'.FL::generateNumber($payment->id).'</td>
					         	</tr>
					         	<tr>
					         		<td style="background:#000; color:#fff;  font-weight:bolder;">&nbsp;&nbsp;Name:</td>
					         		<td>&nbsp;'.$order_detail_list->first()->user->name.'</td>
					         	</tr>

					         	<tr>
					         		<td style="background:#000; color:#fff;  font-weight:bolder;">&nbsp;&nbsp;Department:</td>
					         		<td>&nbsp;'.$order_detail_list->first()->production->department->name.'</td>
					         	</tr>

					       

					         	<tr>
					         		<td style="background:#000; color:#fff; font-weight:bolder;">&nbsp;&nbsp;Print Date:</td>
					         		<td>&nbsp;'.FL::dateFormatWithTime($payment->date).'</td>
					         	</tr>
					         </thead>
				         </table>

				         <div style="float:right">
				        	<img src="'.asset('/graphics/logo.png').'">
				        	<p><b>Company: </b>MOTOLUX INDUSTRIES</p>
				        	<p><b>Address: </b>  <span style="  text-transform: uppercase;">Muzzafarpur, Ugoki Road, </span></p>
				        	<p><b>Address: </b>  <span style="  text-transform: uppercase;">Sialkot, Pakistan</span></p>


				        </div>



				         <div class="col-sm-12 col-md-12 col-lg-12 mt-3 mb-2">
				         <br><br>
			                <table border="2" style="font-size:12px; width: 100%; border-collapse: collapse; clear: both; text-align:center; margin-top:30px;" >
			                  <thead style="background:#000; color:#fff;">
			                  <tr>
						         <td style="text-align:center; font-weight:bolder; height:25px;" colspan="7">WORK DETAIL</td>
						         </tr>
			                    <tr>
			                      <th>DATE</th>
			                      <th>ORDER #</th>

			                      <th>PRODUCT</th>
			                      <th>COLOR</th>
			                      <th>SIZE</th>
			                      <th>QTY</th>

			                      <th>AMOUNT </th>
			                    </tr>
			                  </thead>
			                  <tbody>';


			                    foreach($order_detail_list as $rows)
			                    {
			                    	$html.= '<tr>
				                      <td>'.FL::dateFormatWithTime($rows->date).'</td>
				                    <td>'.$rows->production->order->order_no.'</td>

				                    <td>'.$rows->productVariation->product->name.'</td>
				                    <td>'.$rows->productVariation->size.'</td>
				                    <td>'.$rows->productVariation->color.'</td>
				                    <td>'.$rows->quantity.'</td>

				                    <td>'.FL::numberFOrmat($payment->amount).'</td>
				                    </tr>';
			                    }

			                    
			                  $html.='</tbody>
			                  <tfoot style="font-weight: bold;">
			                  

			                   <tr>
			                    <td colspan="6" style="background:#000; color:#fff;text-align: right;">NET PAYABLE AMOUNT &nbsp;&nbsp;</td>

			                    <td >
			                      RS: '.FL::numberFormat($payment->amount).'
			                    </td>
			                  </tr>
			                  </tfoot>
			                </table>
		                </div>&nbsp;&nbsp;&nbsp;&nbsp;

			              	<div style="padding-top:10%;">
			               <p style="float:left; ">Payment Received By: ______________________</p>
			               <p style="float:right; ">Signature: ______________________</p>
			               <br><br>
			               </div>';



			               



			               return $html;
    }
}
