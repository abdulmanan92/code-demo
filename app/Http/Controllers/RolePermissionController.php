<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
Use Illuminate\Validation\Rule;
use FL;

class RolePermissionController extends Controller
{
	public function getValidationList($validation = [])
	{
		$this->flag = 'fail';
		$this->message = 'Required form validations.';

		return $validation + [

			'name' 	=> 'required|unique:roles|string|min:1|max:50',

		];
	}

	public function index()
	{

		return view('roles_permissions.role',[

			'role_list' =>  Role::whereNotIn('id',['1'])->orderBy('id','ASC')->get(),
		]);
		
	}



    public function storeRole(Request $request)
    {
    	$request->validate($this->getValidationList());

    	$role = Role::create([
			'name' => $request->name,
		]);


		return redirect()->back();
    }

    public function assignPermission(Request $request)
    {
    	try{

    		$role = Role::find(decrypt($request->role));

    		if(isset($role->id))
            {
            	if(isset($request->permissions) && @count($request->permissions) > 0)
            	{
            		$role->syncPermissions($request->permissions);
            	}
            }
    	}
    	catch (DecryptException $e)
        {

        }

        return redirect()->back();
    }

    public function showPermissionByRole(Request $request)
    {
    	try{

    		$role = Role::find(decrypt($request->id));

    		if(isset($role->id))
            {
            	return view('roles_permissions.assign_permission',[

            		'role'            => $role,
            		'module_list' 	  => FL::getModuleList(),
            		'permission_list' => Permission::whereNotIn('id',[1])->get(),
            	]);
            }
    	}
    	catch (DecryptException $e)
        {

        }

        return redirect()->back();
    }

    public function updateRole(Request $request)
    {
    	try{

    		if($request->isMethod('Post'))
    		{
    			$role = decrypt($request->role);

				$request->validate($this->getValidationList([
	                'name' => [
	                    'required',Rule::unique('roles')->ignore(decrypt($request->role)),
	                    ],
	            ]));

	            $role = Role::find($role);

	            if(isset($role->id))
	            {
	            	$role->name = $request->name;

	            	$role->update();
	            }
    		}
    		else
    		{
    			$role = Role::find(decrypt($request->id));

	            if(isset($role->id))
	            {
	            	

	            	return view('roles_permissions.modals.update_role',[
	            		'role' => $role
	            	]);
	            }


	            return '';
    		}

            return redirect()->back();


    	}
    	catch (DecryptException $e)
        {

        }
    }

    public function removeRole($id)
    {
    	try{
    		$id = decrypt($id);

			$role = Role::find($id);

            if(isset($role->id))
            {
            	$role->delete();
            }
		}
        catch (DecryptException $e)
        {

        }

        return redirect()->back();

    }
}
