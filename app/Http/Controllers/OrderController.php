<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Contracts\Encryption\DecryptException;
use App\Product;
use App\Production;

use App\ProductVariation;

use App\Order;
use App\Department;
use Illuminate\Validation\Rule;

class OrderController extends Controller
{
    private $order;
	private $message;
	private $status;
	private $product_variant;
	private $production;

	public function __construct()
    {
    	$this->order 		     = new Order;
    	$this->department 		 = new Department;
    	$this->product 			= new Product;
    	$this->product_variant = new ProductVariation;
    	$this->production = new Production;




    	$this->message    = 'Something is wrong';
    	$this->status     = 'error';

    }

	public function getValidationList($new_array = [])
	{
		return $new_array += [

			'order_no' 		 	  => 'required|min:0|max:255',
			'order_date'	 	  => 'required|date',
			// 'receive_date.*'	   => 'required|date',
		];
	}

	public function index()
	{
		return view('orders.order',[

			'order_list' 		=> $this->order->getAllOrders(),
			'department_list' 	=> $this->department->getAllDepartments(),
			'product_list' 		=> $this->product->getAllProducts(),

		]);
	}

	public function orderDetail($id)
	{
		try{

			$production = $this->production->getProductionById(decrypt($id));

			if(isset($production->id))
			{
				
				return view('orders.modals.order_detail',[

					'production' => $production,

				]);
			}
		}
		catch (DecryptException $e) {
		    //
		}

		return redirect()->back();
	}

	public function showReOrder(Request $request)
	{
		try{

			$order = $this->order->getOrderById(decrypt($request->id));

			if(isset($order->id))
			{
				$order_list = $this->order->getAllOrdersByParent($order->id);

				$selected_department_list = [$order->department_id] + $order_list->pluck('department_id')->toArray();

				return view('orders.reorder',[

					'order_list' 		=> $order_list,

					'department_list' 	=> $this->department->getAllDepartments()->whereNotIn('id',$selected_department_list),

					'product_list' 		=> $this->product->getAllProducts(),

					'order'				=> $order,

				]);
			}
		}
		catch (DecryptException $e) {
		    //
		}

		return redirect()->back();
	}

    public function storeOrder(Request $request)
    {
		try 
		{
			
			$request->validate($this->getValidationList());

			$collect_data = $request->input();

			// if(isset($collect_data['parent']))
			// {
			// 	$collect_data['parent'] = decrypt($collect_data['parent']);
			// }

			// if(isset($collect_data['product']))
			// {
			// 	$collect_data['product'] = decrypt($collect_data['product']);
			// }

			// $collect_data['department'] = decrypt($collect_data['department']);

			


			if(isset($collect_data['product']) && @count($collect_data['product']) > 0)
			{
				foreach ($collect_data['product'] as $key => $value) {

					$collect_data['product'][$key] = decrypt($value);
				}
			}
			else{
				$collect_data['product'] = [];
			}

			$order = $this->order->storeOrder($collect_data);

			if(isset($order->id))
			{
				$this->message = 'Order has added succssfully.';
				$this->status  = 'success';
			}


		} catch (DecryptException $e) {
		    //
		}


		return redirect()->back()->with([

			'status'  => $this->status,
			'message' => $this->message,
		]);
    }


    public function updateOrder(Request $request)
    {
		try 
		{
			if($request->isMethod('Post'))
			{
				$request->validate($this->getValidationList());

				$collect_data = $request->input();

				$collect_data['order'] = decrypt($collect_data['order']);

				// if(isset($collect_data['parent']))
				// {
				// 	$collect_data['parent'] = decrypt($collect_data['parent']);
				// }

				// if(isset($collect_data['product']))
				// {
				// 	$collect_data['product'] = decrypt($collect_data['product']);
				// }

				// $collect_data['department'] = decrypt($collect_data['department']);

				if(isset($collect_data['product']) && @count($collect_data['product']) > 0)
				{
					foreach ($collect_data['product'] as $key => $value) {

						$collect_data['product'][$key] = decrypt($value);
					}
				}
				else{
					$collect_data['product'] = [];
				}

				if(isset($collect_data['product_variant']) && @count($collect_data['product_variant']) > 0)
				{
					foreach ($collect_data['product_variant'] as $key => $value) {

						$collect_data['product_variant'][$key] = decrypt($value);
					}
				}
				else{
					$collect_data['product_variant'] = [];
				}

				


				// if(isset($collect_data['user']) && @count($collect_data['user']) > 0)
				// {
				// 	foreach ($collect_data['user'] as $key => $value) {

				// 		$collect_data['user'][$key] = decrypt($value);
				// 	}
				// }
				

				$order = $this->order->updateOrder($collect_data);

				if(isset($order->id))
				{
					$this->message = 'Order has added succssfully.';
					$this->status  = 'success';
				}
			}
			else
			{
				$id = $request->id;

				$order = $this->order->getOrderById(decrypt($id));

				if(isset($order->id))
				{
					if($order->parent_id > 0)
					{
						$order_list = $this->order->getAllOrdersByParent($order->id);

						$selected_department_list = [$order->department_id] + $order_list->pluck('department_id')->toArray();

						return view('orders.modals.update_order',[

							'order' => $order,

							'department_list' 	=> $this->department->getAllDepartments()->whereNotIn('id',$selected_department_list),

							'product_list' 		=> $this->product->getAllProducts(),

	    				'user_list' => $order->department->users,

						]);
					}
					

					return view('orders.modals.update_order',[

						'order' => $order,
						'department_list' 	=> $this->department->getAllDepartments(),
						'product_list' 		=> $this->product->getAllProducts(),

	    				'user_list' => $order->department->users,

					]);
				}
			}
			


		} catch (DecryptException $e) {
		    //
		}


		return redirect()->back()->with([

			'status'  => $this->status,
			'message' => $this->message,
		]);
    }

    public function removeOrder(Request $request)
    {
		try 
		{

			$order = $this->order->removeOrder(decrypt($request->id));

			if($order)
			{
				$this->message = 'Order has removed succssfully.';
				$this->status  = 'success';
			}


		} catch (DecryptException $e) {
		    //
		}


		return redirect()->back()->with([

			'status'  => $this->status,
			'message' => $this->message,
		]);
    }

    public function loadVariantComponent(Request $request)
    {
    	$view = '';

    	try
    	{
    		if($request->ajax())
	    	{
	    		$department = $this->department->getDepartmentById(decrypt($request->department_id));


	    		if(isset($department->id))
	    		{
	    			$user_list = [];

	    			// if($request->lot == 1)
	    			// {
	    			$user_list = $department->users;
	    			// }

	    			$view = view('productions.components.order_detail_variants',[

	    				'user_list' => $user_list->where('status','Active')->where('type','Team Member'),
	    				'product_variant' =>  decrypt($request->product_variant),
	    				'order_detail' =>  decrypt($request->order_detail),

	    				'lot'				=> $request->lot,
	    				'department'		=> $department->id,


	    			])->render();
	    		}
	    	}

    	} catch (DecryptException $e) {
		    //
		}
    	
    	return $view;
    }

    public function loadProductVariantComponent()
    {
    	$view = '';

    	try
    	{
    		
			$view = view('orders.components.product_variations',[

				'product_list' 		=> $this->product->getAllProducts(),

			])->render();
	    

    	} catch (DecryptException $e) {
		    //
		}
    	
    	return $view;
    }
}
