<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\OrderDetail;
use App\Order;
use App\Department;
use Illuminate\Contracts\Encryption\DecryptException;
use Illuminate\Validation\Rule;
use Crypt;
class OrderDetailController extends Controller
{
    private $order;
    private $department;

	private $message;
	private $status;

	public function __construct()
    {
    	$this->order_detail 		     = new OrderDetail;

    	$this->order 		     = new Order;
    	$this->department 		 = new Department;


    	$this->message    = 'Something is wrong';
    	$this->status     = 'error';

    }


    public function getValidationList($new_array = [])
    {
        return $new_array += [

            // 'order_no'            => 'required|min:0|max:255',
            // 'order_date'          => 'required|date',
            // 'receive_date.*'    => 'required|date',
        ];
    }

    public function index()
    {
    	try{


    		return view('productions.production',[
    			'order_detail_list' => $this->order_detail->getAllOrderDetails(),
    			'department_list'   => $this->department->getAllDepartments(),
    			'order_list'        => $this->order->getAllOrders(),

    		]);
    	}	
    	catch (DecryptException $e) {
		    //
		}
    }

    public function getOrderDetailById(Request $request)
    {
        $view = 0;

        try{

            if($request->ajax())
            {
                $order_detail = $this->order_detail->getOrderDetailById(decrypt($request->id));


                if(isset($order_detail->id))
                {
                    // $total_receive = $order_detail->receiveOrders->sum('quantity');
                    $user_id = 0;

                    if($order_detail->user_id > 0)
                    {
                        $user_id = $order_detail->user_id;
                    }


                    return [

                        'available_amount' => ($order_detail->quantity * $order_detail->rate),
                        'remaining_amount' => (($order_detail->quantity * $order_detail->rate) - $order_detail->paymentHistories->whereIn('payment_type',['Advance Production Amount'])->sum('debit')),
                        'user'             => Crypt::encrypt($order_detail->user_id),
                        // 'is_partial'       => $order_detail->is_partial  
                    ];
                }
            }

            
        }
        catch (DecryptException $e) {
            //
        }

        return $view;
    }

  

    public function getOrderDetail(Request $request)
    {
    	$view = 0;

    	try{

    		if($request->ajax())
    		{
    			$order_detail = $this->order_detail->getOrderDetailById(decrypt($request->order_detail));

				if(isset($order_detail->id))
				{
					$total_receive = $order_detail->receiveOrders->sum('quantity');

					return [
						'remaining' => $order_detail->quantity - $total_receive,
						'is_partial' => $order_detail->is_partial  
					];
				}
    		}

			
		}
		catch (DecryptException $e) {
		    //
		}

    	return $view;
    }

     public function loadOrderVariantComponent(Request $request)
    {
    	$data = ['order' => false, 'view' => ''];

    	try
    	{
    		$order = $this->order->getOrderById(decrypt($request->order));

    		if(isset($order->id))
    		{
    			$data['order'] = $order;
    			
    			$data['view'] = view('productions.components.order_product_variants',[

					'product_variant_list' => $order->productVariations,

				])->render();
    		}

    	} catch (DecryptException $e) {
		    //
		}
    	
    	return $data;
    }
}
