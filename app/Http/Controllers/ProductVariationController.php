<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Contracts\Encryption\DecryptException;
use App\ProductVariation;

class ProductVariationController extends Controller
{
    private $product_variation;
	private $message;
	private $status;

	public function __construct()
    {
    	$this->ProductVariation = new ProductVariation;

    	$this->message    = 'Something is wrong';
    	$this->status     = 'error';

    }

	public function getValidationList($new_array = [])
	{
		return $new_array += [

			'name'  => 'required|min:0|max:255',
			'value' => 'required|min:0|max:255',

		];
	}

	public function index()
	{
		return view('product_variations.product_variation',[

			'product_variation_list' => $this->ProductVariation->getAllProductVariations(),
		]);
	}

    public function storeProductVariation(Request $request)
    {
		try 
		{
			$request->validate($this->getValidationList());

			$collect_data = $request->input();

			$product_variation = $this->ProductVariation->storeProductVariation($collect_data);

			if(isset($product_variation->id))
			{
				$this->message = 'Product variation has added succssfully.';
				$this->status  = 'success';
			}


		} catch (DecryptException $e) {
		    //
		}


		return redirect()->back()->with([

			'status'  => $this->status,
			'message' => $this->message,
		]);
    }


    public function updateProductVariation(Request $request)
    {
		try 
		{
			if($request->isMethod('Post'))
			{
				$request->validate($this->getValidationList());

				$collect_data = $request->input();

				$collect_data['product_variation'] = decrypt($collect_data['product_variation']);

				$product_variation = $this->ProductVariation->updateProductVariation($collect_data);

				if(isset($product_variation->id))
				{
					$this->message = 'Product variation has updated succssfully.';
					$this->status  = 'success';
				}
			}
			else
			{
				$id = $request->id;

				$product_variation = $this->ProductVariation->getProductVariationById(decrypt($id));

				if(isset($product_variation->id))
				{
					return view('product_variations.modals.update_product_variation',[

						'product_variation' => $product_variation,
					]);
				}
			}
			


		} catch (DecryptException $e) {
		    //
		}


		return redirect()->back()->with([

			'status'  => $this->status,
			'message' => $this->message,
		]);
    }

    public function removeProductVariation(Request $request)
    {
		try 
		{

			$product_variation = $this->ProductVariation->removeProductVariation(decrypt($request->id));

			if($product_variation)
			{
				$this->message = 'Product variation has removed succssfully.';
				$this->status  = 'success';
			}


		} catch (DecryptException $e) {
		    //
		}


		return redirect()->back()->with([

			'status'  => $this->status,
			'message' => $this->message,
		]);
    }
}
