<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Department;
use App\Order;
use App\Production;
use App\User;
use App\OrderReceive;
use App\Payment;
use App\OrderDetail;


class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $order = new Order;
        $production = new Production;
        $user = new User;
        $department = new Department;
        $payment = new Payment;
        $order_receive = new OrderReceive;
        $order_detail  = new OrderDetail;







        return view('dashboard',[

            'order_list'      => $order->getAllOrders(),
            'production_list' => $production->getAllProductions(),
            'production_list' => $production->getAllProductions(),
            'user_list'       => $user->getAllUsersByType(['Team Member']),
            'department_list' => $department->getAllDepartments(),
            'pending_list'    => $order_receive->pendingOrderReceive(),
            'payment_list'    => $payment->getAllPayments(['Production Amount']),
            'order_detail_list' => $order_detail->getAllOrderDetail(),



            



        ]);
    }
}
