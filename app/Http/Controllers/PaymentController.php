<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Contracts\Encryption\DecryptException;
use App\OrderReceive;
use App\Payment;
use App\PaymentHistory;
use App\User;
use App\Production;




class PaymentController extends Controller
{
	private $payment;
	private $message;
	private $status;
	private $order_receive;


	public function __construct()
    {
    	$this->payment 		 = new Payment;
    	$this->order_receive = new OrderReceive;
    	$this->production    = new Production;



    	$this->message    = 'Something is wrong';
    	$this->status     = 'error';

    }

	public function getValidationList($new_array = [])
	{
		return $new_array += [

			'advance_amount' => 'nullable|gte:0',
			'pocket_money'   => 'nullable|gte:0',
			'amount'		 => 'nullable|gt:0',

		];
	}

	public function index()
	{
		return view('payments.payment',[

			'payment_list' => $this->payment->getAllPayments(['Production Amount','Advance Production Amount']),
		]);
	}

	public function showPendingOrderReceivePayroll()
    {

    	return view('payments.pending_order_receive',[

    		'pending_list' => $this->order_receive->pendingOrderReceive(),
    	]);
    }


    public function showAdvanceProductionPayment()
    {

    	return view('payments.advance_production_payment',[

    		'production_list'   => $this->production->getAllProductions(),

			'payment_list' => $this->payment->getAdvanceProductionPayments(['Advance Production Amount']),
    		
    	]);
    }


    public function storeAdvancePayment(Request $request)
    {
		try 
		{
			$request->validate($this->getValidationList());

			$collect_data = $request->input();

			$collect_data['order'] = decrypt($collect_data['order']);

			$collect_data['order_detail'] = decrypt($collect_data['order_detail']);

			$collect_data['user'] = decrypt($collect_data['user']);



			$payment = $this->payment->storeAdvancePayment($collect_data);

			if(isset($payment->id))
			{
				$this->message = 'Advance Production Payment has created succssfully.';
				$this->status  = 'success';
			}





		


		} catch (DecryptException $e) {
		    //
		}


		return redirect('payments')->with([

			'status'  => $this->status,
			'message' => $this->message,
		]);
    }

    public function showPendingOrdersPayment(Request $request)
    {
    		$data = ['view' => '', 'status' => 0];

    	try
    	{	

				$ids = [];

				if(isset($request->order_receive) && @count($request->order_receive) > 0)
				{
					foreach ($request->order_receive as $key => $value) {
						
						array_push($ids,decrypt($value));
					}
				}

				$order_receive_list = $this->order_receive->getOrderReceiveByIds($ids);

				$users_ids = $order_receive_list->pluck('orderDetail.user_id')->toArray();

				$dep_ids = $order_receive_list->pluck('orderDetail.production.department_id')->toArray();


				if(@count(array_unique($users_ids)) === 1 || (@count($users_ids)) == 0 && @count($dep_ids) > 0)
				{

					if(@count($order_receive_list) > 0)
					{
						$user = new User;

						$user = $user->getUserById($order_receive_list->first()->orderDetail->user_id);

						$payment_history = new PaymentHistory;

						$common_data = [

						 	'order_receive_list' => $order_receive_list,
						 	// 'ledger_list'		 => [],

							];

						if(isset($user->id))
						{
							$ledger_list = $payment_history->getTyepWiseUserLedger($user->id,['Pocket Money','Advance Amount','Advance Production Amount']);


							$common_data+= ['user' => $user, 'ledger_list' => $ledger_list];


						}


						$data['view'] = view('payments.modals.add_payment',$common_data)->render();

						$data['status'] = 1;

						 
					}
					else
					{
						$data['view'] = 'Selected Payment Labour must be same'; 
						$data['status'] = 1; 
						
					}


				}

				

    	}
    	catch(DecryptException $e){

		}

					return $data;
		
    }


    public function storePayment(Request $request)
    {
		try 
		{
			if($request->isMethod('Post'))
			{
				$request->validate($this->getValidationList());

				$collect_data = $request->input();

				if(isset($collect_data['order_receive']))
				{
					foreach ($collect_data['order_receive']  as $key => $rows) {
						
						$collect_data['order_receive'][$key] = decrypt($rows);
					}
				}
				else
				{
					$collect_data['order_receive'] = [];
				}

				if(isset($collect_data['advance_order_receive']))
				{
					foreach ($collect_data['advance_order_receive']  as $key => $rows) {
						
						$collect_data['advance_order_receive'][$key] = decrypt($rows);
					}
				}
				else
				{
					$collect_data['advance_order_receive'] = [];
				}

				if(isset($collect_data['advance_order_detail']))
				{
					foreach ($collect_data['advance_order_detail']  as $key => $rows) {
						
						$collect_data['advance_order_detail'][$key] = decrypt($rows);
					}
				}
				else
				{
					$collect_data['advance_order_receive'] = [];
				}

				$collect_data['date'] = now();

				$payment = $this->payment->bulkStorePayment($collect_data);

				if(isset($payment->id))
				{
					$this->message = 'Payment has added succssfully.';
					$this->status  = 'success';
				}
			}
			else
			{
				
				

				
			}
			


		} catch (DecryptException $e) {
		    //
		}


		return redirect('payments')->with([

			'status'  => $this->status,
			'message' => $this->message,
		]);
    }

    public function updatePaymentStatus($id)
    {
    	try
    	{
    		$payment = $this->payment->getPaymentById(decrypt($id));

    		if(isset($payment->id))
    		{
    			$payment->status = 'Paid';

    			$payment->update();
    		}

		} catch (DecryptException $e) 
		{

		}


		return redirect()->back()->with([

			'status'  => $this->status,
			'message' => $this->message,
		]);
    }


    public function updatePayment(Request $request)
    {
		try 
		{
			if($request->isMethod('Post'))
			{
				$request->validate($this->getValidationList());

				$collect_data = $request->input();

				$collect_data['Payment'] = decrypt($collect_data['Payment']);

				$payment = $this->payment->updatePayment($collect_data);

				if(isset($payment->id))
				{
					$this->message = 'Payment has updated succssfully.';
					$this->status  = 'success';
				}
			}
			else
			{
				$id = $request->id;

				$payment = $this->payment->getPaymentById(decrypt($id));

				if(isset($payment->id))
				{
					return view('Payments.modals.update_Payment',[

						'Payment' => $payment,
					]);
				}
			}
			


		} catch (DecryptException $e) {
		    //
		}


		return redirect()->back()->with([

			'status'  => $this->status,
			'message' => $this->message,
		]);
    }

    public function removePayment(Request $request)
    {
		try 
		{

			$payment = $this->payment->removePayment(decrypt($request->id));

			if($payment)
			{
				$this->message = 'Payment has removed succssfully.';
				$this->status  = 'success';
			}


		} catch (DecryptException $e) {
		    //
		}


		return redirect()->back()->with([

			'status'  => $this->status,
			'message' => $this->message,
		]);
    }
    
}
