<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\OrderDetail;
use App\Order;
use App\Department;
use App\Production;
use Illuminate\Contracts\Encryption\DecryptException;
use Illuminate\Validation\Rule;
use Crypt;

class ProductionController extends Controller
{
     private $order;
    private $department;

	private $message;
	private $status;

	public function __construct()
    {
    	$this->order_detail 		     = new OrderDetail;

    	$this->order 		     = new Order;
    	$this->department 		 = new Department;
    	$this->production 		 = new Production;



    	$this->message    = 'Something is wrong';
    	$this->status     = 'error';

    }


    public function getValidationList($new_array = [])
    {
        return $new_array += [

            // 'order_no'            => 'required|min:0|max:255',
            // 'order_date'          => 'required|date',
            // 'receive_date.*'    => 'required|date',
        ];
    }

	public function index()
    {
    	try{


    		return view('productions.production',[
                
                'production_list' => $this->production->getAllProductions(),

    		]);
    	}	
    	catch (DecryptException $e) {
		    //
		}
    }

    public function getOrderDetail(Request $request)
    {

        try{

            $production = $this->production->getProductionById(decrypt($request->id));

            if(isset($production->id))
            {
                return view('payments.components.order_detail_drop_down',[
                    'production' => $production,
                ])->render();
            }

            



        }   
        catch (DecryptException $e) {
            //
        }

    }

    public function storeProduction(Request $request)
    {

        try 
        {
            if($request->isMethod('Post'))
            {

                $request->validate($this->getValidationList());

                $collect_data = $request->input();
             
                $collect_data['department'] = decrypt($collect_data['department']);

                if(isset($collect_data['parent']))
                {
                    $collect_data['parent'] = decrypt($collect_data['parent']);

                }



                $collect_data['order'] = decrypt($collect_data['order']);


                if(isset($collect_data['product_variant']) && @count($collect_data['product_variant']) > 0)
                {
                    foreach ($collect_data['product_variant'] as $key => $value) {

                        $collect_data['product_variant'][$key] = decrypt($value);
                    }
                }
                else{
                    $collect_data['product_variant'] = [];
                }

                if(isset($collect_data['parent_order_detail']) && @count($collect_data['parent_order_detail']) > 0)
                {
                    foreach ($collect_data['parent_order_detail'] as $key => $value) {

                        $collect_data['parent_order_detail'][$key] = decrypt($value);
                    }
                }
                else{
                    $collect_data['parent_order_detail'] = [];

                }
                    

                if(isset($collect_data['user']) && @count($collect_data['user']) > 0)
                {
                    foreach ($collect_data['user'] as $key => $value) {

                        $collect_data['user'][$key] = decrypt($value);
                    }
                }
                else{
                    $collect_data['user'] = [];
                }

               




                $production = $this->production->storeProduction($collect_data);

                if(isset($production->id))
                {
                    $this->message = 'Production has created succssfully.';
                    $this->status  = 'success';
                }
            }
            else
            {

                $existing_order_list = $this->production->getAllProductions()->pluck('order_id')->unique()->toArray();

                return view('productions.modals.add_production',[

                    'order_list'        => $this->order->getAllOrders()->whereNotIn('id',$existing_order_list),

                    'department_list'   => $this->department->getAllDepartments(),
                ]);
            }
            


        } 
        catch (DecryptException $e) {
            //
        }

        return redirect('workorderproduction')->with([

            'status'  => $this->status,
            'message' => $this->message,
        ]);
    }

    public function storeNextProduction(Request $request)
    {
        try{

            if($request->isMethod('Post'))
            {
                $this->storeProduction($request);
            }
            else
            {
                $id = decrypt($request->id);

                $production = $this->production->getProductionById($id);

                if(isset($production->id))
                {
                    $department_ids = Production::where('order_id',$production->order_id)->pluck('department_id')->toArray();


                    return view('next_productions.modals.add_next_production',[

                        'production'   => $production,

                        'department_list'   => $this->department->getDepartmentByWithoutId($department_ids),
                    ]);

                }
            }

            
        }
        catch (DecryptException $e) {
            //
        }

        return redirect()->back();

    }


    public function updateNextProduction(Request $request)
    {
        try{

            if($request->isMethod('Post'))
            {
                $this->updateProduction($request);
            }
            else
            {
                $id = decrypt($request->id);

                $production = $this->production->getProductionById($id);

                if(isset($production->id))
                {
                    $parent = $production->parent;

                    return view('next_productions.modals.update_next_production',[

                        'production'   => $production,

                        'parent'    => $parent,

                        'department_list'   => $this->department->getNextDepartment($parent->department->order),
                    ]);

                }
            }

            
        }
        catch (DecryptException $e) {
            //
        }
        return redirect()->back();

    }


    public function updateProduction(Request $request)
    {
        try 
        {
            
           if($request->isMethod('Post'))
           {
                 $request->validate($this->getValidationList());

                $collect_data = $request->input();
             
                $collect_data['department'] = decrypt($collect_data['department']);

                $collect_data['order']      = decrypt($collect_data['order']);

                $collect_data['production'] = decrypt($collect_data['production']);

                if(isset($collect_data['product_variant']) && @count($collect_data['product_variant']) > 0)
                {
                    foreach ($collect_data['product_variant'] as $key => $value) {

                        $collect_data['product_variant'][$key] = decrypt($value);
                    }
                }
                else{
                    $collect_data['product_variant'] = [];
                }

                if(isset($collect_data['user']) && @count($collect_data['user']) > 0)
                {
                    foreach ($collect_data['user'] as $key => $value) {

                        $collect_data['user'][$key] = decrypt($value);
                    }
                }
                else{
                    $collect_data['user'] = [];
                }


                 if(isset($collect_data['order_detail']) && @count($collect_data['order_detail']) > 0)
                {
                    foreach ($collect_data['order_detail'] as $key => $value) {

                        $collect_data['order_detail'][$key] = decrypt($value);
                    }
                }
                else{
                    $collect_data['order_detail'] = [];
                }

                if(isset($collect_data['parent_order_detail']) && @count($collect_data['parent_order_detail']) > 0)
                {
                    foreach ($collect_data['parent_order_detail'] as $key => $value) {

                        $collect_data['parent_order_detail'][$key] = decrypt($value);
                    }
                }
                else{
                    $collect_data['parent_order_detail'] = [];

                }


                $production = $this->production->updateProduction($collect_data);

                if(isset($production->id))
                {
                    $this->message = 'Production has updated succssfully.';
                    $this->status  = 'success';
                }
           }
           else
           {
                $id = $request->id;

                $production = $this->production->getProductionById(decrypt($id));

                if(isset($production->id))
                {
                    return view('productions.modals.update_production',[

                        'production' => $production,
                            'department_list'   => $this->department->getAllDepartments(),
                'order_list'        => $this->order->getAllOrders(),
                    ]);
                }
           }


        } catch (DecryptException $e) {
            //
        }

        return redirect()->back()->with([

            'status'  => $this->status,
            'message' => $this->message,
        ]);
    }

    public function removeProduction(Request $request)
    {
        try 
        {

            $production = $this->production->removeProduction(decrypt($request->id));

            if($production)
            {
                $this->message = 'Order has removed succssfully.';
                $this->status  = 'success';
            }


        } catch (DecryptException $e) {
            //
        }


        return redirect()->back()->with([

            'status'  => $this->status,
            'message' => $this->message,
        ]);
    }

}
