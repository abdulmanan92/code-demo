<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Contracts\Encryption\DecryptException;

use App\DataLog;

class DataLogController extends Controller
{
    public function getDataLogByType(Request $request)
    {
        $data = NULL;

        try {

            if($request->ajax())
            {
                $data_log = new DataLog;

                $data = view('data_logs.view_logs',[
                    'data_log_list' => $data_log->getLogByGenericWithType(decrypt($request->id),decrypt($request->type)),
                ])->render();
            }

        } catch (DecryptException $e) {

        }

        return $data;
    }
}
