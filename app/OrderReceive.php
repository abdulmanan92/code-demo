<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use DB;

class OrderReceive extends Model
{
    public function __construct()
    {
        date_default_timezone_set("Asia/Karachi");
    }


    public function calulateLabourBonus($user_id)
    {
        $debit =  OrderReceive::whereHas('orderDetail',function($query) use ($user_id){

            $query->where('user_id',$user_id);

        })->sum('bonus_cost');

        $credit = PaymentHistory::where('payment_type','Bonus Amount')->whereHas('payment',function($query) use ($user_id){
            $query->where('user_id',$user_id);

        })->sum('debit');

        return $debit - $credit;
    }
    
    public function storeOrderReceive($data)
    {
    	return DB::transaction(function() use ($data) {

    		$receive_order = new OrderReceive;



    		$receive_order->production_id 	   = $data['production'];

            $receive_order->order_detail_id    = $data['order_detail'];

    		$receive_order->date 		= $data['date'];

    		$receive_order->quantity  	= $data['quantity'];

    		$receive_order->save();


            $order_detail = $receive_order->orderDetail;

            if(isset($order_detail->id))
            {
                $receive_order->production_cost = (double)($order_detail->rate * $receive_order->quantity);

                if($order_detail->bonus_limit  > 0 && $receive_order->production_cost >= $order_detail->bonus_limit)
                {
                    $times = (int)($receive_order->production_cost/$order_detail->bonus_limit);

                    $receive_order->bonus_cost = $order_detail->bonus * $times; 
                }
            }

            $receive_order->update();


    		
    		return with($receive_order);
    	});
    }


    public function getProductionReceivingReport($from = [],$to = [],$condition = [])
    {
        $department = 0;
        $product    = 0;
        $user       = 0;



        if(isset($condition['department_id']))
        {
            $department = $condition['department_id'];
            
            unset($condition['department_id']);
        }

        if(isset($condition['product_id']))
        {
            $product = $condition['product_id'];
            
            unset($condition['product_id']);
        }


        if(isset($condition['user_id']))
        {
            $user = $condition['user_id'];
            
            unset($condition['user_id']);
        }

        $query =  OrderReceive::where($condition);

        if(strlen(trim($from)) > 0 && strlen(trim($to)) > 0)
        {
            $query = $query->whereDate('created_at','>=',$from)->whereDate('created_at','<=',$to);
        }


        return $query->whereHas('orderDetail',function($query) use ($department,$product,$user) {

            if($user > 0)
            {
                $query->where('user_id',$user);
            }

            $query->whereHas('production',function($query) use ($department) {

                if($department > 0)
                {
                    $query->where('department_id',$department);
                }

            })->whereHas('productVariation',function($query) use ($product) {

                if($product > 0)
                {
                    $query->where('product_id',$product);
                }

            });

        })->orderBy('id','ASC')->get();

         
        


    }


    public function updateOrderReceive($data)
    {
    	return DB::transaction(function() use ($data) {

    		$receive_order = OrderReceive::find($data['order_receive']);

            if(isset($receive_order->id))
            {
               
	    		$receive_order->production_id 	= $data['production'];

                $receive_order->order_detail_id    = $data['order_detail'];

	    		$receive_order->date 		= $data['date'];

	    		$receive_order->quantity  	= $data['quantity'];

               

	    		$receive_order->update();

            }

    		
    		return with($receive_order);
    	});
    }

    public function removeOrderReceive($id)
    {

    	return DB::transaction(function() use ($id) {

    		$receive_order = OrderReceive::find($id);

    		if(isset($receive_order->id))
    		{

    			$receive_order->delete();

    			return with(true);
    		}

    		return with(false);
    	});
    }

    public function pendingOrderReceive()
    {
        return OrderReceive::where('status','Payable')->orderBy('id','Asc')->get();
    }

    public function getOrderReceiveByIds($ids = [])
    {
        return OrderReceive::whereIn('id',$ids)->orderBy('id','Asc')->get();
    }

    public function getOrderReceiveById($id)
    {
    	return OrderReceive::find($id);
    }

    public function getAllOrderReceiveByOrder($production_id)
    {
    	return OrderReceive::where('production_id',$production_id)->OrderBy('id','Asc')->get();
    }

    public function getReceiveQtyByProduction($production,$product_variation)
    {
        return OrderReceive::where('production_id',$production)->whereHas('production',function($query) use ($production_variation) {

            $query->whereHas('productionDetail',function($query) use ($product_variation){

                $query->where('product_variation_id',$product_variation);

            });

        })->sum('quantity');
    }

   

    public function production()
    {
        return $this->belongsTo(Production::class,'production_id')->withDefault();
    }

    public function paymentHistory()
    {
        return $this->hasOne(PaymentHistory::class,'order_receive_id')->withDefault();
    }

    public function paymentHistories()
    {
        return $this->hasMany(PaymentHistory::class,'order_receive_id');
    }


    public function bonusPaymentHistories()
    {
        return $this->hasMany(PaymentHistory::class,'order_receive_id')->where('payment_type','Bonus Amount');
    }

    public function productionPaymentHistories()
    {
        return $this->hasMany(PaymentHistory::class,'order_receive_id')->where('payment_type','Production Amount');
    }


    public function orderDetail()
    {
        return $this->belongsTo(OrderDetail::class,'order_detail_id')->with(['production'])->withDefault();
    }
}
