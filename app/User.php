<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Spatie\Permission\Traits\HasRoles;
use FL;
use DB;

class User extends Authenticatable
{
    use Notifiable;
    use HasRoles;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];


    public function storeUser($data)
    {
        return DB::transaction(function() use ($data) {


            $user = new User;

            $user->department_id            = $data['department'];
            $user->email                    = FL::emailCreator($data['email']);
            $user->password                 = FL::passwordCreator($data['password']);
            $user->type                     = $data['type'];
            $user->name                     = $data['name'];
            $user->cnic                     = $data['cnic'];
            $user->contact                  = $data['contact_no'];
            $user->address                  = $data['address'];
            $user->joining_date             = $data['date_of_joining'];
            $user->reference                = $data['reference'];
            // $user->advance_amount           = $data['advance_amount'];
            // $user->pocket_money             = $data['pocket_money'];
            $user->payroll_group            = $data['payroll_group'];
            $user->status                   = $data['status'];
            $user->dp                       = FL::uploadImage($data['dp']);


            $user->save();

            if($user->type == "Team Member")
            {
                if(isset($data['cnic_copy']))
                {
                    $user->cnic_copy = 1;
                }
                else
                {
                    $user->cnic_copy = 0;
                }

                if(isset($data['stamp_paper']))
                {
                    $user->stamp_paper = 1;
                }
                else
                {
                    $user->stamp_paper = 0;
                }


                if(isset($data['cheque']))
                {
                    $user->cheque = 1;
                }
                else
                {
                    $user->cheque = 0;
                }


                if(isset($data['photos']))
                {
                    $user->photos = 1;
                }
                else
                {
                    $user->photos = 0;
                }

                $user->update();
               

                if(isset($data['files']) && @count($data['files']) > 0)
                {
                    $count = 0;

                    foreach ($data['files']  as $key => $files) {
                        
                        foreach ($files as $key => $rows) {

                            $document = new Document;

                            $document->storeDocument([

                                'generic_id'   => $user->id,
                                'generic_type' => 'User',
                                'title'        => $data['doc_title'][$count],
                                'date'         => $data['doc_date'][$count],
                                'file'         => $rows,
                            ]);

                            $count++;
                        }
                    }
                    
                }

                if(isset($data['payment_type']) && @count($data['payment_type']) > 0)
                {
                    $count = 0;

                    foreach ($data['payment_type'] as $key => $rows) {

                        $payment = new Payment;

                        $curr_payment = $payment->storePayment([

                            'user'         => $user->id,
                            'status'       => 'Paid',
                            'amount'       => $data['amount'][$count],
                            'date'         => $data['date'][$count],
                        ]);


                        $payment_history = new PaymentHistory;

                        $payment_history->storePaymentHistory([

                            'payment'      => $curr_payment->id,
                            'order_receive'=> 0,
                            'payment_type' => $data['payment_type'][$count],
                            'debit'        => $data['amount'][$count],
                            'credit'       => 0,

                        ]);

                        $count++;
                    }
                    
                }
            }


            if(isset($data['role']))
            {
                $user->syncRoles([$data['role']]);
            }

            return with($user);
        });
    }

    public function updateUser($data)
    {
        return DB::transaction(function() use ($data) {

            $user = User::find($data['user']);
            
            if(isset($user->id))
            {
                if(\Auth::user()->id == "1" && isset($data['from']) && isset($data['to']))
                {

                    $user->from = $data['from'];
                    $user->to   = $data['to'];

                }

                $user->department_id            = $data['department'];
                $user->email                    = FL::emailCreator($data['email'],$user->email);
                $user->password                 = FL::passwordCreator($data['password'],$user->password);
                $user->type                     = $data['type'];
                $user->name                     = $data['name'];
                $user->cnic                     = $data['cnic'];
                $user->contact                  = $data['contact_no'];
                $user->address                  = $data['address'];
                $user->joining_date             = $data['date_of_joining'];
                $user->reference                = $data['reference'];
                // $user->advance_amount           = $data['advance_amount'];
                // $user->pocket_money             = $data['pocket_money'];
                $user->payroll_group            = $data['payroll_group'];

                if(isset($data['status']))
                {
                    $user->status                   = $data['status'];
                }
                
                $user->dp                       = FL::uploadImage($data['dp'],$user->dp);

                if($user->type == "Team Member")
                {
                    if(isset($data['cnic_copy']))
                    {
                        $user->cnic_copy = 1;
                    }
                    else
                    {
                        $user->cnic_copy = 0;
                    }

                    if(isset($data['stamp_paper']))
                    {
                        $user->stamp_paper = 1;
                    }
                    else
                    {
                        $user->stamp_paper = 0;
                    }


                    if(isset($data['cheque']))
                    {
                        $user->cheque = 1;
                    }
                    else
                    {
                        $user->cheque = 0;
                    }


                    if(isset($data['photos']))
                    {
                        $user->photos = 1;
                    }
                    else
                    {
                        $user->photos = 0;
                    }



                    $count = 0;

                    if(isset($data['payment']) && @count($data['payment']) > 0)
                    {
                        foreach ($user->payments as $key => $payment) 
                        {
                                $payment->removePayment($payment->id);
                            
                            // echo $data['payment'][$count] .'--'.$payment->id.'<br>';
                            // if(isset($data['payment'][$count]) && $data['payment'][$count] == $payment->id)
                            // {
                            //     $payment->date   = $data['date'][$count];

                            //     $payment->amount = $data['amount'][$count];

                            //     $payment->update();

                            //     foreach ($payment->PaymentHistories as $key => $rows) {

                            //         $rows->debit = $data['amount'][$count];
                            //         $rows->credit = 0;

                            //         $rows->payment_type = $data['payment_type'][$count];



                            //         $rows->update();

                            //     }

                            //     unset($data['date'][$count]);
                            //     unset($data['amount'][$count]);
                            //     unset($data['payment_type'][$count]);
                            // }
                            // else
                            // {
                                
                            //     $payment->removePayment($payment->id);
                            // }
                            
                            // $count++;
                        }
                    }

// exit;
                    if(isset($data['payment_type']) && @count($data['payment_type']) > 0)
                    {

                        foreach ($data['payment_type'] as $key => $rows) {

                            if(isset($data['payment_type'][$count]))
                            {
                                 $payment = new Payment;

                            $curr_payment = $payment->storePayment([

                                'user'         => $user->id,
                                'status'       => 'Paid',
                                'amount'       => $data['amount'][$count],
                                'date'         => $data['date'][$count],
                            ]);


                            $payment_history = new PaymentHistory;

                            $payment_history->storePaymentHistory([

                                'payment'      => $curr_payment->id,
                                'order_receive'=> 0,
                                'payment_type' => $data['payment_type'][$count],
                                'debit'        => $data['amount'][$count],
                                'credit'       => 0,

                            ]);

                            }

                           
                            $count++;
                        }
                        
                    }

                }


                $user->update();

                if(isset($data['role']))
                {
                    $user->syncRoles([$data['role']]);
                }
            }

            return with($user);
        });
    }

    public function removeUser($id)
    {

        return DB::transaction(function() use ($id) {

            $user = User::find($id);

            if(isset($user->id))
            {

                $user->delete();

                return with(true);
            }

            return with(false);
        });
    }

    public function userOrderDetailVerification($user,$department_id)
    {
        $order_detail = new OrderDetail;

        return $order_detail->userOrderDetailVerification($user,$department_id);
    }

    public function getUserById($id)
    {
        return User::find($id);
    }

    public function getAllUsers()
    {
        return User::orderBy('id','Asc')->get();
    }

    public function getAllUsersByType($type = [])
    {
        return User::whereIn('type',$type)->orderBy('id','Asc')->get();
    }

    public function department()
    {
        return $this->belongsTo(Department::class,'department_id')->withDefault();
    }

    public function documents()
    {
        return $this->hasMany(Document::class,'generic_id')->where('generic_type','User');
    }

    public function getAdvanceAndPocketAmount($user_id)
    {
        $payment_history = new PaymentHistory;

        return $payment_history->getAdvanceAndPocketAmount($user_id);
    }

    public function calculateBonus($user_id)
    {
        $order_receive =  new OrderReceive;

        return  $order_receive->calulateLabourBonus($user_id);
    }

    public function payments()
    {
        return $this->hasMany(Payment::class,'user_id')->whereHas('PaymentHistories',function($query){

            $query->whereIn('payment_type',['Advance Amount','Pocket Money','Bonus Amount'])->where('debit','>',0);

        })->with(['PaymentHistories'])->orderBy('id','ASC');
    }

    public function orderDetails()
    {
        return $this->hasMany(OrderDetail::class,'user_id');

    }

    public function ledgers()
    {
        return $this->hasMany(Payment::class,'user_id');
    }
}
