<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use DB;

use Auth;

class OrderDetail extends Model
{
    public function __construct()
    {
        date_default_timezone_set("Asia/Karachi");
    }


    public function getOrderDetailByOrder($from = false, $to = false, $order_id)
    {
        return OrderDetail::whereHas('production',function($query) use ($order_id){

            $query->where('order_id',$order_id);

        })->orderBy('id','ASC')->get();
    }


    public function getAllOrderDetail()
    {
        return OrderDetail::whereHas('production')->orderBy('id','DESC')->limit(50)->get();
    }


    public function getOrderDetailByIds($ids = [])
    {
        return OrderDetail::whereIn('id',$ids)->orderBy('id','Asc')->get();
    }
    
    public function storeOrderDetail($data)
    {
    	return DB::transaction(function() use ($data) {

    		$order_detail = new OrderDetail;

    		$order_detail->parent_id 		= $data['parent'];

            $order_detail->production_id    = $data['production'];


    		$order_detail->product_variation_id 	= $data['product_variation'];

    		$order_detail->user_id 			= $data['user'];

            if($order_detail->user_id > 0)
            {
                $order_detail->is_lot          = 1;
            }
            else
            {
                $order_detail->is_lot          = 0;

            }

    		$order_detail->quantity  	    = $data['quantity'];

            $order_detail->receive_date         = $data['receive_date'];

            $order_detail->is_partial         = $data['is_partial'];


            

    		$order_detail->save();

            $production_rate = new ProductionRate;

            $production_rate = $production_rate->getProductionRate($order_detail->productVariation->product_id,$order_detail->production->department_id);
            $order_detail->rate        = 0;
            $order_detail->bonus       = 0;
            $order_detail->bonus_limit = 0;
            
            if(isset($production_rate->id))
            {
                $order_detail->rate        = $production_rate->production_rate;

                if($production_rate->is_bonus > 0)
                {
                   $order_detail->bonus       = $production_rate->bonus;
                    $order_detail->bonus_limit = $production_rate->bonus_limit; 
                }

                
            }

            $order_detail->update();

    		return with($order_detail);
    	});
    }

    public function removeOrderDetailByProductVariant($id)
    {

        return DB::transaction(function() use ($id) {

            return OrderDetail::where('product_variation_id',$id)->delete();

            // if(isset($order_detail->id))
            // {

            //     $order_detail->delete();

            //     return with(true);
            // }

            // return with(false);
        });
    }

  
    public function removeOrderDetail($id)
    {

    	return DB::transaction(function() use ($id) {

    		$order_detail = OrderDetail::find($id);

    		if(isset($order_detail->id))
    		{

    			$order_detail->delete();

    			return with(true);
    		}

    		return with(false);
    	});
    }

    public function userOrderDetailVerification($user,$department)
    {
        
        if(Auth::user()->id == 1)
        {
            return 0;
        }
        
        $curr_user = User::find($user);
        

        if(isset($curr_user->id))
        {
            $date = date('Y-m-d');
            $hour = date('H');
            $min  = date('i');

            $from = $curr_user->from;

            $from_date = date('Y-m-d',strtotime($from));
            $from_hour = date('H',strtotime($from));
            $from_min  = date('i',strtotime($from));

            $to = $curr_user->to;

            $to_date = date('Y-m-d',strtotime($to));
            $to_hour = date('H',strtotime($to));
            $to_min  = date('i',strtotime($to));

            if(strlen(trim($to)) > 0 && strlen(trim($from)) > 0)
            {
// dd($from_date,$date,$to_date);

                // DATE VERIFICATION
                if($date >= $from_date && $date <= $to_date)
                {
                 
                    if($date == $to_date)
                    {
                          
                        // if($hour >= $from_hour && $hour <= $to_hour)
                        if($hour <= $to_hour)
    
                        {
  
                            // if($min >= $from_min && $min <= $to_min)
                            if($min <= $to_min)
                                
                            {
                                return 0;
                            }  
                        }
                    }
                    else
                    {
                        return 0;
                    }

                    
                }
            }

        }

        $count = 0;

        $order_detail_list =  OrderDetail::where('user_id',$user)->whereHas('production',function($query) use($department){

            $query->where('department_id',$department);

        })->orderBY('id','ASC')->get();
        
        

        foreach ($order_detail_list as $rows) {
            
            if($rows->quantity == OrderReceive::where('order_detail_id',$rows->id)->sum('quantity'))
            {

            }
            else
            {
                $count++;
            }
        }
        


        return $count;
    }


    public function getProductionIssuanceReport($from = [],$to = [],$condition = [])
    {
        $department = 0;
        $product    = 0;


        if(isset($condition['department_id']))
        {
            $department = $condition['department_id'];
            
            unset($condition['department_id']);
        }

        if(isset($condition['product_id']))
        {
            $product = $condition['product_id'];
            
            unset($condition['product_id']);
        }

        $query =  OrderDetail::where($condition);

        if(strlen(trim($from)) > 0 && strlen(trim($to)) > 0)
        {
            $query = $query->whereDate('created_at','>=',$from)->whereDate('created_at','<=',$to);
        }

        return $query->whereHas('production',function($query) use ($department) {

            if($department > 0)
            {
                $query->where('department_id',$department);
            }

        })->whereHas('productVariation',function($query) use ($product) {

            if($product > 0)
            {
                $query->where('product_id',$product);
            }

        })
        ->orderBy('id','ASC')->get();


    }

    public function paymentHistories()
    {
        return $this->hasMany(PaymentHistory::class,'order_detail_id');
    }

    public function getOrderDetailById($id)
    {
    	return OrderDetail::find($id);
    }

    public function getAllOrderDetails()
    {
    	return OrderDetail::orderBy('id','Asc')->get();
    }

    public function getAllOrderDetailsByParent($parent_id)
    {
    	return OrderDetail::where('parent_id',$parent_id)->OrderDetailBy('id','Asc')->get();
    }

    

    public function receiveOrders()
    {
        return $this->hasMany(OrderReceive::class,'order_detail_id')->orderBy('id','ASC');
    }

    public function user()
    {
        return $this->belongsTo(User::class,'user_id')->withDefault();
    }

    public function labour()
    {
        return $this->belongsTo(User::class,'user_id')->withDefault();
    }




    public function production()
    {
        return $this->belongsTo(Production::class,'production_id')->withDefault();
    }


    public function orderReceive()
    {
        return $this->hasMany(OrderReceive::class,'order_detail_id')->orderBy('id','ASC');
    }

    public function productVariation()
    {
        return $this->belongsTo(ProductVariation::class,'product_variation_id')->withDefault();
    }
}
