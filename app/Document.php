<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use FL;
USE DB;

class Document extends Model
{
    public function __construct()
    {
        date_default_timezone_set("Asia/Karachi");
    }
    
    public function storeDocument($data)
    {
    	return DB::transaction(function() use ($data) {

    		$document = new Document;

    		$document->generic_id 	= $data['generic_id'];

    		$document->generic_type = $data['generic_type'];

    		$document->title 		= $data['title'];

    		$document->date  		= $data['date'];

    		$document->file  		=  FL::uploadImage($data['file']);

    		$document->save();

    		return with($document);
    	});
    }

    public function updateDocument($data)
    {
    	return DB::transaction(function() use ($data) {

    		$document = Document::find($data['document']);

    		if(isset($document->id))
    		{
    			// $document->generic_id 	= $data['generic_id'];

	    		// $document->generic_type = $data['generic_type'];

	    		$document->title 		= $data['title'];

	    		$document->date  		= $data['date'];

	    		$document->file  		=  FL::uploadImage($data['file'],$document->file);

	    		$document->update();
    		}

    		return with($document);
    	});
    }

    public function removeDocument($id)
    {

    	return DB::transaction(function() use ($id) {

    		$document = Document::find($id);

    		if(isset($document->id))
    		{

    			$document->delete();

    			return with(true);
    		}

    		return with(false);
    	});
    }

    public function getDocumentById($id)
    {
    	return Document::find($id);
    }

    public function getAllDocuments()
    {
    	return Document::orderBy('id','Asc')->get();
    }

    public function getAllDocumentByGeneric($generic_id,$generic_type)
    {
    	return Document::where(['generic_id' => $generic_id, 'generic_type' => $generic_type])->orderBy('id','Asc')->get();
    }
}
