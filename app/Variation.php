<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Variation extends Model
{
	public function __construct()
    {
        date_default_timezone_set("Asia/Karachi");
    }
    
    public function productVariation()
    {
    	return $this->belongsTo(ProductVariation::class,'product_variation_id')->withDefault();
    }
}
