<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use DB;

class Order extends Model
{
    public function __construct()
    {
        date_default_timezone_set("Asia/Karachi");
    }
    
    public function storeOrder($data)
    {
    	return DB::transaction(function() use ($data) {

    		$order = new Order;

    		// if(isset($data['parent']))
    		// {
    		// 	$order->parent_id 		= $data['parent'];
    		// }

      //       if(isset($data['is_lot']))
      //       {
      //           $order->is_lot       = 1;
      //       }
      //       else
      //       {
      //           $order->is_lot       = 0;

      //       }


            // NEW ORDER TIME SET
            // if(isset($data['product']))
            // {
            //     $order->product_id       = $data['product'];
            // }
            // else
            // {
            //     $parent_order = Order::find($order->parent_id);

            //     if(isset($parent_order))
            //     {
            //         $order->product_id       = $parent_order->product_id;
            //     }

            // }


    		// $order->department_id 	= $data['department'];

    		$order->order_no 		= $data['order_no'];

    		$order->date  			= $data['order_date'];

            $order->customer_name   = $data['customer_name'];


            if(isset($data['quantity']))
            {
                $order->total_quantity  = array_sum($data['quantity']); 
            }
            else
            {
                $order->total_quantity = 0;
            }

    		$order->save();


    		if(isset($data['color']) && @count($data['color']) > 0)
    		{
    			foreach ($data['color'] as $key => $value) {


                    $product_variation = new ProductVariation;

                    $product_variation->storeProductVariation([

                        'order'         => $order->id,
                        'product'       => $data['product'][$key],
                        'color'         => $data['color'][$key],
                        'size'          => $data['size'][$key],
                        'quantity'      => $data['quantity'][$key],

                        
                    ]);
    			

    			}
    		}

    		return with($order);
    	});
    }

    public function updateOrder($data)
    {
    	return DB::transaction(function() use ($data) {

    		$order = Order::find($data['order']);

            if(isset($order->id))
            {
               

                $order->order_no        = $data['order_no'];

                $order->date            = $data['order_date'];

                $order->customer_name   = $data['customer_name'];


                if(isset($data['quantity']))
                {
                    $order->total_quantity  = array_sum($data['quantity']); 
                }
                else
                {
                    $order->total_quantity = 0;
                }

                $order->update();

                $order_detail = new OrderDetail;


                if(isset($data['color']) && @count($data['color']) > 0)
                {

                    foreach($order->productVariations as $rows)
                    {
                        if(isset($data['product_variant']) && !in_array($rows->id,$data['product_variant']))
                        {
                            $rows->delete();

                            $order_detail->removeOrderDetailByProductVariant($rows->id);


                        }
                    }

                    $key = 0;

                    foreach ($order->productVariations->fresh()->whereNotNull('id') as  $rows) {


                        if(isset($data['product_variant'][$key]) && $data['product_variant'][$key] == $rows->id)
                        {
                            $rows->order_id         = $order->id;
                            $rows->product_id       = $data['product'][$key];
                            $rows->color            = $data['color'][$key];
                            $rows->size             = $data['size'][$key];
                            $rows->quantity         = $data['quantity'][$key];
                          
                            $rows->update();

                            unset($data['product'][$key]);
                            unset($data['color'][$key]);
                            unset($data['size'][$key]);
                            unset($data['quantity'][$key]);
                        }

                        $key++;
                       
                    
                        
                    }




                    foreach ($data['color'] as $key => $value) {

                        $product_variation = new ProductVariation;

                        $new_product_variation = $product_variation->storeProductVariation([

                            'order'         => $order->id,
                            'product'       => $data['product'][$key],
                            'color'         => $data['color'][$key],
                            'size'          => $data['size'][$key],
                            'quantity'      => $data['quantity'][$key],
                        ]);



                        if(isset($new_product_variation))
                        {
                            $production = Production::where('order_id',$order->id)->first();

                            if(isset($production->id))
                            {

                                $order_detail->storeOrderDetail([

                                    'production'            => $production->id,
                                    'product_variation'     => $new_product_variation->id,
                                    'user'                  => 0,
                                    'parent'                => 0, 
                                    'quantity'              => 0,
                                    'receive_date'          => NULL,
                                    'is_partial'            => 0,

                                ]);
                            }

                            
                        }
                    }
             
                }
            }

    		
    		return with($order);
    	});
    }

    public function removeOrder($id)
    {

    	return DB::transaction(function() use ($id) {

    		$order = Order::find($id);

    		if(isset($order->id))
    		{

    			$order->delete();

    			return with(true);
    		}

    		return with(false);
    	});
    }

    public function getOrderById($id)
    {
    	return Order::find($id);
    }

    public function getAllOrders()
    {
    	return Order::where('parent_id','<=',0)->orderBy('id','Asc')->get();
    }

    public function getAllOrdersByParent($parent_id)
    {
    	return Order::where('parent_id',$parent_id)->orderBy('id','Asc')->get();
    }

    public function orderDetail()
    {
    	return $this->hasMany(OrderDetail::class,'order_id')->orderBy('id','ASC');
    }

    public function productVariations()
    {
        return $this->hasMany(ProductVariation::class,'order_id')->orderBy('id','ASC');
    }

    public function product()
    {
        return $this->belongsTo(Product::class,'product_id')->withDefault();
    }

    public function department()
    {
        return $this->belongsTo(Department::class,'department_id')->withDefault();
    }

    public function productions()
    {
        return $this->hasMany(Production::class,'order_id');
    }

    public function parentOrder()
    {
        return $this->belongsTo(Department::class,'id')->withDefault();
    }

}
