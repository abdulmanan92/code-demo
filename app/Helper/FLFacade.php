<?php
namespace App\Helper;
use Illuminate\Support\Facades\Facade;

class FLFacade extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'fl';
    }
}