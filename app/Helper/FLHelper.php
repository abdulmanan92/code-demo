<?php

namespace App\Helper;
use Storage;



class FLHelper
{

    public function defaultObjectClasses()
    {
        $user       = new  \App\User;
        $department = new  \App\Department;
        $product    = new  \App\Product;
        $order      = new  \App\Order;


        return [

            'user_list'         => $user->getAllUsersByType(['Team Member']),
            'department_list'   => $department->getAllDepartments(),
            'product_list'      => $product->getAllProducts(),
            'order_list'        => $order->getAllOrders(),

        ];
    }
 
    public static function getSizeList()
    {
        return ['4XS','3XS','2XS','XS','S','M','L','XL','2XL','3XL','4XL','5XL'];
    }

    function getModuleList()
    {
        return ['Dashboard','Work Orders','Productions','Receive Production','Payments','Payroll','Departments','Labour Team','System Users','Products','Roles and Permissiosn','Reports'];
    }

    public function generateNumber($num)
    {
        return str_pad(($num), 6, '0', STR_PAD_LEFT);
    }
    public static function emailCreator($email,$old_email = "")
    {
        if(trim(strlen($email)) <= 0)
        {
            if(strlen(trim($old_email)) > 0 && strlen(trim($email)) <= 0 )
            {
                $email = $old_email;
            }
            else
            {
   
                $email = 'demo_'.(uniqid()).'@xyz.com';
            }
            
        }
            
        return $email;
    }
    
    public static function passwordCreator($password,$old_password = '')
    {
        if(strlen(trim($password)) <= 0)
        {
            if(strlen(trim($old_password)) > 0 && strlen(trim($password)) <= 0 )
            {
                return $old_password;
            }
            else
            {
                $password = date('y-m-d').uniqid();
            }   
        }

        return \Hash::make($password);
    }

      public function dateFormat($date)
      {
          return str_replace('01-01-1970',' ',date('d-m-Y',strtotime($date)));
      }

      public function dateFormatWithTime($date)
      {
          return str_replace('01-01-1970',' ',date('d-m-Y h:i:s a',strtotime($date)));
      }

      public function dateTimeFormat($date)
      {

          return str_replace('1970-01-01T05:00','',date('Y-m-d\TH:i',strtotime($date)));
      }

      public function datePhpFormat($date)
      {
          if(strlen(trim($date)) > 0)
          {
            return str_replace('1970-01-01',NULL,date('Y-m-d',strtotime($date)));
          }

          return NULL;
      }

      public function getLabourCheckList()
      {
         return ['cnic_copy','stamp_paper','cheque','photos'];
      }

 

  public function getCountryList()
  {
    return array(
    "Afghanistan",
    "Albania",
    "Algeria",
    "Andorra",
    "Angola",
    "Antigua and Barbuda",
    "Argentina",
    "Armenia",
    "Australia",
    "Austria",
    "Azerbaijan",
    "Bahamas",
    "Bahrain",
    "Bangladesh",
    "Barbados",
    "Belarus",
    "Belgium",
    "Belize",
    "Benin",
    "Bhutan",
    "Bolivia",
    "Bosnia and Herzegovina",
    "Botswana",
    "Brazil",
    "Brunei",
    "Bulgaria",
    "Burkina Faso",
    "Burundi",
    "Cambodia",
    "Cameroon",
    "Canada",
    "Cape Verde",
    "Central African Republic",
    "Chad",
    "Chile",
    "China",
    "Colombi",
    "Comoros",
    "Congo (Brazzaville)",
    "Congo",
    "Costa Rica",
    "Cote d'Ivoire",
    "Croatia",
    "Cuba",
    "Cyprus",
    "Czech Republic",
    "Denmark",
    "Djibouti",
    "Dominica",
    "Dominican Republic",
    "East Timor (Timor Timur)",
    "Ecuador",
    "Egypt",
    "El Salvador",
    "Equatorial Guinea",
    "Eritrea",
    "Estonia",
    "Ethiopia",
    "Fiji",
    "Finland",
    "France",
    "Gabon",
    "Gambia, The",
    "Georgia",
    "Germany",
    "Ghana",
    "Greece",
    "Grenada",
    "Guatemala",
    "Guinea",
    "Guinea-Bissau",
    "Guyana",
    "Haiti",
    "Honduras",
    "Hungary",
    "Iceland",
    "India",
    "Indonesia",
    "Iran",
    "Iraq",
    "Ireland",
    "Israel",
    "Italy",
    "Jamaica",
    "Japan",
    "Jordan",
    "Kazakhstan",
    "Kenya",
    "Kiribati",
    "Korea, North",
    "Korea, South",
    "Kuwait",
    "Kyrgyzstan",
    "Laos",
    "Latvia",
    "Lebanon",
    "Lesotho",
    "Liberia",
    "Libya",
    "Liechtenstein",
    "Lithuania",
    "Luxembourg",
    "Macedonia",
    "Madagascar",
    "Malawi",
    "Malaysia",
    "Maldives",
    "Mali",
    "Malta",
    "Marshall Islands",
    "Mauritania",
    "Mauritius",
    "Mexico",
    "Micronesia",
    "Moldova",
    "Monaco",
    "Mongolia",
    "Morocco",
    "Mozambique",
    "Myanmar",
    "Namibia",
    "Nauru",
    "Nepal",
    "Netherlands",
    "New Zealand",
    "Nicaragua",
    "Niger",
    "Nigeria",
    "Norway",
    "Oman",
    "Pakistan",
    "Palau",
    "Panama",
    "Papua New Guinea",
    "Paraguay",
    "Peru",
    "Philippines",
    "Poland",
    "Portugal",
    "Qatar",
    "Romania",
    "Russia",
    "Rwanda",
    "Saint Kitts and Nevis",
    "Saint Lucia",
    "Saint Vincent",
    "Samoa",
    "San Marino",
    "Sao Tome and Principe",
    "Saudi Arabia",
    "Senegal",
    "Serbia and Montenegro",
    "Seychelles",
    "Sierra Leone",
    "Singapore",
    "Slovakia",
    "Slovenia",
    "Solomon Islands",
    "Somalia",
    "South Africa",
    "Spain",
    "Sri Lanka",
    "Sudan",
    "Suriname",
    "Swaziland",
    "Sweden",
    "Switzerland",
    "Syria",
    "Taiwan",
    "Tajikistan",
    "Tanzania",
    "Thailand",
    "Togo",
    "Tonga",
    "Trinidad and Tobago",
    "Tunisia",
    "Turkey",
    "Turkmenistan",
    "Tuvalu",
    "Uganda",
    "Ukraine",
    "United Arab Emirates",
    "United Kingdom",
    "United States",
    "Uruguay",
    "Uzbekistan",
    "Vanuatu",
    "Vatican City",
    "Venezuela",
    "Vietnam",
    "Yemen",
    "Zambia",
    "Zimbabwe"
  );
  }

  public static function numberToWords($number) 

    {

      return ucwords(str_replace('-',' ',(\FL::convertToWords($number))));

    }
public static function convertToWords($number) 

    {



        $hyphen      = '-';

        $conjunction = ' and ';

        $separator   = ', ';

        $negative    = 'negative ';

        $decimal     = ' point ';

        $dictionary  = array(

            0                   => 'zero',

            1                   => 'one',

            2                   => 'two',

            3                   => 'three',

            4                   => 'four',

            5                   => 'five',

            6                   => 'six',

            7                   => 'seven',

            8                   => 'eight',

            9                   => 'nine',

            10                  => 'ten',

            11                  => 'eleven',

            12                  => 'twelve',

            13                  => 'thirteen',

            14                  => 'fourteen',

            15                  => 'fifteen',

            16                  => 'sixteen',

            17                  => 'seventeen',

            18                  => 'eighteen',

            19                  => 'nineteen',

            20                  => 'twenty',

            30                  => 'thirty',

            40                  => 'fourty',

            50                  => 'fifty',

            60                  => 'sixty',

            70                  => 'seventy',

            80                  => 'eighty',

            90                  => 'ninety',

            100                 => 'hundred',

            1000                => 'thousand',

            1000000             => 'million',

            1000000000          => 'billion',

            1000000000000       => 'trillion',

            1000000000000000    => 'quadrillion',

            1000000000000000000 => 'quintillion'

        );

        

        if (!is_numeric($number)) {

            return false;

        }

        

        if (($number >= 0 && (int) $number < 0) || (int) $number < 0 - PHP_INT_MAX) {

            // overflow

            trigger_error(

                'convertToWords only accepts numbers between -' . PHP_INT_MAX . ' and ' . PHP_INT_MAX,

                E_USER_WARNING

            );

            return false;

        }

        if ($number < 0) {

            return $negative . convertToWords(abs($number));

        }

        

        $string = $fraction = null;

        

        if (strpos($number, '.') !== false) {

            list($number, $fraction) = explode('.', $number);

        }

        

        switch (true) {

            case $number < 21:

                $string = $dictionary[$number];

                break;

            case $number < 100:

                $tens   = ((int) ($number / 10)) * 10;

                $units  = $number % 10;

                $string = $dictionary[$tens];

                if ($units) {

                    $string .= $hyphen . $dictionary[$units];

                }

                break;

            case $number < 1000:

                $hundreds  = $number / 100;

                $remainder = $number % 100;

                $string = $dictionary[$hundreds] . ' ' . $dictionary[100];

                if ($remainder) {

                    $string .= $conjunction . \FL::convertToWords($remainder);

                }

                break;

            default:

                $baseUnit = pow(1000, floor(log($number, 1000)));

                $numBaseUnits = (int) ($number / $baseUnit);

                $remainder = $number % $baseUnit;

                $string = \FL::convertToWords($numBaseUnits) . ' ' . $dictionary[$baseUnit];

                if ($remainder) {

                    $string .= $remainder < 100 ? $conjunction : $separator;

                    $string .= \FL::convertToWords($remainder);

                }

                break;

        }

        

        if (null !== $fraction && is_numeric($fraction)) {

            $string .= $decimal;

            $words = array();

            foreach (str_split((string) $fraction) as $number) {

                $words[] = $dictionary[$number];

            }

            $string .= implode(' ', $words);

        }

        

        return $string;

    }

    public static function getPayRollList()
    {
        // return ['FIEXED','SALARY','PRODUCTION BASE'];
        return ['PRODUCTION BASE'];
        
    }

   public static function fileLinker($contant_file)
    {
        $file_name = uniqid().'.'.$contant_file->getClientOriginalExtension();

        \Storage::disk('public')->put('/'.$file_name,file_get_contents($contant_file));

        return $file_name;
    }

    public static function uploadImage($image,$loaded_image = '')
    {
        if((isset($image) && trim(strlen($image)) > 0))
        {
            $image =  \FL::fileLinker($image);

        }
        else
        {
            if(strlen(trim($loaded_image)) > 0)
            {
                $image =  $loaded_image;
            }
            else
            {
               $image = ''; 
            }    
        }

        return $image;
    }
    public static function getImage($file_name)
    {
        return  asset(Storage::url($file_name));   
    }


    public static function numberFormat($value)
    {
        return number_format($value,2);
    }

  
}
