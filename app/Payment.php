<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use DB;

class Payment extends Model
{
    public function __construct()
    {
        date_default_timezone_set("Asia/Karachi");
    }


    public function storeAdvancePayment($data)
    {
        return DB::transaction(function() use ($data) {

            $user_id      = 0;
            $total_amount = 0;


            $payment = new Payment;


            $payment->user_id   = $data['user'];

            $payment->amount    = $data['amount'];

            $payment->status    = 'Unpaid';

            $payment->date      = $data['date'];

            $payment->save(); 

            $payment_history = new PaymentHistory;

            $payment_history->storePaymentHistory([

                'debit'         => $data['amount'],
                'credit'        => 0,
                'order_receive' => 0,
                'order_detail'  => $data['order_detail'],

                'payment'       => $payment->id,
                'payment_type'  => 'Advance Production Amount',

            ]);

         



            return with($payment);
        });
    }

    public function bulkStorePayment($data)
    {
        return DB::transaction(function() use ($data) {

            $user_id      = 0;
            $total_amount = 0;


            $payment = new Payment;

            $order_receive = new OrderReceive;

            // $payment->user_id   = $data['user'];

            // $payment->amount    = $data['amount'];

            $payment->status    = 'Unpaid';


            if(isset($data['previous_advance']))
            {
                $payment->previous_advance = $data['previous_advance'];

                $payment->current_advance = $data['advance_amount'];

                $payment->balance_advance = ($payment->previous_advance - $payment->current_advance);

            }

            if(isset($data['previous_pocket']))
            {
                $payment->previous_pocket = $data['previous_pocket'];

                $payment->current_pocket = $data['pocket_money'];

                $payment->balance_pocket = ($payment->previous_pocket - $payment->current_pocket);

            }


            $payment->date      = $data['date'];

            $payment->save(); 

            foreach ($data['order_receive'] as $rows) {

                $curr_order_receive = $order_receive->getOrderReceiveById($rows);

                if(isset($curr_order_receive->id) && $curr_order_receive->status == "Payable")
                {   
                    $user_id = $curr_order_receive->orderDetail->user_id;

                    $total_amount += $curr_order_receive->production_cost;

                    // $total_amount += $curr_order_receive->bonus_cost; 

                    $curr_order_receive->status = 'Paid';

                    $curr_order_receive->update();

                    if($curr_order_receive->production_cost > 0)
                    {
                        $payment_history = new PaymentHistory;

                        $payment_history->storePaymentHistory([

                            'debit'         => $curr_order_receive->production_cost,
                            'credit'        => 0,
                            'order_receive' => $curr_order_receive->id,
                            'order_detail'  => $curr_order_receive->order_detail_id,

                            'payment'       => $payment->id,
                            'payment_type'  => 'Production Amount',

                        ]);
                    }
                    

                    // if($curr_order_receive->bonus_cost > 0)
                    // {
                    //     $payment_history = new PaymentHistory;

                    //     $payment_history->storePaymentHistory([

                    //         'debit'         => $curr_order_receive->bonus_cost,
                    //         'credit'        => 0,
                    //         'order_receive' => $curr_order_receive->id,
                    //         'payment'       => $payment->id,
                    //         'payment_type'  => 'Bonus Amount',

                    //     ]);
                    // }


                }

            }


            // ADVANCE AMOUNT
            if(isset($data['advance_amount']) && $data['advance_amount'] > 0)
            {
                $total_amount-= $data['advance_amount'];

                $payment_history = new PaymentHistory;

                $payment_history->storePaymentHistory([

                    'debit'         => 0,
                    'credit'        => $data['advance_amount'],
                    'order_receive' => 0,
                    'payment'       => $payment->id,
                    'payment_type'  => 'Advance Amount',

                ]);
            }


            // ADVANCE AMOUNT
            if(isset($data['pocket_money']) && $data['pocket_money'] > 0)
            {
                $total_amount-= $data['pocket_money'];

                $payment_history = new PaymentHistory;

                $payment_history->storePaymentHistory([

                    'debit'         => 0,
                    'credit'        => $data['pocket_money'],
                    'order_receive' => 0,
                    'payment'       => $payment->id,
                    'payment_type'  => 'Pocket Money',

                ]);
            }

            if(isset($data['advance_production']) && @count($data['advance_production']) > 0)
            {
                foreach ($data['advance_production'] as $key => $value) {
                    

                    if($value > 0)
                    {
                        $total_amount -= $value;

                        $payment_history = new PaymentHistory;

                        $payment_history->storePaymentHistory([

                            'debit'         => 0,
                            'credit'        => $value,
                            'order_receive' => $data['advance_order_receive'][$key],
                            'order_detail' => $data['advance_order_detail'][$key],

                            'payment'       => $payment->id,
                            'payment_type'  => 'Advance Production Amount',

                        ]);
                    }
                }
                
            }
            $payment->user_id = $user_id;
            $payment->amount  = $total_amount;

            $payment->update();

            return with($payment);
        });
    }
    
    public function storePayment($data)
    {
    	return DB::transaction(function() use ($data) {

    		$payment = new Payment;

    		$payment->user_id 	= $data['user'];

    		$payment->amount 	= $data['amount'];

    		$payment->status 	= $data['status'];

    		$payment->date  	= $data['date'];

    		$payment->save();

    		return with($payment);
    	});
    }

    public function updatePayment($data)
    {
    	return DB::transaction(function() use ($data) {

    		$payment = Payment::find($data['Payment']);

    		if(isset($payment->id))
    		{
    			$payment->user_id 	= $data['user'];

    			$payment->amount 	= $data['amount'];

    			$payment->status 	= $data['status'];

    			$payment->date  	= $data['date'];

	    		$payment->update();
    		}

    		return with($payment);
    	});
    }

    public function removePayment($id)
    {

    	return DB::transaction(function() use ($id) {

    		$payment = Payment::find($id);

    		if(isset($payment->id))
    		{
    			foreach($payment->paymentHistories as $rows)
    			{
                    $order_receive = $rows->OrderReceive;

                    if(isset($order_receive->id))
                    {
                        $order_receive->status = 'Payable';

                        $order_receive->update();
                    }

    				$rows->delete();
    			}

    			$payment->delete();

    			return with(true);
    		}

    		return with(false);
    	});
    }

    public function getPaymentById($id)
    {
    	return Payment::find($id);
    }


    

    public function getAdvanceProductionPayments()
    {
        return Payment::whereHas('paymentHistories',function($query) {

           
                $query->whereIn('payment_type',['Advance Production Amount'])->where('order_receive_id',0);
        })->orderBy('id','Asc')->get();
    }

    public function getAllPayments($payment_type = [])
    {
    	return Payment::whereHas('paymentHistories',function($query) use ($payment_type){

            if(@count($payment_type) > 0)
            {
                $query->whereIn('payment_type',$payment_type);
            }
        })->orderBy('id','Asc')->get();
    }

    public function user()
    {
    	return $this->belongsTo(User::class,'user_id')->withDefault();
    }


    public function orderDetail()
    {
        return $this->belongsTo(orderDetail::class,'order_detail_id')->withDefault();
    }

    public function paymentHistories()
    {
    	return $this->hasMany(PaymentHistory::class,'payment_id')->orderBy('id','ASC');
    }
}
