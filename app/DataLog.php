<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use DB;

class DataLog extends Model
{
    public function __construct()
    {
        date_default_timezone_set("Asia/Karachi");
    }
    
    public function storeDataLog($data)
    {
    	return DB::transaction(function() use ($data) {

    		$data_log = new DataLog;

    		$data_log->generic_id = $data['generic'];

            $data_log->type = $data['type'];

            $data_log->status = $data['status'];

            $data_log->description = $data['description'];

            $data_log->created_by  = \Auth::user()->id;

            $data_log->save();

            return with($data_log);
            
        });
    }

    public function getLogByGenericWithType($generic_id,$type)
    {
        return DataLog::where(['generic_id' => $generic_id, 'type' => $type])->orderBy('id','ASC')->get();
    }


    public function createdBy()
    {
        return $this->belongsTo(User::class,'created_by')->withDefault();
    }
}
