<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductionRate extends Model
{
    public function department()
    {
    	return $this->belongsTo(Department::class,'department_id')->withDefault();
    }

    public function getProductionRate($product_id,$department_id)
    {
    	return ProductionRate::where(['product_id' => $product_id, 'department_id' => $department_id])->first();
    }
}
