<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class ProductVariation extends Model
{
    public function __construct()
    {
        date_default_timezone_set("Asia/Karachi");
    }
    
    public function storeProductVariation($data)
    {
    	return DB::transaction(function() use ($data) {

    		$product_variation = new ProductVariation;

            $product_variation->product_id  = $data['product'];
            $product_variation->order_id    = $data['order'];
            $product_variation->color       = $data['color'];
    		$product_variation->size        = $data['size'];
            $product_variation->quantity    = (double)$data['quantity'];

    		$product_variation->save();

    		return with($product_variation);
    	});
    }

    public function updateProductVariation($data)
    {
    	return DB::transaction(function() use ($data) {

    		$product_variation = ProductVariation::find($data['product_variation']);

    		if(isset($product_variation->id))
    		{
    			$product_variation->name  = $data['name'];
    			$product_variation->value = $data['value'];

    			$product_variation->update();
    		}

    		return with($product_variation);
    	});
    }

    public function removeProductVariation($id)
    {

    	return DB::transaction(function() use ($id) {

    		$product_variation = ProductVariation::find($id);

    		if(isset($product_variation->id))
    		{

    			$product_variation->delete();

    			return with(true);
    		}

    		return with(false);
    	});
    }

    public function getProductVariationById($id)
    {
    	return ProductVariation::find($id);
    }

    public function getAllProductVariations()
    {
    	return ProductVariation::orderBy('id','Asc')->get();
    }

    public function product()
    {
        return $this->belongsTo(Product::class,'product_id')->withDefault();
    }


    public function isOrderReceive($id)
    {
        return OrderReceive::whereHas('orderDetail',function($query) use ($id){

            $query->where('product_variation_id',$id);

        })->sum('quantity');
    }
}
