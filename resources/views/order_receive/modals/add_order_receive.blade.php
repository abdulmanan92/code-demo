@php($module = 'Receive From Production')

@php($uniq_id = uniqid() )
<div class="modal modal-blur fade" id="md_add_order_receive" tabindex="-1" role="dialog" aria-hidden="true">
      <div class="modal-dialog modal-xl" role="document">
        <div class="modal-content">
           {{ Form::open(array('route' => 'store.workorderreceive','id'=>'form_add_order_receive', 'enctype' => 'multipart/form-data')) }}
          <div class="modal-header">
            <h4 class="modal-title">{{$module}}</h4>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">×</span>
              </button>
          </div>
          <div class="modal-body">
              <div class="row">
              <div class="col-sm-12 col-md-12 col-lg-12 mt-1">
                @php($label = 'Date')
                @php($name = 'date')
                <label class="form-label" >{{$label}} <i style = "color:red;">*</i></label>
                <small style = "color:red;" class="form-errors float-right req" value="*"></small>
                <input  required="" type="datetime-local" name="{{$name}}" value="{{ old($name) }}" class = "input-group mb-3 form-control" placeholder = "{{$label}}" aria-label = "Default" aria-describedby = "inputGroup-sizing-default">
              </div>

               <div class="col-sm-12 col-md-12 col-lg-12 mt-1 mb-2">

               @php($label = 'Select Order Detail / Lot')
                @php($name = 'order_detail')
                <label class="form-label" >{{$label}} <i style = "color:red;">*</i></label>
                <small style = "color:red;" class="form-errors float-right req" value="*"></small>
                <select onchange="orderDetailInfo(event,this,'{{$uniq_id}}');" required="" class=" form-control fstdropdown-select"  data-placeholder="Select Order Detail / Lot"  name="{{$name}}">
                  <option value="{{encrypt(0)}}">Select Order Detail / Lot </option> 
                  @foreach($production->productionDetail as $rows)

                  @if($rows->orderReceive->sum('quantity') < $rows->quantity)
                    <option value="{{encrypt($rows->id)}}">Order No:{{$production->order->order_no}} | {{$production->department->name}} @if($rows->user_id > 0) > {{$rows->user->name}} | @endif  Product:{{$rows->productVariation->product->name}} | Color:{{$rows->productVariation->color}} | Size:{{$rows->productVariation->size}}</option>
                    @endif
                  @endforeach
                </select>
              </div> 

              <div class="col-sm-12 col-md-12 col-lg-12 mt-1">
                @php($label = 'Quantity Issued')
                @php($name = 'available_quantity')
                <label class="form-label" >{{$label}}</label>
                <small style = "color:red;" class="form-errors float-right req" value="*"></small>
                <input readonly="" id="available_quantity_{{$uniq_id}}"  required="" type="number" name="{{$name}}" value="{{ old($name) }}" class = "input-group mb-3 form-control" placeholder = "{{$label}}" aria-label = "Default" aria-describedby = "inputGroup-sizing-default">
              </div>

              <div class="col-sm-12 col-md-12 col-lg-12 mt-1">
                @php($label = 'Received Quantity')
                @php($name = 'quantity')
                <label class="form-label" >{{$label}} <i style = "color:red;">*</i></label>
                <small style = "color:red;" class="form-errors float-right req" value="*"></small>
                <input id="quantity_{{$uniq_id}}"  required="" type="number" name="{{$name}}" value="{{ old($name) }}" class = "input-group mb-3 form-control" placeholder = "{{$label}}" aria-label = "Default" aria-describedby = "inputGroup-sizing-default">
              </div>

              
              

          </div>
          </div>
        
          <div class="modal-footer">
            <input type="hidden" name="production" value="{{encrypt($production->id)}}">
            <button  class="btn btn-danger" data-dismiss="modal">
              Close
            </button>
            <button type="submit" class="btn btn-success ">
              Submit
            </button>
          </div>
        </div>
    {{ Form::close() }}

      </div>
    </div>


    <script type="text/javascript">
    setFstDropdown();
      
    </script>