@php($uniq_id = uniqid() )
<div class="modal modal-blur fade" id="md_update_order_receive" tabindex="-1" role="dialog" aria-hidden="true">
      <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
           {{ Form::open(array('route' => 'update.workorderreceive','id'=>'form_update_order_receive', 'enctype' => 'multipart/form-data')) }}
          <div class="modal-header">
            <h4 class="modal-title">Update Order Receive</h4>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">×</span>
              </button>
          </div>
          <div class="modal-body">
              <div class="row">
              <div class="col-sm-12 col-md-12 col-lg-12 mt-1">
                @php($label = 'Date')
                @php($name = 'date')
                <label class="form-label" >{{$label}} <i style = "color:red;">*</i></label>
                <small style = "color:red;" class="form-errors float-right req" value="*"></small>
                <input value="{{FL::datePhpFormat($order_receive->date)}}"  required="" type="date" name="{{$name}}" value="{{ old($name) }}" class = "input-group mb-3 form-control" placeholder = "{{$label}}" aria-label = "Default" aria-describedby = "inputGroup-sizing-default">
              </div>

              <div class="col-sm-12 col-md-12 col-lg-12 mt-1">
                @php($label = 'Quantity')
                @php($name = 'quantity')
                <label class="form-label" >{{$label}} <i style = "color:red;">*</i></label>
                <small style = "color:red;" class="form-errors float-right req" value="*"></small>
                <input value="{{$order_receive->quantity}}"  required="" type="number" name="{{$name}}" value="{{ old($name) }}" class = "input-group mb-3 form-control" placeholder = "{{$label}}" aria-label = "Default" aria-describedby = "inputGroup-sizing-default">
              </div>

              
              

          </div>
          </div>
        
          <div class="modal-footer">
            <input type="hidden" name="order" value="{{encrypt($order_receive->order_id)}}">

            <input type="hidden" name="order_receive" value="{{encrypt($order_receive->id)}}">

            <button  class="btn btn-danger" data-dismiss="modal">
              Close
            </button>
            <button type="submit" class="btn btn-success ">
              Update
            </button>
          </div>
        </div>
    {{ Form::close() }}

      </div>
    </div>