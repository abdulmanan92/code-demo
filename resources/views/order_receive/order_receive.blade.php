@extends('layouts.master')

@php($module = 'Receive From Production')
@section('title')
{{$module}}
@endsection


@section('header')
<div class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h1 class="m-0">{{$module}}</h1>
      </div><!-- /.col -->
      <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
          <li class="breadcrumb-item"><a href="{{url('home')}}">Dashboard</a></li>

          <li class="breadcrumb-item"><a href="{{route('workorders')}}">Order No. {{$production->order->order_no}}</a></li>

          <li class="breadcrumb-item active">{{$module}}</li>
        </ol>
      </div><!-- /.col -->
    </div><!-- /.row -->
  </div><!-- /.container-fluid -->
</div>
@endsection

@section('content')

<div class="row mx-1">
   <div class="col-lg-4 col-12">
            <!-- small box -->
      <div class="small-box bg-primary">
        <div class="inner">
          <h3>{{$production->total_quantity}}</h3>

          <p>Quantity in Work Order</p>
        </div>
        <div class="icon">
          <i class="ion ion-bag"></i>
        </div>
        <a href="#" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
      </div>
    </div>

     <div class="col-lg-4 col-12">
            <!-- small box -->
      <div class="small-box bg-success">
        <div class="inner">
          <h3>{{$order_receive_list->sum('quantity')}}</h3>

          <p>Quantity in Receiving</p>
        </div>
        <div class="icon">
          <i class="ion ion-bag"></i>
        </div>
        <a href="#" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
      </div>
    </div>

     <div class="col-lg-4 col-12">
            <!-- small box -->
      <div class="small-box bg-danger">
        <div class="inner">
          <h3>{{$production->total_quantity - $order_receive_list->sum('quantity')}}</h3>

          <p>Outstanding Quantity</p>
        </div>
        <div class="icon">
          <i class="ion ion-bag"></i>
        </div>
        <a href="#" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
      </div>
    </div>


	<div class="col-sm-12 col-md-12 col-lg-12">
			<div class="card">
          <div class="card-header">
            <h3 class="card-title">Manage {{$module}}</h3>
            <div class="card-tools">
              @if(Auth::user()->hasAnyPermission(['All','Receive Production']))

                @if(($production->total_quantity - $order_receive_list->sum('quantity')) > 0)
                <button  type="button" class="btn btn-block btn-success float-right" data-toggle="modal" data-target="#md_add_order_receive"><i class="fa fa-plus mr-1"></i> <b>{{$module}}</b></button>
                @endif
              @endif
            </div>
            
          </div>
          @if(Auth::user()->hasAnyPermission(['All','View Receive Production']))

          <!-- /.card-header -->
          <div class="card-body">
            <div  id="tableCustomFilters" class="row mt-1 mb-4"></div>

            <table id="my-datatable" class="table  table-sm table-bordered  text-center">
              <thead class="bg-primary">
                <tr>
                  <th style="width: 10px">#</th>
                  <th>Date</th>

                  <th>Labor</th>
                  <th>Product</th>


                  <th>Size</th>
                  <th>Color</th>
                  <th>Received Quantity</th>
                  <th>Created At</th>
                  <th>Action</th>
                </tr>
              </thead>
              <tbody>
              	 @foreach($order_receive_list->sortByDesc('id') as $rows)
                 <tr>
                   <td>{{$loop->iteration}} - {{$rows->id}}</td>
                   <td>{{FL::dateFormatWithTime($rows->date)}}</td>
                   
                   <td>{{($rows->orderDetail->user->name)}}</td>

                   <td>{{($rows->orderDetail->productVariation->product->name)}}</td>


                   <td>{{($rows->orderDetail->productVariation->size)}}</td>
                   <td>{{($rows->orderDetail->productVariation->color)}}</td>


                   <td>{{($rows->quantity)}}</td>

                   <td>{{FL::dateFormatWithTime($rows->created_at)}}</td>
                   <td style="width: 25%;">

                    @if(true)
              @if(Auth::user()->hasAnyPermission(['All','Remove Receive Production']) && @count($rows->paymentHistories) == 0)
                      

                       <a href="{{route('removeworkorderreceive',['id' => encrypt($rows->id)])}}" class="btn btn-sm btn-outline-danger"><i class="fa fa-trash-alt mr-1"></i>Remove</a>
                       @endif
                     @endif

                   </td>
                 </tr>
                 @endforeach
              </tbody>
            </table>
          </div>
          @endif
      
        </div>
	</div>
</div>
   
@endsection

@section('modals')
  @include('order_receive.modals.add_order_receive')

  <div id="bind_md_update_order_receive"></div>

@endsection



@section('scripts')
<script type="text/javascript">

  function orderDetailInfo(event,obj,key_pair)
  {
    var data_set = {order_detail:$(obj).val()};

    $.get("{{route('orderdetailinfo')}}",data_set,function(data)
    {
        $('#available_quantity_'+key_pair).val(data.remaining);

        if(data.is_partial > 0)
        {
            $('#quantity_'+key_pair).removeAttr('readonly');

            $('#quantity_'+key_pair).attr('min',0);
            $('#quantity_'+key_pair).attr('max',data.remaining);

            $('#quantity_'+key_pair).val(0);


        }
        else
        {
            $('#quantity_'+key_pair).attr('min',0);
            $('#quantity_'+key_pair).attr('max',data.remaining);
            $('#quantity_'+key_pair).attr('readonly',true);

            $('#quantity_'+key_pair).val(data.remaining);
        }

    });
  }


  $(function(){

      $('document').ready(function(){

             $('#my-datatable').DataTable({
              pageLength:50,
              
              "columnDefs": [
                { "type": "numeric-comma", targets: 0 }
            ],

    initComplete: function () {

       var api = this.api();


        



  
            createSearchableDropDown('#tableCustomFilters',api,2,'Select Labour.','col-sm-3');

            createSearchableDropDown('#tableCustomFilters',api,3,'Select Product','col-sm-3');


            createSearchableDropDown('#tableCustomFilters',api,4,'Select Size','col-sm-2');

            createSearchableDropDown('#tableCustomFilters',api,5,'Select Color','col-sm-2');

            createSearchableDropDown('#tableCustomFilters',api,6,'Receive Quantitiy','col-sm-2');

           
    }
  });
  // END
      });

    });
</script>
@endsection
