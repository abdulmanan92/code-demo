@extends('layouts.master')

@php($module = 'Create Production')
@section('title')
{{$module}}
@endsection


@section('header')
<div class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h1 class="m-0">{{$module}}</h1>
      </div><!-- /.col -->
      <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
          <li class="breadcrumb-item"><a href="{{url('home')}}">Dashboard</a></li>
          
          <li class="breadcrumb-item active">{{$module}}</li>
        </ol>
      </div><!-- /.col -->
    </div><!-- /.row -->
  </div><!-- /.container-fluid -->
</div>
@endsection

@section('content')

<div class="row mx-1">
  <div class="col-sm-12 col-md-12 col-lg-12">
      <div class="card">
         {{ Form::open(array('route' => 'store.workorderproduction','id'=>'form_add_production', 'enctype' => 'multipart/form-data')) }}
          <div class="card-header">
            <h3 class="card-title">{{$module}}</h3>
        
            
          </div>
          <!-- /.card-header -->
          <div class="card-body">
          <div class="row">
              <div class="col-sm-12 col-md-6 col-lg-6 mt-1 mb-1">
                @php($label = 'Select Order No.')
                @php($name = 'order')
                <label class="form-label" >{{$label}} <i style = "color:red;"></i></label>
                <small style = "color:red;" class="form-errors float-right req" value="*"></small>
                <select onchange="showOrderProductVariant(event,this);" name="{{$name}}" class="form-control-sm form-control">
                  @if(isset($production->id))
                    <option value="{{encrypt($production->order_id)}}">{{$production->order->order_no}}</option>
                  @else
                    <option value="{{encrypt(0)}}">Select Order No.</option>

                    @foreach($order_list as $rows)
                      <option value="{{encrypt($rows->id)}}">{{$rows->order_no}}</option>
                    @endforeach
                  @endif
                  
                </select>
              </div>

              <div class="col-sm-12 col-md-6 col-lg-6 mt-1 mb-2">
   
               @php($label = 'Select Department')
                @php($name = 'department')
                <label class="form-label" >{{$label}} <i style = "color:red;">*</i></label>
                <small style = "color:red;" class="form-errors float-right req" value="*"></small>
                @php($varient_route = route('departmentvariants'))
                <select id="department" onchange="getVariantTable(event,'{{$varient_route}}','{{encrypt('0')}}','{{encrypt('0')}}','.tb_show_production','html');" required=""  class="form-control form-control-sm"  data-placeholder="Select Product Variations" style="width: 100%;" name="{{$name}}">
                  
                  @foreach($department_list as $rows)
                    <option value="{{encrypt($rows->id)}}">{{$rows->name}}</option>
                  @endforeach
                </select>
                  @php($label = 'Do you want to create a lot?')
                @php($name = 'is_lot')
                <label style="display: none;" class="form-label mt-2" ><input onchange="getVariantTable(event,'{{$varient_route}}','{{encrypt('0')}}','{{encrypt('0')}}','.tb_show_production','html');" id="lot" type="checkbox" name="{{$name}}"> {{$label}} 
                </label>
              </div>

            

        
             



              
              

          </div>
          @isset($production->id)
          <input type="hidden" name="parent" value="{{encrypt($production->id)}}">
          @endif
           <div class="row" id="order_variant">
                
              </div>
          </div>
      <div class="card-footer">
          <button type="submit" class="btn  btn-success float-right">Submit</button>
      </div>
       {{ Form::close() }}
        </div>
  </div>
</div>
   
@endsection

@section('modals')

  <div id="bind_md_order_detail"></div>


@endsection


@section('scripts')
  @include('productions.scripts.production_script')


  <script type="text/javascript">
    $(document).ready(function(){

        showOrderProductVariant(event,'select[name=order]');
    });
  </script>
@endsection





