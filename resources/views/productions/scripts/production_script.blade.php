<script type="text/javascript">

  function getVariantTable(event,route,product_variant,order_detail,bind_table,type = "apend")
  {
    event.preventDefault();


    if(true)
    {
      if(type == 'apend')
      {
          var department = $('#department');
          var order      = $('#order');

          var lot = 0;

          if($('#lot').prop('checked'))
          {
            lot = 1;
            $('.lot_column').show();
          }
          else
          {
            $('.lot_column').hide();

          }

          var data_set = {department_id:department.val(),product_variant:product_variant,lot:lot,order_detail:order_detail};

          $.get(route,data_set,function(data)
          {
              if(type == "apend")
              {
                  $(bind_table).append(data);
              }
              else
              {
                  $(bind_table).html(data);

              }
          });
      }
      else
      {
         $(bind_table).html('');
      }
    }
    else
    {
        alert('Remaining quantity greater than 0');
    }

    

    
  }

  function addNewVariant(event,key_pair,route,bind_table)
  {
      getVariantTable(event,key_pair,route,bind_table);
  }

   function removeTableRow(obj)
  {
      $(obj).closest('tr').remove();
  }

  function isPartial(obj)
  {
     obj = $(obj);

     obj.next().val(0);

     if(obj.prop('checked'))
     {
        obj.next().val(1);
     }
  }

  function showOrderProductVariant(event,obj)
  {
       // event.preventDefault();

       var data_set = {order:$(obj).val()};

      $.get("{{route('loadordervariant')}}",data_set,function(data)
      {
        
          $('#order_variant').html(data.view);
      });
  }


  function calculateTotalAmount(obj)
  {
      obj = $(obj);

      var total_amount = 0;

      var amount = isDefaulter(obj.val());

      var table_obj = obj.closest('tbody');

      var tfooter   = table_obj.next();

      var total_qty = isDefaulter(tfooter.find('.total_qty').val());

      total_amount = verifyCalculationAmount($('.'+table_obj.attr('id')+' .amount_list'));

      var rem_qty = (total_qty - (total_amount - amount));

     
      if(amount > 0 && amount <= rem_qty)
      {
        rem_qty -= amount;

        tfooter.find('.total').html('Selected Quantity: '+(total_amount));
      }
      else
      {
         obj.val('');

         tfooter.find('.total').html('Selected Quantity: '+(total_amount - amount));
      }


      tfooter.find('.remaining').html('Remaining Quantity: '+(rem_qty));

  }


  function verifyCalculationAmount(table_obj)
  {
      var total_amount = 0;

      table_obj.each(function(index,value){

          total_amount += isDefaulter($(this).val()); 
      });

      return total_amount;
  }

</script>