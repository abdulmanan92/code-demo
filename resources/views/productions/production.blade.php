@extends('layouts.master')

@php($module = 'Production')
@section('title')
{{$module}}
@endsection


@section('header')
<div class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h1 class="m-0">{{$module}}</h1>
      </div><!-- /.col -->
      <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
          <li class="breadcrumb-item"><a href="{{url('home')}}">Dashboard</a></li>
          
          <li class="breadcrumb-item active">{{$module}}</li>
        </ol>
      </div><!-- /.col -->
    </div><!-- /.row -->
  </div><!-- /.container-fluid -->
</div>
@endsection

@section('content')

<div class="row mx-1">
	<div class="col-sm-12 col-md-12 col-lg-12">
			<div class="card">
          <div class="card-header">
            <h3 class="card-title">Manage {{$module}}</h3>
            <div class="card-tools">
              @if(Auth::user()->hasAnyPermission(['All','Add Production']))

              <a href="{{route('store.workorderproduction')}}"  class="btn btn-block btn-success float-right" ><i class="fa fa-plus mr-1"></i> <b>Create New Production</b></a>
              @endif
            </div>
            
          </div>
          <!-- /.card-header -->
          <div class="card-body">

            <div  id="tableCustomFilters" class="row mt-1 mb-4"></div>


            <table id="my-datatable" class="table table-bordered  text-center table-sm">
              <thead class="bg-primary">
                <tr>
                  <th style="width: 10px">#</th>
                  <th>Order # </th>
                  <th>Order Date</th>
                  <th>Department</th>
                 
                  <th>Total Qty</th>
                  <th>Received Qty</th>

                  <th>Created On </th>
                  <th>Action</th>
                </tr>
              </thead>
              <tbody>

              	 @foreach($production_list->sortByDesc('id') as $rows)

                 @php($rece_qty = $rows->orderReceive->sum('quantity'))
                  <tr >
                   <td>{{$loop->iteration}}</td>
                   <td>{{$rows->order->order_no}}</td>
                   <td>{{FL::dateFormat($rows->order->date)}}</td>

                   <td>{{$rows->department->name}}</td>
                   


                   <td>{{($rows->total_quantity)}}</td>
                   <td>{{$rece_qty}}</td>

                   <td>{{FL::dateFormatWithTime($rows->created_at)}}</td>
                   <td class="text-center">

                    <ul class="nav nav-pills ml-auto p-2">
                 
                  <li class="nav-item dropdown">
                    <a class="btn btn-xs btn-primary text-white dropdown-toggle" data-toggle="dropdown" href="#">
                      Actions <span class="caret"></span>
                    </a>
                    <div class="dropdown-menu dropdown-menu-right">

                    @if(Auth::user()->hasAnyPermission(['All','Production Detail']))

                      @php($route = route('detailorder', ['id' =>  encrypt($rows->id)]))

                      <a href="#" onclick="onFetchFormModal(event,'{{$route}}','#md_order_detail','#bind_md_order_detail');" class="dropdown-item" tabindex="-1" href="#">Production Detail</a>

                      @endif

              @if(Auth::user()->hasAnyPermission(['All','Receive Production']))

                      @if($rece_qty < $rows->total_quantity)
                        @php($route = route('store.workorderreceive', ['production_id' =>  encrypt($rows->id)]))
                        <div class="dropdown-divider"></div>

                         <a href="#" onclick="onFetchFormModal(event,'{{$route}}','#md_add_order_receive','#bind_md_add_order_receive');" class="dropdown-item" tabindex="-1" href="#">Receive Production</a>
                         @endif
                         @endif
                       
              @if(Auth::user()->hasAnyPermission(['All','View Receive Production']))
                      
                        @php($route = route('workorderreceive', ['id' =>  encrypt($rows->id)]))
                        <div class="dropdown-divider"></div>
                        <a href="{{$route}}" class="dropdown-item" tabindex="-1" href="#">View Receive Production</a>
                        @endif

                      @if($rece_qty > 0 && $rows->verifyParent($rows->id) == 0)
                        @php($route = route('store.nextproduction', ['id' =>  encrypt($rows->id)]))
                        <div class="dropdown-divider"></div>
                        <a href="{{$route}}"  class="dropdown-item" tabindex="-1" href="#">Move Next Production</a>
                      @endif

                      @if($rows->parent_id > 0)
                        @php($route = route('update.nextproduction', ['id' =>  encrypt($rows->id)]))
                      @else
                        @php($route = route('update.workorderproduction', ['id' =>  encrypt($rows->id)]))
                      @endif

              @if(Auth::user()->hasAnyPermission(['All','Edit Production']))

                      <div class="dropdown-divider"></div>
                      <a href="{{$route}}"  class="dropdown-item" tabindex="-1" href="#">Edit</a>
                      @endif

              @if(Auth::user()->hasAnyPermission(['All','Remove Production']))

                      @if(!isset($rows->directParent->id))
                      <div class="dropdown-divider"></div>
                      <a onclick="removePopUp(event,this);" href="{{route('removeworkorderproduction',['id' => encrypt($rows->id)])}}" class="dropdown-item" tabindex="-1" href="#">Remove</a>
                      @endif
                      @endif
                    </div>
                  </li>
                </ul>

                

                   </td>
                 </tr>
                 @endforeach
              </tbody>
            </table>
          </div>
      
        </div>
	</div>
</div>
   
@endsection

@section('modals')
   <div id="bind_md_order_detail"></div>
   <div id="bind_md_add_order_receive"></div>


@endsection


@section('scripts')
<script type="text/javascript">

  function getVariantTable(event,route,product_variant,bind_table,type = "apend")
  {
    event.preventDefault();

    if(type == 'apend')
    {
        var department = $('#department');
        var order      = $('#order');

        var lot = 0;

        if($('#lot').prop('checked'))
        {
          lot = 1;
          $('.lot_column').show();
        }
        else
        {
          $('.lot_column').hide();

        }

        var data_set = {department_id:department.val(),product_variant:product_variant,lot:lot};

        $.get(route,data_set,function(data)
        {
            if(type == "apend")
            {
                $(bind_table).append(data);
            }
            else
            {
                $(bind_table).html(data);

            }
        });
    }
    else
    {
       $(bind_table).html('');
    }

    
  }

  function addNewVariant(event,key_pair,route,bind_table)
  {
      getVariantTable(event,key_pair,route,bind_table);
  }

   function removeTableRow(obj)
  {
      $(obj).closest('tr').remove();
  }

  function isPartial(obj)
  {
     obj = $(obj);

     obj.next().val(0);

     if(obj.prop('checked'))
     {
        obj.next().val(1);
     }
  }

  function orderDetailInfo(event,obj,key_pair)
  {
    var data_set = {order_detail:$(obj).val()};

    $.get("{{route('orderdetailinfo')}}",data_set,function(data)
    {
        $('#available_quantity_'+key_pair).val(data.remaining);

        if(data.is_partial > 0)
        {
            $('#quantity_'+key_pair).removeAttr('readonly');

            $('#quantity_'+key_pair).attr('min',0);
            $('#quantity_'+key_pair).attr('max',data.remaining);

            $('#quantity_'+key_pair).val(0);


        }
        else
        {
            $('#quantity_'+key_pair).attr('min',0);
            $('#quantity_'+key_pair).attr('max',data.remaining);
            $('#quantity_'+key_pair).attr('readonly',true);

            $('#quantity_'+key_pair).val(data.remaining);
        }

    });
  }

  function showOrderProductVariant(event,obj)
  {
       // event.preventDefault();

       var data_set = {order:$(obj).val()};

      $.get("{{route('loadordervariant')}}",data_set,function(data)
      {
        
          $('#order_variant').html(data.view);
      });
  }


      $(function(){

      $('document').ready(function(){

             $('#my-datatable').DataTable({
              pageLength:50,
              "columnDefs": [
                { "type": "numeric-comma", targets: 0 }
            ],

    initComplete: function () {

       var api = this.api();

  
            createSearchableDropDown('#tableCustomFilters',api,1,'Select Order No.','col-sm-3');

            createSearchableDropDown('#tableCustomFilters',api,3,'Select Department','col-sm-3');

            createSearchableDropDown('#tableCustomFilters',api,4,'Total Quantity','col-sm-3');

            createSearchableDropDown('#tableCustomFilters',api,5,'Receive Quantitiy','col-sm-3');

           
    }
  });
  // END
      });

    });

</script>
@endsection





