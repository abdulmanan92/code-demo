<tr>
	<input type="hidden" name="product_variant[]" value="{{encrypt($product_variant)}}">
	<input type="hidden" name="parent_order_detail[]" value="{{encrypt($order_detail)}}">


	
	<td>
		<select  class="form-control form-control-sm fstdropdown-select" name="user[]">
			
			<option value="{{encrypt(0)}}">Select Labour</option>

			@foreach($user_list as $rows)

				<option @if($rows->userOrderDetailVerification($rows->id,$rows->department_id) >= 2 )disabled="" @endif value="{{encrypt($rows->id)}}">{{$rows->name}} </option>
			@endforeach
		</select>
	</td>

	<td>
		<input min="1" required="" onkeyup="calculateTotalAmount(this);" type="number" name="quantity[]" placeholder="Quantity" class="form-control form-control-sm amount_list">
	</td>

	<td>
		<input required="" type="date" name="receive_date[]" placeholder="Quantity" class="form-control form-control-sm">
	</td>

	<td>
		

		<input onchange="isPartial(this);" type="checkbox" name="partial_check" >
		<input class="partial"  type="hidden" name="partial[]" value="0">
	</td>

	<td><button class="btn btn-sm btn-block btn-outline-danger" onclick="removeTableRow(this);">Remove</button></td>



</tr>

<script type="text/javascript">
    setFstDropdown();
	
</script>