@php($varient_route = route('departmentvariants'))

@foreach($product_variant_list as $rows)

    @php($tb_uniqid = uniqid())
	<div class="col-sm-12 my-4">
		<button onclick="getVariantTable(event,'{{$varient_route}}','{{encrypt($rows->id)}}','{{encrypt(0)}}','#tb_production_{{$tb_uniqid}}');" style="width: 35px;"  class="btn btn-block btn-outline-success btn-sm float-right my-2"><i class="fa fa-plus"></i></button>
		<table class="table text-center table-bordered table-sm table-hover">
			<thead class="bg-primary">
				<tr>
					<th>Product: {{$rows->product->name}}</th>
					<th>Quantity: {{$rows->quantity}}</th>

					<th>Size: {{$rows->size}}</th>
					<th>Color: {{$rows->color}}</th>
					<th class="text-center">
						
					</th>
				</tr>
			</thead>
			<thead class="bg-dark">
				<tr>
					<th  class="">Labour</th>
					<th>Quantity</th>
					<th>Rece. Date</th>
					<th>Partial</th>
					<th>Action</th>
				</tr>
			</thead>

			<tbody id="tb_production_{{$tb_uniqid}}" class="tb_show_production tb_production_{{$tb_uniqid}}">
				
			</tbody>

			<tfoot>
				<input type="hidden" class="total_qty" value="{{$rows->quantity}}">
				<tr>
					<th  class=""></th>
					<th class="total">Total Quantity: 0</th>
					<th class="remaining">Remaining:{{$rows->quantity}}</th>
					<th colspan="2"></th>
				</tr>
			</tfoot>
		</table>
	</div>
@endforeach
