@extends('layouts.master')

@php($module = 'Dashboard')
@section('title')
{{$module}}
@endsection


@section('header')
<div class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h1 class="m-0">{{$module}}</h1>
      </div><!-- /.col -->
      <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
          <li class="breadcrumb-item active">{{$module}}</li>
        </ol>
      </div><!-- /.col -->
    </div><!-- /.row -->
  </div><!-- /.container-fluid -->
</div>
@endsection

@section('content')

@php($hasPermission = ['All','Dashboard'])

@if(Auth::user()->hasAnyPermission($hasPermission))
<div class="row mx-2">
          <div class="col-lg-3 col-6">
            <!-- small box -->
            <div class="small-box bg-info">
              <div class="inner">
                <h3>{{@count($order_list)}}</h3>

                <p>Work Order</p>
              </div>
              <div class="icon">
                <i class="ion ion-bag"></i>
              </div>
              <a href="#" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
            </div>
          </div>
          <!-- ./col -->
          <div class="col-lg-3 col-6">
            <!-- small box -->
            <div class="small-box bg-success">
              <div class="inner">
                <h3>{{@count($production_list)}}</h3>
                

                <p>Productions</p>
              </div>
              <div class="icon">
                <i class="ion ion-stats-bars"></i>
              </div>
              <a href="#" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
            </div>
          </div>
          <!-- ./col -->
          <div class="col-lg-3 col-6">
            <!-- small box -->
            <div class="small-box bg-warning">
              <div class="inner">
                <h3>{{@count($user_list)}}</h3>

                <p>Labour Team</p>
              </div>
              <div class="icon">
                <i class="ion ion-person-add"></i>
              </div>
              <a href="#" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
            </div>
          </div>
          <!-- ./col -->
          <div class="col-lg-3 col-6">
            <!-- small box -->
            <div class="small-box bg-danger">
              <div class="inner">
                <h3>{{@count($department_list)}}</h3>
                

                <p>Departments</p>
              </div>
              <div class="icon">
                <i class="ion ion-pie-graph"></i>
              </div>
              <a href="#" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
            </div>
          </div>
          <!-- ./col -->

          <section class="col-lg-12 connectedSortable ui-sortable">
           <div class="card card-dark">
              <div class="card-header">
                <h3 align="center" class="card-title"><b>LATEST 50 - PRODUCTION FLOW STATUS</b></h3>

                <div class="card-tools">
                
                </div>
              </div>
              <div class="card-body" >
                <div class="table-responsive">
            <table id="" class="table table-sm table-bordered  text-center table-sm ">
              <thead class="bg-dark">
                <tr>
                  <th style="width: 10px">#</th>
                  <th>PRODUCT</th>
                
                    <th>LABOUR NAME </th>
                 
                    <th>IS LOT? </th>
                  <th>COLOR</th>
                  <th>SIZE</th>
                  <th>QUANTITY</th>
                  <th>RECEIVING DUE DATE </th>
                  
                </tr>
              </thead>
              <tbody>

                 @foreach($order_detail_list as $rows)

                 @php($rece_qty = $rows->receiveOrders->sum('quantity'))

                 @if($rece_qty < $rows->quantity)
 
                 <tr @if(date('Y-m-d') >= FL::datePhpFormat($rows->receive_date)) 
                    style="background:#E5AEA2;"
                  @endif
                  > 
                   <td>{{$loop->iteration}}</td>
                   <td>{{$rows->productVariation->product->name}}</td>
                   
                   <td>{{$rows->user->name}}</td>
                   <td>
                     @if($rows->is_lot > 0)
                        <span class="badge badge-success">Yes</span>
                     @else
                        <span class="badge badge-danger">No</span>
                     @endif
                   </td>
                   <td>{{$rows->productVariation->color}}</td>
                   <td>{{$rows->productVariation->size}}</td>
                   <td>{{$rows->quantity}}</td>
                   <td>{{FL::dateFormat($rows->receive_date)}}</td>
                   






                   
                 </tr>
                 @endif
                 @endforeach
              </tbody>
            </table>
            </div>
              </div>
              <!-- /.card-body -->
            </div>

          </section>

          <section class="col-lg-12 connectedSortable ui-sortable">
           <div class="card card-info">
              <div class="card-header">
                <h3 class="card-title"><b>LATEST 10 - ORDERS</b></h3>

                <div class="card-tools">
                
                </div>
              </div>
              <div class="card-body" style="height: 500px;">
                <div class="table-responsive">
              <table id="my-datatable" class="table table-bordered  text-center table-sm">
              <thead class="bg-info">
                <tr>
                  <th style="width: 10px">#</th>
                  <th>ORDER # </th>
                  <th>ORDER DATE</th>
                  <th>TOTAL QUANTITY</th>
                  <th>CREATED ON</th>
                </tr>
              </thead>
              <tbody>

                 @foreach($order_list->sortDesc()->take(10) as $rows)
                  <tr >
                   <td>{{$loop->iteration}}</td>
                   <td>{{$rows->order_no}}</td>
                   <td>{{FL::dateFormatWithTime($rows->date)}}</td>

                
                   <td>{{($rows->total_quantity)}}</td>

                   <td>{{FL::dateFormatWithTime($rows->created_at)}}</td>
             
                 </tr>
                 @endforeach
              </tbody>
            </table>
              
            </div>
              </div>
              <!-- /.card-body -->
            </div>

          </section>



          <section class="col-lg-12 connectedSortable ui-sortable">
           <div class="card card-success">
              <div class="card-header">
                <h3 class="card-title"><b>LATEST 10 - PRODUCTION</b></h3>

                <div class="card-tools">
                
                </div>
              </div>
              <div class="card-body" style="height: 500px;">
                <div class="table-responsive">
              <table id="my-datatable" class="table table-bordered  text-center table-sm">
              <thead class="bg-success">
                <tr>
                  <th style="width: 10px">#</th>
                  <th>ORDER #</th>
                  <th>ORDER DATE </th>
                  <th>DEPARTMENT</th>
                 
                  <th>TOTAL QUANTITY </th>
                  <th>RECEIVED QUANTITY</th>

                  <th>CREATED ON</th>
                </tr>
              </thead>
              <tbody>

                 @foreach($production_list->sortDesc()->take(10)  as $rows)

                 @php($rece_qty = $rows->orderReceive->sum('quantity'))
                  <tr >
                   <td>{{$loop->iteration}}</td>
                   <td>{{$rows->order->order_no}}</td>
                   <td>{{FL::dateFormat($rows->order->date)}}</td>

                   <td>{{$rows->department->name}}</td>
                   


                   <td>{{($rows->total_quantity)}}</td>
                   <td>{{$rece_qty}}</td>

                   <td>{{FL::dateFormatWithTime($rows->created_at)}}</td>
              
                 </tr>
                 @endforeach
              </tbody>
            </table>
              
            </div>
              </div>
              <!-- /.card-body -->
            </div>

          </section>


          <section class="col-lg-12 connectedSortable ui-sortable">
           <div class="card card-warning">
              <div class="card-header">
                <h3 class="card-title"><b>LATEST 10 - LABOUR'S DUE PAYMENTS</b></h3>

                <div class="card-tools">
                
                </div>
              </div>
              <div class="card-body" >
                <div class="table-responsive">
               <table id="my-datatable" class="table table-bordered  text-center table-sm">
              <thead class="bg-warning">
                <tr>

                  <th style="width: 10px">#</th>
           
                  <th>Date</th>
                  <th>Order # </th>
                  <th>Labour</th>
                  <th>Department</th>
                  <th>Product</th>
                  <th>Size</th>
                  <th>Color</th>
                  <th>Quantity</th>
                  <th>Rate</th>
                  <th>Bonus</th>
                </tr>
              </thead>
              <tbody>

                 @foreach($pending_list->sortDesc()->take(10) as $rows)
                  <tr>

                   <td>
       {{$loop->iteration}}
                   </td>
                   <td>{{FL::dateFormatWithTime($rows->date)}}</td>
                   <td>{{$rows->production->order->order_no}}</td>
                   <td>{{$rows->orderDetail->user->name}}</td>
                   <td>{{$rows->production->department->name}}</td>
                   <td>{{$rows->orderDetail->productVariation->product->name}}</td>
                   <td>{{$rows->orderDetail->productVariation->size}}</td>
                   <td>{{$rows->orderDetail->productVariation->color}}</td>
                   <td>{{$rows->quantity}}</td>
                   <td>{{FL::numberFOrmat($rows->production_cost)}}</td>
                   <td>{{FL::numberFOrmat($rows->bonus_cost)}}</td>

                
                 </tr>
                 @endforeach
              </tbody>
            </table>
              
            </div>
              </div>
              <!-- /.card-body -->
            </div>

          </section>

          <section class="col-lg-12 connectedSortable ui-sortable">
           <div class="card card-danger">
              <div class="card-header">
                <h3 class="card-title"><b>LATEST 10 - PAYROLL</b></h3>

                <div class="card-tools">
                
                </div>
              </div>
              <div class="card-body" >
                <div class="table-responsive">
                <table id="my-datatable" class="table table-bordered  text-center table-sm">
              <thead class="bg-danger">
                <tr>
                  <th style="width: 10px">#</th>
                  <th>PAYMENT SLIP  # </th>

                  <th>DATE</th>
                  <th>ORDER # </th>
                  <th>DEPARTMENT</th>
                  <th>LABOUR NAME </th>
                  <th>TOTAL AMOUNT </th>
                  <th>STATUS</th>
                </tr>
              </thead>
              <tbody>

                 @foreach($payment_list as $rows)

                  @php($department_list = $rows->paymentHistories->pluck('orderReceive.orderDetail.production.department.name')->unique()->toArray())
                  <tr>
                   <td>{{$loop->iteration}}</td>
                   <td><a target="_blank" href="{{route('invoicedoc',['id' => encrypt($rows->id)])}}">{{FL::generateNumber($rows->id)}}</a></td>

                   <td>{{FL::dateFormatWithTime($rows->date)}}</td>
                   <td>{{implode(' ',$rows->paymentHistories->pluck('orderReceive.orderDetail.production.order.order_no')->unique()->toArray())}}</td>

                   <td>@if(@count($department_list) > 0) {{implode(' ',$department_list)}} @else {{$rows->user->department->name}} @endif</td>
                   <td>{{$rows->user->name}}</td>
                   <td>{{FL::numberFormat($rows->amount)}}</td>
                   <td>{{$rows->status}}</td>

                  
                 </tr>
                 @endforeach
              </tbody>
            </table>
              
            </div>
              </div>
              <!-- /.card-body -->
            </div>

          </section>
        </div>
@endif
@endsection

@section('scripts')
<script>
  $(function () {
    
    //-------------
    //- BAR CHART -
    //-------------
    var barChartCanvas = $('#barChart').get(0).getContext('2d')
    var barChartData = $.extend(true, {}, areaChartData)
    var temp0 = areaChartData.datasets[0]
    var temp1 = areaChartData.datasets[1]
    barChartData.datasets[0] = temp1
    barChartData.datasets[1] = temp0

    var barChartOptions = {
      responsive              : true,
      maintainAspectRatio     : false,
      datasetFill             : false
    }

    new Chart(barChartCanvas, {
      type: 'bar',
      data: barChartData,
      options: barChartOptions
    });
	
	});


</script>
@endsection






