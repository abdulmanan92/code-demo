@extends('layouts.master')

@php($module = ucwords('Profile Of '.$user->name))
@section('title')
{{$module}}
@endsection


@section('header')
<div class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h1 class="m-0">{{$module}}</h1>
      </div><!-- /.col -->
      <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
          <li class="breadcrumb-item"><a href="{{url('home')}}">Dashboard</a></li>
          <li class="breadcrumb-item active">{{$module}}</li>
        </ol>
      </div><!-- /.col -->
    </div><!-- /.row -->
  </div><!-- /.container-fluid -->
</div>
@endsection

@section('content')

<div class="row mx-1">

    <div class="col-sm-12 col-md-3 col-lg-3">
      <div class="card card-primary card-outline">
              <div class="card-body box-profile">
                <div class="text-center">
                  <img class="profile-user-img img-fluid img-circle" onerror="this.src='{{asset("graphics/na-image.png")}}'" src="{{FL::getImage($user->dp)}}"  alt="User profile picture">
                </div>

                <h3 class="profile-username text-center">{{$user->name}}</h3>

                <p class="text-muted text-center">Department: {{$user->department->name}}</p>

                <ul class="list-group list-group-unbordered mb-3">
                  <li class="list-group-item">
                    <b>CNIC #: </b> <a class="float-right">{{$user->cnic}}</a>
                  </li>
                  <li class="list-group-item">
                  <b>Phone: </b> <a class="float-right">{{$user->contact}}</a></li>
                   <li class="list-group-item">
                    <b>Address<b>:</b></b> <a class="float-right">{{$user->address}}</a>
                  </li>
                  <li class="list-group-item">
                    <b>Date of Joining<b>:</b></b> <a class="float-right">{{FL::dateFormat($user->joining_date)}}</a>
                  </li>

                   <li class="list-group-item">
                    <b>Reference<b>:</b></b> <a class="float-right">{{$user->reference}}</a>
                  </li>

                   <li class="list-group-item">
                    <b>Payroll Group<b>:</b></b> <a class="float-right">{{$user->payroll_group}}</a>
                  </li>

                  

                  



                  @php($advance_list = $payment_list->whereIn('payment_type',['Advance Amount']))

                  @php($advance = $advance_list->sum('debit') - $advance_list->sum('credit')) 

                  @php($pocket_list = $payment_list->whereIn('payment_type',['Pocket Money']))

                  @php($pocket = $pocket_list->sum('debit') - $pocket_list->sum('credit') )


                  <li class="list-group-item">
                    <b>Advance Amount<b>:</b></b> <a class="float-right">{{FL::numberFormat($advance)}}</a>
                  </li>

                  <li class="list-group-item">
                    <b>Pocket Money<b>:</b></b> <a class="float-right">{{FL::numberFormat($pocket)}}</a>
                  </li>

                  <li class="list-group-item">
                    <b>Cnic Copy<b>:</b></b> <a class="float-right">
                      @if($user->cnic_copy > 0)
                        <i style="color: green;" class="fa fa-check"></i>
                      @else
                        <i style="color: red;" class="fa fa-times"></i>
                      @endif
                    </a>
                  </li>
                  <li class="list-group-item">
                    <b>Stamp Paper</b><b>:</b> <a class="float-right">
                      @if($user->stamp_paper > 0)
                        <i style="color: green;" class="fa fa-check"></i>
                      @else
                        <i style="color: red;" class="fa fa-times"></i>
                      @endif
                    </a>
                  </li>
                  <li class="list-group-item">
                    <b>Cheque<b>:</b></b> <a class="float-right">
                      @if($user->cheque > 0)
                        <i style="color: green;" class="fa fa-check"></i>
                      @else
                        <i style="color: red;" class="fa fa-times"></i>
                      @endif
                    </a>
                  </li>
                  <li class="list-group-item">
                    <b>Photo<b>:</b></b> <a class="float-right">
                      @if($user->photos > 0)
                        <i style="color: green;" class="fa fa-check"></i>
                      @else
                        <i style="color: red;" class="fa fa-times"></i>
                      @endif
                    </a>
                  </li>
                </ul>

                <a target="_blank" href="{{route('update.teammember',['id' => encrypt($user->id)])}}" class="btn btn-success btn-block"><b>Edit Profile</b></a>
              </div>
              <!-- /.card-body -->
            </div>
    </div>
  

    <div class="col-sm-12 col-lg-9 col-md-9">

      <div class="row">
        <div class="col-sm-12 c0l-lg-12 col-md-12">
          <div class="small-box bg-primary">
            <div class="inner">
              <h3>{{@count($user->documents)}}</h3>

              <p>Documents</p>
            </div>
            <div class="icon">
              <i class="fa fa-file"></i>
            </div>
            <a href="{{route('documents',['generic_id' => encrypt($user->id), 'generic_type' => encrypt('User')])}}" class="small-box-footer">Click Here <i class="fas fa-arrow-circle-right"></i></a>
          </div>
        </div>

        <div class="col-sm-12 c0l-lg-6 col-md-6" style="display: none;">
          <!-- small box -->
          <div class="small-box bg-warning">
            <div class="inner">
              <h3>{{@count($user->documents)}}</h3>

              <p>Production</p>
            </div>
            <div class="icon">
              <i class="fa fa-money-bill-wave-alt"></i>
            </div>
            <a href="{{route('documents',['generic_id' => encrypt($user->id), 'generic_type' => encrypt('User')])}}" class="small-box-footer">Click Here <i class="fas fa-arrow-circle-right"></i></a>
          </div>
        </div>


        <div class="col-sm-12 col-lg-12 col-md-12">
          <div class="card">

            <div class="card-header">
            <h3 class="card-title">Ledger History</h3>
            <div class="card-tools">
              
            </div>
            
          </div>

            <div class="card-body">

              <div class="table-responsive">
                 <table id="example1" class="table table-bordered table-sm  text-center">
                 
                  <thead class="bg-primary">
                    <tr>
                      <th style="width: 30px;">#</th>
                      <th>Date</th>
                      <th>Order No.</th>
                      <th>Department</th>
                      <th>Type</th>
                      <th>Debit</th>
                      <th>Credit</th>
                    </tr>
                  </thead>
                  <tbody>
                     

                     
                     @foreach($payment_list as $key => $rows)

                      @php($department = $rows->orderReceive->orderDetail->production->department->name)
                        <tr>
                       <td>{{$loop->iteration}}</td>
                       <td>{{FL::dateFormatWithTime($rows->payment->date)}}</td>
                       <td>{{$rows->orderReceive->orderDetail->production->order->order_no}}</td>

                       <td>@if(isset($department->id)) {{$department->id}} @else {{$rows->payment->user->department->name}} @endif</td>

                       @if(in_array($rows->payment_type,['Advance Amount','Pocket Money']) && $rows->debit > 0)
                          <td style="background: #EE878A;">{{$rows->payment_type}}</td>
                       @elseif(in_array($rows->payment_type,['Advance Amount','Pocket Money']) && $rows->credit > 0)
                          <td style="background: #7CAC7C;">{{$rows->payment_type}}</td>
                        @else
                       <td>{{$rows->payment_type}}</td>

                       @endif


                       <td>{{FL::numberFormat($rows->debit)}}</td>
                       <td>{{FL::numberFormat($rows->credit)}}</td>
                     </tr>
                     @endforeach
                  </tbody>
                  <tfoot style="font-weight: bolder;">
                    <tr>
                      <td></td>
                      <td class="bg-warning" colspan="2">Legend</td>

                      <td colspan="2" style="text-align: right;">Total</td>
                      <td>{{FL::numberFormat($payment_list->sum('debit'))}}</td>
                      <td>{{FL::numberFormat($payment_list->sum('credit'))}}</td>
                    </tr>
                    <tr>
                      @php($balance = $payment_list->sum('debit') - $payment_list->sum('credit'))
                      <td></td>
                      <td style="background: #EE878A;">+ Liability</td>
                      <td style="background: #7CAC7C;">- Liability</td>

                      <td colspan="2" style="text-align: right;">@if($balance > 0) Total Paid @else Total Payable @endif</td>
                      <td colspan="2">{{FL::numberFormat($balance)}}</td>
                    </tr>
                  </tfoot>
                  
                </table>
              </div>
            </div>
          </div>
        </div>

      </div>

      <!-- small box -->
      
    </div>

</div>
   
@endsection

@section('modals')

@endsection

@section('scripts')

@endsection



