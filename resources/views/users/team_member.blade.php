@extends('layouts.master')

@php($module = 'Labour Team')
@section('title')
{{$module}}
@endsection


@section('header')
<div class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h1 class="m-0">{{$module}}</h1>
      </div><!-- /.col -->
      <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
          <li class="breadcrumb-item"><a href="{{url('home')}}">Dashboard</a></li>
          <li class="breadcrumb-item active">{{$module}}</li>
        </ol>
      </div><!-- /.col -->
    </div><!-- /.row -->
  </div><!-- /.container-fluid -->
</div>
@endsection

@section('content')

<div class="row mx-1">
	<div class="col-sm-12 col-md-12 col-lg-12">
			<div class="card">
          <div class="card-header">
            <h3 class="card-title">Manage {{$module}}</h3>
            <div class="card-tools">
              @if(Auth::user()->hasAnyPermission(['All','Add Labour']))

              <button  type="button" class="btn btn-block btn-success float-right" data-toggle="modal" data-target="#md_add_user"><i class="fa fa-plus mr-1"></i> <b>Add New Labour</b></button>
              @endif
            </div>
            
          </div>
          <!-- /.card-header -->
          <div class="card-body">
            <div  id="tableCustomFilters" class="row mt-1 mb-4"></div>

            <table id="my-datatable" class="table table-bordered  table-sm text-center">
              <thead class="bg-primary">
                <tr>
                  <th style="width: 10px">#</th>
                  <th>Photo</th>
                  <th>Department</th>
                  <th>Name</th>
                  <th>Cnic No.</th>
                  <th>Phone No.</th>
                  <th>Payroll Group</th>
                  <th>Advance</th>
                  <th>Pocket</th>
                  <th>Status</th>

                  <th>Action</th>
                </tr>
              </thead>
              <tbody>
              	 @foreach($user_list as $rows)

                 @php($advance_and_pocket = $rows->getAdvanceAndPocketAmount($rows->id))
                 <tr>
                   <td>{{$loop->iteration}}</td>
                   <td>
                     <img class="profile-user-img  img-md img-circle" onerror="this.src='{{asset("graphics/na-image.png")}}'" src="{{FL::getImage($rows->dp)}}"  alt="User profile picture">
                   </td>
                   <td>{{$rows->department->name}}</td>
                   <td>{{$rows->name}}</td>
                   <td>{{$rows->cnic}}</td>
                   <td>{{$rows->contact}}</td>
                   <td>{{$rows->payroll_group}}</td>
                   <td>{{FL::numberFormat($advance_and_pocket[0])}}</td>
                   <td>{{FL::numberFormat($advance_and_pocket[1])}}</td>
                   <td>{{$rows->status}}</td>
                   <td style="width: 15%;">

                    @if(true)
                      @php($route = route('update.teammember', ['id' =>  encrypt($rows->id)]))

              @if(Auth::user()->hasAnyPermission(['All','View Profile']))

                       <a href="{{route('labourprofile',['id' => encrypt($rows->id)])}}" class="btn btn-sm btn-outline-info"><i class="fa fa-user mr-1"></i>Profile</a>
@endif
              @if(Auth::user()->hasAnyPermission(['All','Remove Labour']))

                      @if(@count($rows->orderDetails) == 0 && @count($rows->ledgers) == 0 && @count($rows->documents) == 0)
                       <a onclick="removePopUp(event,this);" href="{{route('removeuser',['id' => encrypt($rows->id)])}}" class="btn btn-sm btn-outline-danger"><i class="fa fa-trash-alt mr-1"></i>Remove</a>
                       @endif
                       @endif
                     @endif

                   </td>
                 </tr>
                 @endforeach
              </tbody>
            </table>
          </div>
      
        </div>
	</div>
</div>
   
@endsection

@section('modals')
  @include('users.modals.add_team_member')

  <div id="bind_md_update_user"></div>

@endsection

@section('scripts')
<script type="text/javascript">

 

  function addDocument(event,table_name)
  {
      event.preventDefault();

      var view = '<tr><td><input type="date" name="doc_date[]" class="form-control "></td><td><input required placeholder="Title" type="text" name="doc_title[]" class="form-control "></td><td><input style="padding: 2px;" type="file" required name="files[]" class="form-control "></td><td><button class="btn btn-sm btn-block btn-outline-danger" onclick="removeTableRow(this);">Remove</button></td></tr>';

      $(table_name).append(view);
  }


  function addPayment(event,table_name,user_id = 0)
  {
      event.preventDefault();

      var action = '';

      var view = '<tr><td><input required type="date" name="date[]" class="form-control "></td><td><select '+action+' class="form-control" name="payment_type[]"><option value="Advance Amount">Advance Amount</option><option value="Pocket Money">Pocket Money</option><option  value="Bonus Amount">Bonus Amount</option></select></td><td><input placeholder="Amount"  type="number" required name="amount[]" class="form-control "></td><td><button class="btn btn-sm btn-block btn-outline-danger" onclick="removeTableRow(this);">Remove</button></td></tr>';

      $(table_name).append(view);
  }

  function removeTableRow(obj)
  {
      $(obj).closest('tr').remove();
  }

   $(function(){

      $('document').ready(function(){

             $('#my-datatable').DataTable({
              pageLength:50,
              
              "columnDefs": [
                { "type": "numeric-comma", targets: 0 }
            ],

    initComplete: function () {

       var api = this.api();


       



  
            createSearchableDropDown('#tableCustomFilters',api,2,'Select Department.','col-sm-2');

            createSearchableDropDown('#tableCustomFilters',api,3,'Select Name','col-sm-2');

            createSearchableDropDown('#tableCustomFilters',api,4,'Select Cnic No.','col-sm-2');

            createSearchableDropDown('#tableCustomFilters',api,5,'Select Phone No. ','col-sm-2');


            createSearchableDropDown('#tableCustomFilters',api,6,'Payroll Group ','col-sm-2');

            createSearchableDropDown('#tableCustomFilters',api,9,'Select Status ','col-sm-2');




           
    }
  });
  // END
      });

    });
</script>
@endsection



