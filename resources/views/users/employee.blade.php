@extends('layouts.master')

@php($module = 'System User')
@section('title')
{{$module}}
@endsection


@section('header')
<div class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h1 class="m-0">{{$module}}</h1>
      </div><!-- /.col -->
      <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
          <li class="breadcrumb-item"><a href="{{url('home')}}">Dashboard</a></li>
          <li class="breadcrumb-item active">{{$module}}</li>
        </ol>
      </div><!-- /.col -->
    </div><!-- /.row -->
  </div><!-- /.container-fluid -->
</div>
@endsection

@section('content')

<div class="row mx-1">
	<div class="col-sm-12 col-md-12 col-lg-12">
			<div class="card">
          <div class="card-header">
            <h3 class="card-title">Manage {{$module}}</h3>
            <div class="card-tools">
              @if(Auth::user()->hasAnyPermission(['All','Add System User']))

              <button  type="button" class="btn btn-block btn-success float-right" data-toggle="modal" data-target="#md_add_user"><i class="fa fa-plus mr-1"></i> <b>Add System User</b></button>

              @endif
            </div>
            
          </div>
          <!-- /.card-header -->
          <div class="card-body">
            <div  id="tableCustomFilters" class="row mt-1 mb-4"></div>

            <table id="my-datatable" class="table-sm table table-bordered  text-center">
              <thead class="bg-primary">
                <tr>
                  <th style="width: 10px">#</th>
                  <th>Department</th>
                  <th>Name</th>
                  <th>Cnic No.</th>
                  <th>Phone No.</th>
                  <th>Email</th>
                  <th>Status</th>
                  <th>Created At</th>
                  <th>Action</th>
                </tr>
              </thead>
              <tbody>
              	 @foreach($user_list as $rows)
                 <tr>
                   <td>{{$loop->iteration}}</td>
                   <td>{{$rows->department->name}}</td>
                   <td>{{$rows->name}} (USR-000{{$rows->id}})</td>
                   <td>{{$rows->cnic}}</td>
                   <td>{{$rows->contact}}</td>
                   <td>{{$rows->email}}</td>
                   <td>{{$rows->status}}</td>
                   <td>{{FL::dateFormat($rows->created_at)}}</td>
                   <td style="width: 15%;">

                    @if(true)
                      @php($route = route('update.employee', ['id' =>  encrypt($rows->id)]))

              @if(Auth::user()->hasAnyPermission(['All','Edit System User']))

                       <button  onclick="onFetchFormModal(event,'{{$route}}','#md_update_user','#bind_md_update_user')" class="btn btn-sm btn-outline-info"><i class="fa fa-edit mr-1"></i>Edit</button>

                       @endif

              @if(Auth::user()->hasAnyPermission(['All','Remove System User']))


                       <a href="{{route('removeuser',['id' => encrypt($rows->id)])}}" class="btn btn-sm btn-outline-danger"><i class="fa fa-trash-alt mr-1"></i>Remove</a>

                       @endif
                     @endif

                   </td>
                 </tr>
                 @endforeach
              </tbody>
            </table>
          </div>
      
        </div>
	</div>
</div>
   
@endsection

@section('modals')
  @include('users.modals.add_employee')

  <div id="bind_md_update_user"></div>

@endsection


@section('scripts')
<script type="text/javascript">
  $(function(){

      $('document').ready(function(){

             $('#my-datatable').DataTable({
              pageLength:50,
              
              "columnDefs": [
                { "type": "numeric-comma", targets: 0 }
            ],

    initComplete: function () {

       var api = this.api();


       



  
            createSearchableDropDown('#tableCustomFilters',api,1,'Select Department.','col-sm-2');

            createSearchableDropDown('#tableCustomFilters',api,2,'Select Name','col-sm-2');

            createSearchableDropDown('#tableCustomFilters',api,3,'Select Cnic No.','col-sm-2');

            createSearchableDropDown('#tableCustomFilters',api,4,'Select Phone No. ','col-sm-2');

            createSearchableDropDown('#tableCustomFilters',api,5,'Select Email ','col-sm-2');

            createSearchableDropDown('#tableCustomFilters',api,6,'Select Status ','col-sm-2');






           
    }
  });
  // END
      });

    });
</script>
@endsection





