@extends('layouts.master')

@php($module = ucwords('Profile Of '.$user->name))
@section('title')
{{$module}}
@endsection


@section('header')
<div class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h1 class="m-0">{{$module}}</h1>
      </div><!-- /.col -->
      <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
          <li class="breadcrumb-item"><a href="{{url('home')}}">Dashboard</a></li>
          <li class="breadcrumb-item"><a href="{{route('labourprofile',['id' => encrypt($user->id)])}}">{{$module}}</a></li>

          <li class="breadcrumb-item active">{{$module}}</li>
        </ol>
      </div><!-- /.col -->
    </div><!-- /.row -->
  </div><!-- /.container-fluid -->
</div>
@endsection

@section('content')

<div class="row mx-1">
  <div class="col-sm-12 col-md-12 col-lg-12">
      <div class="card">
            {{ Form::open(array('route' => 'update.teammember','id'=>'form_update_user', 'enctype' => 'multipart/form-data')) }}

          <div class="card-header">
            <h3 class="card-title"> {{$module}}</h3>
            <div class="card-tools">
              
            </div>
            
          </div>
          <!-- /.card-header -->
          <div class="card-body">
             <div class="row">


                   <div class="col-sm-12 col-md-12 col-lg-12 mt-1 mb-2">
     
                 @php($label = 'Select Department')
                  @php($name = 'department')
                  <label class="form-label" >{{$label}} <i style = "color:red;"></i></label>
                  <small style = "color:red;" class="form-errors float-right req" value="*"></small>
                  <select class="form-control form-control-sm" name="{{$name}}">
                    
                    <option value="">Select Department</option>

                    @foreach($department_list as $rows)
                      <option @if($user->department_id == $rows->id) selected @endif value="{{encrypt($rows->id)}}">{{$rows->name}}</option>
                    @endforeach
                  </select>
               
                </div> 

                <div class="col-sm-12 col-md-6 col-lg-4 mt-1">
                  @php($label = 'Name')
                  @php($name = 'name')
                  <label class="form-label" >{{$label}} <i style = "color:red;">*</i></label>
                  <small style = "color:red;" class="form-errors float-right req" value="*"></small>
                  <input  required="" type="text" name="{{$name}}" value="{{ ($user->name) }}" class = "input-group mb-3 form-control form-control-sm" placeholder = "{{$label}}" aria-label = "Default" aria-describedby = "inputGroup-sizing-default">
                </div>

                <div class="col-sm-12 col-md-6 col-lg-4 mt-1">
                  @php($label = 'Cnic No.')
                  @php($name = 'cnic')
                  <label class="form-label" >{{$label}} <i style = "color:red;"></i></label>
                  <small style = "color:red;" class="form-errors float-right req" value="*"></small>
                  <input   type="text" name="{{$name}}" value="{{ ($user->cnic) }}" class = "input-group mb-3 form-control form-control-sm" placeholder = "{{$label}}" aria-label = "Default" aria-describedby = "inputGroup-sizing-default">
                </div>

                <div class="col-sm-12 col-md-6 col-lg-4 mt-1">
                  @php($label = 'Contact No.')
                  @php($name = 'contact_no')
                  <label class="form-label" >{{$label}} <i style = "color:red;"></i></label>
                  <small style = "color:red;" class="form-errors float-right req" value="*"></small>
                  <input  type="text" name="{{$name}}" value="{{ ($user->contact) }}" class = "input-group mb-3 form-control form-control-sm" placeholder = "{{$label}}" aria-label = "Default" aria-describedby = "inputGroup-sizing-default">
                </div>

                <div class="col-sm-12 col-md-12 col-lg-12 mt-1">
                  @php($label = 'Address')
                  @php($name = 'address')
                  <label class="form-label" >{{$label}} <i style = "color:red;"></i></label>
                  <small style = "color:red;" class="form-errors float-right req" value="*"></small>
                  <textarea   type="text" name="{{$name}}" value="{{ old($name) }}" class = "input-group mb-3 form-control form-control-sm" placeholder = "{{$label}}" aria-label = "Default" aria-describedby = "inputGroup-sizing-default">{{$user->address}}</textarea>
                </div>

                <div class="col-sm-12 col-md-6 col-lg-4 mt-1">
                  @php($label = 'Date Of Joining')
                  @php($name = 'date_of_joining')
                  <label class="form-label" >{{$label}} <i style = "color:red;"></i></label>
                  <small style = "color:red;" class="form-errors float-right req" value="*"></small>
                  <input   type="date" name="{{$name}}" value="{{ (FL::datePhpFormat($user->joining_date)) }}" class = "input-group mb-3 form-control form-control-sm" placeholder = "{{$label}}" aria-label = "Default" aria-describedby = "inputGroup-sizing-default">
                </div>

                <div class="col-sm-12 col-md-6 col-lg-4 mt-1">

                 @php($label = 'Reference')
                  @php($name = 'reference')
                  <label class="form-label" >{{$label}} <i style = "color:red;"></i></label>
                  <small style = "color:red;" class="form-errors float-right req" value="*"></small>
                  <input   type="text" name="{{$name}}" value="{{ ($user->reference) }}" class = "input-group mb-3 form-control form-control-sm" placeholder = "{{$label}}" aria-label = "Default" aria-describedby = "inputGroup-sizing-default">
                </div>



                
                   <div class="col-sm-12 col-md-6 col-lg-4 mt-1">
     
                 @php($label = 'Payroll Group')
                  @php($name = 'payroll_group')
                  <label class="form-label" >{{$label}} <i style = "color:red;"></i></label>
                  <small style = "color:red;" class="form-errors float-right req" value="*"></small>
                  <select class="form-control form-control-sm" name="{{$name}}">
                    
                    <option value="">Select Payroll</option>

                    @foreach(FL::getPayRollList() as $rows)
                      <option @if($rows == $user->payroll_group)  selected @endif value="{{$rows}}">{{$rows}}</option>
                    @endforeach
                  </select>
               
                </div> 

                <div class="col-sm-12 col-md-12 col-lg-12 mt-1">
                @php($label = 'Select Status')
                @php($name = 'status')
                <label class="form-label" >{{$label}} <i style = "color:red;"></i></label>
                <small style = "color:red;" class="form-errors float-right req" value="*"></small>
                <select name="{{$name}}"  class = "input-group mb-3 form-control form-control-sm" placeholder = "{{$label}}" aria-label = "Default" aria-describedby = "inputGroup-sizing-default">
                  @foreach(['Active','Deavtive'] as $rows)
                  <option @if($rows == $user->status) selected @endif value="{{$rows}}">{{$rows}}</option>
                  @endforeach
                </select>
              </div>

                

                <div class="col-sm-12 col-md-12 col-lg-12 mt-1">

                 @php($label = 'Photo')
                  @php($name = 'photo')
                  <label class="form-label" >{{$label}} <i style = "color:red;"></i></label>
                  <small style = "color:red;" class="form-errors float-right req" value="*"></small>
                  <input style="padding: 2px;"   type="file" name="{{$name}}" value="{{ old($name) }}" class = "input-group mb-3 form-control" placeholder = "{{$label}}" aria-label = "Default" aria-describedby = "inputGroup-sizing-default">
                </div>

                <div class="col-sm-12 col-md-12 col-lg-12">
                  <label class="form-label mr-2" >Check List: <i style = "color:red;"></i></label>

                  @foreach(FL::getLabourCheckList() as $rows)
                  <div class="form-check-inline">
                    <label class="form-check-label">
                      <input @if(isset($user[$rows]) && $user[$rows] == 1) checked @endif type="checkbox" name="{{$rows}}" class="form-check-input">{{ucwords(str_replace('_',' ',$rows))}} 
                    </label>
                  </div>
                  @endforeach
                </div>

                @if(Auth::user()->id == "1")
               <div class="col-sm-12 col-md-12 col-lg-12 mt-1">

                <h5 ><br><br><b>Unlock LOT LOCK for Specicific Time Period</b><br>
                  </h1> 
                  (By defaut system will not permit to issue production to any labour who already have 2 incomplete lots) 
                  </h1>
               </h4>
               </div>

                <div class="col-sm-12 col-md-6 col-lg-6 mt-1 mb-2">
                  @php($label = 'From')
                  @php($name = 'from')
                  <label class="form-label" >{{$label}} <i style = "color:red;"></i></label>
                  <small style = "color:red;" class="form-errors float-right req" value="*"></small>
                  <input   type="datetime-local" name="{{$name}}" value="{{FL::dateTimeFormat($user->from)}}" class = "input-group mb-3 form-control form-control-sm" placeholder = "{{$label}}" aria-label = "Default" aria-describedby = "inputGroup-sizing-default">
                </div>

                <div class="col-sm-12 col-md-6 col-lg-6 mt-1 mb-2">
                  @php($label = 'To')
                  @php($name = 'to')
                  <label class="form-label" >{{$label}} <i style = "color:red;"></i></label>
                  <small style = "color:red;" class="form-errors float-right req" value="*"></small>
                  <input   type="datetime-local" name="{{$name}}" value="{{FL::dateTimeFormat($user->to)}}" class = "input-group mb-3 form-control form-control-sm" placeholder = "{{$label}}" aria-label = "Default" aria-describedby = "inputGroup-sizing-default">
                </div>
                @endif

                <div class="col-sm-12 col-md-12 col-lg-12 mt-1">
                <button style="width: 125px;" onclick="addPayment(event,'#td_show_payments','{{encrypt($user->id)}}');" class="btn btn-block btn-outline-success btn-sm float-right">Add Payment</button>
              </div>
              <div class="col-sm-12 col-md-12 col-lg-12 mt-1">
                <div class="table-responsive">
                  <table class="table table-sm table-bordered">
                    <thead class="bg-primary">
                      <tr>
                        <th>Date <i style = "color:red; font-weight: bolder;">*</i></th>
                        <th>Payment Type <i style = "color:red; font-weight: bolder;">*</i></th>
                        <th>Amount <i style = "color:red; font-weight: bolder;">*</i></th>
                        <th>Action</th>
                      </tr>
                    </thead>
                    <tbody id="td_show_payments">
                      @foreach($user->payments as $payment)

                        @foreach($payment->paymentHistories as $rows)
                          <tr>
                        <input type="hidden" name="payment[]" value="{{encrypt($payment->id)}}">
                            
                            <td>
                              <input required value="{{FL::datePhpFormat($payment->date)}}" type="date" name="date[]" class="form-control ">
                            </td>
                            <td>
                              <select onchange="getBonusInfo(event,this,'{{encrypt($user->id)}}');" class="form-control" name="payment_type[]">
                                <option @if($rows->payment_type == "Bonus Amount") selected @endif value="Bonus Amount">Bonus Amount : {{$user->calculateBonus($user->id)}}</option>
                                <option @if($rows->payment_type == "Advance Amount") selected @endif value="Advance Amount">Advance Amount</option>
                                <option @if($rows->payment_type == "Pocket Money") selected @endif value="Pocket Money">Pocket Money</option>
                              </select>
                            </td>
                            <td>
                              <input value="{{$rows->debit}}" placeholder="Amount"  type="number" required name="amount[]" class="form-control " @if($rows->payment_type == "Bonus Amount") readonly  @endif ></td>
                              <td>
                                <button class="btn btn-sm btn-block btn-outline-danger" onclick="removeTableRow(this);">Remove</button>
                              </td>
                    </tr>
                          @endforeach
                      @endforeach
                    </tbody>
                  </table>
                </div>
                
              </div>

                <div class="col-sm-12 col-md-12 col-lg-12 mt-1">
                  <h3>Preview Documents</h3>
                </div>
                <div class="col-sm-12 col-md-12 col-lg-12 mt-1">
                  <div class="table-responsive">
                    <table class="table table-sm table-bordered text-center">
                      <thead class="bg-primary">
                        <tr>
                          <th>Date</th>
                          <th>Preview </th>
                        </tr>
                      </thead>
                      <tbody id="td_show_update_documents">
                        @foreach($document_list as $rows)

                          <input type="hidden" name="document[]" value="{{encrypt($rows->id)}}">
                          <tr>
                            <td>{{FL::dateFormat($rows->date)}}</td>
                          <td>
                              <a target="_blank" href="{{FL::getImage($rows->file)}}">{{$rows->title}}</a>
                          </td>
                          </tr>
                          
                        @endforeach
                      </tbody>
                    </table>
                  </div>
                  
                </div>


            </div>
          </div>

          <div class="card-footer">
            <input type="hidden" name="user" value="{{encrypt($user->id)}}">
            <button class="btn btn-primary float-right" type="submit">Edit Profile</button>
          </div>

          {{ Form::close() }}

      
    </div>
  </div>
</div>
   
@endsection

@section('modals')
  

@endsection

@section('scripts')
<script type="text/javascript">
  function addPayment(event,table_name,user_id = 0)
  {
      event.preventDefault();

      var action = 'onchange=getBonusInfo(event,this,"'+user_id+'")';

      var view = '<tr><td><input required type="date" name="date[]" class="form-control "></td><td><select '+action+' class="form-control" name="payment_type[]"><option value="Advance Amount">Advance Amount</option><option value="Pocket Money">Pocket Money</option><option  value="Bonus Amount">Bonus Amount</option></select></td><td><input placeholder="Amount"  type="number" required name="amount[]" class="form-control "></td><td><button class="btn btn-sm btn-block btn-outline-danger" onclick="removeTableRow(this);">Remove</button></td></tr>';

      $(table_name).append(view);
  }

  function removeTableRow(obj)
  {
      $(obj).closest('tr').remove();
  }

  function getBonusInfo(event,obj,user_id)
  {

    if($(obj).val() == "Bonus Amount")
    {
        var data_set = {id:user_id};

        $.get("{{route('bonusinfo')}}",data_set,function(data)
        {
            $(obj).closest('td').next().children().val(data);

            $(obj).closest('td').next().children().attr('max',data);


        });
    }
    
  }

</script>
@endsection



