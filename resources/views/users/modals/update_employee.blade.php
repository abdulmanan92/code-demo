@php($uniq_id = uniqid() )
<div class="modal modal-blur fade" id="md_update_user" tabindex="-1" role="dialog" aria-hidden="true">
      <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
           {{ Form::open(array('route' => 'update.employee','id'=>'form_update_user', 'enctype' => 'multipart/form-data')) }}
          <div class="modal-header">
            <h4 class="modal-title">Update Syatem User</h4>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">×</span>
              </button>
          </div>
          <div class="modal-body">
              <div class="row">

                 <div class="col-sm-12 col-md-12 col-lg-12 mt-1 mb-2">
   
               @php($label = 'Select Role')
                @php($name = 'role')
                <label class="form-label" >{{$label}} <i style = "color:red;"></i></label>
                <small style = "color:red;" class="form-errors float-right req" value="*"></small>
                <select class="form-control form-control-sm" name="{{$name}}">
                  
                  <option value="">Select Role</option>

                  @foreach($role_list as $rows)
                    <option @if($user->hasAnyRole([$rows->name])) selected @endif value="{{($rows->name)}}">{{$rows->name}}</option>
                  @endforeach
                </select>
             
              </div> 


                 <div class="col-sm-12 col-md-12 col-lg-12 mt-1 mb-2">
   
               @php($label = 'Select Department')
                @php($name = 'department')
                <label class="form-label" >{{$label}} <i style = "color:red;"></i></label>
                <small style = "color:red;" class="form-errors float-right req" value="*"></small>
                <select class="form-control form-control-sm" name="{{$name}}">
                  
                  <option value="">Select Department</option>

                  @foreach($department_list as $rows)
                    <option @if($user->department_id == $rows->id) selected @endif value="{{encrypt($rows->id)}}">{{$rows->name}}</option>
                  @endforeach
                </select>
             
              </div> 

              <div class="col-sm-12 col-md-6 col-lg-4 mt-1">
                @php($label = 'Name')
                @php($name = 'name')
                <label class="form-label" >{{$label}} <i style = "color:red;">*</i></label>
                <small style = "color:red;" class="form-errors float-right req" value="*"></small>
                <input  required="" type="text" name="{{$name}}" value="{{ ($user->name) }}" class = "input-group mb-3 form-control form-control-sm" placeholder = "{{$label}}" aria-label = "Default" aria-describedby = "inputGroup-sizing-default">
              </div>

              <div class="col-sm-12 col-md-6 col-lg-4 mt-1">
                @php($label = 'Cnic No.')
                @php($name = 'cnic')
                <label class="form-label" >{{$label}} <i style = "color:red;"></i></label>
                <small style = "color:red;" class="form-errors float-right req" value="*"></small>
                <input   type="text" name="{{$name}}" value="{{ ($user->cnic) }}" class = "input-group mb-3 form-control form-control-sm" placeholder = "{{$label}}" aria-label = "Default" aria-describedby = "inputGroup-sizing-default">
              </div>

              <div class="col-sm-12 col-md-6 col-lg-4 mt-1">
                @php($label = 'Contact No.')
                @php($name = 'contact_no')
                <label class="form-label" >{{$label}} <i style = "color:red;"></i></label>
                <small style = "color:red;" class="form-errors float-right req" value="*"></small>
                <input  type="text" name="{{$name}}" value="{{ ($user->contact) }}" class = "input-group mb-3 form-control form-control-sm" placeholder = "{{$label}}" aria-label = "Default" aria-describedby = "inputGroup-sizing-default">
              </div>

              <div class="col-sm-12 col-md-12 col-lg-12 mt-1">
                @php($label = 'Address')
                @php($name = 'address')
                <label class="form-label" >{{$label}} <i style = "color:red;"></i></label>
                <small style = "color:red;" class="form-errors float-right req" value="*"></small>
                <textarea   type="text" name="{{$name}}" value="{{ old($name) }}" class = "input-group mb-3 form-control form-control-sm" placeholder = "{{$label}}" aria-label = "Default" aria-describedby = "inputGroup-sizing-default">{{$user->address}}</textarea>
              </div>

              
              <div class="col-sm-12 col-md-12 col-lg-12 mt-1">
                @php($label = 'E-Mail')
                @php($name = 'email')
                <label class="form-label" >{{$label}} <i style = "color:red;">*</i></label>
                <small style = "color:red;" class="form-errors float-right req" value="*"></small>
                <input required=""  type="email" name="{{$name}}" value="{{ ($user->email) }}" class = "input-group mb-3 form-control form-control-sm" placeholder = "{{$label}}" aria-label = "Default" aria-describedby = "inputGroup-sizing-default">
              </div>

              <div class="col-sm-12 col-md-6 col-lg-6 mt-1">
                @php($label = 'Password')
                @php($name = 'password')
                <label class="form-label" >{{$label}} <i style = "color:red;"></i></label>
                <small style = "color:red;" class="form-errors float-right req" value="*"></small>
                <input  type="password" name="{{$name}}" value="{{ old($name) }}" class = "input-group mb-3 form-control form-control-sm" placeholder = "{{$label}}" aria-label = "Default" aria-describedby = "inputGroup-sizing-default">
              </div>

               <div class="col-sm-12 col-md-6 col-lg-6 mt-1">
                @php($label = 'Confirm Password')
                @php($name = 'confirm_password')
                <label class="form-label" >{{$label}} <i style = "color:red;"></i></label>
                <small style = "color:red;" class="form-errors float-right req" value="*"></small>
                <input  type="password" name="{{$name}}" value="{{ old($name) }}" class = "input-group mb-3 form-control form-control-sm" placeholder = "{{$label}}" aria-label = "Default" aria-describedby = "inputGroup-sizing-default">
              </div>


              <div class="col-sm-12 col-md-12 col-lg-12 mt-1">
                @php($label = 'Select Status')
                @php($name = 'status')
                <label class="form-label" >{{$label}} <i style = "color:red;"></i></label>
                <small style = "color:red;" class="form-errors float-right req" value="*"></small>
                <select name="{{$name}}"  class = "input-group mb-3 form-control form-control-sm" placeholder = "{{$label}}" aria-label = "Default" aria-describedby = "inputGroup-sizing-default">
                  @foreach(['Active','Deavtive'] as $rows)
                  <option @if($rows == $user->status) selected @endif value="{{$rows}}">{{$rows}}</option>
                  @endforeach
                </select>
              </div>

              <div class="col-sm-12 col-md-12 col-lg-12 mt-1">

               @php($label = 'Photo')
                @php($name = 'photo')
                <label class="form-label" >{{$label}} <i style = "color:red;"></i></label>
                <small style = "color:red;" class="form-errors float-right req" value="*"></small>
                <input style="padding: 2px;"   type="file" name="{{$name}}" value="{{ old($name) }}" class = "input-group mb-3 form-control" placeholder = "{{$label}}" aria-label = "Default" aria-describedby = "inputGroup-sizing-default">
              </div>


          </div>
          </div>
        
          <div class="modal-footer">
            <button  class="btn btn-danger" data-dismiss="modal">
              Close
            </button>
            <input type="hidden" name="user" value="{{encrypt($user->id)}}">
            <button type="submit" class="btn btn-success ">
              Update
            </button>
          </div>
        </div>
    {{ Form::close() }}

      </div>
    </div>