@php($uniq_id = uniqid() )
<div class="modal modal-blur fade" id="md_update_user" tabindex="-1" role="dialog" aria-hidden="true">
      <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
           {{ Form::open(array('route' => 'update.teammember','id'=>'form_update_user', 'enctype' => 'multipart/form-data')) }}
          <div class="modal-header">
            <h4 class="modal-title">Update Labour</h4>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">×</span>
              </button>
          </div>
          <div class="modal-body">
              <div class="row">


                 <div class="col-sm-12 col-md-12 col-lg-12 mt-1 mb-2">
   
               @php($label = 'Select Department')
                @php($name = 'department')
                <label class="form-label" >{{$label}} <i style = "color:red;"></i></label>
                <small style = "color:red;" class="form-errors float-right req" value="*"></small>
                <select class="form-control form-control-sm" name="{{$name}}">
                  
                  <option value="">Select Department</option>

                  @foreach($department_list as $rows)
                    <option @if($user->department_id == $rows->id) selected @endif value="{{encrypt($rows->id)}}">{{$rows->name}}</option>
                  @endforeach
                </select>
             
              </div> 

              <div class="col-sm-12 col-md-6 col-lg-4 mt-1">
                @php($label = 'Name')
                @php($name = 'name')
                <label class="form-label" >{{$label}} <i style = "color:red;">*</i></label>
                <small style = "color:red;" class="form-errors float-right req" value="*"></small>
                <input  required="" type="text" name="{{$name}}" value="{{ ($user->name) }}" class = "input-group mb-3 form-control form-control-sm" placeholder = "{{$label}}" aria-label = "Default" aria-describedby = "inputGroup-sizing-default">
              </div>

              <div class="col-sm-12 col-md-6 col-lg-4 mt-1">
                @php($label = 'Cnic No.')
                @php($name = 'cnic')
                <label class="form-label" >{{$label}} <i style = "color:red;"></i></label>
                <small style = "color:red;" class="form-errors float-right req" value="*"></small>
                <input   type="text" name="{{$name}}" value="{{ ($user->cnic) }}" class = "input-group mb-3 form-control form-control-sm" placeholder = "{{$label}}" aria-label = "Default" aria-describedby = "inputGroup-sizing-default">
              </div>

              <div class="col-sm-12 col-md-6 col-lg-4 mt-1">
                @php($label = 'Contact No.')
                @php($name = 'contact_no')
                <label class="form-label" >{{$label}} <i style = "color:red;"></i></label>
                <small style = "color:red;" class="form-errors float-right req" value="*"></small>
                <input  type="text" name="{{$name}}" value="{{ ($user->contact) }}" class = "input-group mb-3 form-control form-control-sm" placeholder = "{{$label}}" aria-label = "Default" aria-describedby = "inputGroup-sizing-default">
              </div>

              <div class="col-sm-12 col-md-12 col-lg-12 mt-1">
                @php($label = 'Address')
                @php($name = 'address')
                <label class="form-label" >{{$label}} <i style = "color:red;"></i></label>
                <small style = "color:red;" class="form-errors float-right req" value="*"></small>
                <textarea   type="text" name="{{$name}}" value="{{ old($name) }}" class = "input-group mb-3 form-control form-control-sm" placeholder = "{{$label}}" aria-label = "Default" aria-describedby = "inputGroup-sizing-default">{{$user->address}}</textarea>
              </div>

              <div class="col-sm-12 col-md-6 col-lg-4 mt-1">
                @php($label = 'Date Of Joining')
                @php($name = 'date_of_joining')
                <label class="form-label" >{{$label}} <i style = "color:red;"></i></label>
                <small style = "color:red;" class="form-errors float-right req" value="*"></small>
                <input   type="date" name="{{$name}}" value="{{ (FL::datePhpFormat($user->joining_date)) }}" class = "input-group mb-3 form-control form-control-sm" placeholder = "{{$label}}" aria-label = "Default" aria-describedby = "inputGroup-sizing-default">
              </div>

              <div class="col-sm-12 col-md-6 col-lg-4 mt-1">

               @php($label = 'Reference')
                @php($name = 'reference')
                <label class="form-label" >{{$label}} <i style = "color:red;"></i></label>
                <small style = "color:red;" class="form-errors float-right req" value="*"></small>
                <input   type="text" name="{{$name}}" value="{{ ($user->reference) }}" class = "input-group mb-3 form-control form-control-sm" placeholder = "{{$label}}" aria-label = "Default" aria-describedby = "inputGroup-sizing-default">
              </div>



              
                 <div class="col-sm-12 col-md-6 col-lg-4 mt-1">
   
               @php($label = 'Payroll Group')
                @php($name = 'payroll_group')
                <label class="form-label" >{{$label}} <i style = "color:red;"></i></label>
                <small style = "color:red;" class="form-errors float-right req" value="*"></small>
                <select class="form-control form-control-sm" name="{{$name}}">
                  
                  <option value="">Select Payroll</option>

                  @foreach(FL::getPayRollList() as $rows)
                    <option @if($rows == $user->payroll_group)  selected @endif value="{{$rows}}">{{$rows}}</option>
                  @endforeach
                </select>
             
              </div> 

              <div class="col-sm-12 col-md-6 col-lg-6 mt-1">

               @php($label = 'Advance Amount')
                @php($name = 'advance_amount')
                <label class="form-label" >{{$label}} <i style = "color:red;"></i></label>
                <small style = "color:red;" class="form-errors float-right req" value="*"></small>
                <input   type="number" name="{{$name}}" value="{{ ($user->advance_amount) }}" class = "input-group mb-3 form-control form-control-sm" placeholder = "{{$label}}" aria-label = "Default" aria-describedby = "inputGroup-sizing-default">
              </div>

               <div class="col-sm-12 col-md-6 col-lg-6 mt-1">

               @php($label = 'Pocket Money')
                @php($name = 'pocket_money')
                <label class="form-label" >{{$label}} <i style = "color:red;"></i></label>
                <small style = "color:red;" class="form-errors float-right req" value="*"></small>
                <input   type="number" name="{{$name}}" value="{{ ($user->pocket_money) }}" class = "input-group mb-3 form-control form-control-sm" placeholder = "{{$label}}" aria-label = "Default" aria-describedby = "inputGroup-sizing-default">
              </div>

              <div class="col-sm-12 col-md-12 col-lg-12 mt-1">

               @php($label = 'Photo')
                @php($name = 'photo')
                <label class="form-label" >{{$label}} <i style = "color:red;"></i></label>
                <small style = "color:red;" class="form-errors float-right req" value="*"></small>
                <input style="padding: 2px;"   type="file" name="{{$name}}" value="{{ old($name) }}" class = "input-group mb-3 form-control" placeholder = "{{$label}}" aria-label = "Default" aria-describedby = "inputGroup-sizing-default">
              </div>

              <div class="col-sm-12 col-md-12 col-lg-12">
                <label class="form-label mr-2" >Check List: <i style = "color:red;"></i></label>

                @foreach(FL::getLabourCheckList() as $rows)
                <div class="form-check-inline">
                  <label class="form-check-label">
                    <input @if(isset($user[$rows]) && $user[$rows] == 1) checked @endif type="checkbox" name="{{$rows}}" class="form-check-input">{{ucwords(str_replace('_',' ',$rows))}} 
                  </label>
                </div>
                @endforeach
              </div>

              <div class="col-sm-12 col-md-12 col-lg-12 mt-1">
                <h3>Preview Documents</h3>
              </div>
              <div class="col-sm-12 col-md-12 col-lg-12 mt-1">
                <div class="table-responsive">
                  <table class="table table-sm table-bordered text-center">
                    <thead class="bg-primary">
                      <tr>
                        <th>Date</th>
                        <th>Preview </th>
                      </tr>
                    </thead>
                    <tbody id="td_show_update_documents">
                      @foreach($document_list as $rows)

                        <input type="hidden" name="document[]" value="{{encrypt($rows->id)}}">
                        <tr>
                          <td>{{FL::dateFormat($rows->date)}}</td>
                        <td>
                            <a target="_blank" href="{{FL::getImage($rows->file)}}">{{$rows->title}}</a>
                        </td>
                        </tr>
                        
                      @endforeach
                    </tbody>
                  </table>
                </div>
                
              </div>


          </div>
          </div>
        
          <div class="modal-footer">
            <button  class="btn btn-danger" data-dismiss="modal">
              Close
            </button>
            <input type="hidden" name="user" value="{{encrypt($user->id)}}">
            <button type="submit" class="btn btn-success ">
              Update
            </button>
          </div>
        </div>
    {{ Form::close() }}

      </div>
    </div>