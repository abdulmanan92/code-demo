@php($uniq_id = uniqid() )
<div class="modal modal-blur fade" id="md_add_user" tabindex="-1" role="dialog" aria-hidden="true">
      <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
           {{ Form::open(array('route' => 'store.teammember','id'=>'form_add_user', 'enctype' => 'multipart/form-data')) }}
          <div class="modal-header">
            <h4 class="modal-title">Add New Labour</h4>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">×</span>
              </button>
          </div>
          <div class="modal-body">
              <div class="row">


                 <div class="col-sm-12 col-md-12 col-lg-12 mt-1 mb-2">
   
               @php($label = 'Select Department')
                @php($name = 'department')
                <label class="form-label" >{{$label}} <i style = "color:red;"></i></label>
                <small style = "color:red;" class="form-errors float-right req" value="*"></small>
                <select class="form-control form-control-sm" name="{{$name}}">
                  
                  <option value="">Select Department</option>

                  @foreach($department_list as $rows)
                    <option value="{{encrypt($rows->id)}}">{{$rows->name}}</option>
                  @endforeach
                </select>
             
              </div> 

              <div class="col-sm-12 col-md-6 col-lg-4 mt-1">
                @php($label = 'Name')
                @php($name = 'name')
                <label class="form-label" >{{$label}} <i style = "color:red;">*</i></label>
                <small style = "color:red;" class="form-errors float-right req" value="*"></small>
                <input  required="" type="text" name="{{$name}}" value="{{ old($name) }}" class = "input-group mb-3 form-control form-control-sm" placeholder = "{{$label}}" aria-label = "Default" aria-describedby = "inputGroup-sizing-default">
              </div>

              <div class="col-sm-12 col-md-6 col-lg-4 mt-1">
                @php($label = 'Cnic No.')
                @php($name = 'cnic')
                <label class="form-label" >{{$label}} <i style = "color:red;"></i></label>
                <small style = "color:red;" class="form-errors float-right req" value="*"></small>
                <input   type="text" name="{{$name}}" value="{{ old($name) }}" class = "input-group mb-3 form-control form-control-sm" placeholder = "{{$label}}" aria-label = "Default" aria-describedby = "inputGroup-sizing-default">
              </div>

              <div class="col-sm-12 col-md-6 col-lg-4 mt-1">
                @php($label = 'Contact No.')
                @php($name = 'contact_no')
                <label class="form-label" >{{$label}} <i style = "color:red;"></i></label>
                <small style = "color:red;" class="form-errors float-right req" value="*"></small>
                <input  type="text" name="{{$name}}" value="{{ old($name) }}" class = "input-group mb-3 form-control form-control-sm" placeholder = "{{$label}}" aria-label = "Default" aria-describedby = "inputGroup-sizing-default">
              </div>

              <div class="col-sm-12 col-md-12 col-lg-12 mt-1">
                @php($label = 'Address')
                @php($name = 'address')
                <label class="form-label" >{{$label}} <i style = "color:red;"></i></label>
                <small style = "color:red;" class="form-errors float-right req" value="*"></small>
                <textarea   type="text" name="{{$name}}" value="{{ old($name) }}" class = "input-group mb-3 form-control form-control-sm" placeholder = "{{$label}}" aria-label = "Default" aria-describedby = "inputGroup-sizing-default"></textarea>
              </div>

              <div class="col-sm-12 col-md-6 col-lg-4 mt-1">
                @php($label = 'Date Of Joining')
                @php($name = 'date_of_joining')
                <label class="form-label" >{{$label}} <i style = "color:red;"></i></label>
                <small style = "color:red;" class="form-errors float-right req" value="*"></small>
                <input   type="date" name="{{$name}}" value="{{ old($name) }}" class = "input-group mb-3 form-control form-control-sm" placeholder = "{{$label}}" aria-label = "Default" aria-describedby = "inputGroup-sizing-default">
              </div>

              <div class="col-sm-12 col-md-6 col-lg-4 mt-1">

               @php($label = 'Reference')
                @php($name = 'reference')
                <label class="form-label" >{{$label}} <i style = "color:red;"></i></label>
                <small style = "color:red;" class="form-errors float-right req" value="*"></small>
                <input   type="text" name="{{$name}}" value="{{ old($name) }}" class = "input-group mb-3 form-control form-control-sm" placeholder = "{{$label}}" aria-label = "Default" aria-describedby = "inputGroup-sizing-default">
              </div>



              
                 <div class="col-sm-12 col-md-6 col-lg-4 mt-1">
   
               @php($label = 'Payroll Group')
                @php($name = 'payroll_group')
                <label class="form-label" >{{$label}} <i style = "color:red;"></i></label>
                <small style = "color:red;" class="form-errors float-right req" value="*"></small>
                <select class="form-control form-control-sm" name="{{$name}}">
                  

                  @foreach(FL::getPayRollList() as $rows)
                    <option value="{{$rows}}">{{$rows}}</option>
                  @endforeach
                </select>
             
              </div> 

              {{--
              <div class="col-sm-12 col-md-6 col-lg-6 mt-1">

               @php($label = 'Advance Amount')
                @php($name = 'advance_amount')
                <label class="form-label" >{{$label}} <i style = "color:red;"></i></label>
                <small style = "color:red;" class="form-errors float-right req" value="*"></small>
                <input   type="number" name="{{$name}}" value="{{ old($name) }}" class = "input-group mb-3 form-control form-control-sm" placeholder = "{{$label}}" aria-label = "Default" aria-describedby = "inputGroup-sizing-default">
              </div>

               <div class="col-sm-12 col-md-6 col-lg-6 mt-1">

               @php($label = 'Pocket Money')
                @php($name = 'pocket_money')
                <label class="form-label" >{{$label}} <i style = "color:red;"></i></label>
                <small style = "color:red;" class="form-errors float-right req" value="*"></small>
                <input   type="number" name="{{$name}}" value="{{ old($name) }}" class = "input-group mb-3 form-control form-control-sm" placeholder = "{{$label}}" aria-label = "Default" aria-describedby = "inputGroup-sizing-default">
              </div>
              --}}

              <div class="col-sm-12 col-md-12 col-lg-12 mt-1">

               @php($label = 'Photo')
                @php($name = 'photo')
                <label class="form-label" >{{$label}} <i style = "color:red;"></i></label>
                <small style = "color:red;" class="form-errors float-right req" value="*"></small>
                <input style="padding: 2px;"   type="file" name="{{$name}}" value="{{ old($name) }}" class = "input-group mb-3 form-control" placeholder = "{{$label}}" aria-label = "Default" aria-describedby = "inputGroup-sizing-default">
              </div>

              <div class="col-sm-12 col-md-12 col-lg-12">
                <label class="form-label mr-2" >Check List: <i style = "color:red;"></i></label>

                @foreach(FL::getLabourCheckList() as $rows)
                <div class="form-check-inline">
                  <label class="form-check-label">
                    <input type="checkbox" name="{{$rows}}" class="form-check-input" >{{ucwords(str_replace('_',' ',$rows))}}
                  </label>
                </div>
                @endforeach
                
              </div>

              <div class="col-sm-12 col-md-12 col-lg-12 mt-1">
                <button style="width: 125px;" onclick="addDocument(event,'#td_show_documents');" class="btn btn-block btn-outline-success btn-sm float-right">Add Documents</button>
              </div>
              <div class="col-sm-12 col-md-12 col-lg-12 mt-1">
                <div class="table-responsive">
                  <table class="table table-sm table-bordered">
                    <thead class="bg-primary">
                      <tr>
                        <th>Date</th>
                        <th>Title <i style = "color:red; font-weight: bolder;">*</i></th>
                        <th>Document <i style = "color:red; font-weight: bolder;">*</i></th>
                        <th>Action</th>
                      </tr>
                    </thead>
                    <tbody id="td_show_documents">
                    </tbody>
                  </table>
                </div>
                
              </div>




              <div class="col-sm-12 col-md-12 col-lg-12 mt-1">
                <button style="width: 125px;" onclick="addPayment(event,'#td_show_payments');" class="btn btn-block btn-outline-success btn-sm float-right">Add Payment</button>
              </div>
              <div class="col-sm-12 col-md-12 col-lg-12 mt-1">
                <div class="table-responsive">
                  <table class="table table-sm table-bordered">
                    <thead class="bg-primary">
                      <tr>
                        <th>Date <i style = "color:red; font-weight: bolder;">*</i></th>
                        <th>Payment Type <i style = "color:red; font-weight: bolder;">*</i></th>
                        <th>Amount <i style = "color:red; font-weight: bolder;">*</i></th>
                        <th>Action</th>
                      </tr>
                    </thead>
                    <tbody id="td_show_payments">
                    </tbody>
                  </table>
                </div>
                
              </div>


          </div>
          </div>
        
          <div class="modal-footer">
            <button  class="btn btn-danger" data-dismiss="modal">
              Close
            </button>
            <button type="submit" class="btn btn-success ">
              Submit
            </button>
          </div>
        </div>
    {{ Form::close() }}

      </div>
    </div>