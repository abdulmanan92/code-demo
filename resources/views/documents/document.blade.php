@extends('layouts.master')

@php($module = 'Documents')
@section('title')
{{$module}}
@endsection


@section('header')
<div class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h1 class="m-0">{{$module}}</h1>
      </div><!-- /.col -->
      <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
          <li class="breadcrumb-item"><a href="{{url('home')}}">Dashboard</a></li>
          <li class="breadcrumb-item active">{{$module}}</li>
        </ol>
      </div><!-- /.col -->
    </div><!-- /.row -->
  </div><!-- /.container-fluid -->
</div>
@endsection

@section('content')

<div class="row mx-1">
	<div class="col-sm-12 col-md-12 col-lg-12">
			<div class="card">
          <div class="card-header">
            <h3 class="card-title">Manage {{$module}}</h3>
            <div class="card-tools">
              <button  type="button" class="btn btn-block btn-outline-success float-right" data-toggle="modal" data-target="#md_add_document"><i class="fa fa-plus mr-1"></i> <b>Add Document</b></button>
            </div>
            
          </div>
          <!-- /.card-header -->
          <div class="card-body">
            <table id="example1" class="table table-sm table-bordered  text-center">
              <thead class="bg-primary">
                <tr>
                  <th style="width: 10px">#</th>
                  <th>Date</th>
                  <th>Title</th>
                  <th>Document</th>
                  <th>Created At</th>
                  <th>Action</th>
                </tr>
              </thead>
              <tbody>

              	 @foreach($document_list as $rows)
                 <tr>
                   <td>{{$loop->iteration}}</td>
                   <td>{{FL::dateFormat($rows->date)}}</td>
                   <td>
                         <a target="_blank" href="{{FL::getImage($rows->file)}}">{{$rows->title}}</a>
                   </td>

                   <td>
                     <img width="35" height="35" onerror="this.src='{{asset("graphics/na-image.png")}}'"  src="{{FL::getImage($rows->file)}}" alt="User Image">
                   </td>
                   <td>{{FL::dateFormat($rows->created_at)}}</td>

                   <td style="width: 25%;">

                      @php($route = route('update.document', ['id' =>  encrypt($rows->id)]))

                       <button  onclick="onFetchFormModal(event,'{{$route}}','#md_update_document','#bind_md_update_document')" class="btn btn-sm btn-outline-info"><i class="fa fa-edit mr-1"></i>Edit</button>

                       <a href="{{route('removedocument',['id' => encrypt($rows->id)])}}" class="btn btn-sm btn-outline-danger"><i class="fa fa-trash-alt mr-1"></i>Remove</a>

                   </td>
                 </tr>
                 @endforeach
              </tbody>
            </table>
          </div>
      
        </div>
	</div>
</div>
   
@endsection

@section('modals')
  @include('documents.modals.add_document')

  <div id="bind_md_update_document"></div>

@endsection






