@extends('layouts.master')

@php($module = 'Dashboard')
@section('title')
{{$module}}
@endsection


@section('header')
<div class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h1 class="m-0">{{$module}}</h1>
      </div><!-- /.col -->
      <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
          <li class="breadcrumb-item active">{{$module}}</li>
        </ol>
      </div><!-- /.col -->
    </div><!-- /.row -->
  </div><!-- /.container-fluid -->
</div>
@endsection

@section('content')

<div class="row mx-2">
    <div class="col-sm-12 col=md-6 col-lg-4">
      <!-- small box -->
      <div class="small-box bg-primary">
        <div class="inner">
          <h3>Production Issuance </h3>

          <p>Report</p>
        </div>
        <div class="icon">
          <i class="ion ion-bag"></i>
        </div>
        <a href="#" data-toggle="modal" data-target="#md_product_issuance" class="small-box-footer">Click Here&nbsp;&nbsp;&nbsp;<i class="fas fa-arrow-circle-right"></i></a>
      </div>
    </div>

    <div class="col-sm-12 col=md-6 col-lg-4">
      <!-- small box -->
      <div class="small-box bg-success">
        <div class="inner">
          <h3>Production Receiving </h3>

          <p>Report</p>
        </div>
        <div class="icon">
          <i class="ion ion-bag"></i>
        </div>
        <a href="#" data-toggle="modal" data-target="#md_product_receiving" class="small-box-footer">Click Here&nbsp;&nbsp;&nbsp;<i class="fas fa-arrow-circle-right"></i></a>
      </div>
    </div>


    <div class="col-sm-12 col=md-6 col-lg-4">
      <!-- small box -->
      <div class="small-box bg-warning">
        <div class="inner">
          <h3>Labour Progress</h3>

          <p>Report</p>
        </div>
        <div class="icon">
          <i class="ion ion-bag"></i>
        </div>
        <a href="#" data-toggle="modal" data-target="#md_labour_progress" class="small-box-footer">Click Here&nbsp;&nbsp;&nbsp;<i class="fas fa-arrow-circle-right"></i></a>
      </div>
    </div>

    <div class="col-sm-12 col=md-6 col-lg-4">
      <!-- small box -->
      <div class="small-box bg-primary">
        <div class="inner">
          <h3>Labour Payment</h3>

          <p>Report</p>
        </div>
        <div class="icon">
          <i class="ion ion-bag"></i>
        </div>
        <a href="#" data-toggle="modal" data-target="#md_labour_payment" class="small-box-footer">Click Here&nbsp;&nbsp;&nbsp;<i class="fas fa-arrow-circle-right"></i></a>
      </div>
    </div>


</div>
@endsection

@section('modals')
  @include('reports.modals.production_issuance')
  @include('reports.modals.production_receiving')
  @include('reports.modals.labour_progress')
  @include('reports.modals.labour_payment')



@endsection






