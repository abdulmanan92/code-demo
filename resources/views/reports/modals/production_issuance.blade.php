@php($uniq_id = uniqid() )
<div class="modal modal-blur fade" id="md_product_issuance" tabindex="-1" role="dialog" aria-hidden="true">
      <div class="modal-dialog modal-md" role="document">
        <div class="modal-content">
           {{ Form::open(array('route' => 'report.productionissuance', 'target' => '_blank', 'loader' => 'false')) }}
          <div class="modal-header">
            <h4 class="modal-title">Production Issuance Report</h4>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">×</span>
              </button>
          </div>
          <div class="modal-body">
              <div class="row">
                <div class="col-sm-12 col-md-6 col-lg-6 mt-1">
                  @php($label = 'From')
                  @php($name = 'from')
                  <label class="form-label" >{{$label}} <i style = "color:red;"></i></label>
                  <small style = "color:red;" class="form-errors float-right req" value="*"></small>
                  <input   type="date" name="{{$name}}" value="{{ old($name) }}" class = "input-group mb-3 form-control" placeholder = "{{$label}}" aria-label = "Default" aria-describedby = "inputGroup-sizing-default">
                </div>

                <div class="col-sm-12 col-md-6 col-lg-6 mt-1">
                  @php($label = 'To')
                  @php($name = 'to')
                  <label class="form-label" >{{$label}} <i style = "color:red;"></i></label>
                  <small style = "color:red;" class="form-errors float-right req" value="*"></small>
                  <input   type="date" name="{{$name}}" value="{{ old($name) }}" class = "input-group mb-3 form-control" placeholder = "{{$label}}" aria-label = "Default" aria-describedby = "inputGroup-sizing-default">
                </div>

                <div class="col-sm-12 col-md-6 col-lg-6 mt-1">
                  @php($label = 'Select Labour')
                  @php($name = 'labour')
                  <label class="form-label" >{{$label}} <i style = "color:red;"></i></label>
                  <small style = "color:red;" class="form-errors float-right req" value="*"></small>
                  
                  <select name="{{$name}}" value="{{ old($name) }}" class = "fstdropdown-select input-group mb-3 form-control" placeholder = "{{$label}}" aria-label = "Default" aria-describedby = "inputGroup-sizing-default">
                    <option value="{{encrypt(0)}}">{{$label}}</option>
                    @foreach($default_object['user_list'] as $rows)
                      <option value="{{encrypt($rows->id)}}">{{$rows->name}}</option>
                    @endforeach
                  </select>
                </div>


                 <div class="col-sm-12 col-md-6 col-lg-6 mt-1">
                  @php($label = 'Select Department')
                  @php($name = 'department')
                  <label class="form-label" >{{$label}} <i style = "color:red;"></i></label>
                  <small style = "color:red;" class="form-errors float-right req" value="*"></small>
                  
                  <select name="{{$name}}" value="{{ old($name) }}" class = "fstdropdown-select input-group mb-3 form-control" placeholder = "{{$label}}" aria-label = "Default" aria-describedby = "inputGroup-sizing-default">
                    <option value="{{encrypt(0)}}">{{$label}}</option>
                    @foreach($default_object['department_list'] as $rows)
                      <option value="{{encrypt($rows->id)}}">{{$rows->name}}</option>
                    @endforeach
                  </select>
                </div>

                <div class="col-sm-12 col-md-12 col-lg-12 mt-2">
                  @php($label = 'Select Product')
                  @php($name = 'product')
                  <label class="form-label" >{{$label}} <i style = "color:red;"></i></label>
                  <small style = "color:red;" class="form-errors float-right req" value="*"></small>
                  
                  <select name="{{$name}}" value="{{ old($name) }}" class = "fstdropdown-select input-group mb-3 form-control" placeholder = "{{$label}}" aria-label = "Default" aria-describedby = "inputGroup-sizing-default">
                    <option value="{{encrypt(0)}}">{{$label}}</option>
                    @foreach($default_object['product_list'] as $rows)
                      <option value="{{encrypt($rows->id)}}">{{$rows->artilce_no}} {{$rows->name}}</option>
                    @endforeach
                  </select>
                </div>

                <div class="col-sm-12 col-md-12 col-lg-12 mt-2">
                  @php($label = 'Select Format')
                  @php($name = 'format')
                  <label class="form-label" >{{$label}} <i style = "color:red;"></i></label>
                  <small style = "color:red;" class="form-errors float-right req" value="*"></small>
                  
                  <select name="{{$name}}" value="{{ old($name) }}" class = " input-group mb-3 form-control" placeholder = "{{$label}}" aria-label = "Default" aria-describedby = "inputGroup-sizing-default">
                    <option value="0">PDF</option>
                    <option value="1">EXCEL</option>

                  
                  </select>
                </div>



          </div>
          </div>
        
          <div class="modal-footer">
            <button  class="btn btn-danger" data-dismiss="modal">
              Close
            </button>
            <button type="submit" class="btn btn-success ">
              Generate
            </button>
          </div>
        </div>
    {{ Form::close() }}

      </div>
    </div>