@extends('layouts.master')

@php($module = 'Departments')
@section('title')
{{$module}}
@endsection


@section('header')
<div class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h1 class="m-0">{{$module}}</h1>
      </div><!-- /.col -->
      <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
          <li class="breadcrumb-item"><a href="{{url('home')}}">Dashboard</a></li>
          <li class="breadcrumb-item active">{{$module}}</li>
        </ol>
      </div><!-- /.col -->
    </div><!-- /.row -->
  </div><!-- /.container-fluid -->
</div>
@endsection

@section('content')

<div class="row mx-1">
	<div class="col-sm-12 col-md-12 col-lg-12">
			<div class="card">
          <div class="card-header">
            <h3 class="card-title">Manage {{$module}}</h3>
            <div class="card-tools">
              @if(Auth::user()->hasAnyPermission(['All','Add Department']))

              <button  type="button" class="btn btn-block btn-success float-right" data-toggle="modal" data-target="#md_add_department"><i class="fa fa-plus mr-1"></i> <b>Add Department</b></button>
              @endif
            </div>
            
          </div>
          <!-- /.card-header -->
          <div class="card-body">
            <table id="example1" class="table table-bordered  text-center table-sm">
              <thead class="bg-primary">
                <tr>
                  <th style="width: 10px">#</th>
                  <th>Name</th>
                  <th>Dep Order</th>

                  <th>Is Payroll</th>
                  <th>Created At</th>
                  <th>Action</th>
                </tr>
              </thead>
              <tbody>
              	 @foreach($department_list as $rows)
                 <tr>
                   <td>{{$loop->iteration}}</td>
                   <td>{{$rows->name}}</td>
                   <td>{{$rows->order}}</td>

                   <td>
                     @if($rows->is_payroll > 0)
                        <span class="badge badge-success">Yes</span>
                     @else
                        <span class="badge badge-danger">No</span>
                     @endif
                   </td>


                   <td>{{FL::dateFormat($rows->created_at)}}</td>
                   <td style="width: 25%;">

                    
                      @php($route = route('update.department', ['id' =>  encrypt($rows->id)]))
              @if(Auth::user()->hasAnyPermission(['All','Edit Department']))

                       <button  onclick="onFetchFormModal(event,'{{$route}}','#md_update_department','#bind_md_update_department')" class="btn btn-sm btn-outline-info"><i class="fa fa-edit mr-1"></i>Edit</button>
                       @endif

                       {{--
                       <a href="{{route('removedepartment',['id' => encrypt($rows->id)])}}" class="btn btn-sm btn-outline-danger"><i class="fa fa-trash-alt mr-1"></i>Remove</a>
                       --}}

                   </td>
                 </tr>
                 @endforeach
              </tbody>
            </table>
          </div>
      
        </div>
	</div>
</div>
   
@endsection

@section('modals')
  @include('departments.modals.add_department')

  <div id="bind_md_update_department"></div>

@endsection




