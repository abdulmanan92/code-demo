@php($uniq_id = uniqid() )
<div class="modal modal-blur fade" id="md_add_department" tabindex="-1" role="dialog" aria-hidden="true">
      <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
           {{ Form::open(array('route' => 'store.department','id'=>'form_add_department', 'enctype' => 'multipart/form-data')) }}
          <div class="modal-header">
            <h4 class="modal-title">Add Department</h4>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">×</span>
              </button>
          </div>
          <div class="modal-body">
              <div class="row">
              <div class="col-sm-12 col-md-12 col-lg-12 mt-1">
                @php($label = 'Name')
                @php($name = 'name')
                <label class="form-label" >{{$label}} <i style = "color:red;">*</i></label>
                <small style = "color:red;" class="form-errors float-right req" value="*"></small>
                <input  required="" type="text" name="{{$name}}" value="{{ old($name) }}" class = "input-group mb-3 form-control" placeholder = "{{$label}}" aria-label = "Default" aria-describedby = "inputGroup-sizing-default">
              </div>

              <div class="col-sm-12 col-md-12 col-lg-12 mt-1">
                @php($label = 'Dep Order')
                @php($name = 'order')
                <label class="form-label" >{{$label}} <i style = "color:red;">*</i></label>
                <small style = "color:red;" class="form-errors float-right req" value="*"></small>
                <input required=""  value="{{old($name)}}"  type="number" name="{{$name}}" value="{{ old($name) }}" class = "input-group mb-3 form-control" placeholder = "{{$label}}" aria-label = "Default" aria-describedby = "inputGroup-sizing-default">
              </div>

              <div class="col-sm-12 col-md-12 col-lg-12 mt-1">
                @php($label = 'Is part of payroll?')
                @php($name = 'is_payroll')
                <label class="form-label" >{{$label}} <i style = "color:red;"></i></label>
                <small style = "color:red;" class="form-errors float-right req" value="*"></small>
                <input type="checkbox" name="{{$name}}" >
              </div>

              
              

          </div>
          </div>
        
          <div class="modal-footer">
            <button  class="btn btn-danger" data-dismiss="modal">
              Close
            </button>
            <button type="submit" class="btn btn-success ">
              Submit
            </button>
          </div>
        </div>
    {{ Form::close() }}

      </div>
    </div>