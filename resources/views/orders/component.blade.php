<tr>
	<td>
		<select class="form-control form-control-sm" name="user[]">
			
			<option value="{{encrypt(0)}}">Select Labour</option>

			@foreach($user_list as $rows)
				<option value="{{encrypt($rows->id)}}">{{$rows->name}}</option>
			@endforeach
		</select>
	</td>

	<td>
		<select class="form-control form-control-sm" name="size[]">
			
			<option value="">Select Size</option>

			@foreach(FL::getSizeList() as $rows)
				<option value="{{$rows}}">{{$rows}}</option>
			@endforeach
		</select>
	</td>

	<td>
		<input type="text" name="color[]" placeholder="Color" class="form-control form-control-sm">
	</td>

	<td>
		<input type="text" name="quantity[]" placeholder="Quantity" class="form-control form-control-sm">
	</td>

	<td>
		<input required="" type="date" name="receive_date[]" placeholder="Quantity" class="form-control form-control-sm">
	</td>

	<td>
		

		<input onchange="isPartial(this);" type="checkbox" name="partial_check" >
		<input class="partial"  type="hidden" name="partial[]" value="0">
	</td>

	<td><button class="btn btn-sm btn-block btn-outline-danger" onclick="removeTableRow(this);">Remove</button></td>



</tr>