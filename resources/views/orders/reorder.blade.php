@extends('layouts.master')

@php($module = 'Reorders')
@section('title')
{{$module}}
@endsection


@section('header')
<div class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h1 class="m-0">Process Orders to Next Department</h1>
      </div><!-- /.col -->
      <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
          <li class="breadcrumb-item"><a href="{{url('home')}}">Dashboard</a></li>
                 <li class="breadcrumb-item"><a href="{{url('workorders')}}">Order No. {{$order->order_no}}</a></li>


          <li class="breadcrumb-item active">Process Orders to Next Department</li>
        </ol>
      </div><!-- /.col -->
    </div><!-- /.row -->
  </div><!-- /.container-fluid -->
</div>
@endsection

@section('content')

<div class="row mx-1">
	<div class="col-sm-12 col-md-12 col-lg-12">
			<div class="card">
          <div class="card-header">
            
            <div class="card-tools">
              <button  type="button" class="btn btn-block btn-outline-success float-right" data-toggle="modal" data-target="#md_add_order"><i class="fa fa-plus mr-1"></i> <b>Send to Next Department</b></button>
            </div>
            
          </div>
          <!-- /.card-header -->
          <div class="card-body">
            <table id="example1" class="table table-bordered  text-center">
         <thead class="bg-primary">
                <tr>
                  <th style="width: 10px">#</th>
                  <th>Order No.</th>
                  <th>Order Date</th>
                  <th>Product</th>
                  <th>Department</th>
                  <th>Total Quantity</th>
                  <th>Created At</th>
                  <th>Action</th>
                </tr>
              </thead>
              <tbody>

                 @foreach($order_list as $rows)
                  <tr >
                   <td>{{$loop->iteration}}</td>
                   <td>{{$rows->order_no}}</td>
                   <td>{{FL::dateFormat($rows->date)}}</td>

                   <td>{{$rows->product->name}}</td>
                   <td>{{$rows->department->name}}</td>
                   <td>{{FL::numberFormat($rows->total_quantity)}}</td>

                   <td>{{FL::dateFormat($rows->created_at)}}</td>
                   <td class="text-center">

                    <ul class="nav nav-pills ml-auto p-2">
                 
                  <li class="nav-item dropdown">
                    <a class="btn btn-sm btn-primary text-white dropdown-toggle" data-toggle="dropdown" href="#">
                      Action <span class="caret"></span>
                    </a>
                    <div class="dropdown-menu">
                       @php($route = route('detailorder', ['id' =>  encrypt($rows->id)]))

                      <a href="#" onclick="onFetchFormModal(event,'{{$route}}','#md_order_detail','#bind_md_order_detail');" class="dropdown-item" tabindex="-1" href="#">Order Detail</a>

                      @php($route = route('reorders', ['id' =>  encrypt($rows->id)]))
                      <div class="dropdown-divider"></div>
                      <a class="dropdown-item" href="{{$route}}" tabindex="-1" href="#">Re Order Order</a>


                      @php($route = route('workorderreceive', ['id' =>  encrypt($rows->id)]))
                      <div class="dropdown-divider"></div>
                      <a href="{{$route}}" class="dropdown-item" tabindex="-1" href="#">Receive Order</a>

                      @php($route = route('update.workorder', ['id' =>  encrypt($rows->id)]))
                      <div class="dropdown-divider"></div>
                      <a href="#" onclick="onFetchFormModal(event,'{{$route}}','#md_update_order','#bind_md_update_order');" class="dropdown-item" tabindex="-1" href="#">Edit Order</a>

                      <div class="dropdown-divider"></div>
                      <a href="{{route('removeworkorder',['id' => encrypt($rows->id)])}}" class="dropdown-item" tabindex="-1" href="#">Remove Order</a>
                    </div>
                  </li>
                </ul>

                

                   </td>
                 </tr>
                 @endforeach
              </tbody>
            </table>
          </div>
      
        </div>
	</div>
</div>
   
@endsection


@section('modals')
  @include('orders.modals.add_order')

  <div id="bind_md_update_order"></div>
  <div id="bind_md_order_detail"></div>


@endsection


@section('scripts')
<script type="text/javascript">

  function getVariantTable(event,key_pair,route,bind_table,type = "apend")
  {
    event.preventDefault();

    var department = $('#department_'+key_pair);

    var lot = 0;

    if($('#lot_'+key_pair).prop('checked'))
    {
      lot = 1;
    }

    var data_set = {department_id:department.val(),lot:lot};

    $.get(route,data_set,function(data)
    {
        if(type == "apend")
        {
            $(bind_table).append(data);
        }
        else
        {
            $(bind_table).html(data);

        }
    });
  }

  function addNewVariant(event,key_pair,route,bind_table)
  {
      getVariantTable(event,key_pair,route,bind_table);
  }

   function removeTableRow(obj)
  {
      $(obj).closest('tr').remove();
  }

  function isPartial(obj)
  {
     obj = $(obj);

     obj.next().val(0);

     if(obj.prop('checked'))
     {
        obj.next().val(1);
     }
  }
</script>
@endsection




