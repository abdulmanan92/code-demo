<tr>
	<td>
		<select  class="form-control form-control-sm fstdropdown-select" name="product[]">
			

			@foreach($product_list as $rows)
				<option value="{{encrypt($rows->id)}}">{{$rows->name}}</option>
			@endforeach
		</select>
	</td>

	<td>
		<select required  class="form-control form-control-sm" name="size[]">
			
			<option value="">Select Size</option>

			@foreach(FL::getSizeList() as $rows)
				<option value="{{$rows}}">{{$rows}}</option>
			@endforeach
		</select>
	</td>

	<td>
		<input required  type="text" name="color[]" placeholder="Color" class="form-control form-control-sm">
	</td>

	<td>
		<input  required type="number" name="quantity[]" placeholder="Quantity" class="form-control form-control-sm amount_list" onkeyup="calculateTotalAmount('#tb_show_product_varient');">
	</td>



	<td><button class="btn btn-sm btn-block btn-outline-danger" onclick="removeTableRow(this);">Remove</button></td>



</tr>

<script type="text/javascript">
    setFstDropdown();
	
</script>