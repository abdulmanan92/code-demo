@extends('layouts.master')

@php($module = 'Work Orders')
@section('title')
{{$module}}
@endsection


@section('header')
<div class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h1 class="m-0">{{$module}}</h1>
      </div><!-- /.col -->
      <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
          <li class="breadcrumb-item"><a href="{{url('home')}}">Dashboard</a></li>
          
          <li class="breadcrumb-item active">{{$module}}</li>
        </ol>
      </div><!-- /.col -->
    </div><!-- /.row -->
  </div><!-- /.container-fluid -->
</div>
@endsection

@section('content')

<div class="row mx-1">
	<div class="col-sm-12 col-md-12 col-lg-12">
			<div class="card">
          <div class="card-header">
            <h3 class="card-title">Manage {{$module}}</h3>
            <div class="card-tools">

              @if(Auth::user()->hasAnyPermission(['All','Add Work Order']))
                <button  type="button" class="btn btn-block btn-success float-right" data-toggle="modal" data-target="#md_add_order"><i class="fa fa-plus mr-1"></i> <b>Add Order</b></button>
              @endif
            </div>
            
          </div>
          <!-- /.card-header -->
          <div class="card-body">
            
            <div  id="tableCustomFilters" class="row mt-1 mb-4"></div>

            <table id="my-datatable" class="table table-bordered  text-center table-sm">
              <thead class="bg-primary">
                <tr>
                  <th style="width: 10px">#</th>
                  <th>Order Number</th>
                  <th>Order Date</th>
                  <th>Total Quantity</th>
                  <th>Created On </th>
                  <th>Action</th>
                </tr>
              </thead>
              <tbody>

              	 @foreach($order_list->sortByDesc('id') as $rows)
                  <tr >
                   <td>{{$loop->iteration}}</td>
                   <td>{{$rows->order_no}}</td>
                   <td>{{FL::dateFormatWithTime($rows->date)}}</td>

                
                   <td>{{($rows->total_quantity)}}</td>

                   <td>{{FL::dateFormatWithTime($rows->created_at)}}</td>
                   <td class="text-center">

                    <ul class="nav nav-pills ml-auto p-2">
                 
                  <li class="nav-item dropdown">
                    <a class="btn btn-xs btn-primary text-white dropdown-toggle" data-toggle="dropdown" href="#">
                      Action <span class="caret"></span>
                    </a>
                    <div class="dropdown-menu dropdown-menu-right">
                     
                    @if(Auth::user()->hasAnyPermission(['All','Edit Work Order']))


                      @php($route = route('update.workorder', ['id' =>  encrypt($rows->id)]))
                      <a href="#" onclick="onFetchFormModal(event,'{{$route}}','#md_update_order','#bind_md_update_order');" class="dropdown-item" tabindex="-1" href="#">Edit Order</a>

                      @endif

                      @if(Auth::user()->hasAnyPermission(['All','Remove Work Order']))

                      @if(@count($rows->productions) == 0)
                      <div class="dropdown-divider"></div>
                      <a onclick="removePopUp(event,this);" href="{{route('removeworkorder',['id' => encrypt($rows->id)])}}" class="dropdown-item" tabindex="-1" href="#">Remove Order</a>
                      @endif
                      @endif
                    </div>
                  </li>
                </ul>

                

                   </td>
                 </tr>
                 @endforeach
              </tbody>
            </table>
          </div>
      
        </div>
	</div>
</div>
   
@endsection

@section('modals')
  @include('orders.modals.add_order')

  <div id="bind_md_update_order"></div>
  <div id="bind_md_order_detail"></div>


@endsection


@section('scripts')
<script type="text/javascript">

  function getVariantTable(event,key_pair,route,bind_table,type = "apend")
  {
    event.preventDefault();

    var department = $('#department_'+key_pair);

    var lot = 0;

    if($('#lot_'+key_pair).prop('checked'))
    {
      lot = 1;
    }

    var data_set = {department_id:department.val(),lot:lot};

    $.get(route,data_set,function(data)
    {
        if(type == "apend")
        {
            $(bind_table).append(data);
        }
        else
        {
            $(bind_table).html(data);

        }
    });
  }

  function addNewVariant(event,key_pair,route,bind_table)
  {
      getVariantTable(event,key_pair,route,bind_table);
  }

   function removeTableRow(obj)
  {
      $(obj).closest('tr').remove();
  }

  function isPartial(obj)
  {
     obj = $(obj);

     obj.next().val(0);

     if(obj.prop('checked'))
     {
        obj.next().val(1);
     }
  }

  function loadProductVariantComponent(event,route,bind_table)
  {
    event.preventDefault();

   

    $.get(route,function(data)
    {
      
      $(bind_table).append(data);
   

    });
  }

  

  function calculateTotalAmount(table_obj)
  {
     var total_amount = 0;

     table_obj = $(table_obj);


     $('#'+table_obj.attr('id')+' .amount_list').each(function(){
        total_amount+= isDefaulter($(this).val());

     });
     
     table_obj.next().find('.total_quantity').html(total_amount);
  }

    $(function(){

      $('document').ready(function(){

             $('#my-datatable').DataTable({
      pageLength:50,

              "columnDefs": [
                { "type": "numeric-comma", targets: 0 }
            ],

    initComplete: function () {

       var api = this.api();

  
            createSearchableDropDown('#tableCustomFilters',api,1,'Select Order No.','col-sm-6');
            createSearchableDropDown('#tableCustomFilters',api,3,'Select Quantity','col-sm-6');
           
    }
  });
  // END
      });

    });
  </script>
@endsection





