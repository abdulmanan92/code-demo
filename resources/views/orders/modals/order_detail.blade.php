@php($uniq_id = uniqid() )
<div class="modal modal-blur fade" id="md_order_detail" tabindex="-1" role="dialog" aria-hidden="true">
      <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
          
          <div class="modal-header">
            <h4 class="modal-title">Order Info (Order No. {{$production->order->order_no}} - Dep: {{$production->department->name}})</h4>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">×</span>
              </button>
          </div>
          <div class="modal-body">
              <div class="row">
              <div class="col-sm-12 col-md-12 col-lg-12 mt-1">
                <table id="example1" class="table table-sm table-bordered  text-center">
              <thead class="bg-primary">
                <tr>
                  <th style="width: 10px">#</th>
                  <th>Product</th>
                
                    <th>Labour</th>
                 
                  <th>Is Lot?</th>
                  <th>Color</th>
                  <th>Size</th>
                  <th>Quantity</th>
                  <th>Receive Date</th>
                  <th>Partial ?</th>
                </tr>
              </thead>
              <tbody>

                 @foreach($production->productionDetail as $rows)

                 @php($rece_qty = $rows->receiveOrders->sum('quantity'))

                 @if($rece_qty < $rows->quantity)
 
                 <tr @if(date('Y-m-d') >= FL::datePhpFormat($rows->receive_date)) 
                    style="background:#E5AEA2;"
                  @endif
                  > 
                   <td>{{$loop->iteration}}</td>
                   <td>{{$rows->productVariation->product->name}}</td>
                   
                   <td>{{$rows->user->name}}</td>
                   <td>
                     @if($rows->is_lot > 0)
                        <span class="badge badge-success">Yes</span>
                     @else
                        <span class="badge badge-danger">No</span>
                     @endif
                   </td>
                   <td>{{$rows->productVariation->color}}</td>
                   <td>{{$rows->productVariation->size}}</td>
                   <td>{{$rows->quantity}}</td>
                   <td>{{FL::dateFormat($rows->receive_date)}}</td>
                   <td>@if($rows->is_partial > 0) Partial @endif</td>






                   
                 </tr>
                 @endif
                 @endforeach
              </tbody>
            </table>
              </div>


              

              
              

          </div>
          </div>
        
          <div class="modal-footer">
            <button  class="btn btn-danger" data-dismiss="modal">
              Close
            </button>
            

           
          </div>
        </div>

      </div>
    </div>