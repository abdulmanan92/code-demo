@php($uniq_id = uniqid() )
<div class="modal modal-blur fade" id="md_update_order" tabindex="-1" role="dialog" aria-hidden="true">
      <div class="modal-dialog modal-xl" role="document">
        <div class="modal-content">
           {{ Form::open(array('route' => 'update.workorder','id'=>'form_update_order', 'enctype' => 'multipart/form-data')) }}
          <div class="modal-header">
            <h4 class="modal-title">Update Order</h4>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">×</span>
              </button>
          </div>
          <div class="modal-body">
              <div class="row">
              <div class="col-sm-12 col-md-6 col-lg-6 mt-1">
                @php($label = 'Order No.')
                @php($name = 'order_no')
                <label class="form-label" >{{$label}} <i style = "color:red;">*</i></label>
                <small style = "color:red;" class="form-errors float-right req" value="*"></small>
                <input  required="" type="text" name="{{$name}}" value="{{$order->order_no}}" class = "input-group mb-3 form-control form-control-sm" placeholder = "{{$label}}" aria-label = "Default" aria-describedby = "inputGroup-sizing-default">
              </div>

              <div class="col-sm-12 col-md-6 col-lg-6 mt-1">
                @php($label = 'Order Date')
                @php($name = 'order_date')
                <label class="form-label" >{{$label}} <i style = "color:red;">*</i></label>
                <small style = "color:red;" class="form-errors float-right req" value="*"></small>
                <input required value="{{FL::dateTimeFormat($order->date)}}" required="" type="datetime-local" name="{{$name}}" 
                 class = "input-group mb-3 form-control form-control-sm" placeholder = "{{$label}}" aria-label = "Default" aria-describedby = "inputGroup-sizing-default">
              </div>

               <div class="col-sm-12 col-md-12 col-lg-12 mt-1">
                @php($label = 'Customer Name')
                @php($name = 'customer_name')
                <label class="form-label" >{{$label}} <i style = "color:red;"></i></label>
                <small style = "color:red;" class="form-errors float-right req" value="*"></small>
                <input  required type="text" name="{{$name}}" value="{{$order->customer_name}}" class = "input-group mb-3 form-control form-control-sm" placeholder = "{{$label}}" aria-label = "Default" aria-describedby = "inputGroup-sizing-default">
              </div>

              @php($pro_count = @count($order->productions))

              @if($pro_count <= 1)
              @php($varient_route = route('productvariants'))

              <div class="col-sm-12 col-md-12 col-lg-12 mt-1">
                <button style="width: 150px;" onclick="loadProductVariantComponent(event,'{{$varient_route}}','#tb_show_up_product_varient');" class="btn btn-block btn-outline-success btn-sm float-right">Add Product Variation</button>
              </div>
              @else
              <div class="col-sm-12 col-md-12 col-lg-12 mt-1">

              <center><strong style="color:red;">Product variant / quantity not be added / removed due to production movie in next stage </strong></center>
            </div>
              @endif
              <div class="col-sm-12 col-md-12 col-lg-12 mt-3 mb-2">
                <table class="table table-sm text-center table-bordered">
                  <thead class="bg-primary">
                    <tr>
                      <th>Product</th>
                      <th>Size</th>
                      <th>Color</th>
                      <th>Quantity</th>
                      <th>Action</th>
                    </tr>
                  </thead>
                  <tbody id="tb_show_up_product_varient">
                    @php($variant_list = $order->productVariations)

                    @foreach($variant_list as $variant_rows)
                    
                    @php($receive_qty = $variant_rows->isOrderReceive($variant_rows->id))

                    @if($receive_qty > 0)
                    <tr>
                      <td colspan="5" style="font-weight: bold; color: red;">Quantity not be editable and deletable due to received quantity against the product.</td>
                    </tr>
                    @endif
                    <tr>
                      <input type="hidden" name="product_variant[]" value="{{encrypt($variant_rows->id)}}" >
                        <td>
                          <select class="fstdropdown-select form-control form-control-sm" name="product[]">
                            

                            @foreach($product_list as $rows)
                              <option @if($rows->id == $variant_rows->product_id) selected @endif value="{{encrypt($rows->id)}}">{{$rows->name}}</option>
                            @endforeach
                          </select>
                        </td>

                        <td>
                          <select required="" class="form-control form-control-sm" name="size[]">
                            
                            <option value="">Select Size</option>

                            @foreach(FL::getSizeList() as $rows)
                              <option @if($rows == $variant_rows->size) selected @endif  value="{{$rows}}">{{$rows}}</option>
                            @endforeach
                          </select>
                        </td>

                        <td>
                          <input required="" value="{{$variant_rows->color}}" type="text" name="color[]" placeholder="Color" class="form-control form-control-sm">
                        </td>

                        <td>
                          <input required="" @if($receive_qty > 0 || $pro_count > 1) readonly @endif  value="{{$variant_rows->quantity}}" type="number" name="quantity[]" onkeyup="calculateTotalAmount('#tb_show_up_product_varient');" placeholder="Quantity" class="form-control form-control-sm amount_list">
                        </td>



                        <td><button @if($receive_qty > 0 || $pro_count > 1) disabled @endif class="btn btn-sm btn-block btn-outline-danger" onclick="removeTableRow(this);">Remove</button></td>



                      </tr>
                    @endforeach
                    
                  </tbody>
                  <tfoot style="font-weight: bold;">
                    <td colspan="3">Total Quantity</td>
                    <td class="total_quantity">{{$variant_list->sum('quantity')}}</td>
                    <td></td>
                  </tfoot>
                </table>
              </div>


              
              

          </div>
          </div>
        
          <div class="modal-footer">
            <input type="hidden" name="order" value="{{encrypt($order->id)}}">
            <button  class="btn btn-danger" data-dismiss="modal">
              Close
            </button>
            <button type="submit" class="btn btn-success ">
              Update
            </button>
          </div>
        </div>
    {{ Form::close() }}

      </div>
    </div>