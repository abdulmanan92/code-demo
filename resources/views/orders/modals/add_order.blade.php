@php($uniq_id = uniqid() )
<div class="modal modal-blur fade" id="md_add_order" tabindex="-1" role="dialog" aria-hidden="true">
      <div class="modal-dialog modal-xl" role="document">
        <div class="modal-content">
           {{ Form::open(array('route' => 'store.workorder','id'=>'form_add_order', 'enctype' => 'multipart/form-data')) }}
          <div class="modal-header">
            <h4 class="modal-title">Add Order</h4>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">×</span>
              </button>
          </div>
          <div class="modal-body">
              <div class="row">
              <div class="col-sm-12 col-md-6 col-lg-6 mt-1">
                @php($label = 'Order No.')
                @php($name = 'order_no')
                <label class="form-label" >{{$label}} <i style = "color:red;">*</i></label>
                <small style = "color:red;" class="form-errors float-right req" value="*"></small>
                <input  required="" type="text" name="{{$name}}" @if(isset($order->id)) readonly value="{{ ($order->order_no) }}" @else value="{{ old($name) }}" @endif class = "input-group mb-3 form-control form-control-sm" placeholder = "{{$label}}" aria-label = "Default" aria-describedby = "inputGroup-sizing-default">
              </div>

              <div class="col-sm-12 col-md-6 col-lg-6 mt-1">
                @php($label = 'Order Date')
                @php($name = 'order_date')
                <label class="form-label" >{{$label}} <i style = "color:red;">*</i></label>
                <small style = "color:red;" class="form-errors float-right req" value="*"></small>
                <input required  @if(isset($order->id)) readonly value="{{FL::datePhpFormat($order->date) }}" @else value="{{ old($name) }}" @endif required="" type="datetime-local" name="{{$name}}" 
                 class = "input-group mb-3 form-control form-control-sm" placeholder = "{{$label}}" aria-label = "Default" aria-describedby = "inputGroup-sizing-default">
              </div>

               <div class="col-sm-12 col-md-12 col-lg-12 mt-1">
                @php($label = 'Customer Name')
                @php($name = 'customer_name')
                <label class="form-label" >{{$label}} <i style = "color:red;"></i></label>
                <small style = "color:red;" class="form-errors float-right req" value="*"></small>
                <input  required  type="text" name="{{$name}}" @if(isset($order->id)) readonly value="{{ ($order->order_no) }}" @else value="{{ old($name) }}" @endif class = "input-group mb-3 form-control form-control-sm" placeholder = "{{$label}}" aria-label = "Default" aria-describedby = "inputGroup-sizing-default">
              </div>

        
              @php($varient_route = route('productvariants'))

              <div class="col-sm-12 col-md-12 col-lg-12 mt-1">
                <button style="width: 150px;" onclick="loadProductVariantComponent(event,'{{$varient_route}}','#tb_show_product_varient');" class="btn btn-block btn-outline-success btn-sm float-right">Add Product Variation</button>
              </div>
              <div class="col-sm-12 col-md-12 col-lg-12 mt-3 mb-2">
                <table class="table table-sm text-center table-bordered">
                  <thead class="bg-primary">
                    <tr>
                      <th>Product</th>
                      <th>Size</th>
                      <th>Color</th>
                      <th>Quantity</th>

                      <th>Action</th>
                    </tr>
                  </thead>
                  <tbody id="tb_show_product_varient">
                    
                  </tbody>
                  <tfoot style="font-weight: bold;">
                    <td colspan="3">Total Quantity</td>
                    <td class="total_quantity">0</td>
                    <td></td>
                  </tfoot>
                </table>
              </div>


              
              

          </div>
          </div>
        
          <div class="modal-footer">
            <button  class="btn btn-danger" data-dismiss="modal">
              Close
            </button>
            <button type="submit" class="btn btn-success ">
              Submit
            </button>
          </div>
        </div>
    {{ Form::close() }}

      </div>
    </div>