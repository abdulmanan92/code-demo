@extends('layouts.master')

@php($module = 'Product Variations')
@section('title')
{{$module}}
@endsection


@section('header')
<div class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h1 class="m-0">{{$module}}</h1>
      </div><!-- /.col -->
      <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
          <li class="breadcrumb-item"><a href="{{url('home')}}">Dashboard</a></li>
          <li class="breadcrumb-item active">{{$module}}</li>
        </ol>
      </div><!-- /.col -->
    </div><!-- /.row -->
  </div><!-- /.container-fluid -->
</div>
@endsection

@section('content')

<div class="row mx-1">
	<div class="col-sm-12 col-md-12 col-lg-12">
			<div class="card">
          <div class="card-header">
            <h3 class="card-title">Manage {{$module}}</h3>
            <div class="card-tools">
              <button  type="button" class="btn btn-block btn-outline-success float-right" data-toggle="modal" data-target="#md_add_product_variation"><i class="fa fa-plus mr-1"></i> <b>Add Product Variation</b></button>
            </div>
            
          </div>
          <!-- /.card-header -->
          <div class="card-body">
            <table id="example1" class="table table-bordered  text-center">
              <thead class="bg-primary">
                <tr>
                  <th style="width: 10px">#</th>
                  <th>Name</th>
                  <th>Value</th>

                  <th>Created At</th>
                  <th>Action</th>
                </tr>
              </thead>
              <tbody>
              	 @foreach($product_variation_list as $rows)
                 <tr>
                   <td>{{$loop->iteration}}</td>
                   <td>{{$rows->name}}</td>
                   <td>{{$rows->value}}</td>

                   <td>{{FL::dateFormat($rows->created_at)}}</td>
                   <td style="width: 25%;">

                    @if(true)
                      @php($route = route('update.productvariation', ['id' =>  encrypt($rows->id)]))

                       <button  onclick="onFetchFormModal(event,'{{$route}}','#md_update_product_variation','#bind_md_update_product_variation')" class="btn btn-sm btn-outline-info"><i class="fa fa-edit mr-1"></i>Edit</button>

                       <a href="{{route('removeproductvariation',['id' => encrypt($rows->id)])}}" class="btn btn-sm btn-outline-danger"><i class="fa fa-trash-alt mr-1"></i>Remove</a>
                     @endif

                   </td>
                 </tr>
                 @endforeach
              </tbody>
            </table>
          </div>
      
        </div>
	</div>
</div>
   
@endsection

@section('modals')
  @include('product_variations.modals.add_product_variation')

  <div id="bind_md_update_product_variation"></div>

@endsection




