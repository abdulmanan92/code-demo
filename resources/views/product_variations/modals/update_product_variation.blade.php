@php($uniq_id = uniqid() )
<div class="modal modal-blur fade" id="md_update_product_variation" tabindex="-1" role="dialog" aria-hidden="true">
      <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
           {{ Form::open(array('route' => 'update.productvariation','id'=>'form_update_product_variation', 'enctype' => 'multipart/form-data')) }}
          <div class="modal-header">
            <h4 class="modal-title">Update Product Variation</h4>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">×</span>
              </button>
          </div>
          <div class="modal-body">
              <div class="row">
              <div class="col-sm-12 col-md-12 col-lg-12 mt-1">
                @php($label = 'Name')
                @php($name = 'name')
                <label class="form-label" >{{$label}} <i style = "color:red;">*</i></label>
                <small style = "color:red;" class="form-errors float-right req" value="*"></small>
                <input  required="" type="text" name="{{$name}}" value="{{ ($product_variation->name) }}" class = "input-group mb-3 form-control" placeholder = "{{$label}}" aria-label = "Default" aria-describedby = "inputGroup-sizing-default">
              </div>

              <div class="col-sm-12 col-md-12 col-lg-12 mt-1">
                @php($label = 'Value')
                @php($name = 'value')
                <label class="form-label" >{{$label}} <i style = "color:red;">*</i></label>
                <small style = "color:red;" class="form-errors float-right req" value="*"></small>
                <input  required="" type="text" name="{{$name}}" value="{{ ($product_variation->value) }}" class = "input-group mb-3 form-control" placeholder = "{{$label}}" aria-label = "Default" aria-describedby = "inputGroup-sizing-default">
              </div>

              
              

          </div>
          </div>
        
          <div class="modal-footer">
            <button  class="btn btn-danger" data-bs-dismiss="modal">
              <input type="hidden" name="product_variation" value="{{encrypt($product_variation->id)}}">
              Close
            </button>
            <button type="submit" class="btn btn-success ">
              Submit
            </button>
          </div>
        </div>
    {{ Form::close() }}

      </div>
    </div>