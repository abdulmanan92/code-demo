@extends('layouts.master')

@php($module = 'Assign Permission')
@section('title')
{{$module}}
@endsection


@section('header')
<div class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h1 class="m-0">{{$module}}</h1>
      </div><!-- /.col -->
      <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
          <li class="breadcrumb-item"><a href="{{url('home')}}">Dashboard</a></li>
          <li class="breadcrumb-item active">{{$module}}</li>
        </ol>
      </div><!-- /.col -->
    </div><!-- /.row -->
  </div><!-- /.container-fluid -->
</div>
@endsection

@section('content')

<div class="row mx-1">
	<div class="col-sm-12 col-md-12 col-lg-12">
			<div class="card">
           {{ Form::open(array('route' => 'permission.assign','id'=>'form_add_role', 'enctype' => 'multipart/form-data')) }}

          <div class="card-header">
            <h3 class="card-title"> {{$module}}</h3>
            <div class="card-tools">
             
            </div>
            
          </div>
          <!-- /.card-header -->
          <div class="card-body">
            <div class="table-responsive">
            <table id="example1" class="table table-bordered table-sm text-wrap  ">
              <tbody>
                @foreach($module_list as $module)
                <tr>
                  <th>{{$module}}</th>
                  <td>
                    <div class="row mx-4">
                    @foreach($permission_list->whereIn('module',[$module]) as $rows)
                    
                      <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group d-inline">
                        <label class="form-check-label">
                        <input value="{{$rows->name}}" {{ $role->hasPermissionTo($rows) ? 'checked' : '' }}  type="checkbox" name="permissions[]" class="form-check-input">{{$rows->name}}
                      </label>
                    </div>
                      </div>
                    @endforeach
                    </div>

                  </td>
                </tr>
                @endforeach
              </tbody>
            </table>
          </div>
          </div>
          <div class="card-footer">
            <input type="hidden" name="role" value="{{encrypt($role->id)}}">
            <button type="submit" class="btn btn-success float-right">Submit</button>
          </div>
           {{ Form::close() }}

      
        </div>
	</div>
</div>
   
@endsection







