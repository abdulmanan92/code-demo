@extends('layouts.master')

@php($module = 'Roles and Permission')
@section('title')
{{$module}}
@endsection


@section('header')
<div class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h1 class="m-0">{{$module}}</h1>
      </div><!-- /.col -->
      <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
          <li class="breadcrumb-item"><a href="{{url('home')}}">Dashboard</a></li>
          <li class="breadcrumb-item active">{{$module}}</li>
        </ol>
      </div><!-- /.col -->
    </div><!-- /.row -->
  </div><!-- /.container-fluid -->
</div>
@endsection

@section('content')

<div class="row mx-1">
	<div class="col-sm-12 col-md-12 col-lg-12">
			<div class="card">
          <div class="card-header">
            <h3 class="card-title">Manage {{$module}}</h3>
            <div class="card-tools">
              @if(Auth::user()->hasAnyPermission(['All','Add Role']))

              <button  type="button" class="btn btn-block btn-success float-right" data-toggle="modal" data-target="#md_add_role"><i class="fa fa-plus mr-1"></i> <b>Add Role</b></button>
              @endif
            </div>
            
          </div>
          <!-- /.card-header -->
          <div class="card-body">
            <table id="example1" class="table table-bordered table-sm  text-center">
              <thead class="bg-primary">
                <tr>
                  <th style="width: 10px">#</th>
                  <th>Name</th>
                  <th>Created At</th>
                  <th>Action</th>
                </tr>
              </thead>
              <tbody>

              	 @foreach($role_list as $rows)
                 <tr>
                   <td>{{$loop->iteration}}</td>
                   
                   <td>{{$rows->name}}</td>
                 


                   <td>{{FL::dateFormat($rows->created_at)}}</td>
                   <td style="width: 25%;">

                    @if(true)
                      @php($route = route('update.role', ['id' =>  encrypt($rows->id)]))
              @if(Auth::user()->hasAnyPermission(['All','Assign Permission']))

                       <a href="{{route('rolepermission',['id' => encrypt($rows->id)])}}" class="btn btn-sm btn-outline-success my-2"><i class="fa fa-lock mr-1"></i>Permission</a>
                       @endif

              @if(Auth::user()->hasAnyPermission(['All','Edit Role']))

                       <button  onclick="onFetchFormModal(event,'{{$route}}','#md_update_role','#bind_md_update_role')" class="btn btn-sm btn-outline-info my-2"><i class="fa fa-edit mr-1"></i>Edit</button>
                       @endif

              @if(Auth::user()->hasAnyPermission(['All','REmove Role']))


                       <a href="{{url('removerole',['id' => encrypt($rows->id)])}}" class="btn btn-sm btn-outline-danger my-2"><i class="fa fa-trash-alt mr-1"></i>Remove</a>
                       @endif
                     @endif

                   </td>
                 </tr>
                 @endforeach
              </tbody>
            </table>
          </div>
      
        </div>
	</div>
</div>
   
@endsection

@section('modals')
  @include('roles_permissions.modals.add_role')

  <div id="bind_md_update_role"></div>

@endsection

@section('scripts')

@endsection






