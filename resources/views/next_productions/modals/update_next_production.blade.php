@extends('layouts.master')

@php($module = 'Update Next Production')
@section('title')
{{$module}}
@endsection


@section('header')
<div class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h1 class="m-0">{{$module}}</h1>
      </div><!-- /.col -->
      <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
          <li class="breadcrumb-item"><a href="{{url('home')}}">Dashboard</a></li>
          
          <li class="breadcrumb-item active">{{$module}}</li>
        </ol>
      </div><!-- /.col -->
    </div><!-- /.row -->
  </div><!-- /.container-fluid -->
</div>
@endsection

@section('content')

<div class="row mx-1">
  <div class="col-sm-12 col-md-12 col-lg-12">
      <div class="card">
         {{ Form::open(array('route' => 'update.nextproduction','id'=>'form_update_production', 'enctype' => 'multipart/form-data')) }}
          <div class="card-header">
            <h3 class="card-title">Update {{$module}}</h3>
        
            
          </div>
          <!-- /.card-header -->
          <div class="card-body">
           <div class="row">
              <div class="col-sm-12 col-md-6 col-lg-6 mt-1 mb-1">
                @php($label = 'Select Order No.')
                @php($name = 'order')
                <label class="form-label" >{{$label}} <i style = "color:red;"></i></label>
                <small style = "color:red;" class="form-errors float-right req" value="*"></small>
                <select onchange="showOrderProductVariant(event,this);" name="{{$name}}" class="form-control-sm form-control">
                  <option selected="" value="{{encrypt($production->order_id)}}">{{$production->order->order_no}}</option>

                 
                </select>
              </div>

              <div class="col-sm-12 col-md-6 col-lg-6 mt-1 mb-2">
   
               @php($label = 'Select Department')
                @php($name = 'department')
                <label class="form-label" >{{$label}} <i style = "color:red;">*</i></label>
                <small style = "color:red;" class="form-errors float-right req" value="*"></small>
                @php($varient_route = route('departmentvariants'))
                <select id="department" onchange="getVariantTable(event,'{{$varient_route}}','{{encrypt('0')}}','{{encrypt('0')}}','.tb_show_production','html');" required=""  class="form-control form-control-sm"  data-placeholder="Select Product Variations" style="width: 100%;" name="{{$name}}">
                  <option   value="{{encrypt($production->department_id)}}">{{$production->department->name}}</option>
                  {{--
                  @if($production->parent_id > 0)
                    <option   value="{{encrypt($production->department_id)}}">{{$production->department->name}}</option>
                  @else
                    @foreach($department_list as $rows)
                      <option  @if($production->department_id == $rows->id) selected @endif  value="{{encrypt($rows->id)}}">{{$rows->name}}</option>
                    @endforeach
                  @endif
                  --}}
                  
                </select>
                  @php($label = 'Do you want to create a lot?')
                @php($name = 'is_lot')
                <label style="display: none;" class="form-label mt-2" ><input  @if($production->is_lot > 0) checked @endif  onchange="getVariantTable(event,'{{$varient_route}}','{{encrypt('0')}}','{{encrypt(0)}}','.tb_show_production','html');" id="lot" type="checkbox" name="{{$name}}"> {{$label}} 
                </label>
              </div>

            

        
             



              
              

          </div>

           <div class="row" id="order_variant">

            @php($lot = $production->is_lot)

            @php($varient_route = route('departmentvariants'))

            @foreach($parent->productionDetail as $rows)

               @php($rece_qty = $rows->orderReceive->sum('quantity'))

              @php($tb_uniqid = uniqid())

              @if($rece_qty > 0)
              <div class="col-sm-12 my-4">
                <button onclick="getVariantTable(event,'{{$varient_route}}','{{encrypt($rows->product_variation_id)}}','{{encrypt($rows->id)}}','#tb_production_{{$tb_uniqid}}');" style="width: 35px;"  class="btn btn-block btn-outline-success btn-sm float-right my-2"><i class="fa fa-plus"></i></button>
                <table class="table table-bordered table-sm table-hover">
                  <thead class="bg-primary">
                    <tr>
                      <th colspan="5"><div align="center">Product: {{$rows->productVariation->product->name}}  &nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp; Color: {{$rows->productVariation->color}} &nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp; Size: {{$rows->productVariation->size}} &nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp; Quantity in Production: {{$rows->quantity}} </div></th>
                    </tr>
                  </thead>
                  <thead class="bg-dark">
                    <tr>
                      <th  class=""><div align="center">Labour</div></th>
                      <th><div align="center">Quantity</div></th>
                      <th><div align="center">Receiving Due Date</div></th>
                      <th><div align="center">Partial</div></th>
                      <th><div align="center">Action</div></th>
                    </tr>
                  </thead>
                  @php($total_quantity = 0)
                  <tbody id="tb_production_{{$tb_uniqid}}" class="tb_production_{{$tb_uniqid}} tb_show_production">
                    @foreach($production->productionDetail->where('parent_id',$rows->id) as $production_detail_rows)
               
                    <tr>
                        <input type="hidden" name="product_variant[]" value="{{encrypt($production_detail_rows->product_variation_id)}}">
                        <input type="hidden" name="order_detail[]" value="{{encrypt($production_detail_rows->id)}}">
                        <input type="hidden" name="parent_order_detail[]" value="{{encrypt($rows->id)}}">



                        @if(true)
                        <td>
                          <select class="form-control form-control-sm fstdropdown-select" name="user[]">
                            
                            <option value="{{encrypt(0)}}">Select Labour</option>

                            @foreach($production->department->users->where('status','Active')->where('type','Team Member') as $user)
                              <option @if($user->userOrderDetailVerification($rows->id,$rows->department_id) >= 2 )disabled="" @endif  @if($production_detail_rows->user_id == $user->id) selected @endif value="{{encrypt($user->id)}}">{{$user->name}}</option>
                            @endforeach
                          </select>
                        </td>
                        @endif

                        <td>
                          <input @if(@count($production_detail_rows->orderReceive) > 0) readonly @endif onkeyup="calculateTotalAmount(this);" value="{{$production_detail_rows->quantity}}" type="text" name="quantity[]" placeholder="Quantity" class="form-control form-control-sm amount_list">
                        </td>

                        <td>
                          <input value="{{FL::datePhpFormat($production_detail_rows->receive_date)}}" required="" type="date" name="receive_date[]" placeholder="Quantity" class="form-control form-control-sm">
                        </td>

                        <td>
                          

                          <input  @if($production_detail_rows->is_partial > 0) checked @endif onchange="isPartial(this);" type="checkbox" name="partial_check" >
                          <input  class="partial"  type="hidden" name="partial[]" value="{{$production_detail_rows->is_partial}}">
                        </td>

                        <td><button class="btn btn-sm btn-block btn-outline-danger" onclick="removeTableRow(this);">Remove</button></td>


                      @php($total_quantity += $production_detail_rows->quantity)

                      </tr>
                    @endforeach
                    
                  </tbody>

                  <tfoot>
                    <input type="hidden" class="total_qty" value="{{$rece_qty}}">
                    <tr>
                      <th  class=""></th>
                      <th class="total">Total Quantity: {{$total_quantity}}</th>
                      <th class="remaining">Remaining Quantity:{{$rece_qty - $total_quantity}}</th>
                      <th colspan="2"></th>
                    </tr>
                  </tfoot>
                </table>
              </div>
              @endif
            @endforeach
                
           </div>
              
          </div>
      <div class="card-footer">
        <input type="hidden" name="production" value="{{encrypt($production->id)}}">
          <button type="submit" class="btn  btn-success float-right">Update</button>
      </div>
       {{ Form::close() }}
        </div>
  </div>
</div>
   
@endsection

@section('modals')

  <div id="bind_md_order_detail"></div>


@endsection


@section('scripts')
  @include('productions.scripts.production_script')
@endsection





