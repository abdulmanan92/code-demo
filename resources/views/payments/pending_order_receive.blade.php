@extends('layouts.master')

@php($module = 'Pending Payment Receive Orders')
@section('title')
{{$module}}
@endsection


@section('header')
<div class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h1 class="m-0">Manage Labour's Payable Payments</h1>
      </div><!-- /.col -->
      <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
          <li class="breadcrumb-item"><a href="{{url('home')}}">Dashboard</a></li>
          
          <li class="breadcrumb-item active"><span class="m-0">Manage Labour's Payable Payments</span></li>
        </ol>
      </div><!-- /.col -->
    </div><!-- /.row -->
  </div><!-- /.container-fluid -->
</div>
@endsection

@section('content')

<div class="row mx-1">
	<div class="col-sm-12 col-md-12 col-lg-12">
			<div class="card">
          <div class="card-header">
            
            <div class="card-tools">
              
            </div>
            
          </div>
          <!-- /.card-header -->
          <div class="card-body">
            
          <div  id="tableCustomFilters" class="row mt-1 mb-4"></div>

            <div class="table-responsive">
            <table id="my-datatable" class="table table-bordered  text-center table-sm">
              <thead class="bg-primary">
                <tr>

                  <th ><input type="checkbox" onchange="selectAllCheckBoxes(this,'.checked_list');" ></th>
           
                  <th>Date</th>
                  <th>Order # </th>
                  <th>Labour</th>
                  <th>Department</th>
                  <th>Product</th>
                  <th>Size</th>
                  <th>Color</th>
                  <th>Quantity</th>
                  <th>Rate</th>
                  <th>Bonus</th>
                </tr>
              </thead>
              <tbody>

              	 @foreach($pending_list->sortByDesc('id') as $rows)
                  <tr>

                   <td>
              @if(Auth::user()->hasAnyPermission(['All','Create Invoice1']))

                     <input  class="checked_list" onchange="selectedPayment(this);" type="checkbox" name="selected_payment">
                     <input  type="hidden" name="checked[]">
                     <input type="hidden" name="order_receive[]" value="{{encrypt($rows->id)}}">
                     @endif
                   </td>
                   <td>{{FL::dateFormatWithTime($rows->date)}}</td>
                   <td>{{$rows->production->order->order_no}} </td>
                   <td>{{$rows->orderDetail->user->name}}</td>
                   <td>{{$rows->production->department->name}}</td>
                   <td>{{$rows->orderDetail->productVariation->product->name}}</td>
                   <td>{{$rows->orderDetail->productVariation->size}}</td>
                   <td>{{$rows->orderDetail->productVariation->color}}</td>
                   <td>{{$rows->quantity}}</td>
                   <td>{{FL::numberFOrmat($rows->production_cost)}}</td>
                   <td>{{FL::numberFOrmat($rows->bonus_cost)}}</td>

                
                 </tr>
                 @endforeach
              </tbody>
            </table>
            </div>
          </div>
      
        </div>
	</div>
</div>
   
@endsection

@section('modals')
  @include('orders.modals.add_order')

  <div id="bind_md_add_payment"></div>


@endsection


@section('scripts')
<script type="text/javascript">

  
  var selected_payments = [];

  function selectedPayment(obj)
  {
     obj = $(obj);

     obj.next().val(0);

     if(obj.prop('checked'))
     {
        obj.next().val(1);

     }

     verifyCheckedList();

  }


  function selectAllCheckBoxes(obj,class_name)
  {
    obj = $(obj);

     $(class_name).each(function(){

      if(obj.prop('checked'))
      {
          $(this).attr('checked',true);

      }
      else
      {
          $(this).removeAttr('checked',true);
      }

     });


     verifyCheckedList();

  }

  function verifyCheckedList()
  {
    selected_payments = [];

    $('.card-tools').html('');

    var flag = false;

     $('.checked_list').each(function(){

          if($(this).prop('checked'))
          {
             $('.card-tools').html('<button onclick="showInvoiceModel(event);" type="button" class="btn btn-block btn-success float-right" ><i class="fa fa-plus mr-1"></i> <b>Generate Payment Slip</b></button>');

             selected_payments.push($(this).next().next().val());

             flag = true;
          }

     });

     return flag;
  }

  function calculateInvoice()
  {
      var advacne = isDefaulter($('input[name=advance_amount]').val());

      var pocket = isDefaulter($('input[name=pocket_money]').val());

      var advance_production = 0

      $('.advance_production').each(function(){

        advance_production += isDefaulter($(this).val());
      });




      var total_cost = isDefaulter($('#total_cost').html());

      $('input[name=net_cost]').val((total_cost - (advacne + pocket + advance_production)));


  }

  function showInvoiceModel(event)
  {
    event.preventDefault();

    if(verifyCheckedList())
    {
  var token = "{{Session::token()}}";
      
      var dataset = {'order_receive':selected_payments,_token:token}

      $.post("{{route('showoendingorderpayments')}}",dataset,function(data)
      {
        $('#bind_md_add_payment').html('');

        if(data.status == 1)
        {
          $('#bind_md_add_payment').append(data.view);

          $('#md_add_payment').modal('show');
        }
        else
        {
          alert(data.view);
        }

        

     

      });
    }

    
  }

  

  
    $(function(){

      $('document').ready(function(){

             $('#my-datatable').DataTable({
            "pageLength": 100,
              
              "columnDefs": [
                { "type": "numeric-comma", targets: 0 }
            ],

    initComplete: function () {

       var api = this.api();

  
            createSearchableDropDown('#tableCustomFilters',api,2,'Select Order No.','col-sm-2');
            createSearchableDropDown('#tableCustomFilters',api,3,'Select Labour','col-sm-2');
            createSearchableDropDown('#tableCustomFilters',api,4,'Select Department','col-sm-2');
            createSearchableDropDown('#tableCustomFilters',api,5,'Select Product','col-sm-2');
            createSearchableDropDown('#tableCustomFilters',api,6,'Select Size','col-sm-2');
            createSearchableDropDown('#tableCustomFilters',api,7,'Select Color','col-sm-2');




           
    }
  });
  // END
      });

    });
  </script>
@endsection





