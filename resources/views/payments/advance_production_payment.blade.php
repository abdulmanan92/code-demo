@extends('layouts.master')

@php($module = 'Advance Production Payment')
@section('title')
{{$module}}
@endsection


@section('header')
<div class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h1 class="m-0">{{$module}}</h1>
      </div><!-- /.col -->
      <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
          <li class="breadcrumb-item"><a href="{{url('home')}}">Dashboard</a></li>
          
          <li class="breadcrumb-item active">{{$module}}</li>
        </ol>
      </div><!-- /.col -->
    </div><!-- /.row -->
  </div><!-- /.container-fluid -->
</div>
@endsection

@section('content')

<div class="row mx-1">
  <div class="col-sm-12 col-md-12 col-lg-12">
      <div class="card">
          <div class="card-header">
            <h3 class="card-title">Manage {{$module}}</h3>
             <div class="card-tools">
              @if(Auth::user()->hasAnyPermission(['All','Add Advance Payment']))

              <button  type="button" class="btn btn-block btn-success float-right" data-toggle="modal" data-target="#md_add_production_payment"><i class="fa fa-plus mr-1"></i> <b>Add Advance Production Payment</b></button>
              @endif
            </div>
            
          </div>
          <!-- /.card-header -->
          <div class="card-body">
            
          <div  id="tableCustomFilters" class="row mt-1 mb-4"></div>

            @if(Auth::user()->hasAnyPermission(['All','View Advance Payment']))

            <div class="table-responsive">
            <table id="my-datatable" class="table table-bordered  text-center table-sm">
              <thead class="bg-primary">
                <tr>
                  <th style="width: 10px">#</th>
                  <th>PAYMENT SLIP  # </th>

                  <th>DATE</th>
                  <th>ORDER # </th>
                  <th>PRODUCT INFO </th>
                  <th>DEPARTMENT</th>
                  <th>LABOUR NAME </th>
                  <th>TOTAL AMOUNT </th>
                  <th>STATUS</th>
                  <th>ACTIONS</th>
                </tr>
              </thead>
              <tbody>

                 @foreach($payment_list->sortByDesc('id') as $rows)

                 
                  @php($department_list = $rows->paymentHistories->pluck('orderDetail.production.department.name')->unique()->toArray())
                  <tr>
                   <td>{{$loop->iteration}}</td>
                <td>{{FL::generateNumber($rows->id)}} 
                    <br>
                    <a class="btn btn-xs btn-primary" target="_blank" href="{{route('invoicedoc',['id' => encrypt($rows->id)])}}">(Office Copy)</a>
                    <br>
                    <a class="btn btn-xs btn-info" target="_blank" href="{{route('invoicelabourdoc',['id' => encrypt($rows->id)])}}">(Labour Copy)</a>
                   </td>

                   <td>{{FL::dateFormatWithTime($rows->date)}}</td>
                   <td>{{implode(' ',$rows->paymentHistories->pluck('orderDetail.production.order.order_no')->unique()->toArray())}}</td>
                    @php($product_info = $rows->paymentHistories->pluck('orderDetail.productVariation')->unique()->first())

                   <td>{{$product_info->product->name}} | {{$product_info->color}} | {{$product_info->size}}</td>

                   <td>@if(@count($department_list) > 0) {{implode(' ',$department_list)}} @else {{$rows->user->department->name}} @endif</td>
                   <td>{{$rows->user->name}}</td>
            
                   <td>{{FL::numberFormat($rows->amount)}}</td>

                   <td>{{$rows->status}}</td>

                   <td>
              @if(Auth::user()->hasAnyPermission(['All','Make Payment']))

                    @if($rows->status == "Unpaid")
                      <a onclick="paymentPopUp(event,this);" class="btn btn-sm btn-outline-success mx-1" href="{{route('updatepaymentstatus',['id' => encrypt($rows->id)])}}"><i class="fa fa-check"></i>&nbsp;Make Payment</a>
                    @endif
                    @endif
              @if(Auth::user()->hasAnyPermission(['All','Remove Payment']))

                    <a onclick="removePopUp(event,this);" class="btn btn-sm btn-outline-danger mx-1" href="{{route('removepayment',['id' => encrypt($rows->id)])}}"><i class="fa fa-times"></i>&nbsp;Remove</a>
                    @endif
                     
                   </td>
                 </tr>
                 @endforeach
              </tbody>
            </table>
            </div>
            @endif
          </div>
      
        </div>
  </div>
</div>
   
@endsection

@section('modals')
 
@include('payments.modals.advance_production_payment')

@endsection


@section('scripts')
<script type="text/javascript">

  
  
  function getProductionOrderDetail(event,obj,key_pair)
  {
    var data_set = {id:$(obj).val()};
    $.LoadingOverlay("show");

    $.get("{{route('productionorderdetail')}}",data_set,function(data)
    {
    $.LoadingOverlay("hide");

        $('#order_detail_'+key_pair).html(data);

      document.querySelector('#order_detail_'+key_pair).fstdropdown.rebind();

    });
  }


  function getOrderDetailInfo(event,obj,key_pair)
  {
    var data_set = {id:$(obj).val()};
    $.LoadingOverlay("show");

    $.get("{{route('productionorderdetailinfo')}}",data_set,function(data)
    {
    $.LoadingOverlay("hide");
      
        $('#available_amount_'+key_pair).val(data.remaining_amount);

        // if(data.is_partial > 0)
        // {
            $('#amount_'+key_pair).removeAttr('readonly');

            $('#amount_'+key_pair).attr('min',0);
            $('#amount_'+key_pair).attr('max',data.remaining_amount);

            $('#amount_'+key_pair).val(0);

            $('#user_'+key_pair).val(data.user);



        // }
        // else
        // {
            // $('#amount_'+key_pair).attr('min',0);
            // $('#amount_'+key_pair).attr('max',data.remaining_amount);
            // $('#amount_'+key_pair).attr('readonly',true);

            // $('#amount_'+key_pair).val(data.remaining_amount);
        // }

    });
  }
  

  
    $(function(){

      $('document').ready(function(){

             $('#my-datatable').DataTable({
              pageLength:50,
              "columnDefs": [
                { "type": "numeric-comma", targets: 0 }
            ],

    initComplete: function () {

       var api = this.api();

  
            createSearchableDropDown('#tableCustomFilters',api,3,'Select Order No.','col-sm-3');
            createSearchableDropDown('#tableCustomFilters',api,4,'Select Department','col-sm-3');

            createSearchableDropDown('#tableCustomFilters',api,5,'Select Labour','col-sm-3');
            createSearchableDropDown('#tableCustomFilters',api,7,'Select Status','col-sm-3');




           
    }
  });
  // END
      });

    });
  </script>
@endsection





