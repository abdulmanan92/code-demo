@extends('layouts.master')

@php($module = 'Payroll')
@section('title')
{{$module}}
@endsection


@section('header')
<div class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h1 class="m-0">{{$module}}</h1>
      </div><!-- /.col -->
      <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
          <li class="breadcrumb-item"><a href="{{url('home')}}">Dashboard</a></li>
          
          <li class="breadcrumb-item active">{{$module}}</li>
        </ol>
      </div><!-- /.col -->
    </div><!-- /.row -->
  </div><!-- /.container-fluid -->
</div>
@endsection

@section('content')

<div class="row mx-1">
	<div class="col-sm-12 col-md-12 col-lg-12">
			<div class="card">
          <div class="card-header">
            <h3 class="card-title">Manage {{$module}}</h3>
            <div class="card-tools">
              
            </div>
            
          </div>
          <!-- /.card-header -->
          <div class="card-body">
            
          <div  id="tableCustomFilters" class="row mt-1 mb-4"></div>

            <div class="table-responsive">
            <table id="my-datatable" class="table table-bordered  text-center table-sm">
              <thead class="bg-primary">
                <tr>
                  <th style="width: 10px">#</th>
                  <th style="width:10%;">PAYMENT SLIP  # </th>

                  <th>DATE</th>
                  <th>ORDER # </th>
                  <th>DEPARTMENT</th>
                  <th>LABOUR NAME </th>
                  <th>TOTAL AMOUNT </th>
                  <th>STATUS</th>
                  <th>ACTIONS</th>
                </tr>
              </thead>
              <tbody>

              	 @foreach($payment_list->sortByDesc('id') as $rows)

                  @php($department_list = $rows->paymentHistories->pluck('orderDetail.production.department.name')->unique()->toArray())
                  <tr>
                   <td>{{$loop->iteration}}</td>
                   <td>{{FL::generateNumber($rows->id)}} 
                    <br>
                    <a class="btn btn-xs btn-primary" target="_blank" href="{{route('invoicedoc',['id' => encrypt($rows->id)])}}">(Office Copy)</a>
                    <br>
                    <a class="btn btn-xs btn-info" target="_blank" href="{{route('invoicelabourdoc',['id' => encrypt($rows->id)])}}">(Labour Copy)</a>
                   </td>

                   <td>{{FL::dateFormatWithTime($rows->date)}}</td>
                   <td>{{implode(' ',$rows->paymentHistories->pluck('orderDetail.production.order.order_no')->unique()->toArray())}}</td>

                   <td>@if(@count($department_list) > 0) {{implode(' ',$department_list)}} @else {{$rows->user->department->name}} @endif</td>
                   <td>{{$rows->user->name}}</td>
                   <td>{{FL::numberFormat($rows->amount)}}</td>
                   <td>{{$rows->status}}</td>

                   <td>
              @if(Auth::user()->hasAnyPermission(['All','Make Payment']))

                    @if($rows->status == "Unpaid")
                      <a onclick="paymentPopUp(event,this);" class="btn btn-sm btn-outline-success mx-1" href="{{route('updatepaymentstatus',['id' => encrypt($rows->id)])}}"><i class="fa fa-check"></i>&nbsp;Make Payment</a>
                    @endif
                    @endif
              @if(Auth::user()->hasAnyPermission(['All','Remove Payment']))

                    <a onclick="removePopUp(event,this);" class="btn btn-sm btn-outline-danger mx-1" href="{{route('removepayment',['id' => encrypt($rows->id)])}}"><i class="fa fa-times"></i>&nbsp;Remove</a>
                    @endif
                     
                   </td>
                 </tr>
                 @endforeach
              </tbody>
            </table>
            </div>
          </div>
      
        </div>
	</div>
</div>
   
@endsection

@section('modals')
 


@endsection


@section('scripts')
<script type="text/javascript">

  
  

  

  
    $(function(){

      $('document').ready(function(){

             $('#my-datatable').DataTable({
              pageLength:50,
              
              "columnDefs": [
                { "type": "numeric-comma", targets: 0 }
            ],

    initComplete: function () {

       var api = this.api();

  
            createSearchableDropDown('#tableCustomFilters',api,3,'Select Order No.','col-sm-3');
            createSearchableDropDown('#tableCustomFilters',api,4,'Select Department','col-sm-3');

            createSearchableDropDown('#tableCustomFilters',api,5,'Select Labour','col-sm-3');
            createSearchableDropDown('#tableCustomFilters',api,7,'Select Status','col-sm-3');




           
    }
  });
  // END
      });

    });
  </script>
@endsection





