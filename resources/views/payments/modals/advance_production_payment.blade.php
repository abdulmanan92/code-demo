@php($module = 'Advance Production Payment')

@php($uniq_id = uniqid() )
<div class="modal modal-blur fade" id="md_add_production_payment" tabindex="-1" role="dialog" aria-hidden="true">
      <div class="modal-dialog modal-xl" role="document">
        <div class="modal-content">
           {{ Form::open(array('route' => 'store.advancepayment','id'=>'form_add_order_receive', 'enctype' => 'multipart/form-data')) }}
          <div class="modal-header">
            <h4 class="modal-title">{{$module}}</h4>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">×</span>
              </button>
          </div>
          <div class="modal-body">
              <div class="row">
              <div class="col-sm-12 col-md-12 col-lg-12 mt-1">
                @php($label = 'Date')
                @php($name = 'date')
                <label class="form-label" >{{$label}} <i style = "color:red;">*</i></label>
                <small style = "color:red;" class="form-errors float-right req" value="*"></small>
                <input  required="" type="datetime-local" name="{{$name}}" value="{{ old($name) }}" class = "input-group mb-3 form-control" placeholder = "{{$label}}" aria-label = "Default" aria-describedby = "inputGroup-sizing-default">
              </div>


              <div class="col-sm-12 col-md-12 col-lg-12 mt-1 mb-2">

               @php($label = 'Select Order')
                @php($name = 'order')
                <label class="form-label" >{{$label}} <i style = "color:red;">*</i></label>
                <small style = "color:red;" class="form-errors float-right req" value="*"></small>
                <select id="order_{{$uniq_id}}" onchange="getProductionOrderDetail(event,this,'{{$uniq_id}}');" required="" class=" form-control fstdropdown-select"  data-placeholder="Select Order"  name="{{$name}}">
                  <option value="{{encrypt(0)}}">Select Production </option> 
                  @foreach($production_list as $rows)
                    <option value="{{encrypt($rows->id)}}">Order No. {{$rows->order->order_no}} | Department: {{$rows->department->name}}</option>
                  @endforeach
                </select>
              </div> 

               <div class="col-sm-12 col-md-12 col-lg-12 mt-1 mb-2">

               @php($label = 'Select Order Detail / Lot')
                @php($name = 'order_detail')
                <label class="form-label" >{{$label}} <i style = "color:red;">*</i></label>
                <small style = "color:red;" class="form-errors float-right req" value="*"></small>
                <select id="order_detail_{{$uniq_id}}" onchange="getOrderDetailInfo(event,this,'{{$uniq_id}}');" required="" class=" form-control fstdropdown-select"  data-placeholder="Select Order Detail / Lot"  name="{{$name}}">
                  <option selected="" value="{{encrypt(0)}}">Select Order Detail / Lot </option> 
                  
                </select>
              </div> 

              <div class="col-sm-12 col-md-12 col-lg-12 mt-1">
                @php($label = 'Available Amount')
                @php($name = 'available_amount')
                <label class="form-label" >{{$label}}</label>
                <small style = "color:red;" class="form-errors float-right req" value="*"></small>
                <input readonly="" id="available_amount_{{$uniq_id}}"  required="" type="number" name="{{$name}}" value="{{ old($name) }}" class = "input-group mb-3 form-control" placeholder = "{{$label}}" aria-label = "Default" aria-describedby = "inputGroup-sizing-default">
              </div>

              <div class="col-sm-12 col-md-12 col-lg-12 mt-1">
                @php($label = 'Amount')
                @php($name = 'amount')
                <label class="form-label" >{{$label}} <i style = "color:red;">*</i></label>
                <small style = "color:red;" class="form-errors float-right req" value="*"></small>
                <input id="amount_{{$uniq_id}}"  required="" type="number" name="{{$name}}" value="{{ old($name) }}" class = "input-group mb-3 form-control" placeholder = "{{$label}}" aria-label = "Default" aria-describedby = "inputGroup-sizing-default">
              </div>

              
              

          </div>
          </div>
        
          <div class="modal-footer">
           <input id="user_{{$uniq_id}}" type="hidden" name="user" value="{{encrypt(0)}}">
            <button  class="btn btn-danger" data-dismiss="modal">
              Close
            </button>
            <button type="submit" class="btn btn-success ">
              Submit
            </button>
          </div>
        </div>
    {{ Form::close() }}

      </div>
    </div>