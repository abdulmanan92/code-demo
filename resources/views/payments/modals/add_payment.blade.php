<style type="text/css">
<!--
.style1 {color: #FF0000}
-->
</style>
@php($uniq_id = uniqid() )
<div class="modal modal-blur fade" id="md_add_payment" tabindex="-1" role="dialog" aria-hidden="true">
      <div class="modal-dialog modal-xl" role="document">
        <div class="modal-content">
           {{ Form::open(array('route' => 'store.payment','id'=>'form_add_order', 'enctype' => 'multipart/form-data')) }}
          <div class="modal-header">
            <h4 class="modal-title">Generate Payment Slip</h4>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">×</span>
            </button>
          </div>
          <div class="modal-body">
              <div class="row">

                <div class="col-sm-12 col-md-12 col-lg-12 mt-1">
                @php($label = 'Payment Referecne Date(s)')
                @php($name = 'ref')
                  <label class="form-label" >{{$label}} </label>
                <small style = "color:red;" class="form-errors float-right req" value="*"></small>
                <input disabled="" type="text" name="{{$name}}" value="{{FL::dateFormat($order_receive_list->first()->date)}} - {{FL::dateFormat($order_receive_list->last()->date)}}"  class = "input-group mb-3 form-control form-control-sm" placeholder = "{{$label}}" aria-label = "Default" aria-describedby = "inputGroup-sizing-default">
              </div>

              <div class="col-sm-12 col-md-6 col-lg-6 mt-1">
                @php($label = 'Name of Labour Team Member')
                @php($name = 'labour')
                  <label class="form-label" >{{$label}} </label>
                <small style = "color:red;" class="form-errors float-right req" value="*"></small>
                <input value="{{$order_receive_list->first()->orderDetail->user->name}}" disabled="" type="text" name="{{$name}}"  class = "input-group mb-3 form-control form-control-sm" placeholder = "{{$label}}" aria-label = "Default" aria-describedby = "inputGroup-sizing-default">
              </div>

               <div class="col-sm-12 col-md-6 col-lg-6 mt-1">
                @php($label = 'Department')
                @php($name = 'department')
                 <label class="form-label" >{{$label}} </label>
                <small style = "color:red;" class="form-errors float-right req" value="*"></small>
                <input value="{{$order_receive_list->first()->production->department->name}}" disabled="" type="text" name="{{$name}}"  class = "input-group mb-3 form-control form-control-sm" placeholder = "{{$label}}" aria-label = "Default" aria-describedby = "inputGroup-sizing-default">
              </div>


              <div class="col-sm-12 col-md-12 col-lg-12 mt-3 mb-2">
                <table class="table table-sm text-center table-bordered">
                  <thead class="bg-primary">
                    <tr>
                      <th>DATE</th>
                      <th>PRODUCT</th>
                      <th>COLOR</th>
                      <th>SIZE</th>
                      <th>QTY</th>

                      <th>RATE (RS) </th>
                      <th>PAYABLE (RS)</th>
                      <!-- <th>BONUS (RS)</th> -->
                    </tr>
                  </thead>
                  <tbody>
                    @foreach($order_receive_list as $rows)
                    <input type="hidden" name="order_receive[]" value="{{encrypt($rows->id)}}">
                    <tr>
                      <td>{{FL::dateFormatWithTime($rows->date)}}</td>
                    <td>{{$rows->orderDetail->productVariation->product->name}}</td>
                    <td>{{$rows->orderDetail->productVariation->size}}</td>
                    <td>{{$rows->orderDetail->productVariation->color}}</td>
                    <td>{{$rows->quantity}}</td>

                    <td>{{$rows->orderDetail->rate}}</td>
                    <td>{{FL::numberFormat($rows->production_cost)}}</td>
                    <!-- <td>{{FL::numberFOrmat($rows->bonus_cost)}}</td> -->
                    </tr>
                    @endforeach
                  </tbody>
                  <tfoot style="font-weight: bold;">
                    <tr>
                    <td colspan="6" style="text-align: right;">TOTAL AMOUNT (RS) </td>
                    {{--

                    <td id="total_cost">{{$order_receive_list->sum('production_cost') + $order_receive_list->sum('bonus_cost')}}</td>
                    --}}

                     <td id="total_cost">{{$order_receive_list->sum('production_cost')}}</td>
                  </tr>

                  @if(@count($ledger_list) > 0)
                    @php($advance_list = $ledger_list->whereIn('payment_type',['Advance Amount']))

                    @php($advance = $advance_list->sum('debit') - $advance_list->sum('credit')) 

                    @php($pocket_list = $ledger_list->whereIn('payment_type',['Pocket Money']))

                    @php($pocket = $pocket_list->sum('debit') - $pocket_list->sum('credit') )




                    @if($advance > 0)
                    <tr>
                      <input type="hidden" name="previous_advance" value="{{$advance}}" >
                      <td><div align="left"><span class="style1">Current Balance of Advance Amount:  Rs {{($advance)}}</span></div></td>
                      <td colspan="5" style="text-align: right;">DEDUCTION OF ADVANCE AMOUNT (RS) -</td>
                      <td><input onkeyup="calculateInvoice();" placeholder="Advacne Amount" class="form-control form-control-sm"  min="0" max="{{abs($advance)}}" type="number" name="advance_amount"></td>
                    </tr>
                    @endif


                    @if($pocket > 0)
                      <tr>
                      <input type="hidden" name="previous_pocket" value="{{$pocket}}" >

                        <td><div align="left"><span class="style1">Current Balance of Pocket Money Amount: Rs {{($pocket)}}</span></div></td>
                      <td colspan="5" style="text-align: right;">DEDUCTION OF POCKET MONEY AMOUNT (RS) - </td>

                      <td><input onkeyup="calculateInvoice();" placeholder="Pocket Money" class="form-control form-control-sm" min="0" max="{{abs($pocket)}}" type="number" name="pocket_money"></td>
                    </tr>
                    @endif
                    @foreach($order_receive_list->unique('order_detail_id') as $order_receive)

                    @php($order_detail = $order_receive->orderDetail)

                      @php($advance_production_list = $ledger_list->whereIn('payment_type',['Advance Production Amount'])->whereIn('order_detail_id',$order_detail['id']))
                     
                      @php($advance_production = $advance_production_list->sum('debit') - $advance_production_list->sum('credit')) 


                      @if($advance_production > 0)
                        <tr>
                          <td><div align="left"><span class="style1">Current Balance of Advance Order No. {{$order_detail->production->order->order_no}} |   {{$order_detail->productVariation->product->name}} | {{$order_detail->productVariation->size}} | {{$order_detail->productVariation->color}} Amount : Rs {{($advance_production)}}</span></div></td>
                        <td colspan="5" style="text-align: right;">DEDUCTION OF ADVANCE PRODUCTION AMOUNT (RS) - </td>

                        <input type="hidden" name="advance_order_detail[]" value="{{encrypt($order_detail['id'])}}">

                        <input type="hidden" name="advance_order_receive[]" value="{{encrypt($order_receive['id'])}}">


                        <td><input onkeyup="calculateInvoice();" placeholder="ADVANCE PRODUCTION AMOUNT " class="form-control advance_production form-control-sm" min="0" max="{{abs($advance_production)}}" type="number" name="advance_production[]"></td>
                      </tr>
                      @endif

                    @endforeach

                    
                  @endif

                   <tr>
                    <td colspan="6" style="text-align: right;">NET PAYABLE AMOUNT (RS)</td>

                    <td >
                      {{--
                                            <input readonly="" class="form-control" type="number" name="net_cost" min="0" value="{{$order_receive_list->sum('production_cost') + $order_receive_list->sum('bonus_cost')}}">
                      --}}
                        <input readonly="" class="form-control" type="number" name="net_cost" min="0" value="{{$order_receive_list->sum('production_cost')}}">
                    </td>
                  </tr>
                  </tfoot>
                </table>
              </div>


              
              

          </div>
          </div>
        
          <div class="modal-footer">
            <button  class="btn btn-danger" data-dismiss="modal">
              Close
            </button>
            <button type="submit" class="btn btn-success ">
              Submit
            </button>
          </div>
        </div>
    {{ Form::close() }}

      </div>
</div>