@extends('layouts.master')

@php($module = 'Products')
@section('title')
{{$module}}
@endsection


@section('header')
<div class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h1 class="m-0">{{$module}}</h1>
      </div><!-- /.col -->
      <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
          <li class="breadcrumb-item"><a href="{{url('home')}}">Dashboard</a></li>
          <li class="breadcrumb-item active">{{$module}}</li>
        </ol>
      </div><!-- /.col -->
    </div><!-- /.row -->
  </div><!-- /.container-fluid -->
</div>
@endsection

@section('content')

<div class="row mx-1">
	<div class="col-sm-12 col-md-12 col-lg-12">
			<div class="card">
          <div class="card-header">
            <h3 class="card-title">Manage {{$module}}</h3>
            <div class="card-tools">
              @if(Auth::user()->hasAnyPermission(['All','Add Product']))

              <button  type="button" class="btn btn-block btn-success float-right" data-toggle="modal" data-target="#md_add_product"><i class="fa fa-plus mr-1"></i> <b>Add Product</b></button>
              @endif
            </div>
            
          </div>
          <!-- /.card-header -->
          <div class="card-body">
            <table id="example1" class="table table-bordered  text-center table-sm">
              <thead class="bg-primary">
                <tr>
                  <th style="width: 10px">#</th>
                  <th>Photo</th>
                  <th>Name</th>
                  <th>Article No.</th>
                  <th>Created At</th>
                  <th>Action</th>
                </tr>
              </thead>
              <tbody>

              	 @foreach($product_list as $rows)
                 <tr>
                   <td>{{$loop->iteration}}</td>
                   <td>
                     <img width="35" height="35" onerror="this.src='{{asset("graphics/na-image.png")}}'"  src="{{FL::getImage($rows->dp)}}" alt="User Image">
                   </td>
                   <td>{{$rows->name}}</td>
                   <td>{{$rows->article_no}}</td>
                 


                   <td>{{FL::dateFormat($rows->created_at)}}</td>
                   <td style="width: 25%;">

                    @if($rows->is_auto == 0)
                      @php($route = route('update.product', ['id' =>  encrypt($rows->id)]))
              @if(Auth::user()->hasAnyPermission(['All','Edit Product']))

                       <button  onclick="onFetchFormModal(event,'{{$route}}','#md_update_product','#bind_md_update_product')" class="btn btn-sm btn-outline-info"><i class="fa fa-edit mr-1"></i>Edit</button>
                       @endif

              @if(Auth::user()->hasAnyPermission(['All','Remove Product']))

                      @if(@count($rows->getProductVariants) == 0)

                       <a onclick="removePopUp(event,this);" href="{{route('removeproduct',['id' => encrypt($rows->id)])}}" class="btn btn-sm btn-outline-danger"><i class="fa fa-trash-alt mr-1"></i>Remove</a>
                       @endif
                       @endif
                     @endif

                     @php($route = route('logsbytype',['id' => encrypt($rows->id), 'type' => encrypt('Product')]))
                     
                     <button  onclick="onFetchFormModal(event,'{{$route}}','#md_view_logs','#bind_md_view_logs')" class="btn btn-sm btn-outline-warning"><i class="fa fa-cogs mr-1"></i>Logs</button>

                   </td>
                 </tr>
                 @endforeach
              </tbody>
            </table>
          </div>
      
        </div>
	</div>
</div>
   
@endsection

@section('modals')
  @include('products.modals.add_product')

  <div id="bind_md_update_product"></div>
  <div id="bind_md_view_logs"></div>


@endsection

@section('scripts')
<script type="text/javascript">
  function bonusIsActive(obj)
  {
     obj = $(obj);

     obj.next().val(0);

     if(obj.prop('checked'))
     {
        obj.next().val(1);
     }
  }



</script>
@endsection






