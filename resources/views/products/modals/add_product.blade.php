@php($uniq_id = uniqid() )
<div class="modal modal-blur fade" id="md_add_product" tabindex="-1" role="dialog" aria-hidden="true">
      <div class="modal-dialog modal-xl" role="document">
        <div class="modal-content">
           {{ Form::open(array('route' => 'store.product','id'=>'form_add_product', 'enctype' => 'multipart/form-data')) }}
          <div class="modal-header">
            <h4 class="modal-title">Add Product</h4>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">×</span>
              </button>
          </div>
          <div class="modal-body">
              <div class="row">
              <div class="col-sm-12 col-md-6 col-lg-6 mt-1">
                @php($label = 'Name')
                @php($name = 'name')
                <label class="form-label" >{{$label}} <i style = "color:red;">*</i></label>
                <small style = "color:red;" class="form-errors float-right req" value="*"></small>
                <input  required="" type="text" name="{{$name}}" value="{{ old($name) }}" class = "input-group mb-3 form-control" placeholder = "{{$label}}" aria-label = "Default" aria-describedby = "inputGroup-sizing-default">
              </div>

              <div class="col-sm-12 col-md-6 col-lg-6 mt-1">
                @php($label = 'Article No.')
                @php($name = 'article_no')
                <label class="form-label" >{{$label}} <i style = "color:red;">*</i></label>
                <small style = "color:red;" class="form-errors float-right req" value="*"></small>
                <input  required="" type="text" name="{{$name}}" value="{{ old($name) }}" class = "input-group mb-3 form-control" placeholder = "{{$label}}" aria-label = "Default" aria-describedby = "inputGroup-sizing-default">
              </div>

              <div class="col-sm-12 col-md-12 col-lg-12 mt-1">

               @php($label = 'Product Icon')
                @php($name = 'photo')
                <label class="form-label" >{{$label}} <i style = "color:red;"></i></label>
                <small style = "color:red;" class="form-errors float-right req" value="*"></small>
                <input style="padding: 2px;"   type="file" name="{{$name}}" value="{{ old($name) }}" class = "input-group mb-3 form-control" placeholder = "{{$label}}" aria-label = "Default" aria-describedby = "inputGroup-sizing-default">
              </div>

              <div class="col-sm-12 col-md-12 col-lg-12 mt-1 mb-2" style="display: none;">
   
               @php($label = 'Select Product Variations')
                @php($name = 'product_variation')
                <label class="form-label" >{{$label}} <i style = "color:red;"></i></label>
                <small style = "color:red;" class="form-errors float-right req" value="*"></small>
                <select  class="select2" multiple="multiple" data-placeholder="Select Product Variations" style="width: 100%;" name="{{$name}}[]">
                  
                  @foreach($product_variation_list as $rows)
                    <option value="{{encrypt($rows->id)}}">{{$rows->name}}:{{$rows->value}}</option>
                  @endforeach
                </select>
              </div> 

              <div class="col-sm-12 col-md-12 col-lg-12 mt-3 mb-2">
                <table class="table table-sm text-center table-bordered">
                  <thead class="bg-primary">
                    <tr>
                      <th>Department</th>
                      <th>Production Amount</th>
                      <th>Bonus Amount</th>
                      <th>Bonus Amount Limit</th>
                      <th>Bonus (Is Active ?)</th>



                    </tr>
                  </thead>
                  <tbody>
                    @foreach($department_list->sortBy('order') as $rows)
                    <tr>
                      <input type="hidden" name="department[]" value="{{encrypt($rows->id)}}">
                      <td>
                        {{$rows->name}}
                      </td>
                      <td>
                        <input class="form-control form-control-sm" type="number" name="production_rate[]" placeholder="Production Amount">
                      </td>
                      <td>
                        <input class="form-control form-control-sm" type="number" name="bonus[]" placeholder="Bonus Amount">
                      </td>
                      <td>
                        <input class="form-control form-control-sm" type="number" name="bonus_limit[]" placeholder="Bonus Limit Amount">
                      </td>
                      <td>
                        <input onchange="bonusIsActive(this);"  type="checkbox" name="is_active[]">

                        <input  type="hidden" name="bonus_is_active[]" value="0">
                      </td>

                    </tr>
                    @endforeach
                  </tbody>
                </table>
              </div>

              
              

          </div>
          </div>
        
          <div class="modal-footer">
            <button  class="btn btn-danger" data-dismiss="modal">
              Close
            </button>
            <button type="submit" class="btn btn-success ">
              Submit
            </button>
          </div>
        </div>
    {{ Form::close() }}

      </div>
    </div>