@php($uniq_id = uniqid() )
<div class="modal modal-blur fade" id="md_update_product" tabindex="-1" role="dialog" aria-hidden="true">
      <div class="modal-dialog modal-xl" role="document">
        <div class="modal-content">
           {{ Form::open(array('route' => 'update.product','id'=>'form_update_product', 'enctype' => 'multipart/form-data')) }}
          <div class="modal-header">
            <h4 class="modal-title">Update Product</h4>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">×</span>
              </button>
          </div>
          <div class="modal-body">
              <div class="row">
              <div class="col-sm-12 col-md-6 col-lg-6 mt-1">
                @php($label = 'Name')
                @php($name = 'name')
                <label class="form-label" >{{$label}} <i style = "color:red;">*</i></label>
                <small style = "color:red;" class="form-errors float-right req" value="*"></small>
                <input  required="" type="text" name="{{$name}}" value="{{ ($product->name) }}" class = "input-group mb-3 form-control" placeholder = "{{$label}}" aria-label = "Default" aria-describedby = "inputGroup-sizing-default">
              </div>

              <div class="col-sm-12 col-md-6 col-lg-6 mt-1">
                @php($label = 'Article No.')
                @php($name = 'article_no')
                <label class="form-label" >{{$label}} <i style = "color:red;">*</i></label>
                <small style = "color:red;" class="form-errors float-right req" value="*"></small>
                <input  required="" type="text" name="{{$name}}" value="{{ ($product->article_no) }}" class = "input-group mb-3 form-control" placeholder = "{{$label}}" aria-label = "Default" aria-describedby = "inputGroup-sizing-default">
              </div>

              <div class="col-sm-12 col-md-12 col-lg-12 mt-1">

               @php($label = 'Product Icon')
                @php($name = 'photo')
                <label class="form-label" >{{$label}} <i style = "color:red;"></i></label>
                <small style = "color:red;" class="form-errors float-right req" value="*"></small>
                <input style="padding: 2px;"   type="file" name="{{$name}}" value="{{ old($name) }}" class = "input-group mb-3 form-control" placeholder = "{{$label}}" aria-label = "Default" aria-describedby = "inputGroup-sizing-default">
              </div>

              <div class="col-sm-12 col-md-12 col-lg-12 mt-1 mb-2" style="display: none;">
   
               @php($label = 'Select Product Variations')
                @php($name = 'product_variation')
                <label class="form-label" >{{$label}} <i style = "color:red;"></i></label>
                <small style = "color:red;" class="form-errors float-right req" value="*"></small>
                <select  class="select2" multiple="multiple" data-placeholder="Select Product Variations" style="width: 100%;" name="{{$name}}[]">
                  
                  @foreach($product_variation_list as $rows)
                    <option @if(in_array($rows->id,$product->variation->pluck('product_variation_id')->toArray())) selected @endif value="{{encrypt($rows->id)}}">{{$rows->name}}:{{$rows->value}}</option>
                  @endforeach
                </select>
              </div> 

              <div class="col-sm-12 col-md-12 col-lg-12 mt-3 mb-2">
                <table class="table table-sm text-center table-bordered">
                  <thead class="bg-primary">
                    <tr>
                      <th>Department</th>
                      <th>Production Rate</th>
                      <th>Bonus Amount</th>
                      <th>Bonus Amount Limit</th>
                      <th>Bonus (Is Active ?)</th>
                    </tr>
                  </thead>
                  <tbody>
                    @foreach($product->productionRates->sortBy('department.order') as $rows)
                    <tr>
                      <input type="hidden" name="department[]" value="{{encrypt($rows->department_id)}}">
                      <td>
                        {{$rows->department->name}}
                      </td>

                      <td>
                        <input class="form-control form-control-sm" type="number" name="production_rate[]" value="{{$rows->production_rate}}">
                      </td>

                       <td>
                        <input value="{{$rows->bonus}}" class="form-control form-control-sm" type="number" name="bonus[]" placeholder="Bonus Amount">
                      </td>
                      <td>
                        <input value="{{$rows->bonus_limit}}" class="form-control form-control-sm" type="number" name="bonus_limit[]" placeholder="Bonus Limit Amount">
                      </td>
                      <td>
                        <input @if($rows->is_bonus) checked @endif onchange="bonusIsActive(this);"  type="checkbox" name="is_active[]">

                        <input  type="hidden" name="bonus_is_active[]" value="{{$rows->is_bonus}}">
                      </td>
                    </tr>
                    @endforeach
                  </tbody>
                </table>
              </div>

              
              

          </div>
          </div>
        
          <div class="modal-footer">
            <button  class="btn btn-danger" data-dismiss="modal">
              Close
            </button>
            <input type="hidden" name="product" value="{{encrypt($product->id)}}">
            <button type="submit" class="btn btn-success ">
              Update
            </button>
          </div>
        </div>
    {{ Form::close() }}

      </div>
    </div>

    <script type="text/javascript">
      
         $('.select2').select2();

    //Initialize Select2 Elements
    $('.select2bs4').select2({
      theme: 'bootstrap4'
    });

    </script>