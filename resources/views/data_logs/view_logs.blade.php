@php($uniq_id = uniqid() )
<div class="modal modal-blur fade" id="md_view_logs" tabindex="-1" role="dialog" aria-hidden="true">
      <div class="modal-dialog modal-xl" role="document">
        <div class="modal-content">
         
          <div class="modal-header">
            <h4 class="modal-title">View Logs</h4>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">×</span>
              </button>
          </div>
          <div class="modal-body">
              <div class="row">
              <div class="col-sm-12 col-md-12 col-lg-12 mt-1">
                <div class="table-responsive table-sm table-hover table-bordered">
                    <table class="table">
                        <tr>
                            <thead class="bg-primary">
                                <th>#</th>
                                <th>Date</th>
                                <th>Status</th>
                                <th>Created By</th>
                                <th>Description</th>
                            </thead>
                        </tr>

                        <tbody>
                            @foreach($data_log_list as $rows)
                            <tr>
                                <td>{{$loop->iteration}}</td>
                                <td>{{FL::dateFormat($rows->created_at)}}</td>
                                <td>{{$rows->status}}</td>
                                <td>{{$rows->createdBy->name}} (USR-000{{$rows->created_by}})</td>
                                <td>{!! $rows->description !!}</td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
              </div>

             

              
              

          </div>
          </div>
        
          <div class="modal-footer">
            <button  class="btn btn-danger" data-dismiss="modal">
              Close
            </button>
          
          </div>
        </div>


      </div>
    </div>