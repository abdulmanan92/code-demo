@php($path = asset('/'))
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>@yield('title')</title>

  <!-- Google Font: Source Sans Pro -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="{{$path}}plugins/fontawesome-free/css/all.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Tempusdominus Bootstrap 4 -->
  <link rel="stylesheet" href="{{$path}}plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css">
  <!-- iCheck -->
  <link rel="stylesheet" href="{{$path}}plugins/icheck-bootstrap/icheck-bootstrap.min.css">
  <!-- JQVMap -->
  <link rel="stylesheet" href="{{$path}}plugins/jqvmap/jqvmap.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="{{$path}}dist/css/adminlte.min.css">
  <!-- overlayScrollbars -->
  <link rel="stylesheet" href="{{$path}}plugins/overlayScrollbars/css/OverlayScrollbars.min.css">
  <!-- Daterange picker -->
  <link rel="stylesheet" href="{{$path}}plugins/daterangepicker/daterangepicker.css">
  <!-- summernote -->
  <link rel="stylesheet" href="{{$path}}plugins/summernote/summernote-bs4.min.css">

    <link rel="stylesheet" href="{{$path}}plugins/sweetalert2-theme-bootstrap-4/bootstrap-4.min.css">
  <!-- Toastr -->
  <link rel="stylesheet" href="{{$path}}plugins/toastr/toastr.min.css">

   <!-- DataTables -->
  <link rel="stylesheet" href="{{asset('/')}}plugins/datatables-bs4/css/dataTables.bootstrap4.min.css">
  <link rel="stylesheet" href="{{asset('/')}}plugins/datatables-responsive/css/responsive.bootstrap4.min.css">
  <link rel="stylesheet" href="{{asset('/')}}plugins/datatables-buttons/css/buttons.bootstrap4.min.css">

   <link rel="stylesheet" href="{{$path}}plugins/select2/css/select2.min.css">
  <link rel="stylesheet" href="{{$path}}plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css">

        <link href="{{ asset('plugins/search-box/css/search_box.min.css')}}" rel="stylesheet">


  <style type="text/css">
    .select2-container--default .select2-selection--multiple .select2-selection__choice {
    background-color: #007bff !important;
    border-color: #006fe6 !important;
    color: #fff !important;
    padding: 0 10px;
    margin-top: .31rem;
    font-weight: bolder !important;
}
  </style>




</head>
<body class="hold-transition sidebar-mini layout-fixed">
<div class="wrapper">

  <!-- Preloader -->
  <div style="background: #A9A9A9 !important;" class="preloader flex-column justify-content-center align-items-center">
    <img class="animation__shake" src="{{$path}}graphics/logo.png" alt="AdminLTELogo" >
  </div>

  <!-- Navbar -->
  <nav class="main-header navbar navbar-expand navbar-white navbar-light">
    <!-- Left navbar links -->
    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
      </li>
      <li class="nav-item d-none d-sm-inline-block">
        <a href="{{url('/')}}" class="nav-link">Profile</a>
      </li>
     
    </ul>

    <!-- Right navbar links -->
    <ul class="navbar-nav ml-auto">
      <!-- Navbar Search -->
      <li class="nav-item">
        <a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();" class="nav-link" data-widget="navbar-search" href="#" role="button">
          <i class="fa fa-sign-out-alt"></i>&nbsp;Logout
          <form  style="display: none;" id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                            @csrf
                                        </form>
        </a>
        
      </li>
      {{--
      <!-- Messages Dropdown Menu -->
      <li class="nav-item dropdown">
        <a class="nav-link" data-toggle="dropdown" href="#">
          <i class="far fa-comments"></i>
          <span class="badge badge-danger navbar-badge">3</span>
        </a>
        <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
          <a href="#" class="dropdown-item">
            <!-- Message Start -->
            <div class="media">
              <img src="{{$path}}dist/img/user1-128x128.jpg" alt="User Avatar" class="img-size-50 mr-3 img-circle">
              <div class="media-body">
                <h3 class="dropdown-item-title">
                  Brad Diesel
                  <span class="float-right text-sm text-danger"><i class="fas fa-star"></i></span>
                </h3>
                <p class="text-sm">Call me whenever you can...</p>
                <p class="text-sm text-muted"><i class="far fa-clock mr-1"></i> 4 Hours Ago</p>
              </div>
            </div>
            <!-- Message End -->
          </a>
          <div class="dropdown-divider"></div>
          <a href="#" class="dropdown-item">
            <!-- Message Start -->
            <div class="media">
              <img src="{{$path}}dist/img/user8-128x128.jpg" alt="User Avatar" class="img-size-50 img-circle mr-3">
              <div class="media-body">
                <h3 class="dropdown-item-title">
                  John Pierce
                  <span class="float-right text-sm text-muted"><i class="fas fa-star"></i></span>
                </h3>
                <p class="text-sm">I got your message bro</p>
                <p class="text-sm text-muted"><i class="far fa-clock mr-1"></i> 4 Hours Ago</p>
              </div>
            </div>
            <!-- Message End -->
          </a>
          <div class="dropdown-divider"></div>
          <a href="#" class="dropdown-item">
            <!-- Message Start -->
            <div class="media">
              <img src="{{$path}}dist/img/user3-128x128.jpg" alt="User Avatar" class="img-size-50 img-circle mr-3">
              <div class="media-body">
                <h3 class="dropdown-item-title">
                  Nora Silvester
                  <span class="float-right text-sm text-warning"><i class="fas fa-star"></i></span>
                </h3>
                <p class="text-sm">The subject goes here</p>
                <p class="text-sm text-muted"><i class="far fa-clock mr-1"></i> 4 Hours Ago</p>
              </div>
            </div>
            <!-- Message End -->
          </a>
          <div class="dropdown-divider"></div>
          <a href="#" class="dropdown-item dropdown-footer">See All Messages</a>
        </div>
      </li>
      <!-- Notifications Dropdown Menu -->
      <li class="nav-item dropdown">
        <a class="nav-link" data-toggle="dropdown" href="#">
          <i class="far fa-bell"></i>
          <span class="badge badge-warning navbar-badge">15</span>
        </a>
        <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
          <span class="dropdown-item dropdown-header">15 Notifications</span>
          <div class="dropdown-divider"></div>
          <a href="#" class="dropdown-item">
            <i class="fas fa-envelope mr-2"></i> 4 new messages
            <span class="float-right text-muted text-sm">3 mins</span>
          </a>
          <div class="dropdown-divider"></div>
          <a href="#" class="dropdown-item">
            <i class="fas fa-users mr-2"></i> 8 friend requests
            <span class="float-right text-muted text-sm">12 hours</span>
          </a>
          <div class="dropdown-divider"></div>
          <a href="#" class="dropdown-item">
            <i class="fas fa-file mr-2"></i> 3 new reports
            <span class="float-right text-muted text-sm">2 days</span>
          </a>
          <div class="dropdown-divider"></div>
          <a href="#" class="dropdown-item dropdown-footer">See All Notifications</a>
        </div>
      </li>
      <li class="nav-item">
        <a class="nav-link" data-widget="fullscreen" href="#" role="button">
          <i class="fas fa-expand-arrows-alt"></i>
        </a>
      </li>
      <li class="nav-item">
        <a class="nav-link" data-widget="control-sidebar" data-slide="true" href="#" role="button">
          <i class="fas fa-th-large"></i>
        </a>
      </li>
      --}}
    </ul>
  </nav>
  <!-- /.navbar -->

  <!-- Main Sidebar Container -->
  <aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="{{url('home')}}" class="brand-link" style="margin-left: 50px;">
      <img src="{{$path}}graphics/logo.png" alt="Motolux" class="brand-image img-circle elevation-3" style="opacity: .8">
      <span class="brand-text font-weight-light">&nbsp;</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar user panel (optional) -->
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
          <img onerror="this.src='{{asset("graphics/na-image.png")}}'" src="{{FL::getImage(Auth::user()->dp)}}" class="img-circle elevation-2" alt="User Image">
        </div>
        <div class="info">
          <a href="{{url('home')}}" class="d-block">{{Auth::user()->name}}</a>
        </div>
      </div>


      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
               @php($has_permission = ['All','Dashboard'] )
            
            @if(Auth::user()->hasAnyPermission($has_permission))
          <li class="nav-item">
            <a href="{{url('home')}}" class="nav-link {{ Request::segment(1) === 'home' ? 'active' : null }}">
              <i class="nav-icon fas fa-tachometer-alt"></i>
              <p>
                Dashboard
              </p>
            </a>
           
          </li>
          @endif
		  
        @php($has_permission = ['All','Add Work Order','Edit Work Order','Remove Work Order'] )
            
            @if(Auth::user()->hasAnyPermission($has_permission))
		      <li class="nav-item ">
            <a href="{{url('workorders')}}" class="nav-link {{ Request::segment(1) === 'workorders' ? 'active' : null }}">
              <i class="nav-icon fas fa-cart-arrow-down"></i>
              <p>
                Work Orders
              </p>
            </a>
           
          </li>
          @endif

            @php($has_permission = ['All','Add Production','Edit Production','Remove Production','View Reeceive Production','Receive Production','Remove Receive Production'] )
            
            @if(Auth::user()->hasAnyPermission($has_permission))
            <li class="nav-item ">
            <a href="{{url('workorderproduction')}}" class="nav-link {{ Request::segment(1) === 'workorderproduction' ? 'active' : null }}">
              <i class="nav-icon fas fa-cubes"></i>
              <p>
                Production
              </p>
            </a>
           
          </li>
          @endif

@php($has_permission = ['All','View Payment','Create Invoice'] )
            
            @if(Auth::user()->hasAnyPermission($has_permission))
          <li class="nav-item ">
            <a href="{{url('allreceiveorders')}}" class="nav-link {{ Request::segment(1) === 'allreceiveorders' ? 'active' : null }}">
              <i class="nav-icon fas fa-money-bill-alt"></i>
              <p>
                Labour's Payments
              </p>
            </a>
           
          </li>
          @endif

          @php($has_permission = ['All','View Advance Payment','Add Advacne Payment'] )

            @if(Auth::user()->hasAnyPermission($has_permission))
          <li class="nav-item ">
            <a href="{{url('advancepayments')}}" class="nav-link {{ Request::segment(1) === 'advancepayments' ? 'active' : null }}">
              <i class="nav-icon fas fa-money-bill-alt"></i>
              <p>
                Advance  Payments
              </p>
            </a>
           
          </li>
          @endif
           @php($has_permission = ['All','Make Payment','Remove Payment','View Payroll'] )
            
            @if(Auth::user()->hasAnyPermission($has_permission))
           <li class="nav-item ">
            <a href="{{url('payments')}}" class="nav-link {{ Request::segment(1) === 'payments' ? 'active' : null }}">
              <i class="nav-icon fas fa-file-invoice"></i>
              <p>
                Payroll
              </p>
            </a>
           
          </li>
          @endif
      
		  
@php($has_permission = ['All','Add Department','Edit Department','Remove Department'] )
            
            @if(Auth::user()->hasAnyPermission($has_permission))
          <li class="nav-item ">
            <a href="{{url('departments')}}" class="nav-link {{ Request::segment(1) === 'departments' ? 'active' : null }}">
              <i class="nav-icon fas fa-building"></i>
              <p>
                Departments
              </p>
            </a>
            @endif
           
          </li>
          @php($has_permission = ['All','Add Labour','Edit Labour','Remove Labour','View Profile'] )
            
            @if(Auth::user()->hasAnyPermission($has_permission))
          <li class="nav-item ">
            <a href="{{url('teammembers')}}" class="nav-link {{ Request::segment(1) === 'teammembers' ? 'active' : null }}">
              <i class="nav-icon fas fa-user-friends"></i>
              <p>
                Labour Team
              </p>
            </a>
           
          </li>
          @endif

          @php($has_permission = ['All','Add System User','Edit System User','Remove System User'] )
            
            @if(Auth::user()->hasAnyPermission($has_permission))
          <li class="nav-item ">
            <a href="{{url('employees')}}" class="nav-link {{ Request::segment(1) === 'employees' ? 'active' : null }}">
              <i class="nav-icon fas fa-users"></i>
              <p>
                System Users
              </p>
            </a>
           
          </li>
          @endif

          {{--
          <li class="nav-item ">
            <a href="{{url('productvariations')}}" class="nav-link {{ Request::segment(1) === 'productvariations' ? 'active' : null }}">
              <i class="nav-icon fas fa-network-wired"></i>
              <p>
                Product Variations
              </p>
            </a>
           
          </li>
          --}}

           @php($has_permission = ['All','Add Product','Edit Product','Remove Product'] )
            
            @if(Auth::user()->hasAnyPermission($has_permission))
          <li class="nav-item ">
            <a href="{{url('products')}}" class="nav-link {{ Request::segment(1) === 'products' ? 'active' : null }}">
              <i class="nav-icon fas fa-luggage-cart"></i>
              <p>
                Products
              </p>
            </a>
           
          </li>
          @endif


          @php($has_permission = ['All','Add Role','Edit Role','Remove Role','Assign Permission'] )
            
            @if(Auth::user()->hasAnyPermission($has_permission))
          <li class="nav-item ">
            <a href="{{url('roles')}}" class="nav-link {{ Request::segment(1) === 'roles' ? 'active' : null }}">
              <i class="nav-icon fas fa-cogs"></i>
              <p>
                Roles and Permission
              </p>
            </a>
           
          </li>
          @endif

          @php($has_permission = ['All','Production Issuance Report','Production Issuance Report','Labour Process Report','Labour Payment Report'] )
            
            @if(Auth::user()->hasAnyPermission($has_permission))
          <li class="nav-item ">
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-file"></i>
              <p>
                Reports
                <i class="fas fa-angle-left right"></i>
              </p>
            </a>



            <ul class="nav nav-treeview" >
              @if(Auth::user()->hasAnyPermission(['All','Production Issuance Report']))

              <li class="nav-item">
                <a data-toggle="modal" data-target="#md_product_issuance" href="#" class="nav-link">
                  
                  <p>Production Issuance Report</p>
                </a>
              </li>
              @endif

              @if(Auth::user()->hasAnyPermission(['All','Production Receiving Report']))

              <li class="nav-item">
                <a href="#" data-toggle="modal" data-target="#md_product_receiving" class="nav-link">
                  
                  <p>Production Receiving Report</p>
                </a>
              </li>
              @endif

              @if(Auth::user()->hasAnyPermission(['All','Production Over Due Report']))

              <li class="nav-item">
                <a href="#" data-toggle="modal" data-target="#md_product_overdue" class="nav-link">
                  
                  <p>Production Over Due Report</p>
                </a>
              </li>
              @endif


              @if(Auth::user()->hasAnyPermission(['All','Production Status Report']))

              <li class="nav-item">
                <a href="#" data-toggle="modal" data-target="#md_product_status" class="nav-link">
                  
                  <p>Production Status Report</p>
                </a>
              </li>
              @endif

              @if(Auth::user()->hasAnyPermission(['All','Labour Process Report']))

              <li class="nav-item">
                <a href="#" data-toggle="modal" data-target="#md_labour_progress" class="nav-link">
                  
                  <p>Labour Progress Report</p>
                </a>
              </li>
              @endif

              @if(Auth::user()->hasAnyPermission(['All','Labour Payment Report']))

              <li class="nav-item">
                <a href="#" data-toggle="modal" data-target="#md_labour_payment" class="nav-link">
                 
                  <p>Labour Payments Report</p>
                </a>
              </li>
              @endif
            </ul>
          </li>
            @endif


          {{--
          <li class="nav-item ">
            <a href="{{url('reports')}}" class="nav-link {{ Request::segment(1) === 'reports' ? 'active' : null }}">
              <i class="nav-icon fas fa-file"></i>
              <p>
                Reports
              </p>
            </a>
           
          </li>
          --}}

          

          






         </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    @yield('header')
    <!-- /.content-header -->
    <!-- ERROR -->
    @if ($errors->any())
      <div class="alert alert-danger">
          <ul>
              @foreach ($errors->all() as $error)
                  <li>{{ $error }}</li>
              @endforeach
          </ul>
      </div>
    @endif
    <!-- END ERROR -->
    <!-- Main content -->
    @yield('content')
    <!-- /.content -->
    @php($default_object = FL::defaultObjectClasses())
    <!-- modals -->
    @yield('modals')
    @include('reports.modals.production_issuance')
    @include('reports.modals.production_receiving')
    @include('reports.modals.labour_progress')
    @include('reports.modals.labour_payment')
    @include('reports.modals.production_over_due')
    @include('reports.modals.production_status')


    <!-- end modals -->

  </div>
  <!-- /.content-wrapper -->
  <footer class="main-footer">
    <strong>Copyright &copy;2021 </a>.</strong>
    All rights reserved.
    <div class="float-right d-none d-sm-inline-block">
      <b>Version</b> 3.1.0
    </div>
  </footer>

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->
</div>
<!-- ./wrapper -->

<!-- jQuery -->
<script src="{{$path}}plugins/jquery/jquery.min.js"></script>
<!-- jQuery UI 1.11.4 -->
<script src="{{$path}}plugins/jquery-ui/jquery-ui.min.js"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  $.widget.bridge('uibutton', $.ui.button);
</script>
<!-- Bootstrap 4 -->
<script src="{{$path}}plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- ChartJS -->
<!-- <script src="{{$path}}plugins/chart.js/Chart.min.js"></script> -->
<!-- Sparkline -->
<!-- <script src="{{$path}}plugins/sparklines/sparkline.js"></script> -->
<!-- JQVMap -->
<!-- <script src="{{$path}}plugins/jqvmap/jquery.vmap.min.js"></script> -->
<!-- <script src="{{$path}}plugins/jqvmap/maps/jquery.vmap.usa.js"></script> -->
<!-- jQuery Knob Chart -->
<!-- <script src="{{$path}}plugins/jquery-knob/jquery.knob.min.js"></script> -->
<!-- daterangepicker -->
<!-- <script src="{{$path}}plugins/moment/moment.min.js"></script> -->
<!-- <script src="{{$path}}plugins/daterangepicker/daterangepicker.js"></script> -->
<!-- Tempusdominus Bootstrap 4 -->
<!-- <script src="{{$path}}plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js"></script> -->
<!-- Summernote -->
<!-- <script src="{{$path}}plugins/summernote/summernote-bs4.min.js"></script> -->
<!-- overlayScrollbars -->
<!-- <script src="{{$path}}plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js"></script> -->
<!-- AdminLTE App -->
<script src="{{$path}}dist/js/adminlte.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="{{$path}}dist/js/demo.js"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<script src="{{$path}}dist/js/pages/dashboard.js"></script>

 <!-- DataTables  & Plugins -->
<script src="{{asset('/')}}plugins/datatables/jquery.dataTables.min.js"></script>
<script src="{{asset('/')}}plugins/datatables-bs4/js/dataTables.bootstrap4.min.js"></script>
<script src="{{asset('/')}}plugins/datatables-responsive/js/dataTables.responsive.min.js"></script>
<script src="{{asset('/')}}plugins/datatables-responsive/js/responsive.bootstrap4.min.js"></script>
<script src="{{asset('/')}}plugins/datatables-buttons/js/dataTables.buttons.min.js"></script>
<script src="{{asset('/')}}plugins/datatables-buttons/js/buttons.bootstrap4.min.js"></script>
<script src="../../plugins/jszip/jszip.min.js"></script>
<script src="{{asset('/')}}plugins/pdfmake/pdfmake.min.js"></script>
<script src="{{asset('/')}}plugins/pdfmake/vfs_fonts.js"></script>
<script src="{{asset('/')}}plugins/datatables-buttons/js/buttons.html5.min.js"></script>
<script src="{{asset('/')}}plugins/datatables-buttons/js/buttons.print.min.js"></script>
<script src="{{asset('/')}}plugins/datatables-buttons/js/buttons.colVis.min.js"></script>
<script src="{{$path}}plugins/sweetalert2/sweetalert2.min.js"></script>
  <script src="{{ asset('js/loader/loadingoverlay.min.js') }}"></script>
    <script src="{{ asset('js/loader/loadingoverlay_progress.min.js') }}"></script>
<!-- Toastr -->
<script src="{{$path}}plugins/toastr/toastr.min.js"></script>

<!-- Select2 -->
<!-- <script src="{{$path}}plugins/select2/js/select2.full.min.js"></script> -->

<script type="text/javascript" src="{{ asset('plugins/search-box/js/search_box.min.js')}}"></script>

<script src="{{asset('js/custom_function.js')}}"></script>

<script type="text/javascript" src="{{ asset('js/datatable_column_sort.js')}}"></script>





@yield('scripts')

<script type="text/javascript">


  $(function () {

    setFstDropdown();

    @if(Session::has('status') && Session::get('status') == "success")

        toastr.success("{{Session::get('message')}}");

    @elseif(Session::has('status') && Session::get('status') == "error")

        toastr.error("{{Session::get('message')}}");

    @endif
  

    //  $('.select2').select2();

    // //Initialize Select2 Elements
    // $('.select2bs4').select2({
    //   theme: 'bootstrap4'
    // });


    $("#example1").DataTable({
      "responsive": true, "lengthChange": false, "autoWidth": false,
      pageLength:50,
      "buttons": ["csv", "excel", "pdf", "print", "colvis"]
    }).buttons().container().appendTo('#example1_wrapper .col-md-6:eq(0)');
    
    $('#example2').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": true,
      "autoWidth": false,
      "responsive": true,
    });

   $("#example4").DataTable({
      "responsive": true, "lengthChange": false, "autoWidth": false,
      "buttons": ["excel", "pdf", "print"]
    }).buttons().container().appendTo('#example4_wrapper .col-md-6:eq(0)');

     $("#example3").DataTable({
      "responsive": true, "lengthChange": false, "autoWidth": false,
      "buttons": ["excel", "pdf", "print"]
    }).buttons().container().appendTo('#example3_wrapper .col-md-6:eq(0)');

  });


  @if(isset($status) && isset($message))
    $(function() {
      var Toast = Swal.mixin({
        toast: true,
        position: 'top-end',
        showConfirmButton: false,
        timer: 3000
      });

      $('.swalDefaultSuccess').click(function() {
        Toast.fire({
          icon:  "{{$status}}",
          title: "{{$message}}"
        })
      });
      
    });
  @endisset


  
</script>



</body>
</html>
